<?xml version="1.0" encoding="UTF-8"?>

<!-- Copyright (c) 2014-2017 Claas Ziemke

   This file is part of ThirdEye.

   This program and the accompanying materials are made available under the
   terms of the Eclipse Public License 2.0 which is available at
   http://www.eclipse.org/legal/epl-2.0.

   SPDX-License-Identifier: EPL-2.0

   Contributors:
   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
-->

<ecore:EPackage xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="worldwind" nsURI="http://worldwind/1.0" nsPrefix="worldwind">
  <eClassifiers xsi:type="ecore:EDataType" name="EAWTPoint" instanceClassName="java.awt.Point"
      serializable="false"/>
  <eClassifiers xsi:type="ecore:EDataType" name="EWWExtent" instanceClassName="gov.nasa.worldwind.geom.Extent"
      serializable="false"/>
  <eClassifiers xsi:type="ecore:EDataType" name="EWWMatrix" instanceClassName="gov.nasa.worldwind.geom.Matrix"/>
  <eClassifiers xsi:type="ecore:EDataType" name="EWWPosition" instanceClassName="gov.nasa.worldwind.geom.Position"
      serializable="false"/>
  <eClassifiers xsi:type="ecore:EDataType" name="EWWPickSupport" instanceClassName="gov.nasa.worldwind.pick.PickSupport"
      serializable="false"/>
  <eClassifiers xsi:type="ecore:EDataType" name="EWWDrawContext" instanceClassName="gov.nasa.worldwind.render.DrawContext"
      serializable="false"/>
  <eClassifiers xsi:type="ecore:EDataType" name="EWWVec4" instanceClassName="gov.nasa.worldwind.geom.Vec4"
      serializable="false"/>
  <eClassifiers xsi:type="ecore:EClass" name="EOrderedRenderable" instanceClassName="gov.nasa.worldwind.render.OrderedRenderable"
      abstract="true" interface="true">
    <eOperations name="draw" lowerBound="1">
      <eParameters name="dc" lowerBound="1" eType="#//EWWDrawContext"/>
    </eOperations>
    <eOperations name="calcMatrix" lowerBound="1">
      <eParameters name="dc" lowerBound="1" eType="#//EWWDrawContext"/>
    </eOperations>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="Cube" eSuperTypes="#//TimeDependentRenderable">
    <eOperations name="rightHandSide" lowerBound="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="//intentionally left blank"/>
      </eAnnotations>
    </eOperations>
    <eOperations name="draw" lowerBound="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="// Vertices of a unit cube, centered on the origin.&#xA;float[][] v = {   {-0.5f, 0.5f, -0.5f}, {-0.5f, 0.5f, 0.5f}, {0.5f, 0.5f, 0.5f}, {0.5f, 0.5f, -0.5f},&#xA;&#x9;&#x9;&#x9;&#x9;{-0.5f, -0.5f, 0.5f}, {0.5f, -0.5f, 0.5f}, {0.5f, -0.5f, -0.5f}, {-0.5f, -0.5f, -0.5f}};&#xA;// Array to group vertices into faces&#xA;int[][] faces = {{0, 1, 2, 3}, {2, 5, 6, 3}, {1, 4, 5, 2}, {0, 7, 4, 1}, {0, 7, 6, 3}, {4, 7, 6, 5}};&#xA;&#xA;// Normal vectors for each face&#xA;float[][] n = {{0, 1, 0}, {1, 0, 0}, {0, 0, 1}, {-1, 0, 0}, {0, 0, -1}, {0, -1, 0}};&#xA;&#xA;GL2 gl = dc.getGL().getGL2(); // GL initialization checks for GL2 compatibility.&#xA;gl.glBegin(GL2.GL_QUADS);&#xA;try {&#xA;&#x9;for (int i = 0; i &lt; faces.length; i++) {&#xA;&#x9;&#x9;gl.glNormal3f(n[i][0], n[i][1], n[i][2]);&#xA;&#x9;&#x9;for(int j = 0; j &lt; faces[0].length; j++) {&#xA;&#x9;&#x9;&#x9;gl.glVertex3f(v[faces[i][j]][0], v[faces[i][j]][1], v[faces[i][j]][2]);&#xA;&#x9;&#x9;}&#xA;&#x9;}&#xA;} finally {&#xA;&#x9;gl.glEnd();&#xA;}"/>
      </eAnnotations>
      <eParameters name="dc" lowerBound="1" eType="#//EWWDrawContext"/>
    </eOperations>
    <eOperations name="calcMatrix" lowerBound="1">
      <eParameters name="dc" lowerBound="1" eType="#//EWWDrawContext"/>
    </eOperations>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="Axis" eSuperTypes="#//TimeDependentRenderable">
    <eOperations name="rightHandSide" lowerBound="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="//intentionally left blank"/>
      </eAnnotations>
    </eOperations>
    <eOperations name="draw" lowerBound="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="GL2 gl = dc.getGL().getGL2(); // GL initialization checks for GL2 compatibility.&#xA;gl.glLineWidth(3.0f);&#xA;// Draw in axis&#xA;gl.glColor3d(1, 0, 0); // COLOR &#xA;gl.glBegin(GL.GL_LINES);&#xA;gl.glVertex3d(400000, 0, 0);&#xA;gl.glVertex3d(0, 0, 0);&#xA;gl.glColor3d(0.0, 0.0, 1.0);&#xA;gl.glEnd();&#xA;&#xA;gl.glColor3d(0, 1, 0); // COLOR &#xA;gl.glBegin(GL.GL_LINES);&#xA;gl.glVertex3d(0, 400000, 0);&#xA;gl.glVertex3d(0, 0, 0);&#xA;gl.glColor3d(0.0, 0.0, 1.0);&#xA;gl.glEnd();&#xA;&#xA;gl.glColor3d(0, 0, 1); // COLOR &#xA;gl.glBegin(GL.GL_LINES);&#xA;gl.glVertex3d(0, 0, 400000);&#xA;gl.glVertex3d(0, 0, 0);&#xA;gl.glColor3d(0.0, 0.0, 1.0);&#xA;gl.glEnd();"/>
      </eAnnotations>
      <eParameters name="dc" lowerBound="1" eType="#//EWWDrawContext"/>
    </eOperations>
    <eOperations name="calcMatrix" lowerBound="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="GL2 gl = dc.getGL().getGL2();&#xA;setMatrix( dc.getView().getModelviewMatrix());&#xA;double[] matrixArray = new double[16];&#xA;getMatrix().toArray(matrixArray, 0, false);&#xA;gl.glLoadMatrixd(matrixArray, 0);"/>
      </eAnnotations>
      <eParameters name="dc" lowerBound="1" eType="#//EWWDrawContext"/>
    </eOperations>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="color" lowerBound="1" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EInt"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="TimeDependentRenderable" abstract="true"
      eSuperTypes="../../org.thirdeye.core.mesh/model/Mesh.ecore#//Model #//EOrderedRenderable">
    <eOperations name="pick" lowerBound="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="render(dc);"/>
      </eAnnotations>
      <eParameters name="dc" lowerBound="1" eType="#//EWWDrawContext"/>
      <eParameters name="pickPoint" eType="#//EAWTPoint"/>
    </eOperations>
    <eOperations name="render" lowerBound="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="if(getExtent() != null) {&#xA;//&#x9;if((!intersectsFrustum(dc))||(dc.isSmall((Extent) getExtent(), 1))) {&#xA;//&#x9;&#x9;return;&#xA;//&#x9;}&#xA;}&#xA;if(dc.isOrderedRenderingMode()) {&#xA;&#x9;drawOrderedRenderable(dc, pickSupport );&#xA;} else {&#xA;&#x9;makeOrderedRenderable(dc);&#xA;}"/>
      </eAnnotations>
      <eParameters name="dc" lowerBound="1" eType="#//EWWDrawContext"/>
    </eOperations>
    <eOperations name="drawOrderedRenderable" lowerBound="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="GL2 gl = dc.getGL().getGL2();&#xA;beginDrawing(dc);&#xA;try {&#xA;&#x9;if(dc.isPickingMode()) {&#xA;&#x9;&#x9;Color pickColor = dc.getUniquePickColor();&#xA;&#x9;&#x9;pickCandidates.addPickableObject(pickColor.getRGB(), this, new Position(Position.fromRadians(getLatLonAlt()[0], getLatLonAlt()[1]), getLatLonAlt()[2]));&#xA;&#x9;&#x9;gl.glColor3ub((byte) pickColor.getRed(), (byte) pickColor.getGreen(), (byte) pickColor.getBlue());&#xA;&#x9;}&#xA;&#x9;gl.glScaled(getRenderSize(), getRenderSize(), getRenderSize());&#xA;&#x9;draw(dc);&#xA;} finally {&#xA;&#x9;endDrawing(dc);&#xA;}"/>
      </eAnnotations>
      <eParameters name="dc" lowerBound="1" eType="#//EWWDrawContext"/>
      <eParameters name="pickCandidates" lowerBound="1" eType="#//EWWPickSupport"/>
    </eOperations>
    <eOperations name="beginDrawing" lowerBound="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="GL2 gl = dc.getGL().getGL2();&#xA;int attrMask = GL2.GL_CURRENT_BIT | GL2.GL_COLOR_BUFFER_BIT;&#xA;gl.glPushAttrib(attrMask);&#xA;if(!dc.isPickingMode()) {&#xA;&#x9;dc.beginStandardLighting();&#xA;&#x9;gl.glEnable(GL.GL_BLEND);&#xA;&#x9;OGLUtil.applyBlending(gl, false);&#xA;&#x9;gl.glEnable(GL2.GL_NORMALIZE);&#xA;}&#xA;gl.glMatrixMode(GL2.GL_MODELVIEW);&#xA;calcMatrix(dc);"/>
      </eAnnotations>
      <eParameters name="dc" lowerBound="1" eType="#//EWWDrawContext"/>
    </eOperations>
    <eOperations name="endDrawing" lowerBound="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="GL2 gl = dc.getGL().getGL2();&#xA;if (!dc.isPickingMode()) dc.endStandardLighting();&#xA;gl.glPopAttrib();"/>
      </eAnnotations>
      <eParameters name="dc" lowerBound="1" eType="#//EWWDrawContext"/>
    </eOperations>
    <eOperations name="intersectsFrustum" lowerBound="1" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="//if(getExtent() == null)&#xA;//&#x9;return true;&#xA;//if (dc.isPickingMode())&#xA;//&#x9;return dc.getPickFrustums().intersectsAny(getExtent());&#xA;//return dc.getView().getFrustumInModelCoordinates().intersects(getExtent());*/&#xA;return true;"/>
      </eAnnotations>
      <eParameters name="dc" lowerBound="1" eType="#//EWWDrawContext"/>
    </eOperations>
    <eOperations name="makeOrderedRenderable" lowerBound="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="if(dc.getFrameTimeStamp() != getFrameTimestamp()) {&#xA;&#x9;Vec4 placePoint = dc.getGlobe().computePointFromPosition(new Position(Position.fromRadians(getLatLonAlt()[0], getLatLonAlt()[1]), getLatLonAlt()[2]));&#xA;&#x9;setDistanceFromEye( dc.getView().getEyePoint().distanceTo3(placePoint) );&#xA;&#x9;setExtent( new Sphere(placePoint, Math.sqrt(3.0) * getRenderSize() / 2.0));&#xA;&#x9;setFrameTimestamp( dc.getFrameTimeStamp());&#xA;}&#xA;dc.addOrderedRenderable((OrderedRenderable) this);"/>
      </eAnnotations>
      <eParameters name="dc" lowerBound="1" eType="#//EWWDrawContext"/>
    </eOperations>
    <eOperations name="calcMatrix" lowerBound="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="GL2 gl = dc.getGL().getGL2();&#xA;gl.glMatrixMode(GL2.GL_MODELVIEW);&#xA;setMatrix( dc.getGlobe().computeSurfaceOrientationAtPosition(new Position(Position.fromRadians(getLatLonAlt()[0], getLatLonAlt()[1]), getLatLonAlt()[2])) );&#xA;setMatrix( dc.getView().getModelviewMatrix().multiply(getMatrix()));&#xA;double[] matrixArray = new double[16];&#xA;getMatrix().toArray(matrixArray, 0, false);&#xA;gl.glLoadMatrixd(matrixArray, 0);"/>
      </eAnnotations>
      <eParameters name="dc" lowerBound="1" eType="#//EWWDrawContext"/>
    </eOperations>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="latLonAlt" ordered="false"
        unique="false" lowerBound="3" upperBound="3" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EDouble">
      <eAnnotations source="ThirdEye">
        <details key="unit" value="rad,rad,m"/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="distanceFromEye" lowerBound="1"
        eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EDouble"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="renderSize" lowerBound="1"
        eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EDouble"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="frameTimestamp" lowerBound="1"
        eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//ELong" defaultValueLiteral="-1"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="extent" lowerBound="1"
        eType="#//EWWExtent" transient="true"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="matrix" lowerBound="1"
        eType="#//EWWMatrix" transient="true"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="GroundTrack" eSuperTypes="#//TimeDependentRenderable">
    <eOperations name="rightHandSide" lowerBound="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="//intentionally left blank"/>
      </eAnnotations>
    </eOperations>
    <eOperations name="draw" lowerBound="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value=" GL2 gl = dc.getGL().getGL2(); // GL initialization checks for GL2 compatibility.&#xA;if( !init  ) {&#xA;&#x9;Vec4 point = dc.getGlobe().computePointFromPosition(Position.fromRadians(getLatLonAlt()[0], getLatLonAlt()[1]), 100000.0);&#xA;&#x9;start = new double[] {point.x,point.y,point.z};&#xA;&#x9;init = true;&#xA;}&#xA;i++;&#xA;if( i == 5) {&#xA;&#x9;i = 0;&#xA;&#x9;if( lastPoint != null ) {&#xA;&#x9;&#x9;Vec4 newPoint = dc.getGlobe().computePointFromPosition(Position.fromRadians(getLatLonAlt()[0], getLatLonAlt()[1]), 100000.0);&#xA;&#x9;&#x9;if( !lastPoint.equals(newPoint)) {&#xA;&#x9;&#x9;&#x9;double[] dx = new double[] { lastPoint.x - newPoint.x,lastPoint.y - newPoint.y, lastPoint.z - newPoint.z};&#xA;&#x9;&#x9;&#x9;list .add(dx);&#xA;&#x9;&#x9;}&#xA;&#x9;}&#xA;&#x9;lastPoint = dc.getGlobe().computePointFromPosition(Position.fromRadians(getLatLonAlt()[0], getLatLonAlt()[1]), 100000.0);&#xA;}&#xA;double[] vec = new double[] {0.0,0.0,0.0};&#xA;vec[0] = vec[0] + start[0];&#xA;vec[1] = vec[1] + start[1];&#xA;vec[2] = vec[2] + start[2];&#xA;for( double[] inc: list ) {&#xA;&#x9;gl.glBegin(GL.GL_LINES);&#xA;&#x9;gl.glVertex3d(vec[0], vec[1], vec[2]);&#xA;&#x9;vec[0]-=inc[0];&#xA;&#x9;vec[1]-=inc[1];&#xA;&#x9;vec[2]-=inc[2];&#xA;&#x9;gl.glVertex3d(vec[0], vec[1], vec[2]);&#xA;&#x9;gl.glColor3d(0.0, 0.0, 1.0);&#xA;&#x9;gl.glEnd();&#xA;}"/>
      </eAnnotations>
      <eParameters name="dc" lowerBound="1" eType="#//EWWDrawContext"/>
    </eOperations>
    <eOperations name="calcMatrix" lowerBound="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="GL2 gl = dc.getGL().getGL2();&#xA;setMatrix( dc.getView().getModelviewMatrix());&#xA;double[] matrixArray = new double[16];&#xA;getMatrix().toArray(matrixArray, 0, false);&#xA;gl.glLoadMatrixd(matrixArray, 0);"/>
      </eAnnotations>
      <eParameters name="dc" lowerBound="1" eType="#//EWWDrawContext"/>
    </eOperations>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="points" ordered="false"
        unique="false" upperBound="-1" eType="#//EWWVec4" transient="true"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="GroundStation" eSuperTypes="#//TimeDependentRenderable">
    <eOperations name="rightHandSide" lowerBound="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="Vec4 gsPoint = globe.computePointFromPosition(Position.fromRadians(getLatLonAlt()[0], getLatLonAlt()[1]), getLatLonAlt()[2]);&#xA;Vec4 satPoint = globe.computePointFromPosition(Position.fromRadians(getSatPos()[0], getSatPos()[1]), getSatPos()[2]);&#xA;&#xA;Matrix mat = globe.computeSurfaceOrientationAtPosition(new Position(Position.fromRadians(getLatLonAlt()[0], getLatLonAlt()[1]), getLatLonAlt()[2]));&#xA;Quaternion quat = Quaternion.fromMatrix(mat);&#xA;&#xA;Vec4 x = new Vec4(1.0,0.0,0.0);&#x9;&#xA;Vec4 y = new Vec4(0.0,1.0,0.0);&#xA;Vec4 z = new Vec4(0.0,0.0,1.0);&#xA;Vec4 localx = x.transformBy3(quat);&#xA;Vec4 localy = y.transformBy3(quat);&#xA;Vec4 localz = z.transformBy3(quat);&#xA;&#xA;Vec4 temp = satPoint.subtract3(gsPoint);&#xA;Vec4 projectz = temp.projectOnto3(localz);&#xA;Vec4 projectx = temp.projectOnto3(localx);&#xA;temp = temp.subtract3(projectz);&#xA;&#xA;double az =  localy.angleBetween3(temp).degrees;&#xA;&#xA;if( projectx.angleBetween3(localx).degrees > 0.0 ) {&#xA;&#x9;az = 360.0 - az;&#xA;}&#xA;setAzelra( new double[] { az, Angle.POS90.subtract(localz.angleBetween3(satPoint.subtract3(gsPoint))).degrees, satPoint.subtract3(gsPoint).getLength3() });&#xA;"/>
      </eAnnotations>
    </eOperations>
    <eOperations name="draw" lowerBound="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="globe =dc.getGlobe();&#xA;Vec4 gsPoint = globe.computePointFromPosition(Position.fromRadians(getLatLonAlt()[0], getLatLonAlt()[1]), getLatLonAlt()[2]);&#xA;Vec4 satPoint = globe.computePointFromPosition(Position.fromRadians(getSatPos()[0], getSatPos()[1]), getSatPos()[2]);&#xA;GL2 gl = dc.getGL().getGL2(); // GL initialization checks for GL2 compatibility.&#xA;gl.glLineWidth(3.0f);&#xA;&#xA;if(getAzelra()[1] > 0.0) {&#xA;&#x9;gl.glColor3d(1, 0, 0); // COLOR &#xA;&#x9;gl.glBegin(GL.GL_LINES);&#xA;&#x9;gl.glVertex3d(gsPoint.x, gsPoint.y, gsPoint.z);&#xA;&#x9;gl.glVertex3d(satPoint.x, satPoint.y, satPoint.z);&#xA;&#x9;gl.glColor3d(0.0, 0.0, 1.0);&#xA;&#x9;gl.glEnd();&#xA;}"/>
      </eAnnotations>
      <eParameters name="dc" lowerBound="1" eType="#//EWWDrawContext"/>
    </eOperations>
    <eOperations name="calcMatrix" lowerBound="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="GL2 gl = dc.getGL().getGL2();&#xA;setMatrix( dc.getView().getModelviewMatrix());&#xA;double[] matrixArray = new double[16];&#xA;getMatrix().toArray(matrixArray, 0, false);&#xA;gl.glLoadMatrixd(matrixArray, 0);"/>
      </eAnnotations>
      <eParameters name="dc" lowerBound="1" eType="#//EWWDrawContext"/>
    </eOperations>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="satPos" ordered="false"
        unique="false" lowerBound="3" upperBound="3" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EDouble">
      <eAnnotations source="ThirdEye">
        <details key="unit" value="m,m,m"/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="azelra" ordered="false"
        unique="false" lowerBound="3" upperBound="3" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EDouble">
      <eAnnotations source="ThirdEye">
        <details key="unit" value="rad,rad,m"/>
      </eAnnotations>
    </eStructuralFeatures>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="LocalCoordinateSystem" eSuperTypes="#//TimeDependentRenderable">
    <eOperations name="rightHandSide" lowerBound="1"/>
    <eOperations name="draw" lowerBound="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="Quaternion quat = new Quaternion(getQuaternion()[0], getQuaternion()[1], getQuaternion()[2], getQuaternion()[3]);&#x9;&#x9;&#xA;//System.out.println( &quot;quat: &quot; + quat.toString());&#xA;&#xA;Vec4 point = dc.getGlobe().computePointFromPosition(Position.fromRadians(getLatLonAlt()[0], getLatLonAlt()[1]), getLatLonAlt()[2]);&#xA;Vec4 x = new Vec4(1000000.0,0.0,0.0).transformBy3(quat);&#x9;&#xA;Vec4 y = new Vec4(0.0,1000000.0,0.0).transformBy3(quat);&#xA;Vec4 z = new Vec4(0.0,0.0,1000000.0).transformBy3(quat);&#xA;&#xA;Vec4 vec[] = new Vec4[] {point.add3(x),point.add3(y),point.add3(z)};&#xA;&#xA;GL2 gl = dc.getGL().getGL2(); // GL initialization checks for GL2 compatibility.&#xA;gl.glLineWidth(3.0f);&#xA;for(int i=0; i&lt;3;i++) {&#xA;&#x9;// Draw in axis&#xA;&#x9;gl.glColor3d(1, 0, 0); // COLOR &#xA;&#x9;gl.glBegin(GL.GL_LINES);&#xA;&#x9;gl.glVertex3d(vec[i].x, vec[i].y, vec[i].z);&#xA;&#x9;gl.glVertex3d(point.x, point.y, point.z);&#xA;&#x9;gl.glColor3d(0.0, 0.0, 1.0);&#xA;&#x9;gl.glEnd();&#xA;}"/>
      </eAnnotations>
      <eParameters name="dc" lowerBound="1" eType="#//EWWDrawContext"/>
    </eOperations>
    <eOperations name="calcMatrix" lowerBound="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="body" value="GL2 gl = dc.getGL().getGL2();&#xA;setMatrix( dc.getView().getModelviewMatrix());&#xA;double[] matrixArray = new double[16];&#xA;getMatrix().toArray(matrixArray, 0, false);&#xA;gl.glLoadMatrixd(matrixArray, 0);"/>
      </eAnnotations>
      <eParameters name="dc" lowerBound="1" eType="#//EWWDrawContext"/>
    </eOperations>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="quaternion" lowerBound="1"
        eType="ecore:EDataType ../../org.thirdeye.core.dependencies/model/Dependencies.ecore#//EDoubleArray">
      <eAnnotations source="ThirdEye">
        <details key="unit" value="none,none,none,none"/>
      </eAnnotations>
    </eStructuralFeatures>
  </eClassifiers>
</ecore:EPackage>
