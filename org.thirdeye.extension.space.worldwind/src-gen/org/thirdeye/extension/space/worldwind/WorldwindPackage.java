/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.worldwind;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import org.thirdeye.core.mesh.MeshPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.thirdeye.extension.space.worldwind.WorldwindFactory
 * @model kind="package"
 * @generated
 */
public interface WorldwindPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "worldwind";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://worldwind/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "worldwind";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorldwindPackage eINSTANCE = org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl.init();

	/**
	 * The meta object id for the '{@link gov.nasa.worldwind.render.OrderedRenderable <em>EOrdered Renderable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gov.nasa.worldwind.render.OrderedRenderable
	 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getEOrderedRenderable()
	 * @generated
	 */
	int EORDERED_RENDERABLE = 0;

	/**
	 * The number of structural features of the '<em>EOrdered Renderable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EORDERED_RENDERABLE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Draw</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EORDERED_RENDERABLE___DRAW__DRAWCONTEXT = 0;

	/**
	 * The operation id for the '<em>Calc Matrix</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EORDERED_RENDERABLE___CALC_MATRIX__DRAWCONTEXT = 1;

	/**
	 * The number of operations of the '<em>EOrdered Renderable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EORDERED_RENDERABLE_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.worldwind.impl.TimeDependentRenderableImpl <em>Time Dependent Renderable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.worldwind.impl.TimeDependentRenderableImpl
	 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getTimeDependentRenderable()
	 * @generated
	 */
	int TIME_DEPENDENT_RENDERABLE = 3;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE__VARIABLES = MeshPackage.MODEL__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE__NAME = MeshPackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Lat Lon Alt</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE__LAT_LON_ALT = MeshPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Distance From Eye</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE__DISTANCE_FROM_EYE = MeshPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Render Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE__RENDER_SIZE = MeshPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Frame Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE__FRAME_TIMESTAMP = MeshPackage.MODEL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Extent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE__EXTENT = MeshPackage.MODEL_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Matrix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE__MATRIX = MeshPackage.MODEL_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Time Dependent Renderable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE_FEATURE_COUNT = MeshPackage.MODEL_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE___RUN = MeshPackage.MODEL___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE___CALC = MeshPackage.MODEL___CALC;

	/**
	 * The operation id for the '<em>Draw</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE___DRAW__DRAWCONTEXT = MeshPackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Pick</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE___PICK__DRAWCONTEXT_POINT = MeshPackage.MODEL_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Render</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE___RENDER__DRAWCONTEXT = MeshPackage.MODEL_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Draw Ordered Renderable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE___DRAW_ORDERED_RENDERABLE__DRAWCONTEXT_PICKSUPPORT = MeshPackage.MODEL_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Begin Drawing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE___BEGIN_DRAWING__DRAWCONTEXT = MeshPackage.MODEL_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>End Drawing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE___END_DRAWING__DRAWCONTEXT = MeshPackage.MODEL_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Intersects Frustum</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE___INTERSECTS_FRUSTUM__DRAWCONTEXT = MeshPackage.MODEL_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Make Ordered Renderable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE___MAKE_ORDERED_RENDERABLE__DRAWCONTEXT = MeshPackage.MODEL_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Calc Matrix</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE___CALC_MATRIX__DRAWCONTEXT = MeshPackage.MODEL_OPERATION_COUNT + 9;

	/**
	 * The number of operations of the '<em>Time Dependent Renderable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT = MeshPackage.MODEL_OPERATION_COUNT + 10;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.worldwind.impl.CubeImpl <em>Cube</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.worldwind.impl.CubeImpl
	 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getCube()
	 * @generated
	 */
	int CUBE = 1;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE__VARIABLES = TIME_DEPENDENT_RENDERABLE__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE__NAME = TIME_DEPENDENT_RENDERABLE__NAME;

	/**
	 * The feature id for the '<em><b>Lat Lon Alt</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE__LAT_LON_ALT = TIME_DEPENDENT_RENDERABLE__LAT_LON_ALT;

	/**
	 * The feature id for the '<em><b>Distance From Eye</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE__DISTANCE_FROM_EYE = TIME_DEPENDENT_RENDERABLE__DISTANCE_FROM_EYE;

	/**
	 * The feature id for the '<em><b>Render Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE__RENDER_SIZE = TIME_DEPENDENT_RENDERABLE__RENDER_SIZE;

	/**
	 * The feature id for the '<em><b>Frame Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE__FRAME_TIMESTAMP = TIME_DEPENDENT_RENDERABLE__FRAME_TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Extent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE__EXTENT = TIME_DEPENDENT_RENDERABLE__EXTENT;

	/**
	 * The feature id for the '<em><b>Matrix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE__MATRIX = TIME_DEPENDENT_RENDERABLE__MATRIX;

	/**
	 * The number of structural features of the '<em>Cube</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_FEATURE_COUNT = TIME_DEPENDENT_RENDERABLE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE___RUN = TIME_DEPENDENT_RENDERABLE___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE___CALC = TIME_DEPENDENT_RENDERABLE___CALC;

	/**
	 * The operation id for the '<em>Pick</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE___PICK__DRAWCONTEXT_POINT = TIME_DEPENDENT_RENDERABLE___PICK__DRAWCONTEXT_POINT;

	/**
	 * The operation id for the '<em>Render</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE___RENDER__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___RENDER__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>Draw Ordered Renderable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE___DRAW_ORDERED_RENDERABLE__DRAWCONTEXT_PICKSUPPORT = TIME_DEPENDENT_RENDERABLE___DRAW_ORDERED_RENDERABLE__DRAWCONTEXT_PICKSUPPORT;

	/**
	 * The operation id for the '<em>Begin Drawing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE___BEGIN_DRAWING__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___BEGIN_DRAWING__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>End Drawing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE___END_DRAWING__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___END_DRAWING__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>Intersects Frustum</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE___INTERSECTS_FRUSTUM__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___INTERSECTS_FRUSTUM__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>Make Ordered Renderable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE___MAKE_ORDERED_RENDERABLE__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___MAKE_ORDERED_RENDERABLE__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>Right Hand Side</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE___RIGHT_HAND_SIDE = TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Draw</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE___DRAW__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Calc Matrix</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE___CALC_MATRIX__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Cube</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_OPERATION_COUNT = TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.worldwind.impl.AxisImpl <em>Axis</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.worldwind.impl.AxisImpl
	 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getAxis()
	 * @generated
	 */
	int AXIS = 2;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS__VARIABLES = TIME_DEPENDENT_RENDERABLE__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS__NAME = TIME_DEPENDENT_RENDERABLE__NAME;

	/**
	 * The feature id for the '<em><b>Lat Lon Alt</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS__LAT_LON_ALT = TIME_DEPENDENT_RENDERABLE__LAT_LON_ALT;

	/**
	 * The feature id for the '<em><b>Distance From Eye</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS__DISTANCE_FROM_EYE = TIME_DEPENDENT_RENDERABLE__DISTANCE_FROM_EYE;

	/**
	 * The feature id for the '<em><b>Render Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS__RENDER_SIZE = TIME_DEPENDENT_RENDERABLE__RENDER_SIZE;

	/**
	 * The feature id for the '<em><b>Frame Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS__FRAME_TIMESTAMP = TIME_DEPENDENT_RENDERABLE__FRAME_TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Extent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS__EXTENT = TIME_DEPENDENT_RENDERABLE__EXTENT;

	/**
	 * The feature id for the '<em><b>Matrix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS__MATRIX = TIME_DEPENDENT_RENDERABLE__MATRIX;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS__COLOR = TIME_DEPENDENT_RENDERABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Axis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_FEATURE_COUNT = TIME_DEPENDENT_RENDERABLE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS___RUN = TIME_DEPENDENT_RENDERABLE___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS___CALC = TIME_DEPENDENT_RENDERABLE___CALC;

	/**
	 * The operation id for the '<em>Pick</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS___PICK__DRAWCONTEXT_POINT = TIME_DEPENDENT_RENDERABLE___PICK__DRAWCONTEXT_POINT;

	/**
	 * The operation id for the '<em>Render</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS___RENDER__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___RENDER__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>Draw Ordered Renderable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS___DRAW_ORDERED_RENDERABLE__DRAWCONTEXT_PICKSUPPORT = TIME_DEPENDENT_RENDERABLE___DRAW_ORDERED_RENDERABLE__DRAWCONTEXT_PICKSUPPORT;

	/**
	 * The operation id for the '<em>Begin Drawing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS___BEGIN_DRAWING__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___BEGIN_DRAWING__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>End Drawing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS___END_DRAWING__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___END_DRAWING__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>Intersects Frustum</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS___INTERSECTS_FRUSTUM__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___INTERSECTS_FRUSTUM__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>Make Ordered Renderable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS___MAKE_ORDERED_RENDERABLE__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___MAKE_ORDERED_RENDERABLE__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>Right Hand Side</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS___RIGHT_HAND_SIDE = TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Draw</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS___DRAW__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Calc Matrix</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS___CALC_MATRIX__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Axis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AXIS_OPERATION_COUNT = TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.worldwind.impl.GroundTrackImpl <em>Ground Track</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.worldwind.impl.GroundTrackImpl
	 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getGroundTrack()
	 * @generated
	 */
	int GROUND_TRACK = 4;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK__VARIABLES = TIME_DEPENDENT_RENDERABLE__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK__NAME = TIME_DEPENDENT_RENDERABLE__NAME;

	/**
	 * The feature id for the '<em><b>Lat Lon Alt</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK__LAT_LON_ALT = TIME_DEPENDENT_RENDERABLE__LAT_LON_ALT;

	/**
	 * The feature id for the '<em><b>Distance From Eye</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK__DISTANCE_FROM_EYE = TIME_DEPENDENT_RENDERABLE__DISTANCE_FROM_EYE;

	/**
	 * The feature id for the '<em><b>Render Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK__RENDER_SIZE = TIME_DEPENDENT_RENDERABLE__RENDER_SIZE;

	/**
	 * The feature id for the '<em><b>Frame Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK__FRAME_TIMESTAMP = TIME_DEPENDENT_RENDERABLE__FRAME_TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Extent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK__EXTENT = TIME_DEPENDENT_RENDERABLE__EXTENT;

	/**
	 * The feature id for the '<em><b>Matrix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK__MATRIX = TIME_DEPENDENT_RENDERABLE__MATRIX;

	/**
	 * The feature id for the '<em><b>Points</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK__POINTS = TIME_DEPENDENT_RENDERABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ground Track</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK_FEATURE_COUNT = TIME_DEPENDENT_RENDERABLE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK___RUN = TIME_DEPENDENT_RENDERABLE___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK___CALC = TIME_DEPENDENT_RENDERABLE___CALC;

	/**
	 * The operation id for the '<em>Pick</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK___PICK__DRAWCONTEXT_POINT = TIME_DEPENDENT_RENDERABLE___PICK__DRAWCONTEXT_POINT;

	/**
	 * The operation id for the '<em>Render</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK___RENDER__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___RENDER__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>Draw Ordered Renderable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK___DRAW_ORDERED_RENDERABLE__DRAWCONTEXT_PICKSUPPORT = TIME_DEPENDENT_RENDERABLE___DRAW_ORDERED_RENDERABLE__DRAWCONTEXT_PICKSUPPORT;

	/**
	 * The operation id for the '<em>Begin Drawing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK___BEGIN_DRAWING__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___BEGIN_DRAWING__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>End Drawing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK___END_DRAWING__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___END_DRAWING__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>Intersects Frustum</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK___INTERSECTS_FRUSTUM__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___INTERSECTS_FRUSTUM__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>Make Ordered Renderable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK___MAKE_ORDERED_RENDERABLE__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___MAKE_ORDERED_RENDERABLE__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>Right Hand Side</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK___RIGHT_HAND_SIDE = TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Draw</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK___DRAW__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Calc Matrix</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK___CALC_MATRIX__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Ground Track</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_TRACK_OPERATION_COUNT = TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.worldwind.impl.GroundStationImpl <em>Ground Station</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.worldwind.impl.GroundStationImpl
	 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getGroundStation()
	 * @generated
	 */
	int GROUND_STATION = 5;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION__VARIABLES = TIME_DEPENDENT_RENDERABLE__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION__NAME = TIME_DEPENDENT_RENDERABLE__NAME;

	/**
	 * The feature id for the '<em><b>Lat Lon Alt</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION__LAT_LON_ALT = TIME_DEPENDENT_RENDERABLE__LAT_LON_ALT;

	/**
	 * The feature id for the '<em><b>Distance From Eye</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION__DISTANCE_FROM_EYE = TIME_DEPENDENT_RENDERABLE__DISTANCE_FROM_EYE;

	/**
	 * The feature id for the '<em><b>Render Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION__RENDER_SIZE = TIME_DEPENDENT_RENDERABLE__RENDER_SIZE;

	/**
	 * The feature id for the '<em><b>Frame Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION__FRAME_TIMESTAMP = TIME_DEPENDENT_RENDERABLE__FRAME_TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Extent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION__EXTENT = TIME_DEPENDENT_RENDERABLE__EXTENT;

	/**
	 * The feature id for the '<em><b>Matrix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION__MATRIX = TIME_DEPENDENT_RENDERABLE__MATRIX;

	/**
	 * The feature id for the '<em><b>Sat Pos</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION__SAT_POS = TIME_DEPENDENT_RENDERABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Azelra</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION__AZELRA = TIME_DEPENDENT_RENDERABLE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Ground Station</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_FEATURE_COUNT = TIME_DEPENDENT_RENDERABLE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION___RUN = TIME_DEPENDENT_RENDERABLE___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION___CALC = TIME_DEPENDENT_RENDERABLE___CALC;

	/**
	 * The operation id for the '<em>Pick</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION___PICK__DRAWCONTEXT_POINT = TIME_DEPENDENT_RENDERABLE___PICK__DRAWCONTEXT_POINT;

	/**
	 * The operation id for the '<em>Render</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION___RENDER__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___RENDER__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>Draw Ordered Renderable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION___DRAW_ORDERED_RENDERABLE__DRAWCONTEXT_PICKSUPPORT = TIME_DEPENDENT_RENDERABLE___DRAW_ORDERED_RENDERABLE__DRAWCONTEXT_PICKSUPPORT;

	/**
	 * The operation id for the '<em>Begin Drawing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION___BEGIN_DRAWING__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___BEGIN_DRAWING__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>End Drawing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION___END_DRAWING__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___END_DRAWING__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>Intersects Frustum</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION___INTERSECTS_FRUSTUM__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___INTERSECTS_FRUSTUM__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>Make Ordered Renderable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION___MAKE_ORDERED_RENDERABLE__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___MAKE_ORDERED_RENDERABLE__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>Right Hand Side</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION___RIGHT_HAND_SIDE = TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Draw</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION___DRAW__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Calc Matrix</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION___CALC_MATRIX__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Ground Station</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_STATION_OPERATION_COUNT = TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.worldwind.impl.LocalCoordinateSystemImpl <em>Local Coordinate System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.worldwind.impl.LocalCoordinateSystemImpl
	 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getLocalCoordinateSystem()
	 * @generated
	 */
	int LOCAL_COORDINATE_SYSTEM = 6;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM__VARIABLES = TIME_DEPENDENT_RENDERABLE__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM__NAME = TIME_DEPENDENT_RENDERABLE__NAME;

	/**
	 * The feature id for the '<em><b>Lat Lon Alt</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM__LAT_LON_ALT = TIME_DEPENDENT_RENDERABLE__LAT_LON_ALT;

	/**
	 * The feature id for the '<em><b>Distance From Eye</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM__DISTANCE_FROM_EYE = TIME_DEPENDENT_RENDERABLE__DISTANCE_FROM_EYE;

	/**
	 * The feature id for the '<em><b>Render Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM__RENDER_SIZE = TIME_DEPENDENT_RENDERABLE__RENDER_SIZE;

	/**
	 * The feature id for the '<em><b>Frame Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM__FRAME_TIMESTAMP = TIME_DEPENDENT_RENDERABLE__FRAME_TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Extent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM__EXTENT = TIME_DEPENDENT_RENDERABLE__EXTENT;

	/**
	 * The feature id for the '<em><b>Matrix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM__MATRIX = TIME_DEPENDENT_RENDERABLE__MATRIX;

	/**
	 * The feature id for the '<em><b>Quaternion</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM__QUATERNION = TIME_DEPENDENT_RENDERABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Local Coordinate System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM_FEATURE_COUNT = TIME_DEPENDENT_RENDERABLE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM___RUN = TIME_DEPENDENT_RENDERABLE___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM___CALC = TIME_DEPENDENT_RENDERABLE___CALC;

	/**
	 * The operation id for the '<em>Pick</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM___PICK__DRAWCONTEXT_POINT = TIME_DEPENDENT_RENDERABLE___PICK__DRAWCONTEXT_POINT;

	/**
	 * The operation id for the '<em>Render</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM___RENDER__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___RENDER__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>Draw Ordered Renderable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM___DRAW_ORDERED_RENDERABLE__DRAWCONTEXT_PICKSUPPORT = TIME_DEPENDENT_RENDERABLE___DRAW_ORDERED_RENDERABLE__DRAWCONTEXT_PICKSUPPORT;

	/**
	 * The operation id for the '<em>Begin Drawing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM___BEGIN_DRAWING__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___BEGIN_DRAWING__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>End Drawing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM___END_DRAWING__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___END_DRAWING__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>Intersects Frustum</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM___INTERSECTS_FRUSTUM__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___INTERSECTS_FRUSTUM__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>Make Ordered Renderable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM___MAKE_ORDERED_RENDERABLE__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE___MAKE_ORDERED_RENDERABLE__DRAWCONTEXT;

	/**
	 * The operation id for the '<em>Right Hand Side</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM___RIGHT_HAND_SIDE = TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Draw</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM___DRAW__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Calc Matrix</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM___CALC_MATRIX__DRAWCONTEXT = TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Local Coordinate System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_COORDINATE_SYSTEM_OPERATION_COUNT = TIME_DEPENDENT_RENDERABLE_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '<em>EAWT Point</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.awt.Point
	 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getEAWTPoint()
	 * @generated
	 */
	int EAWT_POINT = 7;

	/**
	 * The meta object id for the '<em>EWW Extent</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gov.nasa.worldwind.geom.Extent
	 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getEWWExtent()
	 * @generated
	 */
	int EWW_EXTENT = 8;

	/**
	 * The meta object id for the '<em>EWW Matrix</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gov.nasa.worldwind.geom.Matrix
	 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getEWWMatrix()
	 * @generated
	 */
	int EWW_MATRIX = 9;

	/**
	 * The meta object id for the '<em>EWW Position</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gov.nasa.worldwind.geom.Position
	 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getEWWPosition()
	 * @generated
	 */
	int EWW_POSITION = 10;

	/**
	 * The meta object id for the '<em>EWW Pick Support</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gov.nasa.worldwind.pick.PickSupport
	 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getEWWPickSupport()
	 * @generated
	 */
	int EWW_PICK_SUPPORT = 11;

	/**
	 * The meta object id for the '<em>EWW Draw Context</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gov.nasa.worldwind.render.DrawContext
	 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getEWWDrawContext()
	 * @generated
	 */
	int EWW_DRAW_CONTEXT = 12;

	/**
	 * The meta object id for the '<em>EWW Vec4</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see gov.nasa.worldwind.geom.Vec4
	 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getEWWVec4()
	 * @generated
	 */
	int EWW_VEC4 = 13;


	/**
	 * Returns the meta object for class '{@link gov.nasa.worldwind.render.OrderedRenderable <em>EOrdered Renderable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EOrdered Renderable</em>'.
	 * @see gov.nasa.worldwind.render.OrderedRenderable
	 * @model instanceClass="gov.nasa.worldwind.render.OrderedRenderable"
	 * @generated
	 */
	EClass getEOrderedRenderable();

	/**
	 * Returns the meta object for the '{@link gov.nasa.worldwind.render.OrderedRenderable#draw(gov.nasa.worldwind.render.DrawContext) <em>Draw</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Draw</em>' operation.
	 * @see gov.nasa.worldwind.render.OrderedRenderable#draw(gov.nasa.worldwind.render.DrawContext)
	 * @generated
	 */
	EOperation getEOrderedRenderable__Draw__DrawContext();

	/**
	 * Returns the meta object for the '{@link gov.nasa.worldwind.render.OrderedRenderable#calcMatrix(gov.nasa.worldwind.render.DrawContext) <em>Calc Matrix</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc Matrix</em>' operation.
	 * @see gov.nasa.worldwind.render.OrderedRenderable#calcMatrix(gov.nasa.worldwind.render.DrawContext)
	 * @generated
	 */
	EOperation getEOrderedRenderable__CalcMatrix__DrawContext();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.worldwind.Cube <em>Cube</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cube</em>'.
	 * @see org.thirdeye.extension.space.worldwind.Cube
	 * @generated
	 */
	EClass getCube();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.Cube#rightHandSide() <em>Right Hand Side</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Right Hand Side</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.Cube#rightHandSide()
	 * @generated
	 */
	EOperation getCube__RightHandSide();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.Cube#draw(gov.nasa.worldwind.render.DrawContext) <em>Draw</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Draw</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.Cube#draw(gov.nasa.worldwind.render.DrawContext)
	 * @generated
	 */
	EOperation getCube__Draw__DrawContext();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.Cube#calcMatrix(gov.nasa.worldwind.render.DrawContext) <em>Calc Matrix</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc Matrix</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.Cube#calcMatrix(gov.nasa.worldwind.render.DrawContext)
	 * @generated
	 */
	EOperation getCube__CalcMatrix__DrawContext();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.worldwind.Axis <em>Axis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Axis</em>'.
	 * @see org.thirdeye.extension.space.worldwind.Axis
	 * @generated
	 */
	EClass getAxis();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.worldwind.Axis#getColor <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Color</em>'.
	 * @see org.thirdeye.extension.space.worldwind.Axis#getColor()
	 * @see #getAxis()
	 * @generated
	 */
	EAttribute getAxis_Color();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.Axis#rightHandSide() <em>Right Hand Side</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Right Hand Side</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.Axis#rightHandSide()
	 * @generated
	 */
	EOperation getAxis__RightHandSide();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.Axis#draw(gov.nasa.worldwind.render.DrawContext) <em>Draw</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Draw</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.Axis#draw(gov.nasa.worldwind.render.DrawContext)
	 * @generated
	 */
	EOperation getAxis__Draw__DrawContext();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.Axis#calcMatrix(gov.nasa.worldwind.render.DrawContext) <em>Calc Matrix</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc Matrix</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.Axis#calcMatrix(gov.nasa.worldwind.render.DrawContext)
	 * @generated
	 */
	EOperation getAxis__CalcMatrix__DrawContext();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable <em>Time Dependent Renderable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time Dependent Renderable</em>'.
	 * @see org.thirdeye.extension.space.worldwind.TimeDependentRenderable
	 * @generated
	 */
	EClass getTimeDependentRenderable();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getLatLonAlt <em>Lat Lon Alt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Lat Lon Alt</em>'.
	 * @see org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getLatLonAlt()
	 * @see #getTimeDependentRenderable()
	 * @generated
	 */
	EAttribute getTimeDependentRenderable_LatLonAlt();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getDistanceFromEye <em>Distance From Eye</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Distance From Eye</em>'.
	 * @see org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getDistanceFromEye()
	 * @see #getTimeDependentRenderable()
	 * @generated
	 */
	EAttribute getTimeDependentRenderable_DistanceFromEye();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getRenderSize <em>Render Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Render Size</em>'.
	 * @see org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getRenderSize()
	 * @see #getTimeDependentRenderable()
	 * @generated
	 */
	EAttribute getTimeDependentRenderable_RenderSize();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getFrameTimestamp <em>Frame Timestamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Frame Timestamp</em>'.
	 * @see org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getFrameTimestamp()
	 * @see #getTimeDependentRenderable()
	 * @generated
	 */
	EAttribute getTimeDependentRenderable_FrameTimestamp();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getExtent <em>Extent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Extent</em>'.
	 * @see org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getExtent()
	 * @see #getTimeDependentRenderable()
	 * @generated
	 */
	EAttribute getTimeDependentRenderable_Extent();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getMatrix <em>Matrix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Matrix</em>'.
	 * @see org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getMatrix()
	 * @see #getTimeDependentRenderable()
	 * @generated
	 */
	EAttribute getTimeDependentRenderable_Matrix();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#pick(gov.nasa.worldwind.render.DrawContext, java.awt.Point) <em>Pick</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pick</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.TimeDependentRenderable#pick(gov.nasa.worldwind.render.DrawContext, java.awt.Point)
	 * @generated
	 */
	EOperation getTimeDependentRenderable__Pick__DrawContext_Point();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#render(gov.nasa.worldwind.render.DrawContext) <em>Render</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Render</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.TimeDependentRenderable#render(gov.nasa.worldwind.render.DrawContext)
	 * @generated
	 */
	EOperation getTimeDependentRenderable__Render__DrawContext();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#drawOrderedRenderable(gov.nasa.worldwind.render.DrawContext, gov.nasa.worldwind.pick.PickSupport) <em>Draw Ordered Renderable</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Draw Ordered Renderable</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.TimeDependentRenderable#drawOrderedRenderable(gov.nasa.worldwind.render.DrawContext, gov.nasa.worldwind.pick.PickSupport)
	 * @generated
	 */
	EOperation getTimeDependentRenderable__DrawOrderedRenderable__DrawContext_PickSupport();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#beginDrawing(gov.nasa.worldwind.render.DrawContext) <em>Begin Drawing</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Begin Drawing</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.TimeDependentRenderable#beginDrawing(gov.nasa.worldwind.render.DrawContext)
	 * @generated
	 */
	EOperation getTimeDependentRenderable__BeginDrawing__DrawContext();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#endDrawing(gov.nasa.worldwind.render.DrawContext) <em>End Drawing</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>End Drawing</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.TimeDependentRenderable#endDrawing(gov.nasa.worldwind.render.DrawContext)
	 * @generated
	 */
	EOperation getTimeDependentRenderable__EndDrawing__DrawContext();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#intersectsFrustum(gov.nasa.worldwind.render.DrawContext) <em>Intersects Frustum</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Intersects Frustum</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.TimeDependentRenderable#intersectsFrustum(gov.nasa.worldwind.render.DrawContext)
	 * @generated
	 */
	EOperation getTimeDependentRenderable__IntersectsFrustum__DrawContext();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#makeOrderedRenderable(gov.nasa.worldwind.render.DrawContext) <em>Make Ordered Renderable</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Make Ordered Renderable</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.TimeDependentRenderable#makeOrderedRenderable(gov.nasa.worldwind.render.DrawContext)
	 * @generated
	 */
	EOperation getTimeDependentRenderable__MakeOrderedRenderable__DrawContext();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#calcMatrix(gov.nasa.worldwind.render.DrawContext) <em>Calc Matrix</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc Matrix</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.TimeDependentRenderable#calcMatrix(gov.nasa.worldwind.render.DrawContext)
	 * @generated
	 */
	EOperation getTimeDependentRenderable__CalcMatrix__DrawContext();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.worldwind.GroundTrack <em>Ground Track</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ground Track</em>'.
	 * @see org.thirdeye.extension.space.worldwind.GroundTrack
	 * @generated
	 */
	EClass getGroundTrack();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.worldwind.GroundTrack#getPoints <em>Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Points</em>'.
	 * @see org.thirdeye.extension.space.worldwind.GroundTrack#getPoints()
	 * @see #getGroundTrack()
	 * @generated
	 */
	EAttribute getGroundTrack_Points();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.GroundTrack#rightHandSide() <em>Right Hand Side</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Right Hand Side</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.GroundTrack#rightHandSide()
	 * @generated
	 */
	EOperation getGroundTrack__RightHandSide();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.GroundTrack#draw(gov.nasa.worldwind.render.DrawContext) <em>Draw</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Draw</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.GroundTrack#draw(gov.nasa.worldwind.render.DrawContext)
	 * @generated
	 */
	EOperation getGroundTrack__Draw__DrawContext();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.GroundTrack#calcMatrix(gov.nasa.worldwind.render.DrawContext) <em>Calc Matrix</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc Matrix</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.GroundTrack#calcMatrix(gov.nasa.worldwind.render.DrawContext)
	 * @generated
	 */
	EOperation getGroundTrack__CalcMatrix__DrawContext();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.worldwind.GroundStation <em>Ground Station</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ground Station</em>'.
	 * @see org.thirdeye.extension.space.worldwind.GroundStation
	 * @generated
	 */
	EClass getGroundStation();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.worldwind.GroundStation#getSatPos <em>Sat Pos</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Sat Pos</em>'.
	 * @see org.thirdeye.extension.space.worldwind.GroundStation#getSatPos()
	 * @see #getGroundStation()
	 * @generated
	 */
	EAttribute getGroundStation_SatPos();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.worldwind.GroundStation#getAzelra <em>Azelra</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Azelra</em>'.
	 * @see org.thirdeye.extension.space.worldwind.GroundStation#getAzelra()
	 * @see #getGroundStation()
	 * @generated
	 */
	EAttribute getGroundStation_Azelra();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.GroundStation#rightHandSide() <em>Right Hand Side</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Right Hand Side</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.GroundStation#rightHandSide()
	 * @generated
	 */
	EOperation getGroundStation__RightHandSide();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.GroundStation#draw(gov.nasa.worldwind.render.DrawContext) <em>Draw</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Draw</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.GroundStation#draw(gov.nasa.worldwind.render.DrawContext)
	 * @generated
	 */
	EOperation getGroundStation__Draw__DrawContext();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.GroundStation#calcMatrix(gov.nasa.worldwind.render.DrawContext) <em>Calc Matrix</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc Matrix</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.GroundStation#calcMatrix(gov.nasa.worldwind.render.DrawContext)
	 * @generated
	 */
	EOperation getGroundStation__CalcMatrix__DrawContext();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.worldwind.LocalCoordinateSystem <em>Local Coordinate System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Local Coordinate System</em>'.
	 * @see org.thirdeye.extension.space.worldwind.LocalCoordinateSystem
	 * @generated
	 */
	EClass getLocalCoordinateSystem();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.worldwind.LocalCoordinateSystem#getQuaternion <em>Quaternion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quaternion</em>'.
	 * @see org.thirdeye.extension.space.worldwind.LocalCoordinateSystem#getQuaternion()
	 * @see #getLocalCoordinateSystem()
	 * @generated
	 */
	EAttribute getLocalCoordinateSystem_Quaternion();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.LocalCoordinateSystem#rightHandSide() <em>Right Hand Side</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Right Hand Side</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.LocalCoordinateSystem#rightHandSide()
	 * @generated
	 */
	EOperation getLocalCoordinateSystem__RightHandSide();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.LocalCoordinateSystem#draw(gov.nasa.worldwind.render.DrawContext) <em>Draw</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Draw</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.LocalCoordinateSystem#draw(gov.nasa.worldwind.render.DrawContext)
	 * @generated
	 */
	EOperation getLocalCoordinateSystem__Draw__DrawContext();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.worldwind.LocalCoordinateSystem#calcMatrix(gov.nasa.worldwind.render.DrawContext) <em>Calc Matrix</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc Matrix</em>' operation.
	 * @see org.thirdeye.extension.space.worldwind.LocalCoordinateSystem#calcMatrix(gov.nasa.worldwind.render.DrawContext)
	 * @generated
	 */
	EOperation getLocalCoordinateSystem__CalcMatrix__DrawContext();

	/**
	 * Returns the meta object for data type '{@link java.awt.Point <em>EAWT Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EAWT Point</em>'.
	 * @see java.awt.Point
	 * @model instanceClass="java.awt.Point" serializeable="false"
	 * @generated
	 */
	EDataType getEAWTPoint();

	/**
	 * Returns the meta object for data type '{@link gov.nasa.worldwind.geom.Extent <em>EWW Extent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EWW Extent</em>'.
	 * @see gov.nasa.worldwind.geom.Extent
	 * @model instanceClass="gov.nasa.worldwind.geom.Extent" serializeable="false"
	 * @generated
	 */
	EDataType getEWWExtent();

	/**
	 * Returns the meta object for data type '{@link gov.nasa.worldwind.geom.Matrix <em>EWW Matrix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EWW Matrix</em>'.
	 * @see gov.nasa.worldwind.geom.Matrix
	 * @model instanceClass="gov.nasa.worldwind.geom.Matrix"
	 * @generated
	 */
	EDataType getEWWMatrix();

	/**
	 * Returns the meta object for data type '{@link gov.nasa.worldwind.geom.Position <em>EWW Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EWW Position</em>'.
	 * @see gov.nasa.worldwind.geom.Position
	 * @model instanceClass="gov.nasa.worldwind.geom.Position" serializeable="false"
	 * @generated
	 */
	EDataType getEWWPosition();

	/**
	 * Returns the meta object for data type '{@link gov.nasa.worldwind.pick.PickSupport <em>EWW Pick Support</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EWW Pick Support</em>'.
	 * @see gov.nasa.worldwind.pick.PickSupport
	 * @model instanceClass="gov.nasa.worldwind.pick.PickSupport" serializeable="false"
	 * @generated
	 */
	EDataType getEWWPickSupport();

	/**
	 * Returns the meta object for data type '{@link gov.nasa.worldwind.render.DrawContext <em>EWW Draw Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EWW Draw Context</em>'.
	 * @see gov.nasa.worldwind.render.DrawContext
	 * @model instanceClass="gov.nasa.worldwind.render.DrawContext" serializeable="false"
	 * @generated
	 */
	EDataType getEWWDrawContext();

	/**
	 * Returns the meta object for data type '{@link gov.nasa.worldwind.geom.Vec4 <em>EWW Vec4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EWW Vec4</em>'.
	 * @see gov.nasa.worldwind.geom.Vec4
	 * @model instanceClass="gov.nasa.worldwind.geom.Vec4" serializeable="false"
	 * @generated
	 */
	EDataType getEWWVec4();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WorldwindFactory getWorldwindFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link gov.nasa.worldwind.render.OrderedRenderable <em>EOrdered Renderable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gov.nasa.worldwind.render.OrderedRenderable
		 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getEOrderedRenderable()
		 * @generated
		 */
		EClass EORDERED_RENDERABLE = eINSTANCE.getEOrderedRenderable();

		/**
		 * The meta object literal for the '<em><b>Draw</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation EORDERED_RENDERABLE___DRAW__DRAWCONTEXT = eINSTANCE.getEOrderedRenderable__Draw__DrawContext();

		/**
		 * The meta object literal for the '<em><b>Calc Matrix</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation EORDERED_RENDERABLE___CALC_MATRIX__DRAWCONTEXT = eINSTANCE.getEOrderedRenderable__CalcMatrix__DrawContext();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.worldwind.impl.CubeImpl <em>Cube</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.worldwind.impl.CubeImpl
		 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getCube()
		 * @generated
		 */
		EClass CUBE = eINSTANCE.getCube();

		/**
		 * The meta object literal for the '<em><b>Right Hand Side</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CUBE___RIGHT_HAND_SIDE = eINSTANCE.getCube__RightHandSide();

		/**
		 * The meta object literal for the '<em><b>Draw</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CUBE___DRAW__DRAWCONTEXT = eINSTANCE.getCube__Draw__DrawContext();

		/**
		 * The meta object literal for the '<em><b>Calc Matrix</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CUBE___CALC_MATRIX__DRAWCONTEXT = eINSTANCE.getCube__CalcMatrix__DrawContext();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.worldwind.impl.AxisImpl <em>Axis</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.worldwind.impl.AxisImpl
		 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getAxis()
		 * @generated
		 */
		EClass AXIS = eINSTANCE.getAxis();

		/**
		 * The meta object literal for the '<em><b>Color</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AXIS__COLOR = eINSTANCE.getAxis_Color();

		/**
		 * The meta object literal for the '<em><b>Right Hand Side</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AXIS___RIGHT_HAND_SIDE = eINSTANCE.getAxis__RightHandSide();

		/**
		 * The meta object literal for the '<em><b>Draw</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AXIS___DRAW__DRAWCONTEXT = eINSTANCE.getAxis__Draw__DrawContext();

		/**
		 * The meta object literal for the '<em><b>Calc Matrix</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AXIS___CALC_MATRIX__DRAWCONTEXT = eINSTANCE.getAxis__CalcMatrix__DrawContext();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.worldwind.impl.TimeDependentRenderableImpl <em>Time Dependent Renderable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.worldwind.impl.TimeDependentRenderableImpl
		 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getTimeDependentRenderable()
		 * @generated
		 */
		EClass TIME_DEPENDENT_RENDERABLE = eINSTANCE.getTimeDependentRenderable();

		/**
		 * The meta object literal for the '<em><b>Lat Lon Alt</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_DEPENDENT_RENDERABLE__LAT_LON_ALT = eINSTANCE.getTimeDependentRenderable_LatLonAlt();

		/**
		 * The meta object literal for the '<em><b>Distance From Eye</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_DEPENDENT_RENDERABLE__DISTANCE_FROM_EYE = eINSTANCE.getTimeDependentRenderable_DistanceFromEye();

		/**
		 * The meta object literal for the '<em><b>Render Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_DEPENDENT_RENDERABLE__RENDER_SIZE = eINSTANCE.getTimeDependentRenderable_RenderSize();

		/**
		 * The meta object literal for the '<em><b>Frame Timestamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_DEPENDENT_RENDERABLE__FRAME_TIMESTAMP = eINSTANCE.getTimeDependentRenderable_FrameTimestamp();

		/**
		 * The meta object literal for the '<em><b>Extent</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_DEPENDENT_RENDERABLE__EXTENT = eINSTANCE.getTimeDependentRenderable_Extent();

		/**
		 * The meta object literal for the '<em><b>Matrix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME_DEPENDENT_RENDERABLE__MATRIX = eINSTANCE.getTimeDependentRenderable_Matrix();

		/**
		 * The meta object literal for the '<em><b>Pick</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TIME_DEPENDENT_RENDERABLE___PICK__DRAWCONTEXT_POINT = eINSTANCE.getTimeDependentRenderable__Pick__DrawContext_Point();

		/**
		 * The meta object literal for the '<em><b>Render</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TIME_DEPENDENT_RENDERABLE___RENDER__DRAWCONTEXT = eINSTANCE.getTimeDependentRenderable__Render__DrawContext();

		/**
		 * The meta object literal for the '<em><b>Draw Ordered Renderable</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TIME_DEPENDENT_RENDERABLE___DRAW_ORDERED_RENDERABLE__DRAWCONTEXT_PICKSUPPORT = eINSTANCE.getTimeDependentRenderable__DrawOrderedRenderable__DrawContext_PickSupport();

		/**
		 * The meta object literal for the '<em><b>Begin Drawing</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TIME_DEPENDENT_RENDERABLE___BEGIN_DRAWING__DRAWCONTEXT = eINSTANCE.getTimeDependentRenderable__BeginDrawing__DrawContext();

		/**
		 * The meta object literal for the '<em><b>End Drawing</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TIME_DEPENDENT_RENDERABLE___END_DRAWING__DRAWCONTEXT = eINSTANCE.getTimeDependentRenderable__EndDrawing__DrawContext();

		/**
		 * The meta object literal for the '<em><b>Intersects Frustum</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TIME_DEPENDENT_RENDERABLE___INTERSECTS_FRUSTUM__DRAWCONTEXT = eINSTANCE.getTimeDependentRenderable__IntersectsFrustum__DrawContext();

		/**
		 * The meta object literal for the '<em><b>Make Ordered Renderable</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TIME_DEPENDENT_RENDERABLE___MAKE_ORDERED_RENDERABLE__DRAWCONTEXT = eINSTANCE.getTimeDependentRenderable__MakeOrderedRenderable__DrawContext();

		/**
		 * The meta object literal for the '<em><b>Calc Matrix</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TIME_DEPENDENT_RENDERABLE___CALC_MATRIX__DRAWCONTEXT = eINSTANCE.getTimeDependentRenderable__CalcMatrix__DrawContext();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.worldwind.impl.GroundTrackImpl <em>Ground Track</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.worldwind.impl.GroundTrackImpl
		 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getGroundTrack()
		 * @generated
		 */
		EClass GROUND_TRACK = eINSTANCE.getGroundTrack();

		/**
		 * The meta object literal for the '<em><b>Points</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GROUND_TRACK__POINTS = eINSTANCE.getGroundTrack_Points();

		/**
		 * The meta object literal for the '<em><b>Right Hand Side</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation GROUND_TRACK___RIGHT_HAND_SIDE = eINSTANCE.getGroundTrack__RightHandSide();

		/**
		 * The meta object literal for the '<em><b>Draw</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation GROUND_TRACK___DRAW__DRAWCONTEXT = eINSTANCE.getGroundTrack__Draw__DrawContext();

		/**
		 * The meta object literal for the '<em><b>Calc Matrix</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation GROUND_TRACK___CALC_MATRIX__DRAWCONTEXT = eINSTANCE.getGroundTrack__CalcMatrix__DrawContext();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.worldwind.impl.GroundStationImpl <em>Ground Station</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.worldwind.impl.GroundStationImpl
		 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getGroundStation()
		 * @generated
		 */
		EClass GROUND_STATION = eINSTANCE.getGroundStation();

		/**
		 * The meta object literal for the '<em><b>Sat Pos</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GROUND_STATION__SAT_POS = eINSTANCE.getGroundStation_SatPos();

		/**
		 * The meta object literal for the '<em><b>Azelra</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GROUND_STATION__AZELRA = eINSTANCE.getGroundStation_Azelra();

		/**
		 * The meta object literal for the '<em><b>Right Hand Side</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation GROUND_STATION___RIGHT_HAND_SIDE = eINSTANCE.getGroundStation__RightHandSide();

		/**
		 * The meta object literal for the '<em><b>Draw</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation GROUND_STATION___DRAW__DRAWCONTEXT = eINSTANCE.getGroundStation__Draw__DrawContext();

		/**
		 * The meta object literal for the '<em><b>Calc Matrix</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation GROUND_STATION___CALC_MATRIX__DRAWCONTEXT = eINSTANCE.getGroundStation__CalcMatrix__DrawContext();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.worldwind.impl.LocalCoordinateSystemImpl <em>Local Coordinate System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.worldwind.impl.LocalCoordinateSystemImpl
		 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getLocalCoordinateSystem()
		 * @generated
		 */
		EClass LOCAL_COORDINATE_SYSTEM = eINSTANCE.getLocalCoordinateSystem();

		/**
		 * The meta object literal for the '<em><b>Quaternion</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCAL_COORDINATE_SYSTEM__QUATERNION = eINSTANCE.getLocalCoordinateSystem_Quaternion();

		/**
		 * The meta object literal for the '<em><b>Right Hand Side</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LOCAL_COORDINATE_SYSTEM___RIGHT_HAND_SIDE = eINSTANCE.getLocalCoordinateSystem__RightHandSide();

		/**
		 * The meta object literal for the '<em><b>Draw</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LOCAL_COORDINATE_SYSTEM___DRAW__DRAWCONTEXT = eINSTANCE.getLocalCoordinateSystem__Draw__DrawContext();

		/**
		 * The meta object literal for the '<em><b>Calc Matrix</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LOCAL_COORDINATE_SYSTEM___CALC_MATRIX__DRAWCONTEXT = eINSTANCE.getLocalCoordinateSystem__CalcMatrix__DrawContext();

		/**
		 * The meta object literal for the '<em>EAWT Point</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.awt.Point
		 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getEAWTPoint()
		 * @generated
		 */
		EDataType EAWT_POINT = eINSTANCE.getEAWTPoint();

		/**
		 * The meta object literal for the '<em>EWW Extent</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gov.nasa.worldwind.geom.Extent
		 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getEWWExtent()
		 * @generated
		 */
		EDataType EWW_EXTENT = eINSTANCE.getEWWExtent();

		/**
		 * The meta object literal for the '<em>EWW Matrix</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gov.nasa.worldwind.geom.Matrix
		 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getEWWMatrix()
		 * @generated
		 */
		EDataType EWW_MATRIX = eINSTANCE.getEWWMatrix();

		/**
		 * The meta object literal for the '<em>EWW Position</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gov.nasa.worldwind.geom.Position
		 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getEWWPosition()
		 * @generated
		 */
		EDataType EWW_POSITION = eINSTANCE.getEWWPosition();

		/**
		 * The meta object literal for the '<em>EWW Pick Support</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gov.nasa.worldwind.pick.PickSupport
		 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getEWWPickSupport()
		 * @generated
		 */
		EDataType EWW_PICK_SUPPORT = eINSTANCE.getEWWPickSupport();

		/**
		 * The meta object literal for the '<em>EWW Draw Context</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gov.nasa.worldwind.render.DrawContext
		 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getEWWDrawContext()
		 * @generated
		 */
		EDataType EWW_DRAW_CONTEXT = eINSTANCE.getEWWDrawContext();

		/**
		 * The meta object literal for the '<em>EWW Vec4</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see gov.nasa.worldwind.geom.Vec4
		 * @see org.thirdeye.extension.space.worldwind.impl.WorldwindPackageImpl#getEWWVec4()
		 * @generated
		 */
		EDataType EWW_VEC4 = eINSTANCE.getEWWVec4();

	}

} //WorldwindPackage
