/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.worldwind;

import gov.nasa.worldwind.geom.Extent;
import gov.nasa.worldwind.geom.Matrix;

import gov.nasa.worldwind.pick.PickSupport;

import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.OrderedRenderable;

import java.awt.Point;

import org.eclipse.emf.common.util.EList;

import org.thirdeye.core.mesh.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Dependent Renderable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getLatLonAlt <em>Lat Lon Alt</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getDistanceFromEye <em>Distance From Eye</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getRenderSize <em>Render Size</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getFrameTimestamp <em>Frame Timestamp</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getExtent <em>Extent</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getMatrix <em>Matrix</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extension.space.worldwind.WorldwindPackage#getTimeDependentRenderable()
 * @model abstract="true" superTypes="org.thirdeye.core.mesh.Model org.thirdeye.extension.space.worldwind.EOrderedRenderable"
 * @generated
 */
public interface TimeDependentRenderable extends Model, OrderedRenderable {
	/**
	 * Returns the value of the '<em><b>Lat Lon Alt</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lat Lon Alt</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lat Lon Alt</em>' attribute list.
	 * @see org.thirdeye.extension.space.worldwind.WorldwindPackage#getTimeDependentRenderable_LatLonAlt()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 *        annotation="ThirdEye unit='rad,rad,m'"
	 * @generated
	 */
	EList<Double> getLatLonAlt();

	/**
	 * Returns the value of the '<em><b>Distance From Eye</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Distance From Eye</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Distance From Eye</em>' attribute.
	 * @see #setDistanceFromEye(double)
	 * @see org.thirdeye.extension.space.worldwind.WorldwindPackage#getTimeDependentRenderable_DistanceFromEye()
	 * @model required="true"
	 * @generated
	 */
	double getDistanceFromEye();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getDistanceFromEye <em>Distance From Eye</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Distance From Eye</em>' attribute.
	 * @see #getDistanceFromEye()
	 * @generated
	 */
	void setDistanceFromEye(double value);

	/**
	 * Returns the value of the '<em><b>Render Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Render Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Render Size</em>' attribute.
	 * @see #setRenderSize(double)
	 * @see org.thirdeye.extension.space.worldwind.WorldwindPackage#getTimeDependentRenderable_RenderSize()
	 * @model required="true"
	 * @generated
	 */
	double getRenderSize();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getRenderSize <em>Render Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Render Size</em>' attribute.
	 * @see #getRenderSize()
	 * @generated
	 */
	void setRenderSize(double value);

	/**
	 * Returns the value of the '<em><b>Frame Timestamp</b></em>' attribute.
	 * The default value is <code>"-1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Frame Timestamp</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Frame Timestamp</em>' attribute.
	 * @see #setFrameTimestamp(long)
	 * @see org.thirdeye.extension.space.worldwind.WorldwindPackage#getTimeDependentRenderable_FrameTimestamp()
	 * @model default="-1" required="true"
	 * @generated
	 */
	long getFrameTimestamp();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getFrameTimestamp <em>Frame Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Frame Timestamp</em>' attribute.
	 * @see #getFrameTimestamp()
	 * @generated
	 */
	void setFrameTimestamp(long value);

	/**
	 * Returns the value of the '<em><b>Extent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extent</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extent</em>' attribute.
	 * @see #setExtent(Extent)
	 * @see org.thirdeye.extension.space.worldwind.WorldwindPackage#getTimeDependentRenderable_Extent()
	 * @model dataType="org.thirdeye.extension.space.worldwind.EWWExtent" required="true" transient="true"
	 * @generated
	 */
	Extent getExtent();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getExtent <em>Extent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extent</em>' attribute.
	 * @see #getExtent()
	 * @generated
	 */
	void setExtent(Extent value);

	/**
	 * Returns the value of the '<em><b>Matrix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Matrix</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Matrix</em>' attribute.
	 * @see #setMatrix(Matrix)
	 * @see org.thirdeye.extension.space.worldwind.WorldwindPackage#getTimeDependentRenderable_Matrix()
	 * @model dataType="org.thirdeye.extension.space.worldwind.EWWMatrix" required="true" transient="true"
	 * @generated
	 */
	Matrix getMatrix();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.worldwind.TimeDependentRenderable#getMatrix <em>Matrix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Matrix</em>' attribute.
	 * @see #getMatrix()
	 * @generated
	 */
	void setMatrix(Matrix value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dcDataType="org.thirdeye.extension.space.worldwind.EWWDrawContext" dcRequired="true" pickPointDataType="org.thirdeye.extension.space.worldwind.EAWTPoint"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='render(dc);'"
	 * @generated
	 */
	void pick(DrawContext dc, Point pickPoint);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dcDataType="org.thirdeye.extension.space.worldwind.EWWDrawContext" dcRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if(getExtent() != null) {\n//\tif((!intersectsFrustum(dc))||(dc.isSmall((Extent) getExtent(), 1))) {\n//\t\treturn;\n//\t}\n}\nif(dc.isOrderedRenderingMode()) {\n\tdrawOrderedRenderable(dc, pickSupport );\n} else {\n\tmakeOrderedRenderable(dc);\n}'"
	 * @generated
	 */
	void render(DrawContext dc);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dcDataType="org.thirdeye.extension.space.worldwind.EWWDrawContext" dcRequired="true" pickCandidatesDataType="org.thirdeye.extension.space.worldwind.EWWPickSupport" pickCandidatesRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='GL2 gl = dc.getGL().getGL2();\nbeginDrawing(dc);\ntry {\n\tif(dc.isPickingMode()) {\n\t\tColor pickColor = dc.getUniquePickColor();\n\t\tpickCandidates.addPickableObject(pickColor.getRGB(), this, new Position(Position.fromRadians(getLatLonAlt()[0], getLatLonAlt()[1]), getLatLonAlt()[2]));\n\t\tgl.glColor3ub((byte) pickColor.getRed(), (byte) pickColor.getGreen(), (byte) pickColor.getBlue());\n\t}\n\tgl.glScaled(getRenderSize(), getRenderSize(), getRenderSize());\n\tdraw(dc);\n} finally {\n\tendDrawing(dc);\n}'"
	 * @generated
	 */
	void drawOrderedRenderable(DrawContext dc, PickSupport pickCandidates);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dcDataType="org.thirdeye.extension.space.worldwind.EWWDrawContext" dcRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='GL2 gl = dc.getGL().getGL2();\nint attrMask = GL2.GL_CURRENT_BIT | GL2.GL_COLOR_BUFFER_BIT;\ngl.glPushAttrib(attrMask);\nif(!dc.isPickingMode()) {\n\tdc.beginStandardLighting();\n\tgl.glEnable(GL.GL_BLEND);\n\tOGLUtil.applyBlending(gl, false);\n\tgl.glEnable(GL2.GL_NORMALIZE);\n}\ngl.glMatrixMode(GL2.GL_MODELVIEW);\ncalcMatrix(dc);'"
	 * @generated
	 */
	void beginDrawing(DrawContext dc);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dcDataType="org.thirdeye.extension.space.worldwind.EWWDrawContext" dcRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='GL2 gl = dc.getGL().getGL2();\nif (!dc.isPickingMode()) dc.endStandardLighting();\ngl.glPopAttrib();'"
	 * @generated
	 */
	void endDrawing(DrawContext dc);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" dcDataType="org.thirdeye.extension.space.worldwind.EWWDrawContext" dcRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='//if(getExtent() == null)\n//\treturn true;\n//if (dc.isPickingMode())\n//\treturn dc.getPickFrustums().intersectsAny(getExtent());\n//return dc.getView().getFrustumInModelCoordinates().intersects(getExtent());\052/\nreturn true;'"
	 * @generated
	 */
	boolean intersectsFrustum(DrawContext dc);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dcDataType="org.thirdeye.extension.space.worldwind.EWWDrawContext" dcRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if(dc.getFrameTimeStamp() != getFrameTimestamp()) {\n\tVec4 placePoint = dc.getGlobe().computePointFromPosition(new Position(Position.fromRadians(getLatLonAlt()[0], getLatLonAlt()[1]), getLatLonAlt()[2]));\n\tsetDistanceFromEye( dc.getView().getEyePoint().distanceTo3(placePoint) );\n\tsetExtent( new Sphere(placePoint, Math.sqrt(3.0) * getRenderSize() / 2.0));\n\tsetFrameTimestamp( dc.getFrameTimeStamp());\n}\ndc.addOrderedRenderable((OrderedRenderable) this);'"
	 * @generated
	 */
	void makeOrderedRenderable(DrawContext dc);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dcDataType="org.thirdeye.extension.space.worldwind.EWWDrawContext" dcRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='GL2 gl = dc.getGL().getGL2();\ngl.glMatrixMode(GL2.GL_MODELVIEW);\nsetMatrix( dc.getGlobe().computeSurfaceOrientationAtPosition(new Position(Position.fromRadians(getLatLonAlt()[0], getLatLonAlt()[1]), getLatLonAlt()[2])) );\nsetMatrix( dc.getView().getModelviewMatrix().multiply(getMatrix()));\ndouble[] matrixArray = new double[16];\ngetMatrix().toArray(matrixArray, 0, false);\ngl.glLoadMatrixd(matrixArray, 0);'"
	 * @generated
	 */
	void calcMatrix(DrawContext dc);

} // TimeDependentRenderable
