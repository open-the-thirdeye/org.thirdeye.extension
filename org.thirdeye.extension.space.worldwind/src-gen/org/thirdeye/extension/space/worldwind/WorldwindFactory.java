/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.worldwind;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.thirdeye.extension.space.worldwind.WorldwindPackage
 * @generated
 */
public interface WorldwindFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorldwindFactory eINSTANCE = org.thirdeye.extension.space.worldwind.impl.WorldwindFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Cube</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cube</em>'.
	 * @generated
	 */
	Cube createCube();

	/**
	 * Returns a new object of class '<em>Axis</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Axis</em>'.
	 * @generated
	 */
	Axis createAxis();

	/**
	 * Returns a new object of class '<em>Ground Track</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ground Track</em>'.
	 * @generated
	 */
	GroundTrack createGroundTrack();

	/**
	 * Returns a new object of class '<em>Ground Station</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ground Station</em>'.
	 * @generated
	 */
	GroundStation createGroundStation();

	/**
	 * Returns a new object of class '<em>Local Coordinate System</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Local Coordinate System</em>'.
	 * @generated
	 */
	LocalCoordinateSystem createLocalCoordinateSystem();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	WorldwindPackage getWorldwindPackage();

} //WorldwindFactory
