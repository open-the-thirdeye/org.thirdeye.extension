/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.worldwind;

import gov.nasa.worldwind.render.DrawContext;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cube</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.thirdeye.extension.space.worldwind.WorldwindPackage#getCube()
 * @model
 * @generated
 */
public interface Cube extends TimeDependentRenderable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='//intentionally left blank'"
	 * @generated
	 */
	void rightHandSide();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dcDataType="org.thirdeye.extension.space.worldwind.EWWDrawContext" dcRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='// Vertices of a unit cube, centered on the origin.\nfloat[][] v = {   {-0.5f, 0.5f, -0.5f}, {-0.5f, 0.5f, 0.5f}, {0.5f, 0.5f, 0.5f}, {0.5f, 0.5f, -0.5f},\n\t\t\t\t{-0.5f, -0.5f, 0.5f}, {0.5f, -0.5f, 0.5f}, {0.5f, -0.5f, -0.5f}, {-0.5f, -0.5f, -0.5f}};\n// Array to group vertices into faces\nint[][] faces = {{0, 1, 2, 3}, {2, 5, 6, 3}, {1, 4, 5, 2}, {0, 7, 4, 1}, {0, 7, 6, 3}, {4, 7, 6, 5}};\n\n// Normal vectors for each face\nfloat[][] n = {{0, 1, 0}, {1, 0, 0}, {0, 0, 1}, {-1, 0, 0}, {0, 0, -1}, {0, -1, 0}};\n\nGL2 gl = dc.getGL().getGL2(); // GL initialization checks for GL2 compatibility.\ngl.glBegin(GL2.GL_QUADS);\ntry {\n\tfor (int i = 0; i &lt; faces.length; i++) {\n\t\tgl.glNormal3f(n[i][0], n[i][1], n[i][2]);\n\t\tfor(int j = 0; j &lt; faces[0].length; j++) {\n\t\t\tgl.glVertex3f(v[faces[i][j]][0], v[faces[i][j]][1], v[faces[i][j]][2]);\n\t\t}\n\t}\n} finally {\n\tgl.glEnd();\n}'"
	 * @generated
	 */
	void draw(DrawContext dc);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dcDataType="org.thirdeye.extension.space.worldwind.EWWDrawContext" dcRequired="true"
	 * @generated
	 */
	void calcMatrix(DrawContext dc);

} // Cube
