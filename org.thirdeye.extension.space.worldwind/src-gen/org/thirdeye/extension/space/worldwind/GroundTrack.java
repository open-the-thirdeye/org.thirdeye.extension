/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.worldwind;

import gov.nasa.worldwind.geom.Vec4;

import gov.nasa.worldwind.render.DrawContext;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ground Track</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.worldwind.GroundTrack#getPoints <em>Points</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extension.space.worldwind.WorldwindPackage#getGroundTrack()
 * @model
 * @generated
 */
public interface GroundTrack extends TimeDependentRenderable {
	/**
	 * Returns the value of the '<em><b>Points</b></em>' attribute list.
	 * The list contents are of type {@link gov.nasa.worldwind.geom.Vec4}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Points</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Points</em>' attribute list.
	 * @see org.thirdeye.extension.space.worldwind.WorldwindPackage#getGroundTrack_Points()
	 * @model unique="false" dataType="org.thirdeye.extension.space.worldwind.EWWVec4" transient="true" ordered="false"
	 * @generated
	 */
	EList<Vec4> getPoints();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='//intentionally left blank'"
	 * @generated
	 */
	void rightHandSide();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dcDataType="org.thirdeye.extension.space.worldwind.EWWDrawContext" dcRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body=' GL2 gl = dc.getGL().getGL2(); // GL initialization checks for GL2 compatibility.\nif( !init  ) {\n\tVec4 point = dc.getGlobe().computePointFromPosition(Position.fromRadians(getLatLonAlt()[0], getLatLonAlt()[1]), 100000.0);\n\tstart = new double[] {point.x,point.y,point.z};\n\tinit = true;\n}\ni++;\nif( i == 5) {\n\ti = 0;\n\tif( lastPoint != null ) {\n\t\tVec4 newPoint = dc.getGlobe().computePointFromPosition(Position.fromRadians(getLatLonAlt()[0], getLatLonAlt()[1]), 100000.0);\n\t\tif( !lastPoint.equals(newPoint)) {\n\t\t\tdouble[] dx = new double[] { lastPoint.x - newPoint.x,lastPoint.y - newPoint.y, lastPoint.z - newPoint.z};\n\t\t\tlist .add(dx);\n\t\t}\n\t}\n\tlastPoint = dc.getGlobe().computePointFromPosition(Position.fromRadians(getLatLonAlt()[0], getLatLonAlt()[1]), 100000.0);\n}\ndouble[] vec = new double[] {0.0,0.0,0.0};\nvec[0] = vec[0] + start[0];\nvec[1] = vec[1] + start[1];\nvec[2] = vec[2] + start[2];\nfor( double[] inc: list ) {\n\tgl.glBegin(GL.GL_LINES);\n\tgl.glVertex3d(vec[0], vec[1], vec[2]);\n\tvec[0]-=inc[0];\n\tvec[1]-=inc[1];\n\tvec[2]-=inc[2];\n\tgl.glVertex3d(vec[0], vec[1], vec[2]);\n\tgl.glColor3d(0.0, 0.0, 1.0);\n\tgl.glEnd();\n}'"
	 * @generated
	 */
	void draw(DrawContext dc);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dcDataType="org.thirdeye.extension.space.worldwind.EWWDrawContext" dcRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='GL2 gl = dc.getGL().getGL2();\nsetMatrix( dc.getView().getModelviewMatrix());\ndouble[] matrixArray = new double[16];\ngetMatrix().toArray(matrixArray, 0, false);\ngl.glLoadMatrixd(matrixArray, 0);'"
	 * @generated
	 */
	void calcMatrix(DrawContext dc);

} // GroundTrack
