/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.worldwind;

import gov.nasa.worldwind.render.DrawContext;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Local Coordinate System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.worldwind.LocalCoordinateSystem#getQuaternion <em>Quaternion</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extension.space.worldwind.WorldwindPackage#getLocalCoordinateSystem()
 * @model
 * @generated
 */
public interface LocalCoordinateSystem extends TimeDependentRenderable {
	/**
	 * Returns the value of the '<em><b>Quaternion</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quaternion</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quaternion</em>' attribute.
	 * @see #setQuaternion(double[])
	 * @see org.thirdeye.extension.space.worldwind.WorldwindPackage#getLocalCoordinateSystem_Quaternion()
	 * @model dataType="org.thirdeye.core.dependencies.EDoubleArray" required="true"
	 * @generated
	 */
	double[] getQuaternion();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.worldwind.LocalCoordinateSystem#getQuaternion <em>Quaternion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quaternion</em>' attribute.
	 * @see #getQuaternion()
	 * @generated
	 */
	void setQuaternion(double[] value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void rightHandSide();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dcDataType="org.thirdeye.extension.space.worldwind.EWWDrawContext" dcRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='Quaternion quat = new Quaternion(getQuaternion()[0], getQuaternion()[1], getQuaternion()[2], getQuaternion()[3]);\t\t\n//System.out.println( \"quat: \" + quat.toString());\n\nVec4 point = dc.getGlobe().computePointFromPosition(Position.fromRadians(getLatLonAlt()[0], getLatLonAlt()[1]), getLatLonAlt()[2]);\nVec4 x = new Vec4(1000000.0,0.0,0.0).transformBy3(quat);\t\nVec4 y = new Vec4(0.0,1000000.0,0.0).transformBy3(quat);\nVec4 z = new Vec4(0.0,0.0,1000000.0).transformBy3(quat);\n\nVec4 vec[] = new Vec4[] {point.add3(x),point.add3(y),point.add3(z)};\n\nGL2 gl = dc.getGL().getGL2(); // GL initialization checks for GL2 compatibility.\ngl.glLineWidth(3.0f);\nfor(int i=0; i&lt;3;i++) {\n\t// Draw in axis\n\tgl.glColor3d(1, 0, 0); // COLOR \n\tgl.glBegin(GL.GL_LINES);\n\tgl.glVertex3d(vec[i].x, vec[i].y, vec[i].z);\n\tgl.glVertex3d(point.x, point.y, point.z);\n\tgl.glColor3d(0.0, 0.0, 1.0);\n\tgl.glEnd();\n}'"
	 * @generated
	 */
	void draw(DrawContext dc);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dcDataType="org.thirdeye.extension.space.worldwind.EWWDrawContext" dcRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='GL2 gl = dc.getGL().getGL2();\nsetMatrix( dc.getView().getModelviewMatrix());\ndouble[] matrixArray = new double[16];\ngetMatrix().toArray(matrixArray, 0, false);\ngl.glLoadMatrixd(matrixArray, 0);'"
	 * @generated
	 */
	void calcMatrix(DrawContext dc);

} // LocalCoordinateSystem
