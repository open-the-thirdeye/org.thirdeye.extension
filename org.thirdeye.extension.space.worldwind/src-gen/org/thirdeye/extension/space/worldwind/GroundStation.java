/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.worldwind;

import gov.nasa.worldwind.render.DrawContext;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ground Station</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.worldwind.GroundStation#getSatPos <em>Sat Pos</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.worldwind.GroundStation#getAzelra <em>Azelra</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extension.space.worldwind.WorldwindPackage#getGroundStation()
 * @model
 * @generated
 */
public interface GroundStation extends TimeDependentRenderable {
	/**
	 * Returns the value of the '<em><b>Sat Pos</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sat Pos</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sat Pos</em>' attribute list.
	 * @see org.thirdeye.extension.space.worldwind.WorldwindPackage#getGroundStation_SatPos()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 * @generated
	 */
	EList<Double> getSatPos();

	/**
	 * Returns the value of the '<em><b>Azelra</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Azelra</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Azelra</em>' attribute list.
	 * @see org.thirdeye.extension.space.worldwind.WorldwindPackage#getGroundStation_Azelra()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 * @generated
	 */
	EList<Double> getAzelra();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='Vec4 gsPoint = globe.computePointFromPosition(Position.fromRadians(getLatLonAlt()[0], getLatLonAlt()[1]), getLatLonAlt()[2]);\nVec4 satPoint = globe.computePointFromPosition(Position.fromRadians(getSatPos()[0], getSatPos()[1]), getSatPos()[2]);\n\nMatrix mat = globe.computeSurfaceOrientationAtPosition(new Position(Position.fromRadians(getLatLonAlt()[0], getLatLonAlt()[1]), getLatLonAlt()[2]));\nQuaternion quat = Quaternion.fromMatrix(mat);\n\nVec4 x = new Vec4(1.0,0.0,0.0);\t\nVec4 y = new Vec4(0.0,1.0,0.0);\nVec4 z = new Vec4(0.0,0.0,1.0);\nVec4 localx = x.transformBy3(quat);\nVec4 localy = y.transformBy3(quat);\nVec4 localz = z.transformBy3(quat);\n\nVec4 temp = satPoint.subtract3(gsPoint);\nVec4 projectz = temp.projectOnto3(localz);\nVec4 projectx = temp.projectOnto3(localx);\ntemp = temp.subtract3(projectz);\n\ndouble az =  localy.angleBetween3(temp).degrees;\n\nif( projectx.angleBetween3(localx).degrees &gt; 0.0 ) {\n\taz = 360.0 - az;\n}\nsetAzelra( new double[] { az, Angle.POS90.subtract(localz.angleBetween3(satPoint.subtract3(gsPoint))).degrees, satPoint.subtract3(gsPoint).getLength3() });\n'"
	 * @generated
	 */
	void rightHandSide();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dcDataType="org.thirdeye.extension.space.worldwind.EWWDrawContext" dcRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='globe =dc.getGlobe();\nVec4 gsPoint = globe.computePointFromPosition(Position.fromRadians(getLatLonAlt()[0], getLatLonAlt()[1]), getLatLonAlt()[2]);\nVec4 satPoint = globe.computePointFromPosition(Position.fromRadians(getSatPos()[0], getSatPos()[1]), getSatPos()[2]);\nGL2 gl = dc.getGL().getGL2(); // GL initialization checks for GL2 compatibility.\ngl.glLineWidth(3.0f);\n\nif(getAzelra()[1] &gt; 0.0) {\n\tgl.glColor3d(1, 0, 0); // COLOR \n\tgl.glBegin(GL.GL_LINES);\n\tgl.glVertex3d(gsPoint.x, gsPoint.y, gsPoint.z);\n\tgl.glVertex3d(satPoint.x, satPoint.y, satPoint.z);\n\tgl.glColor3d(0.0, 0.0, 1.0);\n\tgl.glEnd();\n}'"
	 * @generated
	 */
	void draw(DrawContext dc);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dcDataType="org.thirdeye.extension.space.worldwind.EWWDrawContext" dcRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='GL2 gl = dc.getGL().getGL2();\nsetMatrix( dc.getView().getModelviewMatrix());\ndouble[] matrixArray = new double[16];\ngetMatrix().toArray(matrixArray, 0, false);\ngl.glLoadMatrixd(matrixArray, 0);'"
	 * @generated
	 */
	void calcMatrix(DrawContext dc);

} // GroundStation
