/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.worldwind.impl;

import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Matrix;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Quaternion;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.globes.Globe;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.OrderedRenderable;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.thirdeye.extension.space.worldwind.GroundStation;
import org.thirdeye.extension.space.worldwind.TimeDependentRenderable;
import org.thirdeye.extension.space.worldwind.WorldwindPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ground Station</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.worldwind.impl.GroundStationImpl#getSatPos <em>Sat Pos</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.worldwind.impl.GroundStationImpl#getAzelra <em>Azelra</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GroundStationImpl extends TimeDependentRenderableImpl implements GroundStation {
	/**
	 * The cached value of the '{@link #getSatPos() <em>Sat Pos</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSatPos()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> satPos;

	/**
	 * The cached value of the '{@link #getAzelra() <em>Azelra</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAzelra()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> azelra;

	private Globe globe;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GroundStationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorldwindPackage.Literals.GROUND_STATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getSatPos() {
		if (satPos == null) {
			satPos = new EDataTypeEList<Double>(Double.class, this, WorldwindPackage.GROUND_STATION__SAT_POS);
		}
		return satPos;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getAzelra() {
		if (azelra == null) {
			azelra = new EDataTypeEList<Double>(Double.class, this, WorldwindPackage.GROUND_STATION__AZELRA);
		}
		return azelra;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void rightHandSide() {
		Vec4 gsPoint = globe.computePointFromPosition(Position.fromRadians(getLatLonAlt().get(0), getLatLonAlt().get(1)), getLatLonAlt().get(2));
		Vec4 satPoint = globe.computePointFromPosition(Position.fromRadians(getSatPos().get(0), getSatPos().get(1)), getSatPos().get(2));
		
		Matrix mat = globe.computeSurfaceOrientationAtPosition(new Position(Position.fromRadians(getLatLonAlt().get(0), getLatLonAlt().get(1)), getLatLonAlt().get(2)));
		Quaternion quat = Quaternion.fromMatrix(mat);
		
		Vec4 x = new Vec4(1.0,0.0,0.0);	
		Vec4 y = new Vec4(0.0,1.0,0.0);
		Vec4 z = new Vec4(0.0,0.0,1.0);
		Vec4 localx = x.transformBy3(quat);
		Vec4 localy = y.transformBy3(quat);
		Vec4 localz = z.transformBy3(quat);
		
		Vec4 temp = satPoint.subtract3(gsPoint);
		Vec4 projectz = temp.projectOnto3(localz);
		Vec4 projectx = temp.projectOnto3(localx);
		temp = temp.subtract3(projectz);
		
		double az =  localy.angleBetween3(temp).degrees;
		
		if( projectx.angleBetween3(localx).degrees > 0.0 ) {
			az = 360.0 - az;
		}
		getAzelra().set( 0, az );
		getAzelra().set( 1, Angle.POS90.subtract(localz.angleBetween3(satPoint.subtract3(gsPoint))).degrees );
		getAzelra().set( 2, satPoint.subtract3(gsPoint).getLength3() );
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void draw(final DrawContext dc) {
		globe =dc.getGlobe();
		Vec4 gsPoint = globe.computePointFromPosition(Position.fromRadians(getLatLonAlt().get(0), getLatLonAlt().get(1)), getLatLonAlt().get(2));
		Vec4 satPoint = globe.computePointFromPosition(Position.fromRadians(getSatPos().get(0), getSatPos().get(1)), getSatPos().get(2));
		GL2 gl = dc.getGL().getGL2(); // GL initialization checks for GL2 compatibility.
		gl.glLineWidth(3.0f);
		
		if(getAzelra().get(1) > 0.0) {
			gl.glColor3d(1, 0, 0); // COLOR 
			gl.glBegin(GL.GL_LINES);
			gl.glVertex3d(gsPoint.x, gsPoint.y, gsPoint.z);
			gl.glVertex3d(satPoint.x, satPoint.y, satPoint.z);
			gl.glColor3d(0.0, 0.0, 1.0);
			gl.glEnd();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void calcMatrix(final DrawContext dc) {
		GL2 gl = dc.getGL().getGL2();
		setMatrix( dc.getView().getModelviewMatrix());
		double[] matrixArray = new double[16];
		getMatrix().toArray(matrixArray, 0, false);
		gl.glLoadMatrixd(matrixArray, 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorldwindPackage.GROUND_STATION__SAT_POS:
				return getSatPos();
			case WorldwindPackage.GROUND_STATION__AZELRA:
				return getAzelra();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorldwindPackage.GROUND_STATION__SAT_POS:
				getSatPos().clear();
				getSatPos().addAll((Collection<? extends Double>)newValue);
				return;
			case WorldwindPackage.GROUND_STATION__AZELRA:
				getAzelra().clear();
				getAzelra().addAll((Collection<? extends Double>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorldwindPackage.GROUND_STATION__SAT_POS:
				getSatPos().clear();
				return;
			case WorldwindPackage.GROUND_STATION__AZELRA:
				getAzelra().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorldwindPackage.GROUND_STATION__SAT_POS:
				return satPos != null && !satPos.isEmpty();
			case WorldwindPackage.GROUND_STATION__AZELRA:
				return azelra != null && !azelra.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == OrderedRenderable.class) {
			switch (baseOperationID) {
				case WorldwindPackage.EORDERED_RENDERABLE___DRAW__DRAWCONTEXT: return WorldwindPackage.GROUND_STATION___DRAW__DRAWCONTEXT;
				case WorldwindPackage.EORDERED_RENDERABLE___CALC_MATRIX__DRAWCONTEXT: return WorldwindPackage.GROUND_STATION___CALC_MATRIX__DRAWCONTEXT;
				default: return super.eDerivedOperationID(baseOperationID, baseClass);
			}
		}
		if (baseClass == TimeDependentRenderable.class) {
			switch (baseOperationID) {
				case WorldwindPackage.TIME_DEPENDENT_RENDERABLE___CALC_MATRIX__DRAWCONTEXT: return WorldwindPackage.GROUND_STATION___CALC_MATRIX__DRAWCONTEXT;
				case WorldwindPackage.TIME_DEPENDENT_RENDERABLE___DRAW__DRAWCONTEXT: return WorldwindPackage.GROUND_STATION___DRAW__DRAWCONTEXT;
				default: return super.eDerivedOperationID(baseOperationID, baseClass);
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case WorldwindPackage.GROUND_STATION___RIGHT_HAND_SIDE:
				rightHandSide();
				return null;
			case WorldwindPackage.GROUND_STATION___DRAW__DRAWCONTEXT:
				draw((DrawContext)arguments.get(0));
				return null;
			case WorldwindPackage.GROUND_STATION___CALC_MATRIX__DRAWCONTEXT:
				calcMatrix((DrawContext)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (satPos: ");
		result.append(satPos);
		result.append(", azelra: ");
		result.append(azelra);
		result.append(')');
		return result.toString();
	}

	@Override
	public void calc() {
		// TODO Auto-generated method stub
		
	}

} //GroundStationImpl
