/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.worldwind.impl;

import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Quaternion;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.OrderedRenderable;

import java.lang.reflect.InvocationTargetException;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.thirdeye.extension.space.worldwind.LocalCoordinateSystem;
import org.thirdeye.extension.space.worldwind.TimeDependentRenderable;
import org.thirdeye.extension.space.worldwind.WorldwindPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Local Coordinate System</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.worldwind.impl.LocalCoordinateSystemImpl#getQuaternion <em>Quaternion</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LocalCoordinateSystemImpl extends TimeDependentRenderableImpl implements LocalCoordinateSystem {
	/**
	 * The default value of the '{@link #getQuaternion() <em>Quaternion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuaternion()
	 * @generated
	 * @ordered
	 */
	protected static final double[] QUATERNION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQuaternion() <em>Quaternion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuaternion()
	 * @generated
	 * @ordered
	 */
	protected double[] quaternion = QUATERNION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LocalCoordinateSystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorldwindPackage.Literals.LOCAL_COORDINATE_SYSTEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double[] getQuaternion() {
		return quaternion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuaternion(double[] newQuaternion) {
		double[] oldQuaternion = quaternion;
		quaternion = newQuaternion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldwindPackage.LOCAL_COORDINATE_SYSTEM__QUATERNION, oldQuaternion, quaternion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void rightHandSide() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void draw(final DrawContext dc) {
		Quaternion quat = new Quaternion(getQuaternion()[0], getQuaternion()[1], getQuaternion()[2], getQuaternion()[3]);		
		//System.out.println( "quat: " + quat.toString());
		
		Vec4 point = dc.getGlobe().computePointFromPosition(Position.fromRadians(getLatLonAlt().get(0), getLatLonAlt().get(1)), getLatLonAlt().get(2));
		Vec4 x = new Vec4(1000000.0,0.0,0.0).transformBy3(quat);	
		Vec4 y = new Vec4(0.0,1000000.0,0.0).transformBy3(quat);
		Vec4 z = new Vec4(0.0,0.0,1000000.0).transformBy3(quat);
		
		Vec4 vec[] = new Vec4[] {point.add3(x),point.add3(y),point.add3(z)};
		
		GL2 gl = dc.getGL().getGL2(); // GL initialization checks for GL2 compatibility.
		gl.glLineWidth(3.0f);
		for(int i=0; i<3;i++) {
			// Draw in axis
			gl.glColor3d(1, 0, 0); // COLOR 
			gl.glBegin(GL.GL_LINES);
			gl.glVertex3d(vec[i].x, vec[i].y, vec[i].z);
			gl.glVertex3d(point.x, point.y, point.z);
			gl.glColor3d(0.0, 0.0, 1.0);
			gl.glEnd();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void calcMatrix(final DrawContext dc) {
		GL2 gl = dc.getGL().getGL2();
		setMatrix( dc.getView().getModelviewMatrix());
		double[] matrixArray = new double[16];
		getMatrix().toArray(matrixArray, 0, false);
		gl.glLoadMatrixd(matrixArray, 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorldwindPackage.LOCAL_COORDINATE_SYSTEM__QUATERNION:
				return getQuaternion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorldwindPackage.LOCAL_COORDINATE_SYSTEM__QUATERNION:
				setQuaternion((double[])newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorldwindPackage.LOCAL_COORDINATE_SYSTEM__QUATERNION:
				setQuaternion(QUATERNION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorldwindPackage.LOCAL_COORDINATE_SYSTEM__QUATERNION:
				return QUATERNION_EDEFAULT == null ? quaternion != null : !QUATERNION_EDEFAULT.equals(quaternion);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == OrderedRenderable.class) {
			switch (baseOperationID) {
				case WorldwindPackage.EORDERED_RENDERABLE___DRAW__DRAWCONTEXT: return WorldwindPackage.LOCAL_COORDINATE_SYSTEM___DRAW__DRAWCONTEXT;
				case WorldwindPackage.EORDERED_RENDERABLE___CALC_MATRIX__DRAWCONTEXT: return WorldwindPackage.LOCAL_COORDINATE_SYSTEM___CALC_MATRIX__DRAWCONTEXT;
				default: return super.eDerivedOperationID(baseOperationID, baseClass);
			}
		}
		if (baseClass == TimeDependentRenderable.class) {
			switch (baseOperationID) {
				case WorldwindPackage.TIME_DEPENDENT_RENDERABLE___CALC_MATRIX__DRAWCONTEXT: return WorldwindPackage.LOCAL_COORDINATE_SYSTEM___CALC_MATRIX__DRAWCONTEXT;
				case WorldwindPackage.TIME_DEPENDENT_RENDERABLE___DRAW__DRAWCONTEXT: return WorldwindPackage.LOCAL_COORDINATE_SYSTEM___DRAW__DRAWCONTEXT;
				default: return super.eDerivedOperationID(baseOperationID, baseClass);
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case WorldwindPackage.LOCAL_COORDINATE_SYSTEM___RIGHT_HAND_SIDE:
				rightHandSide();
				return null;
			case WorldwindPackage.LOCAL_COORDINATE_SYSTEM___DRAW__DRAWCONTEXT:
				draw((DrawContext)arguments.get(0));
				return null;
			case WorldwindPackage.LOCAL_COORDINATE_SYSTEM___CALC_MATRIX__DRAWCONTEXT:
				calcMatrix((DrawContext)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (quaternion: ");
		result.append(quaternion);
		result.append(')');
		return result.toString();
	}

	@Override
	public void calc() {
		// TODO Auto-generated method stub
		
	}

} //LocalCoordinateSystemImpl
