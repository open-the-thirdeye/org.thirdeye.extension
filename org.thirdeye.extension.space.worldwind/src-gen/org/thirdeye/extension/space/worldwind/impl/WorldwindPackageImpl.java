/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.worldwind.impl;

import gov.nasa.worldwind.geom.Extent;
import gov.nasa.worldwind.geom.Matrix;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;

import gov.nasa.worldwind.pick.PickSupport;

import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.OrderedRenderable;

import java.awt.Point;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.thirdeye.core.dependencies.DependenciesPackage;

import org.thirdeye.core.mesh.MeshPackage;
import org.thirdeye.extension.space.worldwind.Axis;
import org.thirdeye.extension.space.worldwind.Cube;
import org.thirdeye.extension.space.worldwind.GroundStation;
import org.thirdeye.extension.space.worldwind.GroundTrack;
import org.thirdeye.extension.space.worldwind.LocalCoordinateSystem;
import org.thirdeye.extension.space.worldwind.TimeDependentRenderable;
import org.thirdeye.extension.space.worldwind.WorldwindFactory;
import org.thirdeye.extension.space.worldwind.WorldwindPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorldwindPackageImpl extends EPackageImpl implements WorldwindPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eOrderedRenderableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cubeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass axisEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timeDependentRenderableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass groundTrackEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass groundStationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass localCoordinateSystemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType eawtPointEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType ewwExtentEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType ewwMatrixEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType ewwPositionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType ewwPickSupportEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType ewwDrawContextEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType ewwVec4EDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.thirdeye.extension.space.worldwind.WorldwindPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private WorldwindPackageImpl() {
		super(eNS_URI, WorldwindFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link WorldwindPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static WorldwindPackage init() {
		if (isInited) return (WorldwindPackage)EPackage.Registry.INSTANCE.getEPackage(WorldwindPackage.eNS_URI);

		// Obtain or create and register package
		WorldwindPackageImpl theWorldwindPackage = (WorldwindPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof WorldwindPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new WorldwindPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		DependenciesPackage.eINSTANCE.eClass();
		MeshPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theWorldwindPackage.createPackageContents();

		// Initialize created meta-data
		theWorldwindPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theWorldwindPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(WorldwindPackage.eNS_URI, theWorldwindPackage);
		return theWorldwindPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEOrderedRenderable() {
		return eOrderedRenderableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getEOrderedRenderable__Draw__DrawContext() {
		return eOrderedRenderableEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getEOrderedRenderable__CalcMatrix__DrawContext() {
		return eOrderedRenderableEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCube() {
		return cubeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCube__RightHandSide() {
		return cubeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCube__Draw__DrawContext() {
		return cubeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCube__CalcMatrix__DrawContext() {
		return cubeEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAxis() {
		return axisEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAxis_Color() {
		return (EAttribute)axisEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAxis__RightHandSide() {
		return axisEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAxis__Draw__DrawContext() {
		return axisEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAxis__CalcMatrix__DrawContext() {
		return axisEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimeDependentRenderable() {
		return timeDependentRenderableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeDependentRenderable_LatLonAlt() {
		return (EAttribute)timeDependentRenderableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeDependentRenderable_DistanceFromEye() {
		return (EAttribute)timeDependentRenderableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeDependentRenderable_RenderSize() {
		return (EAttribute)timeDependentRenderableEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeDependentRenderable_FrameTimestamp() {
		return (EAttribute)timeDependentRenderableEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeDependentRenderable_Extent() {
		return (EAttribute)timeDependentRenderableEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeDependentRenderable_Matrix() {
		return (EAttribute)timeDependentRenderableEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTimeDependentRenderable__Pick__DrawContext_Point() {
		return timeDependentRenderableEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTimeDependentRenderable__Render__DrawContext() {
		return timeDependentRenderableEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTimeDependentRenderable__DrawOrderedRenderable__DrawContext_PickSupport() {
		return timeDependentRenderableEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTimeDependentRenderable__BeginDrawing__DrawContext() {
		return timeDependentRenderableEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTimeDependentRenderable__EndDrawing__DrawContext() {
		return timeDependentRenderableEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTimeDependentRenderable__IntersectsFrustum__DrawContext() {
		return timeDependentRenderableEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTimeDependentRenderable__MakeOrderedRenderable__DrawContext() {
		return timeDependentRenderableEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTimeDependentRenderable__CalcMatrix__DrawContext() {
		return timeDependentRenderableEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGroundTrack() {
		return groundTrackEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGroundTrack_Points() {
		return (EAttribute)groundTrackEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getGroundTrack__RightHandSide() {
		return groundTrackEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getGroundTrack__Draw__DrawContext() {
		return groundTrackEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getGroundTrack__CalcMatrix__DrawContext() {
		return groundTrackEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGroundStation() {
		return groundStationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGroundStation_SatPos() {
		return (EAttribute)groundStationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGroundStation_Azelra() {
		return (EAttribute)groundStationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getGroundStation__RightHandSide() {
		return groundStationEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getGroundStation__Draw__DrawContext() {
		return groundStationEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getGroundStation__CalcMatrix__DrawContext() {
		return groundStationEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLocalCoordinateSystem() {
		return localCoordinateSystemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLocalCoordinateSystem_Quaternion() {
		return (EAttribute)localCoordinateSystemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLocalCoordinateSystem__RightHandSide() {
		return localCoordinateSystemEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLocalCoordinateSystem__Draw__DrawContext() {
		return localCoordinateSystemEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLocalCoordinateSystem__CalcMatrix__DrawContext() {
		return localCoordinateSystemEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEAWTPoint() {
		return eawtPointEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEWWExtent() {
		return ewwExtentEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEWWMatrix() {
		return ewwMatrixEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEWWPosition() {
		return ewwPositionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEWWPickSupport() {
		return ewwPickSupportEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEWWDrawContext() {
		return ewwDrawContextEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEWWVec4() {
		return ewwVec4EDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorldwindFactory getWorldwindFactory() {
		return (WorldwindFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		eOrderedRenderableEClass = createEClass(EORDERED_RENDERABLE);
		createEOperation(eOrderedRenderableEClass, EORDERED_RENDERABLE___DRAW__DRAWCONTEXT);
		createEOperation(eOrderedRenderableEClass, EORDERED_RENDERABLE___CALC_MATRIX__DRAWCONTEXT);

		cubeEClass = createEClass(CUBE);
		createEOperation(cubeEClass, CUBE___RIGHT_HAND_SIDE);
		createEOperation(cubeEClass, CUBE___DRAW__DRAWCONTEXT);
		createEOperation(cubeEClass, CUBE___CALC_MATRIX__DRAWCONTEXT);

		axisEClass = createEClass(AXIS);
		createEAttribute(axisEClass, AXIS__COLOR);
		createEOperation(axisEClass, AXIS___RIGHT_HAND_SIDE);
		createEOperation(axisEClass, AXIS___DRAW__DRAWCONTEXT);
		createEOperation(axisEClass, AXIS___CALC_MATRIX__DRAWCONTEXT);

		timeDependentRenderableEClass = createEClass(TIME_DEPENDENT_RENDERABLE);
		createEAttribute(timeDependentRenderableEClass, TIME_DEPENDENT_RENDERABLE__LAT_LON_ALT);
		createEAttribute(timeDependentRenderableEClass, TIME_DEPENDENT_RENDERABLE__DISTANCE_FROM_EYE);
		createEAttribute(timeDependentRenderableEClass, TIME_DEPENDENT_RENDERABLE__RENDER_SIZE);
		createEAttribute(timeDependentRenderableEClass, TIME_DEPENDENT_RENDERABLE__FRAME_TIMESTAMP);
		createEAttribute(timeDependentRenderableEClass, TIME_DEPENDENT_RENDERABLE__EXTENT);
		createEAttribute(timeDependentRenderableEClass, TIME_DEPENDENT_RENDERABLE__MATRIX);
		createEOperation(timeDependentRenderableEClass, TIME_DEPENDENT_RENDERABLE___PICK__DRAWCONTEXT_POINT);
		createEOperation(timeDependentRenderableEClass, TIME_DEPENDENT_RENDERABLE___RENDER__DRAWCONTEXT);
		createEOperation(timeDependentRenderableEClass, TIME_DEPENDENT_RENDERABLE___DRAW_ORDERED_RENDERABLE__DRAWCONTEXT_PICKSUPPORT);
		createEOperation(timeDependentRenderableEClass, TIME_DEPENDENT_RENDERABLE___BEGIN_DRAWING__DRAWCONTEXT);
		createEOperation(timeDependentRenderableEClass, TIME_DEPENDENT_RENDERABLE___END_DRAWING__DRAWCONTEXT);
		createEOperation(timeDependentRenderableEClass, TIME_DEPENDENT_RENDERABLE___INTERSECTS_FRUSTUM__DRAWCONTEXT);
		createEOperation(timeDependentRenderableEClass, TIME_DEPENDENT_RENDERABLE___MAKE_ORDERED_RENDERABLE__DRAWCONTEXT);
		createEOperation(timeDependentRenderableEClass, TIME_DEPENDENT_RENDERABLE___CALC_MATRIX__DRAWCONTEXT);

		groundTrackEClass = createEClass(GROUND_TRACK);
		createEAttribute(groundTrackEClass, GROUND_TRACK__POINTS);
		createEOperation(groundTrackEClass, GROUND_TRACK___RIGHT_HAND_SIDE);
		createEOperation(groundTrackEClass, GROUND_TRACK___DRAW__DRAWCONTEXT);
		createEOperation(groundTrackEClass, GROUND_TRACK___CALC_MATRIX__DRAWCONTEXT);

		groundStationEClass = createEClass(GROUND_STATION);
		createEAttribute(groundStationEClass, GROUND_STATION__SAT_POS);
		createEAttribute(groundStationEClass, GROUND_STATION__AZELRA);
		createEOperation(groundStationEClass, GROUND_STATION___RIGHT_HAND_SIDE);
		createEOperation(groundStationEClass, GROUND_STATION___DRAW__DRAWCONTEXT);
		createEOperation(groundStationEClass, GROUND_STATION___CALC_MATRIX__DRAWCONTEXT);

		localCoordinateSystemEClass = createEClass(LOCAL_COORDINATE_SYSTEM);
		createEAttribute(localCoordinateSystemEClass, LOCAL_COORDINATE_SYSTEM__QUATERNION);
		createEOperation(localCoordinateSystemEClass, LOCAL_COORDINATE_SYSTEM___RIGHT_HAND_SIDE);
		createEOperation(localCoordinateSystemEClass, LOCAL_COORDINATE_SYSTEM___DRAW__DRAWCONTEXT);
		createEOperation(localCoordinateSystemEClass, LOCAL_COORDINATE_SYSTEM___CALC_MATRIX__DRAWCONTEXT);

		// Create data types
		eawtPointEDataType = createEDataType(EAWT_POINT);
		ewwExtentEDataType = createEDataType(EWW_EXTENT);
		ewwMatrixEDataType = createEDataType(EWW_MATRIX);
		ewwPositionEDataType = createEDataType(EWW_POSITION);
		ewwPickSupportEDataType = createEDataType(EWW_PICK_SUPPORT);
		ewwDrawContextEDataType = createEDataType(EWW_DRAW_CONTEXT);
		ewwVec4EDataType = createEDataType(EWW_VEC4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		MeshPackage theMeshPackage = (MeshPackage)EPackage.Registry.INSTANCE.getEPackage(MeshPackage.eNS_URI);
		DependenciesPackage theDependenciesPackage = (DependenciesPackage)EPackage.Registry.INSTANCE.getEPackage(DependenciesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		cubeEClass.getESuperTypes().add(this.getTimeDependentRenderable());
		axisEClass.getESuperTypes().add(this.getTimeDependentRenderable());
		timeDependentRenderableEClass.getESuperTypes().add(theMeshPackage.getModel());
		timeDependentRenderableEClass.getESuperTypes().add(this.getEOrderedRenderable());
		groundTrackEClass.getESuperTypes().add(this.getTimeDependentRenderable());
		groundStationEClass.getESuperTypes().add(this.getTimeDependentRenderable());
		localCoordinateSystemEClass.getESuperTypes().add(this.getTimeDependentRenderable());

		// Initialize classes, features, and operations; add parameters
		initEClass(eOrderedRenderableEClass, OrderedRenderable.class, "EOrderedRenderable", IS_ABSTRACT, IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getEOrderedRenderable__Draw__DrawContext(), null, "draw", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWDrawContext(), "dc", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getEOrderedRenderable__CalcMatrix__DrawContext(), null, "calcMatrix", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWDrawContext(), "dc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(cubeEClass, Cube.class, "Cube", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getCube__RightHandSide(), null, "rightHandSide", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getCube__Draw__DrawContext(), null, "draw", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWDrawContext(), "dc", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getCube__CalcMatrix__DrawContext(), null, "calcMatrix", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWDrawContext(), "dc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(axisEClass, Axis.class, "Axis", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAxis_Color(), ecorePackage.getEInt(), "color", null, 1, 1, Axis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getAxis__RightHandSide(), null, "rightHandSide", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAxis__Draw__DrawContext(), null, "draw", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWDrawContext(), "dc", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAxis__CalcMatrix__DrawContext(), null, "calcMatrix", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWDrawContext(), "dc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(timeDependentRenderableEClass, TimeDependentRenderable.class, "TimeDependentRenderable", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTimeDependentRenderable_LatLonAlt(), ecorePackage.getEDouble(), "latLonAlt", null, 3, 3, TimeDependentRenderable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTimeDependentRenderable_DistanceFromEye(), ecorePackage.getEDouble(), "distanceFromEye", null, 1, 1, TimeDependentRenderable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeDependentRenderable_RenderSize(), ecorePackage.getEDouble(), "renderSize", null, 1, 1, TimeDependentRenderable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeDependentRenderable_FrameTimestamp(), ecorePackage.getELong(), "frameTimestamp", "-1", 1, 1, TimeDependentRenderable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeDependentRenderable_Extent(), this.getEWWExtent(), "extent", null, 1, 1, TimeDependentRenderable.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimeDependentRenderable_Matrix(), this.getEWWMatrix(), "matrix", null, 1, 1, TimeDependentRenderable.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getTimeDependentRenderable__Pick__DrawContext_Point(), null, "pick", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWDrawContext(), "dc", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEAWTPoint(), "pickPoint", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getTimeDependentRenderable__Render__DrawContext(), null, "render", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWDrawContext(), "dc", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getTimeDependentRenderable__DrawOrderedRenderable__DrawContext_PickSupport(), null, "drawOrderedRenderable", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWDrawContext(), "dc", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWPickSupport(), "pickCandidates", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getTimeDependentRenderable__BeginDrawing__DrawContext(), null, "beginDrawing", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWDrawContext(), "dc", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getTimeDependentRenderable__EndDrawing__DrawContext(), null, "endDrawing", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWDrawContext(), "dc", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getTimeDependentRenderable__IntersectsFrustum__DrawContext(), ecorePackage.getEBoolean(), "intersectsFrustum", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWDrawContext(), "dc", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getTimeDependentRenderable__MakeOrderedRenderable__DrawContext(), null, "makeOrderedRenderable", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWDrawContext(), "dc", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getTimeDependentRenderable__CalcMatrix__DrawContext(), null, "calcMatrix", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWDrawContext(), "dc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(groundTrackEClass, GroundTrack.class, "GroundTrack", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGroundTrack_Points(), this.getEWWVec4(), "points", null, 0, -1, GroundTrack.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEOperation(getGroundTrack__RightHandSide(), null, "rightHandSide", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getGroundTrack__Draw__DrawContext(), null, "draw", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWDrawContext(), "dc", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getGroundTrack__CalcMatrix__DrawContext(), null, "calcMatrix", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWDrawContext(), "dc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(groundStationEClass, GroundStation.class, "GroundStation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGroundStation_SatPos(), ecorePackage.getEDouble(), "satPos", null, 3, 3, GroundStation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getGroundStation_Azelra(), ecorePackage.getEDouble(), "azelra", null, 3, 3, GroundStation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEOperation(getGroundStation__RightHandSide(), null, "rightHandSide", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getGroundStation__Draw__DrawContext(), null, "draw", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWDrawContext(), "dc", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getGroundStation__CalcMatrix__DrawContext(), null, "calcMatrix", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWDrawContext(), "dc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(localCoordinateSystemEClass, LocalCoordinateSystem.class, "LocalCoordinateSystem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLocalCoordinateSystem_Quaternion(), theDependenciesPackage.getEDoubleArray(), "quaternion", null, 1, 1, LocalCoordinateSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getLocalCoordinateSystem__RightHandSide(), null, "rightHandSide", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getLocalCoordinateSystem__Draw__DrawContext(), null, "draw", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWDrawContext(), "dc", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getLocalCoordinateSystem__CalcMatrix__DrawContext(), null, "calcMatrix", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEWWDrawContext(), "dc", 1, 1, IS_UNIQUE, IS_ORDERED);

		// Initialize data types
		initEDataType(eawtPointEDataType, Point.class, "EAWTPoint", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(ewwExtentEDataType, Extent.class, "EWWExtent", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(ewwMatrixEDataType, Matrix.class, "EWWMatrix", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(ewwPositionEDataType, Position.class, "EWWPosition", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(ewwPickSupportEDataType, PickSupport.class, "EWWPickSupport", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(ewwDrawContextEDataType, DrawContext.class, "EWWDrawContext", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(ewwVec4EDataType, Vec4.class, "EWWVec4", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// ThirdEye
		createThirdEyeAnnotations();
	}

	/**
	 * Initializes the annotations for <b>ThirdEye</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createThirdEyeAnnotations() {
		String source = "ThirdEye";	
		addAnnotation
		  (getTimeDependentRenderable_LatLonAlt(), 
		   source, 
		   new String[] {
			 "unit", "rad,rad,m"
		   });
	}

} //WorldwindPackageImpl
