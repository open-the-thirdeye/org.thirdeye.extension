/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.worldwind.impl;

import gov.nasa.worldwind.geom.Extent;
import gov.nasa.worldwind.geom.Matrix;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sphere;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.pick.PickSupport;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.OrderedRenderable;
import gov.nasa.worldwind.util.OGLUtil;

import java.awt.Color;
import java.awt.Point;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.thirdeye.core.mesh.impl.ModelImpl;
import org.thirdeye.extension.space.worldwind.TimeDependentRenderable;
import org.thirdeye.extension.space.worldwind.WorldwindPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Dependent Renderable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.worldwind.impl.TimeDependentRenderableImpl#getLatLonAlt <em>Lat Lon Alt</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.worldwind.impl.TimeDependentRenderableImpl#getDistanceFromEye <em>Distance From Eye</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.worldwind.impl.TimeDependentRenderableImpl#getRenderSize <em>Render Size</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.worldwind.impl.TimeDependentRenderableImpl#getFrameTimestamp <em>Frame Timestamp</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.worldwind.impl.TimeDependentRenderableImpl#getExtent <em>Extent</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.worldwind.impl.TimeDependentRenderableImpl#getMatrix <em>Matrix</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class TimeDependentRenderableImpl extends ModelImpl implements TimeDependentRenderable {
	/**
	 * The cached value of the '{@link #getLatLonAlt() <em>Lat Lon Alt</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLatLonAlt()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> latLonAlt;

	/**
	 * The default value of the '{@link #getDistanceFromEye() <em>Distance From Eye</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDistanceFromEye()
	 * @generated
	 * @ordered
	 */
	protected static final double DISTANCE_FROM_EYE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getDistanceFromEye() <em>Distance From Eye</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDistanceFromEye()
	 * @generated
	 * @ordered
	 */
	protected double distanceFromEye = DISTANCE_FROM_EYE_EDEFAULT;

	/**
	 * The default value of the '{@link #getRenderSize() <em>Render Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRenderSize()
	 * @generated
	 * @ordered
	 */
	protected static final double RENDER_SIZE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getRenderSize() <em>Render Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRenderSize()
	 * @generated
	 * @ordered
	 */
	protected double renderSize = RENDER_SIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getFrameTimestamp() <em>Frame Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrameTimestamp()
	 * @generated
	 * @ordered
	 */
	protected static final long FRAME_TIMESTAMP_EDEFAULT = -1L;

	/**
	 * The cached value of the '{@link #getFrameTimestamp() <em>Frame Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrameTimestamp()
	 * @generated
	 * @ordered
	 */
	protected long frameTimestamp = FRAME_TIMESTAMP_EDEFAULT;

	/**
	 * The default value of the '{@link #getExtent() <em>Extent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtent()
	 * @generated
	 * @ordered
	 */
	protected static final Extent EXTENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExtent() <em>Extent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtent()
	 * @generated
	 * @ordered
	 */
	protected Extent extent = EXTENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getMatrix() <em>Matrix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMatrix()
	 * @generated
	 * @ordered
	 */
	protected static final Matrix MATRIX_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMatrix() <em>Matrix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMatrix()
	 * @generated
	 * @ordered
	 */
	protected Matrix matrix = MATRIX_EDEFAULT;

	private PickSupport pickSupport = new PickSupport();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimeDependentRenderableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorldwindPackage.Literals.TIME_DEPENDENT_RENDERABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getLatLonAlt() {
		if (latLonAlt == null) {
			latLonAlt = new EDataTypeEList<Double>(Double.class, this, WorldwindPackage.TIME_DEPENDENT_RENDERABLE__LAT_LON_ALT);
		}
		return latLonAlt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getDistanceFromEye() {
		return distanceFromEye;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDistanceFromEye(double newDistanceFromEye) {
		double oldDistanceFromEye = distanceFromEye;
		distanceFromEye = newDistanceFromEye;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldwindPackage.TIME_DEPENDENT_RENDERABLE__DISTANCE_FROM_EYE, oldDistanceFromEye, distanceFromEye));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getRenderSize() {
		return renderSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRenderSize(double newRenderSize) {
		double oldRenderSize = renderSize;
		renderSize = newRenderSize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldwindPackage.TIME_DEPENDENT_RENDERABLE__RENDER_SIZE, oldRenderSize, renderSize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getFrameTimestamp() {
		return frameTimestamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFrameTimestamp(long newFrameTimestamp) {
		long oldFrameTimestamp = frameTimestamp;
		frameTimestamp = newFrameTimestamp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldwindPackage.TIME_DEPENDENT_RENDERABLE__FRAME_TIMESTAMP, oldFrameTimestamp, frameTimestamp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Extent getExtent() {
		return extent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExtent(Extent newExtent) {
		Extent oldExtent = extent;
		extent = newExtent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldwindPackage.TIME_DEPENDENT_RENDERABLE__EXTENT, oldExtent, extent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Matrix getMatrix() {
		return matrix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMatrix(Matrix newMatrix) {
		Matrix oldMatrix = matrix;
		matrix = newMatrix;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldwindPackage.TIME_DEPENDENT_RENDERABLE__MATRIX, oldMatrix, matrix));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void pick(final DrawContext dc, final Point pickPoint) {
		render(dc);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void render(final DrawContext dc) {
		if(getExtent() != null) {
		//	if((!intersectsFrustum(dc))||(dc.isSmall((Extent) getExtent(), 1))) {
		//		return;
		//	}
		}
		if(dc.isOrderedRenderingMode()) {
			drawOrderedRenderable(dc, pickSupport );
		} else {
			makeOrderedRenderable(dc);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public synchronized void drawOrderedRenderable(final DrawContext dc, final PickSupport pickCandidates) {
		GL2 gl = dc.getGL().getGL2();
		beginDrawing(dc);
		try {
			if(dc.isPickingMode()) {
				Color pickColor = dc.getUniquePickColor();
				pickCandidates.addPickableObject(pickColor.getRGB(), this, new Position(Position.fromRadians(getLatLonAlt().get(0), getLatLonAlt().get(1)), getLatLonAlt().get(2)));
				gl.glColor3ub((byte) pickColor.getRed(), (byte) pickColor.getGreen(), (byte) pickColor.getBlue());
			}
			gl.glScaled(getRenderSize(), getRenderSize(), getRenderSize());
			draw(dc);
		} finally {
			endDrawing(dc);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void beginDrawing(final DrawContext dc) {
		GL2 gl = dc.getGL().getGL2();
		int attrMask = GL2.GL_CURRENT_BIT | GL2.GL_COLOR_BUFFER_BIT;
		gl.glPushAttrib(attrMask);
		if(!dc.isPickingMode()) {
			dc.beginStandardLighting();
			gl.glEnable(GL.GL_BLEND);
			OGLUtil.applyBlending(gl, false);
			gl.glEnable(GL2.GL_NORMALIZE);
		}
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		calcMatrix(dc);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void endDrawing(final DrawContext dc) {
		GL2 gl = dc.getGL().getGL2();
		if (!dc.isPickingMode()) dc.endStandardLighting();
		gl.glPopAttrib();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean intersectsFrustum(final DrawContext dc) {
		//if(getExtent() == null)
		//	return true;
		//if (dc.isPickingMode())
		//	return dc.getPickFrustums().intersectsAny(getExtent());
		//return dc.getView().getFrustumInModelCoordinates().intersects(getExtent());*/
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public synchronized void makeOrderedRenderable(final DrawContext dc) {
		if(dc.getFrameTimeStamp() != getFrameTimestamp()) {
			Vec4 placePoint = dc.getGlobe().computePointFromPosition(new Position(Position.fromRadians(getLatLonAlt().get(0), getLatLonAlt().get(1)), getLatLonAlt().get(2)));
			setDistanceFromEye( dc.getView().getEyePoint().distanceTo3(placePoint) );
			setExtent( new Sphere(placePoint, Math.sqrt(3.0) * getRenderSize() / 2.0));
			setFrameTimestamp( dc.getFrameTimeStamp());
		}
		dc.addOrderedRenderable((OrderedRenderable) this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public synchronized void calcMatrix(final DrawContext dc) {
		GL2 gl = dc.getGL().getGL2();
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		//System.out.println("calcMatrix of " + getName() );
		//System.out.println("LatLonAlt size = " + getLatLonAlt().size() );
		setMatrix( dc.getGlobe().computeSurfaceOrientationAtPosition(new Position(Position.fromRadians(getLatLonAlt().get(0), getLatLonAlt().get(1)), getLatLonAlt().get(2))) );
		setMatrix( dc.getView().getModelviewMatrix().multiply(getMatrix()));
		double[] matrixArray = new double[16];
		getMatrix().toArray(matrixArray, 0, false);
		gl.glLoadMatrixd(matrixArray, 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void draw(DrawContext dc) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__LAT_LON_ALT:
				return getLatLonAlt();
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__DISTANCE_FROM_EYE:
				return getDistanceFromEye();
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__RENDER_SIZE:
				return getRenderSize();
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__FRAME_TIMESTAMP:
				return getFrameTimestamp();
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__EXTENT:
				return getExtent();
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__MATRIX:
				return getMatrix();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public synchronized void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__LAT_LON_ALT:
				getLatLonAlt().clear();
				getLatLonAlt().addAll((Collection<? extends Double>)newValue);
				return;
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__DISTANCE_FROM_EYE:
				setDistanceFromEye((Double)newValue);
				return;
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__RENDER_SIZE:
				setRenderSize((Double)newValue);
				return;
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__FRAME_TIMESTAMP:
				setFrameTimestamp((Long)newValue);
				return;
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__EXTENT:
				setExtent((Extent)newValue);
				return;
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__MATRIX:
				setMatrix((Matrix)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__LAT_LON_ALT:
				getLatLonAlt().clear();
				return;
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__DISTANCE_FROM_EYE:
				setDistanceFromEye(DISTANCE_FROM_EYE_EDEFAULT);
				return;
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__RENDER_SIZE:
				setRenderSize(RENDER_SIZE_EDEFAULT);
				return;
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__FRAME_TIMESTAMP:
				setFrameTimestamp(FRAME_TIMESTAMP_EDEFAULT);
				return;
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__EXTENT:
				setExtent(EXTENT_EDEFAULT);
				return;
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__MATRIX:
				setMatrix(MATRIX_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__LAT_LON_ALT:
				return latLonAlt != null && !latLonAlt.isEmpty();
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__DISTANCE_FROM_EYE:
				return distanceFromEye != DISTANCE_FROM_EYE_EDEFAULT;
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__RENDER_SIZE:
				return renderSize != RENDER_SIZE_EDEFAULT;
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__FRAME_TIMESTAMP:
				return frameTimestamp != FRAME_TIMESTAMP_EDEFAULT;
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__EXTENT:
				return EXTENT_EDEFAULT == null ? extent != null : !EXTENT_EDEFAULT.equals(extent);
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE__MATRIX:
				return MATRIX_EDEFAULT == null ? matrix != null : !MATRIX_EDEFAULT.equals(matrix);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == OrderedRenderable.class) {
			switch (baseOperationID) {
				case WorldwindPackage.EORDERED_RENDERABLE___DRAW__DRAWCONTEXT: return WorldwindPackage.TIME_DEPENDENT_RENDERABLE___DRAW__DRAWCONTEXT;
				case WorldwindPackage.EORDERED_RENDERABLE___CALC_MATRIX__DRAWCONTEXT: return WorldwindPackage.TIME_DEPENDENT_RENDERABLE___CALC_MATRIX__DRAWCONTEXT;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE___PICK__DRAWCONTEXT_POINT:
				pick((DrawContext)arguments.get(0), (Point)arguments.get(1));
				return null;
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE___RENDER__DRAWCONTEXT:
				render((DrawContext)arguments.get(0));
				return null;
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE___DRAW_ORDERED_RENDERABLE__DRAWCONTEXT_PICKSUPPORT:
				drawOrderedRenderable((DrawContext)arguments.get(0), (PickSupport)arguments.get(1));
				return null;
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE___BEGIN_DRAWING__DRAWCONTEXT:
				beginDrawing((DrawContext)arguments.get(0));
				return null;
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE___END_DRAWING__DRAWCONTEXT:
				endDrawing((DrawContext)arguments.get(0));
				return null;
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE___INTERSECTS_FRUSTUM__DRAWCONTEXT:
				return intersectsFrustum((DrawContext)arguments.get(0));
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE___MAKE_ORDERED_RENDERABLE__DRAWCONTEXT:
				makeOrderedRenderable((DrawContext)arguments.get(0));
				return null;
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE___CALC_MATRIX__DRAWCONTEXT:
				calcMatrix((DrawContext)arguments.get(0));
				return null;
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE___DRAW__DRAWCONTEXT:
				draw((DrawContext)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (latLonAlt: ");
		result.append(latLonAlt);
		result.append(", distanceFromEye: ");
		result.append(distanceFromEye);
		result.append(", renderSize: ");
		result.append(renderSize);
		result.append(", frameTimestamp: ");
		result.append(frameTimestamp);
		result.append(", extent: ");
		result.append(extent);
		result.append(", matrix: ");
		result.append(matrix);
		result.append(')');
		return result.toString();
	}

} //TimeDependentRenderableImpl
