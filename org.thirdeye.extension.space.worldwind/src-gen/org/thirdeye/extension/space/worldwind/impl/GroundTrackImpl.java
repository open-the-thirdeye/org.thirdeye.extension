/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.worldwind.impl;

import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.OrderedRenderable;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.thirdeye.extension.space.worldwind.GroundTrack;
import org.thirdeye.extension.space.worldwind.TimeDependentRenderable;
import org.thirdeye.extension.space.worldwind.WorldwindPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ground Track</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.worldwind.impl.GroundTrackImpl#getPoints <em>Points</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GroundTrackImpl extends TimeDependentRenderableImpl implements GroundTrack {
	/**
	 * The cached value of the '{@link #getPoints() <em>Points</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPoints()
	 * @generated
	 * @ordered
	 */
	protected EList<Vec4> points;
	private boolean init = false;
	private double[] start;
	private int i = 0;
	private Vec4 lastPoint = null;
	private EList<double[]> list = new BasicEList<double[]>();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GroundTrackImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorldwindPackage.Literals.GROUND_TRACK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Vec4> getPoints() {
		if (points == null) {
			points = new EDataTypeEList<Vec4>(Vec4.class, this, WorldwindPackage.GROUND_TRACK__POINTS);
		}
		return points;
	}
	
	@Override
	public void calc() {
		rightHandSide();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void rightHandSide() {
		//intentionally left blank
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void draw(final DrawContext dc) {
		 GL2 gl = dc.getGL().getGL2(); // GL initialization checks for GL2 compatibility.
		if( !init  ) {
			Vec4 point = dc.getGlobe().computePointFromPosition(Position.fromRadians(getLatLonAlt().get(0), getLatLonAlt().get(1)), 100000.0);
			start = new double[] {point.x,point.y,point.z};
			init = true;
		}
		i++;
		if( i == 5) {
			i = 0;
			if( lastPoint != null ) {
				Vec4 newPoint = dc.getGlobe().computePointFromPosition(Position.fromRadians(getLatLonAlt().get(0), getLatLonAlt().get(1)), 100000.0);
				if( !lastPoint.equals(newPoint)) {
					double[] dx = new double[] { lastPoint.x - newPoint.x,lastPoint.y - newPoint.y, lastPoint.z - newPoint.z};
					list .add(dx);
				}
			}
			lastPoint = dc.getGlobe().computePointFromPosition(Position.fromRadians(getLatLonAlt().get(0), getLatLonAlt().get(1)), 100000.0);
		}
		double[] vec = new double[] {0.0,0.0,0.0};
		vec[0] = vec[0] + start[0];
		vec[1] = vec[1] + start[1];
		vec[2] = vec[2] + start[2];
		for( double[] inc: list ) {
			gl.glBegin(GL.GL_LINES);
			gl.glVertex3d(vec[0], vec[1], vec[2]);
			vec[0]-=inc[0];
			vec[1]-=inc[1];
			vec[2]-=inc[2];
			gl.glVertex3d(vec[0], vec[1], vec[2]);
			gl.glColor3d(0.0, 0.0, 1.0);
			gl.glEnd();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void calcMatrix(final DrawContext dc) {
		GL2 gl = dc.getGL().getGL2();
		setMatrix( dc.getView().getModelviewMatrix());
		double[] matrixArray = new double[16];
		getMatrix().toArray(matrixArray, 0, false);
		gl.glLoadMatrixd(matrixArray, 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorldwindPackage.GROUND_TRACK__POINTS:
				return getPoints();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorldwindPackage.GROUND_TRACK__POINTS:
				getPoints().clear();
				getPoints().addAll((Collection<? extends Vec4>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorldwindPackage.GROUND_TRACK__POINTS:
				getPoints().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorldwindPackage.GROUND_TRACK__POINTS:
				return points != null && !points.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == OrderedRenderable.class) {
			switch (baseOperationID) {
				case WorldwindPackage.EORDERED_RENDERABLE___DRAW__DRAWCONTEXT: return WorldwindPackage.GROUND_TRACK___DRAW__DRAWCONTEXT;
				case WorldwindPackage.EORDERED_RENDERABLE___CALC_MATRIX__DRAWCONTEXT: return WorldwindPackage.GROUND_TRACK___CALC_MATRIX__DRAWCONTEXT;
				default: return super.eDerivedOperationID(baseOperationID, baseClass);
			}
		}
		if (baseClass == TimeDependentRenderable.class) {
			switch (baseOperationID) {
				case WorldwindPackage.TIME_DEPENDENT_RENDERABLE___CALC_MATRIX__DRAWCONTEXT: return WorldwindPackage.GROUND_TRACK___CALC_MATRIX__DRAWCONTEXT;
				case WorldwindPackage.TIME_DEPENDENT_RENDERABLE___DRAW__DRAWCONTEXT: return WorldwindPackage.GROUND_TRACK___DRAW__DRAWCONTEXT;
				default: return super.eDerivedOperationID(baseOperationID, baseClass);
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case WorldwindPackage.GROUND_TRACK___RIGHT_HAND_SIDE:
				rightHandSide();
				return null;
			case WorldwindPackage.GROUND_TRACK___DRAW__DRAWCONTEXT:
				draw((DrawContext)arguments.get(0));
				return null;
			case WorldwindPackage.GROUND_TRACK___CALC_MATRIX__DRAWCONTEXT:
				calcMatrix((DrawContext)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (points: ");
		result.append(points);
		result.append(')');
		return result.toString();
	}

} //GroundTrackImpl
