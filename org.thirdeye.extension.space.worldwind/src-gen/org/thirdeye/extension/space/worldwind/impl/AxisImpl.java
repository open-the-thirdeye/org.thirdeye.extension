/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.worldwind.impl;

import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.OrderedRenderable;

import java.lang.reflect.InvocationTargetException;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.thirdeye.extension.space.worldwind.Axis;
import org.thirdeye.extension.space.worldwind.TimeDependentRenderable;
import org.thirdeye.extension.space.worldwind.WorldwindPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Axis</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.worldwind.impl.AxisImpl#getColor <em>Color</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AxisImpl extends TimeDependentRenderableImpl implements Axis {
	/**
	 * The default value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected static final int COLOR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected int color = COLOR_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AxisImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorldwindPackage.Literals.AXIS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getColor() {
		return color;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setColor(int newColor) {
		int oldColor = color;
		color = newColor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorldwindPackage.AXIS__COLOR, oldColor, color));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void rightHandSide() {
		//intentionally left blank
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void draw(final DrawContext dc) {
		GL2 gl = dc.getGL().getGL2(); // GL initialization checks for GL2 compatibility.
		gl.glLineWidth(3.0f);
		// Draw in axis
		gl.glColor3d(1, 0, 0); // COLOR 
		gl.glBegin(GL.GL_LINES);
		gl.glVertex3d(400000, 0, 0);
		gl.glVertex3d(0, 0, 0);
		gl.glColor3d(0.0, 0.0, 1.0);
		gl.glEnd();
		
		gl.glColor3d(0, 1, 0); // COLOR 
		gl.glBegin(GL.GL_LINES);
		gl.glVertex3d(0, 400000, 0);
		gl.glVertex3d(0, 0, 0);
		gl.glColor3d(0.0, 0.0, 1.0);
		gl.glEnd();
		
		gl.glColor3d(0, 0, 1); // COLOR 
		gl.glBegin(GL.GL_LINES);
		gl.glVertex3d(0, 0, 400000);
		gl.glVertex3d(0, 0, 0);
		gl.glColor3d(0.0, 0.0, 1.0);
		gl.glEnd();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void calcMatrix(final DrawContext dc) {
		GL2 gl = dc.getGL().getGL2();
		setMatrix( dc.getView().getModelviewMatrix());
		double[] matrixArray = new double[16];
		getMatrix().toArray(matrixArray, 0, false);
		gl.glLoadMatrixd(matrixArray, 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorldwindPackage.AXIS__COLOR:
				return getColor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorldwindPackage.AXIS__COLOR:
				setColor((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorldwindPackage.AXIS__COLOR:
				setColor(COLOR_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorldwindPackage.AXIS__COLOR:
				return color != COLOR_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == OrderedRenderable.class) {
			switch (baseOperationID) {
				case WorldwindPackage.EORDERED_RENDERABLE___DRAW__DRAWCONTEXT: return WorldwindPackage.AXIS___DRAW__DRAWCONTEXT;
				case WorldwindPackage.EORDERED_RENDERABLE___CALC_MATRIX__DRAWCONTEXT: return WorldwindPackage.AXIS___CALC_MATRIX__DRAWCONTEXT;
				default: return super.eDerivedOperationID(baseOperationID, baseClass);
			}
		}
		if (baseClass == TimeDependentRenderable.class) {
			switch (baseOperationID) {
				case WorldwindPackage.TIME_DEPENDENT_RENDERABLE___CALC_MATRIX__DRAWCONTEXT: return WorldwindPackage.AXIS___CALC_MATRIX__DRAWCONTEXT;
				case WorldwindPackage.TIME_DEPENDENT_RENDERABLE___DRAW__DRAWCONTEXT: return WorldwindPackage.AXIS___DRAW__DRAWCONTEXT;
				default: return super.eDerivedOperationID(baseOperationID, baseClass);
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case WorldwindPackage.AXIS___RIGHT_HAND_SIDE:
				rightHandSide();
				return null;
			case WorldwindPackage.AXIS___DRAW__DRAWCONTEXT:
				draw((DrawContext)arguments.get(0));
				return null;
			case WorldwindPackage.AXIS___CALC_MATRIX__DRAWCONTEXT:
				calcMatrix((DrawContext)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (color: ");
		result.append(color);
		result.append(')');
		return result.toString();
	}

	@Override
	public void calc() {
		// TODO Auto-generated method stub
		
	}

} //AxisImpl
