/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.worldwind;

import gov.nasa.worldwind.render.DrawContext;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Axis</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.worldwind.Axis#getColor <em>Color</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extension.space.worldwind.WorldwindPackage#getAxis()
 * @model
 * @generated
 */
public interface Axis extends TimeDependentRenderable {
	/**
	 * Returns the value of the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Color</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Color</em>' attribute.
	 * @see #setColor(int)
	 * @see org.thirdeye.extension.space.worldwind.WorldwindPackage#getAxis_Color()
	 * @model required="true"
	 * @generated
	 */
	int getColor();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.worldwind.Axis#getColor <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Color</em>' attribute.
	 * @see #getColor()
	 * @generated
	 */
	void setColor(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='//intentionally left blank'"
	 * @generated
	 */
	void rightHandSide();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dcDataType="org.thirdeye.extension.space.worldwind.EWWDrawContext" dcRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='GL2 gl = dc.getGL().getGL2(); // GL initialization checks for GL2 compatibility.\ngl.glLineWidth(3.0f);\n// Draw in axis\ngl.glColor3d(1, 0, 0); // COLOR \ngl.glBegin(GL.GL_LINES);\ngl.glVertex3d(400000, 0, 0);\ngl.glVertex3d(0, 0, 0);\ngl.glColor3d(0.0, 0.0, 1.0);\ngl.glEnd();\n\ngl.glColor3d(0, 1, 0); // COLOR \ngl.glBegin(GL.GL_LINES);\ngl.glVertex3d(0, 400000, 0);\ngl.glVertex3d(0, 0, 0);\ngl.glColor3d(0.0, 0.0, 1.0);\ngl.glEnd();\n\ngl.glColor3d(0, 0, 1); // COLOR \ngl.glBegin(GL.GL_LINES);\ngl.glVertex3d(0, 0, 400000);\ngl.glVertex3d(0, 0, 0);\ngl.glColor3d(0.0, 0.0, 1.0);\ngl.glEnd();'"
	 * @generated
	 */
	void draw(DrawContext dc);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dcDataType="org.thirdeye.extension.space.worldwind.EWWDrawContext" dcRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='GL2 gl = dc.getGL().getGL2();\nsetMatrix( dc.getView().getModelviewMatrix());\ndouble[] matrixArray = new double[16];\ngetMatrix().toArray(matrixArray, 0, false);\ngl.glLoadMatrixd(matrixArray, 0);'"
	 * @generated
	 */
	void calcMatrix(DrawContext dc);

} // Axis
