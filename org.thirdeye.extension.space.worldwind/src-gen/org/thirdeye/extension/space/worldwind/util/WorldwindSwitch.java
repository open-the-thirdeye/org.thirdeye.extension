/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.worldwind.util;

import gov.nasa.worldwind.render.OrderedRenderable;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.thirdeye.core.mesh.Model;
import org.thirdeye.core.mesh.Node;
import org.thirdeye.extension.space.worldwind.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.thirdeye.extension.space.worldwind.WorldwindPackage
 * @generated
 */
public class WorldwindSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static WorldwindPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorldwindSwitch() {
		if (modelPackage == null) {
			modelPackage = WorldwindPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case WorldwindPackage.EORDERED_RENDERABLE: {
				OrderedRenderable eOrderedRenderable = (OrderedRenderable)theEObject;
				T result = caseEOrderedRenderable(eOrderedRenderable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorldwindPackage.CUBE: {
				Cube cube = (Cube)theEObject;
				T result = caseCube(cube);
				if (result == null) result = caseTimeDependentRenderable(cube);
				if (result == null) result = caseModel(cube);
				if (result == null) result = caseEOrderedRenderable(cube);
				if (result == null) result = caseNode(cube);
				if (result == null) result = caseERunnable(cube);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorldwindPackage.AXIS: {
				Axis axis = (Axis)theEObject;
				T result = caseAxis(axis);
				if (result == null) result = caseTimeDependentRenderable(axis);
				if (result == null) result = caseModel(axis);
				if (result == null) result = caseEOrderedRenderable(axis);
				if (result == null) result = caseNode(axis);
				if (result == null) result = caseERunnable(axis);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorldwindPackage.TIME_DEPENDENT_RENDERABLE: {
				TimeDependentRenderable timeDependentRenderable = (TimeDependentRenderable)theEObject;
				T result = caseTimeDependentRenderable(timeDependentRenderable);
				if (result == null) result = caseModel(timeDependentRenderable);
				if (result == null) result = caseEOrderedRenderable(timeDependentRenderable);
				if (result == null) result = caseNode(timeDependentRenderable);
				if (result == null) result = caseERunnable(timeDependentRenderable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorldwindPackage.GROUND_TRACK: {
				GroundTrack groundTrack = (GroundTrack)theEObject;
				T result = caseGroundTrack(groundTrack);
				if (result == null) result = caseTimeDependentRenderable(groundTrack);
				if (result == null) result = caseModel(groundTrack);
				if (result == null) result = caseEOrderedRenderable(groundTrack);
				if (result == null) result = caseNode(groundTrack);
				if (result == null) result = caseERunnable(groundTrack);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorldwindPackage.GROUND_STATION: {
				GroundStation groundStation = (GroundStation)theEObject;
				T result = caseGroundStation(groundStation);
				if (result == null) result = caseTimeDependentRenderable(groundStation);
				if (result == null) result = caseModel(groundStation);
				if (result == null) result = caseEOrderedRenderable(groundStation);
				if (result == null) result = caseNode(groundStation);
				if (result == null) result = caseERunnable(groundStation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WorldwindPackage.LOCAL_COORDINATE_SYSTEM: {
				LocalCoordinateSystem localCoordinateSystem = (LocalCoordinateSystem)theEObject;
				T result = caseLocalCoordinateSystem(localCoordinateSystem);
				if (result == null) result = caseTimeDependentRenderable(localCoordinateSystem);
				if (result == null) result = caseModel(localCoordinateSystem);
				if (result == null) result = caseEOrderedRenderable(localCoordinateSystem);
				if (result == null) result = caseNode(localCoordinateSystem);
				if (result == null) result = caseERunnable(localCoordinateSystem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EOrdered Renderable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EOrdered Renderable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEOrderedRenderable(OrderedRenderable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cube</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cube</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCube(Cube object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Axis</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Axis</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAxis(Axis object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Time Dependent Renderable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Time Dependent Renderable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimeDependentRenderable(TimeDependentRenderable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ground Track</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ground Track</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGroundTrack(GroundTrack object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ground Station</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ground Station</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGroundStation(GroundStation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Local Coordinate System</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Local Coordinate System</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLocalCoordinateSystem(LocalCoordinateSystem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ERunnable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ERunnable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseERunnable(Runnable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNode(Node object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModel(Model object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //WorldwindSwitch
