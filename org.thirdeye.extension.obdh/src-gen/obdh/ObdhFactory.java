/**
 */
package obdh;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see obdh.ObdhPackage
 * @generated
 */
public interface ObdhFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ObdhFactory eINSTANCE = obdh.impl.ObdhFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>From OBDH Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>From OBDH Model</em>'.
	 * @generated
	 */
	FromOBDHModel createFromOBDHModel();

	/**
	 * Returns a new object of class '<em>To OBDH Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>To OBDH Model</em>'.
	 * @generated
	 */
	ToOBDHModel createToOBDHModel();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ObdhPackage getObdhPackage();

} //ObdhFactory
