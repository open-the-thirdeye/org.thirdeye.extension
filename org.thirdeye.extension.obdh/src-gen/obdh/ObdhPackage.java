/**
 */
package obdh;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import org.thirdeye.core.mesh.MeshPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see obdh.ObdhFactory
 * @model kind="package"
 * @generated
 */
public interface ObdhPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "obdh";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://obdh/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "obdh";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ObdhPackage eINSTANCE = obdh.impl.ObdhPackageImpl.init();

	/**
	 * The meta object id for the '{@link obdh.impl.FromOBDHModelImpl <em>From OBDH Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see obdh.impl.FromOBDHModelImpl
	 * @see obdh.impl.ObdhPackageImpl#getFromOBDHModel()
	 * @generated
	 */
	int FROM_OBDH_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_OBDH_MODEL__VARIABLES = MeshPackage.MODEL__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_OBDH_MODEL__NAME = MeshPackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Output</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_OBDH_MODEL__OUTPUT = MeshPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_OBDH_MODEL__PORT = MeshPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Hostname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_OBDH_MODEL__HOSTNAME = MeshPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>From OBDH Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_OBDH_MODEL_FEATURE_COUNT = MeshPackage.MODEL_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_OBDH_MODEL___RUN = MeshPackage.MODEL___RUN;

	/**
	 * The operation id for the '<em>Logic</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_OBDH_MODEL___LOGIC = MeshPackage.MODEL___LOGIC;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_OBDH_MODEL___CALC = MeshPackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>From OBDH Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_OBDH_MODEL_OPERATION_COUNT = MeshPackage.MODEL_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link obdh.impl.ToOBDHModelImpl <em>To OBDH Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see obdh.impl.ToOBDHModelImpl
	 * @see obdh.impl.ObdhPackageImpl#getToOBDHModel()
	 * @generated
	 */
	int TO_OBDH_MODEL = 1;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_OBDH_MODEL__VARIABLES = MeshPackage.MODEL__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_OBDH_MODEL__NAME = MeshPackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Input</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_OBDH_MODEL__INPUT = MeshPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_OBDH_MODEL__PORT = MeshPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Hostname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_OBDH_MODEL__HOSTNAME = MeshPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>To OBDH Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_OBDH_MODEL_FEATURE_COUNT = MeshPackage.MODEL_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_OBDH_MODEL___RUN = MeshPackage.MODEL___RUN;

	/**
	 * The operation id for the '<em>Logic</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_OBDH_MODEL___LOGIC = MeshPackage.MODEL___LOGIC;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_OBDH_MODEL___CALC = MeshPackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>To OBDH Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_OBDH_MODEL_OPERATION_COUNT = MeshPackage.MODEL_OPERATION_COUNT + 1;

	/**
	 * Returns the meta object for class '{@link obdh.FromOBDHModel <em>From OBDH Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>From OBDH Model</em>'.
	 * @see obdh.FromOBDHModel
	 * @generated
	 */
	EClass getFromOBDHModel();

	/**
	 * Returns the meta object for the attribute list '{@link obdh.FromOBDHModel#getOutput <em>Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Output</em>'.
	 * @see obdh.FromOBDHModel#getOutput()
	 * @see #getFromOBDHModel()
	 * @generated
	 */
	EAttribute getFromOBDHModel_Output();

	/**
	 * Returns the meta object for the attribute '{@link obdh.FromOBDHModel#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Port</em>'.
	 * @see obdh.FromOBDHModel#getPort()
	 * @see #getFromOBDHModel()
	 * @generated
	 */
	EAttribute getFromOBDHModel_Port();

	/**
	 * Returns the meta object for the attribute '{@link obdh.FromOBDHModel#getHostname <em>Hostname</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hostname</em>'.
	 * @see obdh.FromOBDHModel#getHostname()
	 * @see #getFromOBDHModel()
	 * @generated
	 */
	EAttribute getFromOBDHModel_Hostname();

	/**
	 * Returns the meta object for the '{@link obdh.FromOBDHModel#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see obdh.FromOBDHModel#calc()
	 * @generated
	 */
	EOperation getFromOBDHModel__Calc();

	/**
	 * Returns the meta object for class '{@link obdh.ToOBDHModel <em>To OBDH Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>To OBDH Model</em>'.
	 * @see obdh.ToOBDHModel
	 * @generated
	 */
	EClass getToOBDHModel();

	/**
	 * Returns the meta object for the attribute list '{@link obdh.ToOBDHModel#getInput <em>Input</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Input</em>'.
	 * @see obdh.ToOBDHModel#getInput()
	 * @see #getToOBDHModel()
	 * @generated
	 */
	EAttribute getToOBDHModel_Input();

	/**
	 * Returns the meta object for the attribute '{@link obdh.ToOBDHModel#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Port</em>'.
	 * @see obdh.ToOBDHModel#getPort()
	 * @see #getToOBDHModel()
	 * @generated
	 */
	EAttribute getToOBDHModel_Port();

	/**
	 * Returns the meta object for the attribute '{@link obdh.ToOBDHModel#getHostname <em>Hostname</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hostname</em>'.
	 * @see obdh.ToOBDHModel#getHostname()
	 * @see #getToOBDHModel()
	 * @generated
	 */
	EAttribute getToOBDHModel_Hostname();

	/**
	 * Returns the meta object for the '{@link obdh.ToOBDHModel#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see obdh.ToOBDHModel#calc()
	 * @generated
	 */
	EOperation getToOBDHModel__Calc();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ObdhFactory getObdhFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link obdh.impl.FromOBDHModelImpl <em>From OBDH Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see obdh.impl.FromOBDHModelImpl
		 * @see obdh.impl.ObdhPackageImpl#getFromOBDHModel()
		 * @generated
		 */
		EClass FROM_OBDH_MODEL = eINSTANCE.getFromOBDHModel();

		/**
		 * The meta object literal for the '<em><b>Output</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FROM_OBDH_MODEL__OUTPUT = eINSTANCE.getFromOBDHModel_Output();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FROM_OBDH_MODEL__PORT = eINSTANCE.getFromOBDHModel_Port();

		/**
		 * The meta object literal for the '<em><b>Hostname</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FROM_OBDH_MODEL__HOSTNAME = eINSTANCE.getFromOBDHModel_Hostname();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FROM_OBDH_MODEL___CALC = eINSTANCE.getFromOBDHModel__Calc();

		/**
		 * The meta object literal for the '{@link obdh.impl.ToOBDHModelImpl <em>To OBDH Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see obdh.impl.ToOBDHModelImpl
		 * @see obdh.impl.ObdhPackageImpl#getToOBDHModel()
		 * @generated
		 */
		EClass TO_OBDH_MODEL = eINSTANCE.getToOBDHModel();

		/**
		 * The meta object literal for the '<em><b>Input</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TO_OBDH_MODEL__INPUT = eINSTANCE.getToOBDHModel_Input();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TO_OBDH_MODEL__PORT = eINSTANCE.getToOBDHModel_Port();

		/**
		 * The meta object literal for the '<em><b>Hostname</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TO_OBDH_MODEL__HOSTNAME = eINSTANCE.getToOBDHModel_Hostname();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TO_OBDH_MODEL___CALC = eINSTANCE.getToOBDHModel__Calc();

	}

} //ObdhPackage
