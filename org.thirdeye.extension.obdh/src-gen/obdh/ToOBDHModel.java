/**
 */
package obdh;

import org.eclipse.emf.common.util.EList;

import org.thirdeye.core.mesh.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>To OBDH Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link obdh.ToOBDHModel#getInput <em>Input</em>}</li>
 *   <li>{@link obdh.ToOBDHModel#getPort <em>Port</em>}</li>
 *   <li>{@link obdh.ToOBDHModel#getHostname <em>Hostname</em>}</li>
 * </ul>
 *
 * @see obdh.ObdhPackage#getToOBDHModel()
 * @model
 * @generated
 */
public interface ToOBDHModel extends Model {
	/**
	 * Returns the value of the '<em><b>Input</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input</em>' attribute list.
	 * @see obdh.ObdhPackage#getToOBDHModel_Input()
	 * @model unique="false" required="true" ordered="false"
	 *        annotation="ThirdEye unit='none'"
	 * @generated
	 */
	EList<Double> getInput();

	/**
	 * Returns the value of the '<em><b>Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port</em>' attribute.
	 * @see #setPort(int)
	 * @see obdh.ObdhPackage#getToOBDHModel_Port()
	 * @model required="true"
	 *        annotation="ThirdEye unit='none'"
	 * @generated
	 */
	int getPort();

	/**
	 * Sets the value of the '{@link obdh.ToOBDHModel#getPort <em>Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port</em>' attribute.
	 * @see #getPort()
	 * @generated
	 */
	void setPort(int value);

	/**
	 * Returns the value of the '<em><b>Hostname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hostname</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hostname</em>' attribute.
	 * @see #setHostname(String)
	 * @see obdh.ObdhPackage#getToOBDHModel_Hostname()
	 * @model required="true"
	 *        annotation="ThirdEye unit='none'"
	 * @generated
	 */
	String getHostname();

	/**
	 * Sets the value of the '{@link obdh.ToOBDHModel#getHostname <em>Hostname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hostname</em>' attribute.
	 * @see #getHostname()
	 * @generated
	 */
	void setHostname(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void calc();

} // ToOBDHModel
