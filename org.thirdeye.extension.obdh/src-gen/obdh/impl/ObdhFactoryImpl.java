/**
 */
package obdh.impl;

import obdh.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ObdhFactoryImpl extends EFactoryImpl implements ObdhFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ObdhFactory init() {
		try {
			ObdhFactory theObdhFactory = (ObdhFactory) EPackage.Registry.INSTANCE.getEFactory(ObdhPackage.eNS_URI);
			if (theObdhFactory != null) {
				return theObdhFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ObdhFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObdhFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case ObdhPackage.FROM_OBDH_MODEL:
			return createFromOBDHModel();
		case ObdhPackage.TO_OBDH_MODEL:
			return createToOBDHModel();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FromOBDHModel createFromOBDHModel() {
		FromOBDHModelImpl fromOBDHModel = new FromOBDHModelImpl();
		return fromOBDHModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ToOBDHModel createToOBDHModel() {
		ToOBDHModelImpl toOBDHModel = new ToOBDHModelImpl();
		return toOBDHModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObdhPackage getObdhPackage() {
		return (ObdhPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ObdhPackage getPackage() {
		return ObdhPackage.eINSTANCE;
	}

} //ObdhFactoryImpl
