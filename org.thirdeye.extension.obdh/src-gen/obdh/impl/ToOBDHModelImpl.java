/**
 */
package obdh.impl;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Collection;

import obdh.ObdhPackage;
import obdh.ToOBDHModel;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.thirdeye.core.mesh.impl.ModelImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>To OBDH Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link obdh.impl.ToOBDHModelImpl#getInput <em>Input</em>}</li>
 *   <li>{@link obdh.impl.ToOBDHModelImpl#getPort <em>Port</em>}</li>
 *   <li>{@link obdh.impl.ToOBDHModelImpl#getHostname <em>Hostname</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ToOBDHModelImpl extends ModelImpl implements ToOBDHModel {

	protected Socket clientSocket = null;
	protected DataOutputStream outToServer = null;

	/**
	 * The cached value of the '{@link #getInput() <em>Input</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInput()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> input;

	/**
	 * The default value of the '{@link #getPort() <em>Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort()
	 * @generated
	 * @ordered
	 */
	protected static final int PORT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getPort() <em>Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort()
	 * @generated
	 * @ordered
	 */
	protected int port = PORT_EDEFAULT;

	/**
	 * The default value of the '{@link #getHostname() <em>Hostname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHostname()
	 * @generated
	 * @ordered
	 */
	protected static final String HOSTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHostname() <em>Hostname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHostname()
	 * @generated
	 * @ordered
	 */
	protected String hostname = HOSTNAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ToOBDHModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ObdhPackage.Literals.TO_OBDH_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getInput() {
		if (input == null) {
			input = new EDataTypeEList<Double>(Double.class, this, ObdhPackage.TO_OBDH_MODEL__INPUT);
		}
		return input;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPort() {
		return port;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPort(int newPort) {
		int oldPort = port;
		port = newPort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ObdhPackage.TO_OBDH_MODEL__PORT, oldPort, port));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHostname() {
		return hostname;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHostname(String newHostname) {
		String oldHostname = hostname;
		hostname = newHostname;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ObdhPackage.TO_OBDH_MODEL__HOSTNAME, oldHostname,
					hostname));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ObdhPackage.TO_OBDH_MODEL__INPUT:
			return getInput();
		case ObdhPackage.TO_OBDH_MODEL__PORT:
			return getPort();
		case ObdhPackage.TO_OBDH_MODEL__HOSTNAME:
			return getHostname();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ObdhPackage.TO_OBDH_MODEL__INPUT:
			getInput().clear();
			getInput().addAll((Collection<? extends Double>) newValue);
			return;
		case ObdhPackage.TO_OBDH_MODEL__PORT:
			setPort((Integer) newValue);
			return;
		case ObdhPackage.TO_OBDH_MODEL__HOSTNAME:
			setHostname((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ObdhPackage.TO_OBDH_MODEL__INPUT:
			getInput().clear();
			return;
		case ObdhPackage.TO_OBDH_MODEL__PORT:
			setPort(PORT_EDEFAULT);
			return;
		case ObdhPackage.TO_OBDH_MODEL__HOSTNAME:
			setHostname(HOSTNAME_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ObdhPackage.TO_OBDH_MODEL__INPUT:
			return input != null && !input.isEmpty();
		case ObdhPackage.TO_OBDH_MODEL__PORT:
			return port != PORT_EDEFAULT;
		case ObdhPackage.TO_OBDH_MODEL__HOSTNAME:
			return HOSTNAME_EDEFAULT == null ? hostname != null : !HOSTNAME_EDEFAULT.equals(hostname);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (input: ");
		result.append(input);
		result.append(", port: ");
		result.append(port);
		result.append(", hostname: ");
		result.append(hostname);
		result.append(')');
		return result.toString();
	}
	@Override
	public void calc() {
		//System.out.println("ToOBDH: calc()");
	}

	private Socket getClientSocket() {
		if( clientSocket == null ) {
			try {
				clientSocket = new Socket("10.137.2.15", getPort());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return clientSocket;
	};

	private DataOutputStream getOutputStream() {
		if( outToServer == null ) {
			try {
				outToServer = new DataOutputStream(getClientSocket().getOutputStream());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return outToServer;
	};

	protected void sendToOBDH(double[] toOBDH) {
		try {
			String send = "";
			for (Double d : toOBDH) {
				send = send + d.toString() + " ";
			}
			send = send.substring(0, send.length() - 1) + "\n";
			System.out.print("Sending: " + send);
			getOutputStream().writeBytes(send);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void logic() {
		double[] toOBDH = new double[getInput().size()];
		int i = 0;
		for (Double temp : getInput()) {
			toOBDH[i++] = temp;
		}
		sendToOBDH(toOBDH);
	}
} //ToOBDHModelImpl
