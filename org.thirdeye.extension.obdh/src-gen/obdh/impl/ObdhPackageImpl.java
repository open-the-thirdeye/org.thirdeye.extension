/**
 */
package obdh.impl;

import obdh.FromOBDHModel;
import obdh.ObdhFactory;
import obdh.ObdhPackage;

import obdh.ToOBDHModel;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.thirdeye.core.dependencies.DependenciesPackage;

import org.thirdeye.core.mesh.MeshPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ObdhPackageImpl extends EPackageImpl implements ObdhPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fromOBDHModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass toOBDHModelEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see obdh.ObdhPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ObdhPackageImpl() {
		super(eNS_URI, ObdhFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ObdhPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ObdhPackage init() {
		if (isInited)
			return (ObdhPackage) EPackage.Registry.INSTANCE.getEPackage(ObdhPackage.eNS_URI);

		// Obtain or create and register package
		ObdhPackageImpl theObdhPackage = (ObdhPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof ObdhPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new ObdhPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		DependenciesPackage.eINSTANCE.eClass();
		MeshPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theObdhPackage.createPackageContents();

		// Initialize created meta-data
		theObdhPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theObdhPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ObdhPackage.eNS_URI, theObdhPackage);
		return theObdhPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFromOBDHModel() {
		return fromOBDHModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFromOBDHModel_Output() {
		return (EAttribute) fromOBDHModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFromOBDHModel_Port() {
		return (EAttribute) fromOBDHModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFromOBDHModel_Hostname() {
		return (EAttribute) fromOBDHModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFromOBDHModel__Calc() {
		return fromOBDHModelEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getToOBDHModel() {
		return toOBDHModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getToOBDHModel_Input() {
		return (EAttribute) toOBDHModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getToOBDHModel_Port() {
		return (EAttribute) toOBDHModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getToOBDHModel_Hostname() {
		return (EAttribute) toOBDHModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getToOBDHModel__Calc() {
		return toOBDHModelEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObdhFactory getObdhFactory() {
		return (ObdhFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		fromOBDHModelEClass = createEClass(FROM_OBDH_MODEL);
		createEAttribute(fromOBDHModelEClass, FROM_OBDH_MODEL__OUTPUT);
		createEAttribute(fromOBDHModelEClass, FROM_OBDH_MODEL__PORT);
		createEAttribute(fromOBDHModelEClass, FROM_OBDH_MODEL__HOSTNAME);
		createEOperation(fromOBDHModelEClass, FROM_OBDH_MODEL___CALC);

		toOBDHModelEClass = createEClass(TO_OBDH_MODEL);
		createEAttribute(toOBDHModelEClass, TO_OBDH_MODEL__INPUT);
		createEAttribute(toOBDHModelEClass, TO_OBDH_MODEL__PORT);
		createEAttribute(toOBDHModelEClass, TO_OBDH_MODEL__HOSTNAME);
		createEOperation(toOBDHModelEClass, TO_OBDH_MODEL___CALC);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		MeshPackage theMeshPackage = (MeshPackage) EPackage.Registry.INSTANCE.getEPackage(MeshPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		fromOBDHModelEClass.getESuperTypes().add(theMeshPackage.getModel());
		toOBDHModelEClass.getESuperTypes().add(theMeshPackage.getModel());

		// Initialize classes, features, and operations; add parameters
		initEClass(fromOBDHModelEClass, FromOBDHModel.class, "FromOBDHModel", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFromOBDHModel_Output(), ecorePackage.getEDoubleObject(), "output", null, 1, -1,
				FromOBDHModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE,
				!IS_DERIVED, !IS_ORDERED);
		initEAttribute(getFromOBDHModel_Port(), ecorePackage.getEInt(), "port", null, 1, 1, FromOBDHModel.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFromOBDHModel_Hostname(), ecorePackage.getEString(), "hostname", null, 1, 1,
				FromOBDHModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEOperation(getFromOBDHModel__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(toOBDHModelEClass, ToOBDHModel.class, "ToOBDHModel", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getToOBDHModel_Input(), ecorePackage.getEDoubleObject(), "input", null, 1, -1, ToOBDHModel.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED,
				!IS_ORDERED);
		initEAttribute(getToOBDHModel_Port(), ecorePackage.getEInt(), "port", null, 1, 1, ToOBDHModel.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getToOBDHModel_Hostname(), ecorePackage.getEString(), "hostname", null, 1, 1, ToOBDHModel.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getToOBDHModel__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// ThirdEye
		createThirdEyeAnnotations();
	}

	/**
	 * Initializes the annotations for <b>ThirdEye</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createThirdEyeAnnotations() {
		String source = "ThirdEye";
		addAnnotation(getFromOBDHModel_Output(), source, new String[] { "unit", "none" });
		addAnnotation(getFromOBDHModel_Port(), source, new String[] { "unit", "none" });
		addAnnotation(getFromOBDHModel_Hostname(), source, new String[] { "unit", "none" });
		addAnnotation(getToOBDHModel_Input(), source, new String[] { "unit", "none" });
		addAnnotation(getToOBDHModel_Port(), source, new String[] { "unit", "none" });
		addAnnotation(getToOBDHModel_Hostname(), source, new String[] { "unit", "none" });
	}

} //ObdhPackageImpl
