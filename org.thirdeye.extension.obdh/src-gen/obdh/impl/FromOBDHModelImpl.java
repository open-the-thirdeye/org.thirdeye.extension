/**
 */
package obdh.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Collection;

import obdh.FromOBDHModel;
import obdh.ObdhPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.thirdeye.core.mesh.impl.ModelImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>From OBDH Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link obdh.impl.FromOBDHModelImpl#getOutput <em>Output</em>}</li>
 *   <li>{@link obdh.impl.FromOBDHModelImpl#getPort <em>Port</em>}</li>
 *   <li>{@link obdh.impl.FromOBDHModelImpl#getHostname <em>Hostname</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FromOBDHModelImpl extends ModelImpl implements FromOBDHModel {

	protected Socket clientSocket = null;
	protected BufferedReader inFromServer = null;

	/**
	 * The cached value of the '{@link #getOutput() <em>Output</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutput()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> output;

	/**
	 * The default value of the '{@link #getPort() <em>Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort()
	 * @generated
	 * @ordered
	 */
	protected static final int PORT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getPort() <em>Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort()
	 * @generated
	 * @ordered
	 */
	protected int port = PORT_EDEFAULT;

	/**
	 * The default value of the '{@link #getHostname() <em>Hostname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHostname()
	 * @generated
	 * @ordered
	 */
	protected static final String HOSTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHostname() <em>Hostname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHostname()
	 * @generated
	 * @ordered
	 */
	protected String hostname = HOSTNAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FromOBDHModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ObdhPackage.Literals.FROM_OBDH_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getOutput() {
		if (output == null) {
			output = new EDataTypeEList<Double>(Double.class, this, ObdhPackage.FROM_OBDH_MODEL__OUTPUT);
		}
		return output;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPort() {
		return port;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPort(int newPort) {
		int oldPort = port;
		port = newPort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ObdhPackage.FROM_OBDH_MODEL__PORT, oldPort, port));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHostname() {
		return hostname;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHostname(String newHostname) {
		String oldHostname = hostname;
		hostname = newHostname;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ObdhPackage.FROM_OBDH_MODEL__HOSTNAME, oldHostname,
					hostname));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ObdhPackage.FROM_OBDH_MODEL__OUTPUT:
			return getOutput();
		case ObdhPackage.FROM_OBDH_MODEL__PORT:
			return getPort();
		case ObdhPackage.FROM_OBDH_MODEL__HOSTNAME:
			return getHostname();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ObdhPackage.FROM_OBDH_MODEL__OUTPUT:
			getOutput().clear();
			getOutput().addAll((Collection<? extends Double>) newValue);
			return;
		case ObdhPackage.FROM_OBDH_MODEL__PORT:
			setPort((Integer) newValue);
			return;
		case ObdhPackage.FROM_OBDH_MODEL__HOSTNAME:
			setHostname((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ObdhPackage.FROM_OBDH_MODEL__OUTPUT:
			getOutput().clear();
			return;
		case ObdhPackage.FROM_OBDH_MODEL__PORT:
			setPort(PORT_EDEFAULT);
			return;
		case ObdhPackage.FROM_OBDH_MODEL__HOSTNAME:
			setHostname(HOSTNAME_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ObdhPackage.FROM_OBDH_MODEL__OUTPUT:
			return output != null && !output.isEmpty();
		case ObdhPackage.FROM_OBDH_MODEL__PORT:
			return port != PORT_EDEFAULT;
		case ObdhPackage.FROM_OBDH_MODEL__HOSTNAME:
			return HOSTNAME_EDEFAULT == null ? hostname != null : !HOSTNAME_EDEFAULT.equals(hostname);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (output: ");
		result.append(output);
		result.append(", port: ");
		result.append(port);
		result.append(", hostname: ");
		result.append(hostname);
		result.append(')');
		return result.toString();
	}
	@Override
	public void calc() {
		//System.out.println("OBDH: calc()");
	}

	private Socket getClientSocket() {
		if( clientSocket == null ) {
			try {
				clientSocket = new Socket("10.137.2.15", getPort());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return clientSocket;
	};

	private BufferedReader getInputStream() {
			if( inFromServer == null ) {
			try {
				inFromServer = new BufferedReader(new InputStreamReader(getClientSocket().getInputStream()));
				} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return inFromServer;
	};

	protected double[] receiveFromOBDH() {
		try {
			String received = getInputStream().readLine();
			received = getInputStream().readLine();
			System.out.println("Received: " + received);
			String[] strings = received.split(" ");
			double[] doubles = new double[strings.length];
			int i = 0;
			for (String temp: strings ) {
				doubles[i++] = Double.parseDouble(temp);
			}
			return doubles;
		} catch (IOException e) {
			e.printStackTrace();
			double[] doubles = {0.0,0.0,0.0,0.0,0.0};
			return doubles;
		}
	}

	@Override
	public void logic() {
		double[] fromOBDH = new double[getOutput().size()];
		fromOBDH = receiveFromOBDH();
		for (int j = 0; j < getOutput().size(); j++) {
			getOutput().set(j, fromOBDH[j]);
		}
	}
} //FromOBDHModelImpl
