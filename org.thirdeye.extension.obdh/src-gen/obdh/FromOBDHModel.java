/**
 */
package obdh;

import org.eclipse.emf.common.util.EList;

import org.thirdeye.core.mesh.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>From OBDH Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link obdh.FromOBDHModel#getOutput <em>Output</em>}</li>
 *   <li>{@link obdh.FromOBDHModel#getPort <em>Port</em>}</li>
 *   <li>{@link obdh.FromOBDHModel#getHostname <em>Hostname</em>}</li>
 * </ul>
 *
 * @see obdh.ObdhPackage#getFromOBDHModel()
 * @model
 * @generated
 */
public interface FromOBDHModel extends Model {
	/**
	 * Returns the value of the '<em><b>Output</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output</em>' attribute list.
	 * @see obdh.ObdhPackage#getFromOBDHModel_Output()
	 * @model unique="false" required="true" ordered="false"
	 *        annotation="ThirdEye unit='none'"
	 * @generated
	 */
	EList<Double> getOutput();

	/**
	 * Returns the value of the '<em><b>Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port</em>' attribute.
	 * @see #setPort(int)
	 * @see obdh.ObdhPackage#getFromOBDHModel_Port()
	 * @model required="true"
	 *        annotation="ThirdEye unit='none'"
	 * @generated
	 */
	int getPort();

	/**
	 * Sets the value of the '{@link obdh.FromOBDHModel#getPort <em>Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port</em>' attribute.
	 * @see #getPort()
	 * @generated
	 */
	void setPort(int value);

	/**
	 * Returns the value of the '<em><b>Hostname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hostname</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hostname</em>' attribute.
	 * @see #setHostname(String)
	 * @see obdh.ObdhPackage#getFromOBDHModel_Hostname()
	 * @model required="true"
	 *        annotation="ThirdEye unit='none'"
	 * @generated
	 */
	String getHostname();

	/**
	 * Sets the value of the '{@link obdh.FromOBDHModel#getHostname <em>Hostname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hostname</em>' attribute.
	 * @see #getHostname()
	 * @generated
	 */
	void setHostname(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void calc();

} // FromOBDHModel
