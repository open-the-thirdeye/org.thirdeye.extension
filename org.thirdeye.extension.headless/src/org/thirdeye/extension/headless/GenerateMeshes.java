package org.thirdeye.extension.headless;

import java.io.IOException;
import java.util.Collections;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import org.thirdeye.core.mesh.Accessor;
import org.thirdeye.core.mesh.Edge;
import org.thirdeye.core.mesh.impl.MeshPackageImpl;
import org.thirdeye.core.mesh.Mesh;
import org.thirdeye.core.mesh.MeshFactory;
import org.thirdeye.core.mesh.Node;
import org.thirdeye.core.integrator.IntegratorFactory;
import org.thirdeye.core.integrator.ODE;
import org.thirdeye.core.integrator.impl.IntegratorPackageImpl;
import org.thirdeye.extension.space.SpaceFactory;
import org.thirdeye.extension.space.ThermalConnection;
import org.thirdeye.extension.space.ThermalNode;
import org.thirdeye.extension.space.impl.SpacePackageImpl;

public class GenerateMeshes {

	public static void main(String[] args) {
		MeshPackageImpl.init();
		IntegratorPackageImpl.init();
		SpacePackageImpl.init();

		MeshFactory meshFactory = MeshFactory.eINSTANCE;
		IntegratorFactory intFactory = IntegratorFactory.eINSTANCE;
		SpaceFactory spaceFactory = SpaceFactory.eINSTANCE;

		for(int dimensions = 64; dimensions < 129; dimensions = dimensions *2) {
			Mesh mesh = meshFactory.createMesh();
	
			int max = dimensions;
	
			//Make Nodes
			for(int i = 0; i<max; i++) {
				System.out.print(".");
				for(int j = 0; j<max; j++) {
					ThermalNode node = spaceFactory.createThermalNode();
					Double[] temp = {0.0,0.0,0.0,0.0};
					node.setQ(temp);
					node.setDTemperature(0.0);
					node.setTemperature(100.0 + i * j);
					node.setHeatCapacity(200.0);
					node.setName("Node_" + i + "_" + j );
					mesh.getModelNodes().add(node);
					if( j < max-1 ) {
						ThermalConnection connection = spaceFactory.createThermalConnection();
						Double[] tempT = {0.0,0.0};
						connection.setTemperature(tempT);
						connection.setHeatFlow(tempT);
						connection.setHeatConductivity(10.0);
						connection.setName("ConnectionH" + i + "_" + j + "_" + i + "_" + (j+1) );
						//System.out.println("H Connection = " + connection.getName());
						mesh.getConnectionNodes().add(connection);
					}
				}
				for(int j = 0; j<max; j++) {
					if( i < max-1 ) {
						ThermalConnection connection = spaceFactory.createThermalConnection();
						Double[] tempT = {0.0,0.0};
						connection.setTemperature(tempT);
						connection.setHeatFlow(tempT);
						connection.setHeatConductivity(10.0);
						connection.setName("ConnectionV" + i + "_" + j + "_" + (i+1)+ "_" + j );
						//System.out.println("V Connection = " + connection.getName());
						mesh.getConnectionNodes().add(connection);
					}
				}
			}
			System.out.println();
	
			//Make dT -> T ODE
			for(int i = 0; i<max; i++) {
				//System.out.print(".");
				for(int j = 0; j<max; j++) {
					//System.out.println("i = " + i + " j = " + j );
					Node node = mesh.getModelNodes().get((i*max)+j); //Get ThermalNode[i][j]
	
					ODE ode = intFactory.createODE();
					ode.setEqIndex(i*max+j);
					Accessor from = node.getVariables().get(2).getAccessors().get(0);
					Accessor to = node.getVariables().get(0).getAccessors().get(0);
					ode.setFrom(from);
					ode.getTo().add(to);
	
					int index = (i*((2*max)-1))+(j-1);
					//System.out.println("index = " + index );
					Node connection = null;
					if( j > 0 ) {
						connection = mesh.getConnectionNodes().get( index );//Get ThermalConnection Left
						to = connection.getVariables().get(2).getAccessors().get(1);
						ode.getTo().add(to);
					}
					if( j < max-1 ) {
						connection = mesh.getConnectionNodes().get( index+1 );//Get ThermalConnection Right
						to = connection.getVariables().get(2).getAccessors().get(1);
						ode.getTo().add(to);
					}
					index = (i*((2*max)-1))-(max-j);
					//System.out.println("index = " + index );
					connection = null;
					if( i > 0 ) {
						connection = mesh.getConnectionNodes().get( index );//Get ThermalConnection Down
						to = connection.getVariables().get(2).getAccessors().get(1);
						ode.getTo().add(to);
					}
					if( i < max-1 ) {
						connection = mesh.getConnectionNodes().get( index+((2*max)-1) );//Get ThermalConnection Up
						to = connection.getVariables().get(2).getAccessors().get(1);
						ode.getTo().add(to);
					}
					mesh.getIntegratorEdges().add(ode);
				}
			}
			System.out.println();
	
			//Make q_Connection -> q_Node edge
			for(int i = 0; i<max; i++) {
				//System.out.print(".");
				for(int j = 0; j<max; j++) {
					Node node = mesh.getModelNodes().get((i*max)+j); //Get ThermalNode[i][j]

					int index = (i*((2*max)-1))+(j-1);
					//System.out.println("index = " + index );
					Node connection = null;
					if( j > 0 ) {
						connection = mesh.getConnectionNodes().get( index );//Get ThermalConnection Left

						Edge edge = meshFactory.createEdge();
						Accessor from = connection.getVariables().get(1).getAccessors().get(1);
						Accessor to = node.getVariables().get(1).getAccessors().get(1);
						edge.setFrom(from);
						edge.getTo().add(to);
						mesh.getFromConnectionEdges().add(edge);
					}
					if( j < max-1 ) {
						connection = mesh.getConnectionNodes().get( index+1 );//Get ThermalConnection Right

						Edge edge = meshFactory.createEdge();
						Accessor from = connection.getVariables().get(1).getAccessors().get(2);
						Accessor to = node.getVariables().get(1).getAccessors().get(2);
						edge.setFrom(from);
						edge.getTo().add(to);
						mesh.getFromConnectionEdges().add(edge);
					}
					index = (i*((2*max)-1))-(max-j);
					//System.out.println("index = " + index );
					connection = null;
					if( i > 0 ) {
						connection = mesh.getConnectionNodes().get( index );//Get ThermalConnection Down

						Edge edge = meshFactory.createEdge();
						Accessor from = connection.getVariables().get(1).getAccessors().get(1);
						Accessor to = node.getVariables().get(1).getAccessors().get(3);
						edge.setFrom(from);
						edge.getTo().add(to);
						mesh.getFromConnectionEdges().add(edge);
					}
					if( i < max-1 ) {
						connection = mesh.getConnectionNodes().get( index+((2*max)-1) );//Get ThermalConnection Up

						Edge edge = meshFactory.createEdge();
						Accessor from = connection.getVariables().get(1).getAccessors().get(2);
						Accessor to = node.getVariables().get(1).getAccessors().get(4);
						edge.setFrom(from);
						edge.getTo().add(to);
						mesh.getFromConnectionEdges().add(edge);
					}
					
					
					
/*					if( j > 0 ) {
						Node connection = null;
						if(i==max-1) {
							connection = mesh.getConnectionNodes().get( i*((2*max)-1) + (j-1) );//Get ThermalConnection i
						} else {
							connection = mesh.getConnectionNodes().get( i*((2*max)-1) + (2*(j-1)) );//Get ThermalConnection i						
						}
						Edge edge = meshFactory.createEdge();
						Accessor from = connection.getVariables().get(1).getAccessors().get(1);
						Accessor to = node.getVariables().get(1).getAccessors().get(1);
						edge.setFrom(from);
						edge.getTo().add(to);
						mesh.getFromConnectionEdges().add(edge);
					}
					if( j < max-1 ) {
						Node connection = null;
						if(i==max-1) {
							connection = mesh.getConnectionNodes().get( i*((2*max)-1) + (j) );//Get ThermalConnection i
						} else {
							connection = mesh.getConnectionNodes().get( i*((2*max)-1) + (2*j) );//Get ThermalConnection i						
						}
						Edge edge = meshFactory.createEdge();
						Accessor from = connection.getVariables().get(1).getAccessors().get(2);
						Accessor to = node.getVariables().get(1).getAccessors().get(2);
						edge.setFrom(from);
						edge.getTo().add(to);
						mesh.getFromConnectionEdges().add(edge);
					}
					if( i > 0 ) {
						Node connection = null;
						if(j==max-1) {
							connection = mesh.getConnectionNodes().get( (i-1)*((2*max)-1) + (2*j) );//Get ThermalConnection i
						} else {
							connection = mesh.getConnectionNodes().get( (i-1)*((2*max)-1) + (2*j)+1 );//Get ThermalConnection i						
						}
						Edge edge = meshFactory.createEdge();
						Accessor from = connection.getVariables().get(1).getAccessors().get(1);
						Accessor to = node.getVariables().get(1).getAccessors().get(3);
						edge.setFrom(from);
						edge.getTo().add(to);
						mesh.getFromConnectionEdges().add(edge);
					}
					if( i < max-1 ) {
						Node connection = null;
						if(j==max-1) {
							connection = mesh.getConnectionNodes().get( i*((2*max)-1) + (2*j) );//Get ThermalConnection i
						} else {
							connection = mesh.getConnectionNodes().get( i*((2*max)-1) + (2*j)+1 );//Get ThermalConnection i						
						}
						Edge edge = meshFactory.createEdge();
						Accessor from = connection.getVariables().get(1).getAccessors().get(2);
						Accessor to = node.getVariables().get(1).getAccessors().get(4);
						edge.setFrom(from);
						edge.getTo().add(to);
						mesh.getFromConnectionEdges().add(edge);
					}*/
				}
			}
			System.out.println();
	
			Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		    ResourceSet resourceSet = new ResourceSetImpl();
		    //Resource resource = resourceSet.getResource(URI.createFileURI("test.mesh"), true);
		    Resource resource = resourceSet.createResource(URI.createFileURI( "" + max + "_x_" + max + "_Thermal.mesh"));
			System.out.println(resource.toString());
			resource.getContents().add(mesh);
			try {
				resource.save(Collections.EMPTY_MAP);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}