package org.thirdeye.extension.headless;

import java.util.Arrays;
import java.util.Date;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import org.thirdeye.core.mesh.impl.MeshPackageImpl;
import org.thirdeye.core.persistence.PersistenceFactory;
import org.thirdeye.core.persistence.SimulationRun;
import org.thirdeye.core.persistence.impl.PersistencePackageImpl;
import org.thirdeye.core.sequence.SequenceFactory;
import org.thirdeye.core.sequence.SimpleSequence;
import org.thirdeye.core.sequence.impl.SequencePackageImpl;
import org.thirdeye.core.mesh.Mesh;
import org.thirdeye.core.integrator.impl.IntegratorPackageImpl;
import org.thirdeye.extension.space.impl.SpacePackageImpl;

public class RunMesh {

	public static void main(String[] args) {

		int dim = Integer.parseInt(args[0]);
		int steps = Integer.parseInt(args[1]);
		String fileName = dim + "_x_" + dim + "_Thermal.mesh";
		System.out.println("Starting Classic Kernel with Mesh=" + fileName + " (This can take a while)");

		MeshPackageImpl.init();
		IntegratorPackageImpl.init();
		PersistencePackageImpl.init();
		SpacePackageImpl.init();

		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
	    ResourceSet resourceSet = new ResourceSetImpl();
	    Resource resource = resourceSet.getResource(URI.createFileURI("meshes/" + fileName), true);
	    
		Mesh mesh = (Mesh) resource.getContents().get(0);

		PersistenceFactory runFactory = PersistenceFactory.eINSTANCE;
		SimulationRun runner = runFactory.createSimulationRun();
		
		runner.setDescription("HEADLESS Test run");
		runner.setGitVersion("none");
		runner.setName("Headless");
		runner.setSteps(1);
		
		SequencePackageImpl.init();
		SequenceFactory seqFactory = SequenceFactory.eINSTANCE;
		SimpleSequence seq = seqFactory.createSimpleSequence();

		seq.setMesh(mesh);
		
		runner.setSequence(seq);
		
		//System.out.println("Runner: execute");
		runner.setStartTime(new Date());
		((SimpleSequence )runner.getSequence()).init();
		runner.setRunning(true);
		
		System.out.println("Y:    " + Arrays.toString(seq.getIntegrator().getEquationSystem().getY()));
		//System.out.println("YDot: " + Arrays.toString(seq.getIntegrator().getEquationSystem().getYDot()));

		long before = System.currentTimeMillis();
		for( int i=0; i < steps;i++) {
			runner.run();
			//long after = System.currentTimeMillis();
			//System.out.println( i + " " + (after - before));
		}
		long after = System.currentTimeMillis();
		System.out.println("Time Before [ms]: " + before);
		System.out.println( "Time After [ms]: " + after);
		System.out.println( "Time Elapsed [ms]: " + (after - before));
		System.out.println("Y:    " + Arrays.toString(seq.getIntegrator().getEquationSystem().getY()));
		//System.out.println("YDot: " + Arrays.toString(seq.getIntegrator().getEquationSystem().getYDot()));
	}
}