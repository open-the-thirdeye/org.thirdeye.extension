/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space;

import org.eclipse.emf.common.util.EList;
import org.thirdeye.core.mesh.Connection;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Thermal Connection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.ThermalConnection#getHeatConductivity <em>Heat Conductivity</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.ThermalConnection#getHeatFlowList <em>Heat Flow</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.ThermalConnection#getTemperatureList <em>Temperature</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extension.space.SpacePackage#getThermalConnection()
 * @model
 * @generated
 */
public interface ThermalConnection extends Connection {
	/**
	 * Returns the value of the '<em><b>Heat Conductivity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Heat Conductivity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Heat Conductivity</em>' attribute.
	 * @see #setHeatConductivity(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getThermalConnection_HeatConductivity()
	 * @model required="true"
	 *        annotation="ThirdEye unit='J/(s*K)'"
	 * @generated
	 */
	Double getHeatConductivity();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.ThermalConnection#getHeatConductivity <em>Heat Conductivity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Heat Conductivity</em>' attribute.
	 * @see #getHeatConductivity()
	 * @generated
	 */
	void setHeatConductivity(Double value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getHeatFlow();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getHeatFlow(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getHeatFlowLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setHeatFlow(Double[] newHeatFlow);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setHeatFlow(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Heat Flow</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Heat Flow</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Heat Flow</em>' attribute list.
	 * @see org.thirdeye.extension.space.SpacePackage#getThermalConnection_HeatFlow()
	 * @model unique="false" lower="2" upper="2" ordered="false"
	 *        annotation="ThirdEye unit='J/s,J/s'"
	 * @generated
	 */
	EList<Double> getHeatFlowList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getTemperature();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getTemperature(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getTemperatureLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setTemperature(Double[] newTemperature);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setTemperature(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Temperature</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Temperature</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Temperature</em>' attribute list.
	 * @see org.thirdeye.extension.space.SpacePackage#getThermalConnection_Temperature()
	 * @model unique="false" lower="2" upper="2" ordered="false"
	 *        annotation="ThirdEye unit='K,K'"
	 * @generated
	 */
	EList<Double> getTemperatureList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='double deltaT = (double)getFromTemperature().get(-1) - (double)getToTemperature().get(-1);\ndouble flow = deltaT * getHeatConductivity();\ngetTo().set(getToIndex(),flow);\ngetFrom().set(getFromIndex(),-flow);'"
	 * @generated
	 */
	void calc();

} // ThermalConnection
