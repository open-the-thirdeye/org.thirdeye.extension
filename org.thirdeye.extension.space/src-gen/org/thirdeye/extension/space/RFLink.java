/**
 */
package org.thirdeye.extension.space;

import org.eclipse.emf.common.util.EList;

import org.thirdeye.core.mesh.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>RF Link</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.RFLink#getAzelraList <em>Azelra</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.RFLink#getAttenuation <em>Attenuation</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.RFLink#getFrequency <em>Frequency</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.RFLink#getDoppler <em>Doppler</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.RFLink#getRxLocationList <em>Rx Location</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.RFLink#getRxSpeedList <em>Rx Speed</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.RFLink#getTxLocationList <em>Tx Location</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.RFLink#getTxSpeedList <em>Tx Speed</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.RFLink#getTxPower <em>Tx Power</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.RFLink#getRxPower <em>Rx Power</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extension.space.SpacePackage#getRFLink()
 * @model
 * @generated
 */
public interface RFLink extends Model {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getAzelra();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getAzelra(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getAzelraLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setAzelra(Double[] newAzelra);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setAzelra(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Azelra</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Azelra</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Azelra</em>' attribute list.
	 * @see org.thirdeye.extension.space.SpacePackage#getRFLink_Azelra()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 *        annotation="ThirdEye unit='rad,rad,m'"
	 * @generated
	 */
	EList<Double> getAzelraList();

	/**
	 * Returns the value of the '<em><b>Attenuation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attenuation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attenuation</em>' attribute.
	 * @see #setAttenuation(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getRFLink_Attenuation()
	 * @model required="true" ordered="false"
	 *        annotation="ThirdEye unit='dB'"
	 * @generated
	 */
	Double getAttenuation();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.RFLink#getAttenuation <em>Attenuation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attenuation</em>' attribute.
	 * @see #getAttenuation()
	 * @generated
	 */
	void setAttenuation(Double value);

	/**
	 * Returns the value of the '<em><b>Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Frequency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Frequency</em>' attribute.
	 * @see #setFrequency(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getRFLink_Frequency()
	 * @model required="true" ordered="false"
	 *        annotation="ThirdEye unit='Hz'"
	 * @generated
	 */
	Double getFrequency();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.RFLink#getFrequency <em>Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Frequency</em>' attribute.
	 * @see #getFrequency()
	 * @generated
	 */
	void setFrequency(Double value);

	/**
	 * Returns the value of the '<em><b>Doppler</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Doppler</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Doppler</em>' attribute.
	 * @see #setDoppler(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getRFLink_Doppler()
	 * @model required="true" ordered="false"
	 *        annotation="ThirdEye unit='Hz'"
	 * @generated
	 */
	Double getDoppler();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.RFLink#getDoppler <em>Doppler</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Doppler</em>' attribute.
	 * @see #getDoppler()
	 * @generated
	 */
	void setDoppler(Double value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getRxLocation();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getRxLocation(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getRxLocationLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setRxLocation(Double[] newRxLocation);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setRxLocation(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Rx Location</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rx Location</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rx Location</em>' attribute list.
	 * @see org.thirdeye.extension.space.SpacePackage#getRFLink_RxLocation()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 *        annotation="ThirdEye unit='m,m,m'"
	 * @generated
	 */
	EList<Double> getRxLocationList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getRxSpeed();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getRxSpeed(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getRxSpeedLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setRxSpeed(Double[] newRxSpeed);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setRxSpeed(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Rx Speed</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rx Speed</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rx Speed</em>' attribute list.
	 * @see org.thirdeye.extension.space.SpacePackage#getRFLink_RxSpeed()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 *        annotation="ThirdEye unit='m/s,m/s,m/s'"
	 * @generated
	 */
	EList<Double> getRxSpeedList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getTxLocation();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getTxLocation(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getTxLocationLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setTxLocation(Double[] newTxLocation);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setTxLocation(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Tx Location</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tx Location</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tx Location</em>' attribute list.
	 * @see org.thirdeye.extension.space.SpacePackage#getRFLink_TxLocation()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 *        annotation="ThirdEye unit='m,m,m'"
	 * @generated
	 */
	EList<Double> getTxLocationList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getTxSpeed();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getTxSpeed(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getTxSpeedLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setTxSpeed(Double[] newTxSpeed);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setTxSpeed(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Tx Speed</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tx Speed</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tx Speed</em>' attribute list.
	 * @see org.thirdeye.extension.space.SpacePackage#getRFLink_TxSpeed()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 *        annotation="ThirdEye unit='m/s,m/s,m/s'"
	 * @generated
	 */
	EList<Double> getTxSpeedList();

	/**
	 * Returns the value of the '<em><b>Tx Power</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tx Power</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tx Power</em>' attribute.
	 * @see #setTxPower(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getRFLink_TxPower()
	 * @model required="true" ordered="false"
	 *        annotation="ThirdEye unit='dB'"
	 * @generated
	 */
	Double getTxPower();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.RFLink#getTxPower <em>Tx Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tx Power</em>' attribute.
	 * @see #getTxPower()
	 * @generated
	 */
	void setTxPower(Double value);

	/**
	 * Returns the value of the '<em><b>Rx Power</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rx Power</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rx Power</em>' attribute.
	 * @see #setRxPower(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getRFLink_RxPower()
	 * @model required="true" ordered="false"
	 *        annotation="ThirdEye unit='dB'"
	 * @generated
	 */
	Double getRxPower();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.RFLink#getRxPower <em>Rx Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rx Power</em>' attribute.
	 * @see #getRxPower()
	 * @generated
	 */
	void setRxPower(Double value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void calc();

} // RFLink
