/**
 */
package org.thirdeye.extension.space;

import org.eclipse.emf.common.util.EList;

import org.thirdeye.core.mesh.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fixed Attitude</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.FixedAttitude#getPositionList <em>Position</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.FixedAttitude#getSpeedList <em>Speed</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.FixedAttitude#getQuaternionList <em>Quaternion</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extension.space.SpacePackage#getFixedAttitude()
 * @model
 * @generated
 */
public interface FixedAttitude extends Model {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getPosition();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getPosition(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getPositionLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setPosition(Double[] newPosition);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setPosition(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Position</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Position</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position</em>' attribute list.
	 * @see org.thirdeye.extension.space.SpacePackage#getFixedAttitude_Position()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 *        annotation="ThirdEye unit='m,m,m'"
	 * @generated
	 */
	EList<Double> getPositionList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getSpeed();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getSpeed(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getSpeedLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setSpeed(Double[] newSpeed);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setSpeed(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Speed</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Speed</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Speed</em>' attribute list.
	 * @see org.thirdeye.extension.space.SpacePackage#getFixedAttitude_Speed()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 *        annotation="ThirdEye unit='m/s,m/s,m/s'"
	 * @generated
	 */
	EList<Double> getSpeedList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getQuaternion();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getQuaternion(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getQuaternionLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setQuaternion(Double[] newQuaternion);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setQuaternion(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Quaternion</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quaternion</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quaternion</em>' attribute list.
	 * @see org.thirdeye.extension.space.SpacePackage#getFixedAttitude_Quaternion()
	 * @model unique="false" lower="4" upper="4" ordered="false"
	 *        annotation="ThirdEye unit='none,none,none,none'"
	 * @generated
	 */
	EList<Double> getQuaternionList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void calc();

} // FixedAttitude
