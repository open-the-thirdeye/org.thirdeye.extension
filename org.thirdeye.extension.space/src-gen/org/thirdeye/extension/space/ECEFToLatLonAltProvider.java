/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space;

import org.thirdeye.core.mesh.Provider;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ECEF To Lat Lon Alt Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.thirdeye.extension.space.SpacePackage#getECEFToLatLonAltProvider()
 * @model
 * @generated
 */
public interface ECEFToLatLonAltProvider extends Provider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void calc();

} // ECEFToLatLonAltProvider
