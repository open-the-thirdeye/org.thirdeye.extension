/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.thirdeye.extension.space.SpacePackage
 * @generated
 */
public interface SpaceFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SpaceFactory eINSTANCE = org.thirdeye.extension.space.impl.SpaceFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Time</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Time</em>'.
	 * @generated
	 */
	Time createTime();

	/**
	 * Returns a new object of class '<em>Gravity Field</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Gravity Field</em>'.
	 * @generated
	 */
	GravityField createGravityField();

	/**
	 * Returns a new object of class '<em>Magnetic Field</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Magnetic Field</em>'.
	 * @generated
	 */
	MagneticField createMagneticField();

	/**
	 * Returns a new object of class '<em>Point Mass</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Point Mass</em>'.
	 * @generated
	 */
	PointMass createPointMass();

	/**
	 * Returns a new object of class '<em>Thermal Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Thermal Node</em>'.
	 * @generated
	 */
	ThermalNode createThermalNode();

	/**
	 * Returns a new object of class '<em>Thermal Connection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Thermal Connection</em>'.
	 * @generated
	 */
	ThermalConnection createThermalConnection();

	/**
	 * Returns a new object of class '<em>ECI To ECEF Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ECI To ECEF Provider</em>'.
	 * @generated
	 */
	ECIToECEFProvider createECIToECEFProvider();

	/**
	 * Returns a new object of class '<em>ECEF To Lat Lon Alt Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ECEF To Lat Lon Alt Provider</em>'.
	 * @generated
	 */
	ECEFToLatLonAltProvider createECEFToLatLonAltProvider();

	/**
	 * Returns a new object of class '<em>ECEF To ECI Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ECEF To ECI Provider</em>'.
	 * @generated
	 */
	ECEFToECIProvider createECEFToECIProvider();

	/**
	 * Returns a new object of class '<em>Point Mass Gravity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Point Mass Gravity</em>'.
	 * @generated
	 */
	PointMassGravity createPointMassGravity();

	/**
	 * Returns a new object of class '<em>Fixed Attitude</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fixed Attitude</em>'.
	 * @generated
	 */
	FixedAttitude createFixedAttitude();

	/**
	 * Returns a new object of class '<em>SC Local To ECI Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SC Local To ECI Provider</em>'.
	 * @generated
	 */
	SCLocalToECIProvider createSCLocalToECIProvider();

	/**
	 * Returns a new object of class '<em>EC Ito SC Local Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EC Ito SC Local Provider</em>'.
	 * @generated
	 */
	ECItoSCLocalProvider createECItoSCLocalProvider();

	/**
	 * Returns a new object of class '<em>Attitude Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attitude Model</em>'.
	 * @generated
	 */
	AttitudeModel createAttitudeModel();

	/**
	 * Returns a new object of class '<em>RF Link</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>RF Link</em>'.
	 * @generated
	 */
	RFLink createRFLink();

	/**
	 * Returns a new object of class '<em>Electrical Load</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Electrical Load</em>'.
	 * @generated
	 */
	ElectricalLoad createElectricalLoad();

	/**
	 * Returns a new object of class '<em>Solar Cell</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Solar Cell</em>'.
	 * @generated
	 */
	SolarCell createSolarCell();

	/**
	 * Returns a new object of class '<em>Receiver</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Receiver</em>'.
	 * @generated
	 */
	Receiver createReceiver();

	/**
	 * Returns a new object of class '<em>Transmitter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Transmitter</em>'.
	 * @generated
	 */
	Transmitter createTransmitter();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	SpacePackage getSpacePackage();

} //SpaceFactory
