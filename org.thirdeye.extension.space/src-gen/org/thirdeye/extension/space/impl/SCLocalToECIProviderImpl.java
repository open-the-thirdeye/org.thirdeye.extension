/**
 */
package org.thirdeye.extension.space.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.thirdeye.core.mesh.impl.ProviderImpl;

import org.thirdeye.extension.space.SCLocalToECIProvider;
import org.thirdeye.extension.space.SpacePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SC Local To ECI Provider</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.impl.SCLocalToECIProviderImpl#getQuaternionList <em>Quaternion</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SCLocalToECIProviderImpl extends ProviderImpl implements SCLocalToECIProvider {
	/**
	 * The cached value of the '{@link #getQuaternionList() <em>Quaternion</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuaternionList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> quaternion;

	/**
	 * The empty value for the '{@link #getQuaternion() <em>Quaternion</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuaternion()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] QUATERNION_EEMPTY_ARRAY = new Double [0];

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SCLocalToECIProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpacePackage.Literals.SC_LOCAL_TO_ECI_PROVIDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getQuaternion() {
		if (quaternion == null || quaternion.isEmpty()) return QUATERNION_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)quaternion;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getQuaternion(int index) {
		return getQuaternionList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getQuaternionLength() {
		return quaternion == null ? 0 : quaternion.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuaternion(Double[] newQuaternion) {
		((BasicEList<Double>)getQuaternionList()).setData(newQuaternion.length, newQuaternion);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuaternion(int index, Double element) {
		getQuaternionList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getQuaternionList() {
		if (quaternion == null) {
			quaternion = new EDataTypeEList<Double>(Double.class, this, SpacePackage.SC_LOCAL_TO_ECI_PROVIDER__QUATERNION);
		}
		return quaternion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpacePackage.SC_LOCAL_TO_ECI_PROVIDER__QUATERNION:
				return getQuaternionList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpacePackage.SC_LOCAL_TO_ECI_PROVIDER__QUATERNION:
				getQuaternionList().clear();
				getQuaternionList().addAll((Collection<? extends Double>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpacePackage.SC_LOCAL_TO_ECI_PROVIDER__QUATERNION:
				getQuaternionList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpacePackage.SC_LOCAL_TO_ECI_PROVIDER__QUATERNION:
				return quaternion != null && !quaternion.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (quaternion: ");
		result.append(quaternion);
		result.append(')');
		return result.toString();
	}

	@Override
	public void calc() {
		// TODO Auto-generated method stub
		
	}

} //SCLocalToECIProviderImpl
