/**
 */
package org.thirdeye.extension.space.impl;

import java.util.Collection;

import org.apache.commons.math3.complex.Quaternion;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.thirdeye.core.mesh.impl.ModelImpl;

import org.thirdeye.extension.space.FixedAttitude;
import org.thirdeye.extension.space.SpacePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fixed Attitude</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.impl.FixedAttitudeImpl#getPositionList <em>Position</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.FixedAttitudeImpl#getSpeedList <em>Speed</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.FixedAttitudeImpl#getQuaternionList <em>Quaternion</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FixedAttitudeImpl extends ModelImpl implements FixedAttitude {
	/**
	 * The cached value of the '{@link #getPositionList() <em>Position</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositionList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> position;

	/**
	 * The empty value for the '{@link #getPosition() <em>Position</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPosition()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] POSITION_EEMPTY_ARRAY = new Double [0];

	/**
	 * The cached value of the '{@link #getSpeedList() <em>Speed</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpeedList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> speed;

	/**
	 * The empty value for the '{@link #getSpeed() <em>Speed</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpeed()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] SPEED_EEMPTY_ARRAY = new Double [0];

	/**
	 * The cached value of the '{@link #getQuaternionList() <em>Quaternion</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuaternionList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> quaternion;

	/**
	 * The empty value for the '{@link #getQuaternion() <em>Quaternion</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuaternion()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] QUATERNION_EEMPTY_ARRAY = new Double [0];

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FixedAttitudeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpacePackage.Literals.FIXED_ATTITUDE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getPosition() {
		if (position == null || position.isEmpty()) return POSITION_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)position;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getPosition(int index) {
		return getPositionList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPositionLength() {
		return position == null ? 0 : position.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPosition(Double[] newPosition) {
		((BasicEList<Double>)getPositionList()).setData(newPosition.length, newPosition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPosition(int index, Double element) {
		getPositionList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getPositionList() {
		if (position == null) {
			position = new EDataTypeEList<Double>(Double.class, this, SpacePackage.FIXED_ATTITUDE__POSITION);
		}
		return position;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getSpeed() {
		if (speed == null || speed.isEmpty()) return SPEED_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)speed;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getSpeed(int index) {
		return getSpeedList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSpeedLength() {
		return speed == null ? 0 : speed.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpeed(Double[] newSpeed) {
		((BasicEList<Double>)getSpeedList()).setData(newSpeed.length, newSpeed);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpeed(int index, Double element) {
		getSpeedList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getSpeedList() {
		if (speed == null) {
			speed = new EDataTypeEList<Double>(Double.class, this, SpacePackage.FIXED_ATTITUDE__SPEED);
		}
		return speed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getQuaternion() {
		if (quaternion == null || quaternion.isEmpty()) return QUATERNION_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)quaternion;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getQuaternion(int index) {
		return getQuaternionList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getQuaternionLength() {
		return quaternion == null ? 0 : quaternion.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuaternion(Double[] newQuaternion) {
		((BasicEList<Double>)getQuaternionList()).setData(newQuaternion.length, newQuaternion);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuaternion(int index, Double element) {
		getQuaternionList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getQuaternionList() {
		if (quaternion == null) {
			quaternion = new EDataTypeEList<Double>(Double.class, this, SpacePackage.FIXED_ATTITUDE__QUATERNION);
		}
		return quaternion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpacePackage.FIXED_ATTITUDE__POSITION:
				return getPositionList();
			case SpacePackage.FIXED_ATTITUDE__SPEED:
				return getSpeedList();
			case SpacePackage.FIXED_ATTITUDE__QUATERNION:
				return getQuaternionList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpacePackage.FIXED_ATTITUDE__POSITION:
				getPositionList().clear();
				getPositionList().addAll((Collection<? extends Double>)newValue);
				return;
			case SpacePackage.FIXED_ATTITUDE__SPEED:
				getSpeedList().clear();
				getSpeedList().addAll((Collection<? extends Double>)newValue);
				return;
			case SpacePackage.FIXED_ATTITUDE__QUATERNION:
				getQuaternionList().clear();
				getQuaternionList().addAll((Collection<? extends Double>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpacePackage.FIXED_ATTITUDE__POSITION:
				getPositionList().clear();
				return;
			case SpacePackage.FIXED_ATTITUDE__SPEED:
				getSpeedList().clear();
				return;
			case SpacePackage.FIXED_ATTITUDE__QUATERNION:
				getQuaternionList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpacePackage.FIXED_ATTITUDE__POSITION:
				return position != null && !position.isEmpty();
			case SpacePackage.FIXED_ATTITUDE__SPEED:
				return speed != null && !speed.isEmpty();
			case SpacePackage.FIXED_ATTITUDE__QUATERNION:
				return quaternion != null && !quaternion.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (position: ");
		result.append(position);
		result.append(", speed: ");
		result.append(speed);
		result.append(", quaternion: ");
		result.append(quaternion);
		result.append(')');
		return result.toString();
	}

	@Override
	public void calc() {
		Vector3D pos = new Vector3D( getPosition(0), getPosition(1), getPosition(2) );
		Vector3D speed = new Vector3D( getSpeed(0), getSpeed(1), getSpeed(2) );
		pos = pos.normalize();
		speed = speed.normalize();
		
		Vector3D z = pos;
		Vector3D y = pos.crossProduct(speed);
		Vector3D x = y.crossProduct(z);
		
		x = x.normalize();
		y = y.normalize();
		z = z.normalize();
		
		double[][] m3 = new double[3][3];
		m3[0][0] = x.getX();
		m3[0][1] = x.getY();
		m3[0][2] = x.getZ();
		m3[1][0] = y.getX();
		m3[1][1] = y.getY();
		m3[1][2] = y.getZ();
		m3[2][0] = z.getX();
		m3[2][1] = z.getY();
		m3[2][2] = z.getZ();

		double[][] m4 = new double[4][4];
		m4[0][0] = m3[0][0]+m3[1][1]+m3[2][2];
		m4[1][0] = m3[1][2]-m3[2][1];
		m4[1][1] = m3[0][0]-m3[1][1]-m3[2][2];
		m4[2][0] = m3[2][0]-m3[0][2];
		m4[2][1] = m3[0][1]+m3[1][0];
		m4[2][2] = -m3[0][0]+m3[1][1]-m3[2][2];
		m4[3][0] = m3[0][1]-m3[1][0];
		m4[3][1] = m3[2][0]+m3[0][2];
		m4[3][2] = m3[1][2]+m3[2][1];
		m4[3][3] = -m3[0][0]-m3[1][1]+m3[2][2];	
		m4[0][1] = m4[1][0];
		m4[0][2] = m4[2][0];
		m4[0][3] = m4[3][0];
		m4[1][2] = m4[2][1];
		m4[1][3] = m4[3][1];
		m4[2][3] = m4[3][2];				
		
		RealMatrix B = new Array2DRowRealMatrix(m4);
		EigenDecomposition dec = new EigenDecomposition(B);
		double ev[] = dec.getRealEigenvalues();

		//System.out.println();
		int index = 0;
		for( int i = 1; i < ev.length; i++ ) {
		    if ( ev[i] > ev[index] ) {
		        index = i;
		    }
		}
		RealVector quat = dec.getEigenvector(index);
		Quaternion q = new Quaternion(quat.getEntry(0), quat.getEntry(1), quat.getEntry(2), quat.getEntry(3));
		q = q.normalize();
		q = q.getInverse();

		setQuaternion(0, q.getQ0());
		setQuaternion(1, q.getQ1());
		setQuaternion(2, q.getQ2());
		setQuaternion(3, q.getQ3());

		//System.out.println("Quaternion = " + getQuaternion()[0] + " " +getQuaternion()[1] + " " +getQuaternion()[2] + " " +getQuaternion()[3]);
		//System.out.println("Position = " + pos.getX() + " " + pos.getY() + " " + pos.getZ());
		//System.out.println("Speed = " + speed.getX() + " " + speed.getY() + " " + speed.getZ());
		//System.out.println("X = " + x.getX() + " " + x.getY() + " " + x.getZ());
		//System.out.println("Y = " + y.getX() + " " + y.getY() + " " + y.getZ());
		//System.out.println("Z = " + z.getX() + " " + z.getY() + " " + z.getZ());

		//Rotation rot = new Rotation(q.getQ0(),q.getQ1(),q.getQ2(),q.getQ3(),false);

		//Vector3D test = new Vector3D(1.0,0.0,0.0);
		//Vector3D temp = rot.applyTo(test);
		//System.out.println("X' = " + temp.getX() + " " + temp.getY() + " " + temp.getZ());
		//test = new Vector3D(0.0,1.0,0.0);
		//temp = rot.applyTo(test);
		//System.out.println("Y' = " + temp.getX() + " " + temp.getY() + " " + temp.getZ());
		//test = new Vector3D(0.0,0.0,1.0);
		//temp = rot.applyTo(test);
		//System.out.println("Z' = " + temp.getX() + " " + temp.getY() + " " + temp.getZ());
	}
} //FixedAttitudeImpl
