/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 *   Ivan Kossev  - Implementation of coordinate transformartion algorithm
 ********************************************************************************/
package org.thirdeye.extension.space.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.thirdeye.core.mesh.impl.ProviderImpl;
import org.thirdeye.extension.space.ECEFToLatLonAltProvider;
import org.thirdeye.extension.space.SpacePackage;

import jat.coreNOSA.cm.Constants;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ECEF To Lat Lon Alt Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ECEFToLatLonAltProviderImpl extends ProviderImpl implements ECEFToLatLonAltProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ECEFToLatLonAltProviderImpl() {
		super();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void calc() {
		//Intentionally left blank
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Object apply(Object obj) {
		Double[] temp;
		if( obj instanceof EList ) {
			@SuppressWarnings("unchecked") EList<Double> tempList = (EList<Double>)obj;
			temp = (Double[]) tempList.toArray();
		} else {
			temp = (Double[]) obj;
		}
		Double[] lla = getPositionEllipsoidLatLonAlt(temp);
		Double[] returnVal = lla;
		return returnVal;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpacePackage.Literals.ECEF_TO_LAT_LON_ALT_PROVIDER;
	}
	
	/**
	 * Calculation of Latitude, Longitude, Altitude based on ellipsoidal Earth. 
	 * This algorithm requires no iterations and provides an approximation of
	 * the Latitude, Longitude and Altitude above a ellipsoidal Earth. This
	 * algorithm is given in (in German):
	 *
	 * W. Fichter, W. Grimm: Flugmechanik, 2009, p. 14-15.
	 * 
	 * @author I. Kossev
	 * @return S/C position as Latitude, Longitude, Altitude.
	 */
	public Double[] getPositionEllipsoidLatLonAlt(Double[] ecef) {
		Double[] result = new Double[3];
		final double A = Constants.rEarth_WGS84;
		final double e = 0.08181919; /* eccentricity */

		double b, p, theta, e_prim_quadr, Re;

		// Calculation of auxiliary quantities:
		b = A * Math.sqrt(1.0 - Math.pow(e, 2.0));
		e_prim_quadr = (Math.pow(A, 2.0) - Math.pow(b, 2.0)) / Math.pow(b, 2.0);
		p = Math.sqrt(Math.pow(ecef[0], 2.0) + Math.pow(ecef[1], 2.0));
		theta = Math.atan(ecef[2] * A / (p * b));
		// Calculation of the latitude:
		result[0] = Math.atan((ecef[2] + e_prim_quadr * b * Math.pow(Math.sin(theta), 3.0)) / (p - Math.pow(e, 2.0) * A * Math.pow(Math.cos(theta), 3.0)));
		// auxiliary quantity
		Re = A / Math.sqrt(1.0 - Math.pow(e, 2.0) * Math.pow(Math.sin(result[0]), 2.0));
		// Calculation of the longitude:
		result[1] = Math.atan2(ecef[1], ecef[0]);
		// Calculation of the altitude:
		result[2] = p / Math.cos(result[0]) - Re;

		return result;
	}

} //ECEFToLatLonAltProviderImpl
