/**
 */
package org.thirdeye.extension.space.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.thirdeye.core.mesh.impl.ModelImpl;

import org.thirdeye.extension.space.SpacePackage;
import org.thirdeye.extension.space.Transmitter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transmitter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.impl.TransmitterImpl#getFrequency <em>Frequency</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.TransmitterImpl#getPower <em>Power</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.TransmitterImpl#getGain <em>Gain</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.TransmitterImpl#getTxPower <em>Tx Power</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransmitterImpl extends ModelImpl implements Transmitter {
	/**
	 * The default value of the '{@link #getFrequency() <em>Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrequency()
	 * @generated
	 * @ordered
	 */
	protected static final Double FREQUENCY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFrequency() <em>Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrequency()
	 * @generated
	 * @ordered
	 */
	protected Double frequency = FREQUENCY_EDEFAULT;

	/**
	 * The default value of the '{@link #getPower() <em>Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPower()
	 * @generated
	 * @ordered
	 */
	protected static final Double POWER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPower() <em>Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPower()
	 * @generated
	 * @ordered
	 */
	protected Double power = POWER_EDEFAULT;

	/**
	 * The default value of the '{@link #getGain() <em>Gain</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGain()
	 * @generated
	 * @ordered
	 */
	protected static final Double GAIN_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getGain() <em>Gain</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGain()
	 * @generated
	 * @ordered
	 */
	protected Double gain = GAIN_EDEFAULT;

	/**
	 * The default value of the '{@link #getTxPower() <em>Tx Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTxPower()
	 * @generated
	 * @ordered
	 */
	protected static final Double TX_POWER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTxPower() <em>Tx Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTxPower()
	 * @generated
	 * @ordered
	 */
	protected Double txPower = TX_POWER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransmitterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpacePackage.Literals.TRANSMITTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getFrequency() {
		return frequency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFrequency(Double newFrequency) {
		Double oldFrequency = frequency;
		frequency = newFrequency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.TRANSMITTER__FREQUENCY, oldFrequency, frequency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getPower() {
		return power;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPower(Double newPower) {
		Double oldPower = power;
		power = newPower;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.TRANSMITTER__POWER, oldPower, power));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getGain() {
		return gain;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGain(Double newGain) {
		Double oldGain = gain;
		gain = newGain;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.TRANSMITTER__GAIN, oldGain, gain));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getTxPower() {
		return txPower;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTxPower(Double newTxPower) {
		Double oldTxPower = txPower;
		txPower = newTxPower;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.TRANSMITTER__TX_POWER, oldTxPower, txPower));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpacePackage.TRANSMITTER__FREQUENCY:
				return getFrequency();
			case SpacePackage.TRANSMITTER__POWER:
				return getPower();
			case SpacePackage.TRANSMITTER__GAIN:
				return getGain();
			case SpacePackage.TRANSMITTER__TX_POWER:
				return getTxPower();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpacePackage.TRANSMITTER__FREQUENCY:
				setFrequency((Double)newValue);
				return;
			case SpacePackage.TRANSMITTER__POWER:
				setPower((Double)newValue);
				return;
			case SpacePackage.TRANSMITTER__GAIN:
				setGain((Double)newValue);
				return;
			case SpacePackage.TRANSMITTER__TX_POWER:
				setTxPower((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpacePackage.TRANSMITTER__FREQUENCY:
				setFrequency(FREQUENCY_EDEFAULT);
				return;
			case SpacePackage.TRANSMITTER__POWER:
				setPower(POWER_EDEFAULT);
				return;
			case SpacePackage.TRANSMITTER__GAIN:
				setGain(GAIN_EDEFAULT);
				return;
			case SpacePackage.TRANSMITTER__TX_POWER:
				setTxPower(TX_POWER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpacePackage.TRANSMITTER__FREQUENCY:
				return FREQUENCY_EDEFAULT == null ? frequency != null : !FREQUENCY_EDEFAULT.equals(frequency);
			case SpacePackage.TRANSMITTER__POWER:
				return POWER_EDEFAULT == null ? power != null : !POWER_EDEFAULT.equals(power);
			case SpacePackage.TRANSMITTER__GAIN:
				return GAIN_EDEFAULT == null ? gain != null : !GAIN_EDEFAULT.equals(gain);
			case SpacePackage.TRANSMITTER__TX_POWER:
				return TX_POWER_EDEFAULT == null ? txPower != null : !TX_POWER_EDEFAULT.equals(txPower);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (frequency: ");
		result.append(frequency);
		result.append(", power: ");
		result.append(power);
		result.append(", gain: ");
		result.append(gain);
		result.append(", txPower: ");
		result.append(txPower);
		result.append(')');
		return result.toString();
	}

	@Override
	public void calc() {
		setTxPower(10 * Math.log10(getPower()) + 30 + getGain());
	}

} //TransmitterImpl
