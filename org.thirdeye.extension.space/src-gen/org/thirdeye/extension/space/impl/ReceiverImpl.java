/**
 */
package org.thirdeye.extension.space.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.thirdeye.core.mesh.impl.ModelImpl;

import org.thirdeye.extension.space.Receiver;
import org.thirdeye.extension.space.SpacePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Receiver</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.impl.ReceiverImpl#getFrequency <em>Frequency</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.ReceiverImpl#getPower <em>Power</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.ReceiverImpl#getGain <em>Gain</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.ReceiverImpl#getRxPower <em>Rx Power</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReceiverImpl extends ModelImpl implements Receiver {
	/**
	 * The default value of the '{@link #getFrequency() <em>Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrequency()
	 * @generated
	 * @ordered
	 */
	protected static final Double FREQUENCY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFrequency() <em>Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrequency()
	 * @generated
	 * @ordered
	 */
	protected Double frequency = FREQUENCY_EDEFAULT;

	/**
	 * The default value of the '{@link #getPower() <em>Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPower()
	 * @generated
	 * @ordered
	 */
	protected static final Double POWER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPower() <em>Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPower()
	 * @generated
	 * @ordered
	 */
	protected Double power = POWER_EDEFAULT;

	/**
	 * The default value of the '{@link #getGain() <em>Gain</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGain()
	 * @generated
	 * @ordered
	 */
	protected static final Double GAIN_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getGain() <em>Gain</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGain()
	 * @generated
	 * @ordered
	 */
	protected Double gain = GAIN_EDEFAULT;

	/**
	 * The default value of the '{@link #getRxPower() <em>Rx Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRxPower()
	 * @generated
	 * @ordered
	 */
	protected static final Double RX_POWER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRxPower() <em>Rx Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRxPower()
	 * @generated
	 * @ordered
	 */
	protected Double rxPower = RX_POWER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReceiverImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpacePackage.Literals.RECEIVER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getFrequency() {
		return frequency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFrequency(Double newFrequency) {
		Double oldFrequency = frequency;
		frequency = newFrequency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.RECEIVER__FREQUENCY, oldFrequency, frequency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getPower() {
		return power;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPower(Double newPower) {
		Double oldPower = power;
		power = newPower;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.RECEIVER__POWER, oldPower, power));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getGain() {
		return gain;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGain(Double newGain) {
		Double oldGain = gain;
		gain = newGain;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.RECEIVER__GAIN, oldGain, gain));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getRxPower() {
		return rxPower;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRxPower(Double newRxPower) {
		Double oldRxPower = rxPower;
		rxPower = newRxPower;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.RECEIVER__RX_POWER, oldRxPower, rxPower));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpacePackage.RECEIVER__FREQUENCY:
				return getFrequency();
			case SpacePackage.RECEIVER__POWER:
				return getPower();
			case SpacePackage.RECEIVER__GAIN:
				return getGain();
			case SpacePackage.RECEIVER__RX_POWER:
				return getRxPower();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpacePackage.RECEIVER__FREQUENCY:
				setFrequency((Double)newValue);
				return;
			case SpacePackage.RECEIVER__POWER:
				setPower((Double)newValue);
				return;
			case SpacePackage.RECEIVER__GAIN:
				setGain((Double)newValue);
				return;
			case SpacePackage.RECEIVER__RX_POWER:
				setRxPower((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpacePackage.RECEIVER__FREQUENCY:
				setFrequency(FREQUENCY_EDEFAULT);
				return;
			case SpacePackage.RECEIVER__POWER:
				setPower(POWER_EDEFAULT);
				return;
			case SpacePackage.RECEIVER__GAIN:
				setGain(GAIN_EDEFAULT);
				return;
			case SpacePackage.RECEIVER__RX_POWER:
				setRxPower(RX_POWER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpacePackage.RECEIVER__FREQUENCY:
				return FREQUENCY_EDEFAULT == null ? frequency != null : !FREQUENCY_EDEFAULT.equals(frequency);
			case SpacePackage.RECEIVER__POWER:
				return POWER_EDEFAULT == null ? power != null : !POWER_EDEFAULT.equals(power);
			case SpacePackage.RECEIVER__GAIN:
				return GAIN_EDEFAULT == null ? gain != null : !GAIN_EDEFAULT.equals(gain);
			case SpacePackage.RECEIVER__RX_POWER:
				return RX_POWER_EDEFAULT == null ? rxPower != null : !RX_POWER_EDEFAULT.equals(rxPower);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (frequency: ");
		result.append(frequency);
		result.append(", power: ");
		result.append(power);
		result.append(", gain: ");
		result.append(gain);
		result.append(", rxPower: ");
		result.append(rxPower);
		result.append(')');
		return result.toString();
	}

	@Override
	public void calc() {
		// TODO Auto-generated method stub
		double db = getRxPower() + getGain();
		double rx = Math.pow(10, ( db -30 )/10);
		setPower(rx);
	}

} //ReceiverImpl
