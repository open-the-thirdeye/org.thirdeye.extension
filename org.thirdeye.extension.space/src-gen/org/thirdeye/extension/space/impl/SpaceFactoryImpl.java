/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.thirdeye.extension.space.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SpaceFactoryImpl extends EFactoryImpl implements SpaceFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SpaceFactory init() {
		try {
			SpaceFactory theSpaceFactory = (SpaceFactory)EPackage.Registry.INSTANCE.getEFactory(SpacePackage.eNS_URI);
			if (theSpaceFactory != null) {
				return theSpaceFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SpaceFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpaceFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SpacePackage.TIME: return createTime();
			case SpacePackage.GRAVITY_FIELD: return createGravityField();
			case SpacePackage.MAGNETIC_FIELD: return createMagneticField();
			case SpacePackage.POINT_MASS: return createPointMass();
			case SpacePackage.THERMAL_NODE: return createThermalNode();
			case SpacePackage.THERMAL_CONNECTION: return createThermalConnection();
			case SpacePackage.ECI_TO_ECEF_PROVIDER: return createECIToECEFProvider();
			case SpacePackage.ECEF_TO_LAT_LON_ALT_PROVIDER: return createECEFToLatLonAltProvider();
			case SpacePackage.ECEF_TO_ECI_PROVIDER: return createECEFToECIProvider();
			case SpacePackage.POINT_MASS_GRAVITY: return createPointMassGravity();
			case SpacePackage.FIXED_ATTITUDE: return createFixedAttitude();
			case SpacePackage.SC_LOCAL_TO_ECI_PROVIDER: return createSCLocalToECIProvider();
			case SpacePackage.EC_ITO_SC_LOCAL_PROVIDER: return createECItoSCLocalProvider();
			case SpacePackage.ATTITUDE_MODEL: return createAttitudeModel();
			case SpacePackage.RF_LINK: return createRFLink();
			case SpacePackage.ELECTRICAL_LOAD: return createElectricalLoad();
			case SpacePackage.SOLAR_CELL: return createSolarCell();
			case SpacePackage.RECEIVER: return createReceiver();
			case SpacePackage.TRANSMITTER: return createTransmitter();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Time createTime() {
		TimeImpl time = new TimeImpl();
		return time;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GravityField createGravityField() {
		GravityFieldImpl gravityField = new GravityFieldImpl();
		return gravityField;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MagneticField createMagneticField() {
		MagneticFieldImpl magneticField = new MagneticFieldImpl();
		return magneticField;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PointMass createPointMass() {
		PointMassImpl pointMass = new PointMassImpl();
		return pointMass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThermalNode createThermalNode() {
		ThermalNodeImpl thermalNode = new ThermalNodeImpl();
		return thermalNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThermalConnection createThermalConnection() {
		ThermalConnectionImpl thermalConnection = new ThermalConnectionImpl();
		return thermalConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ECIToECEFProvider createECIToECEFProvider() {
		ECIToECEFProviderImpl eciToECEFProvider = new ECIToECEFProviderImpl();
		return eciToECEFProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ECEFToLatLonAltProvider createECEFToLatLonAltProvider() {
		ECEFToLatLonAltProviderImpl ecefToLatLonAltProvider = new ECEFToLatLonAltProviderImpl();
		return ecefToLatLonAltProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ECEFToECIProvider createECEFToECIProvider() {
		ECEFToECIProviderImpl ecefToECIProvider = new ECEFToECIProviderImpl();
		return ecefToECIProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PointMassGravity createPointMassGravity() {
		PointMassGravityImpl pointMassGravity = new PointMassGravityImpl();
		return pointMassGravity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixedAttitude createFixedAttitude() {
		FixedAttitudeImpl fixedAttitude = new FixedAttitudeImpl();
		return fixedAttitude;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SCLocalToECIProvider createSCLocalToECIProvider() {
		SCLocalToECIProviderImpl scLocalToECIProvider = new SCLocalToECIProviderImpl();
		return scLocalToECIProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ECItoSCLocalProvider createECItoSCLocalProvider() {
		ECItoSCLocalProviderImpl ecItoSCLocalProvider = new ECItoSCLocalProviderImpl();
		return ecItoSCLocalProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttitudeModel createAttitudeModel() {
		AttitudeModelImpl attitudeModel = new AttitudeModelImpl();
		return attitudeModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RFLink createRFLink() {
		RFLinkImpl rfLink = new RFLinkImpl();
		return rfLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElectricalLoad createElectricalLoad() {
		ElectricalLoadImpl electricalLoad = new ElectricalLoadImpl();
		return electricalLoad;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SolarCell createSolarCell() {
		SolarCellImpl solarCell = new SolarCellImpl();
		return solarCell;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Receiver createReceiver() {
		ReceiverImpl receiver = new ReceiverImpl();
		return receiver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transmitter createTransmitter() {
		TransmitterImpl transmitter = new TransmitterImpl();
		return transmitter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpacePackage getSpacePackage() {
		return (SpacePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SpacePackage getPackage() {
		return SpacePackage.eINSTANCE;
	}

} //SpaceFactoryImpl
