/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.impl;

import java.util.Collection;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.thirdeye.core.mesh.impl.ModelImpl;
import org.thirdeye.extension.space.PointMassGravity;
import org.thirdeye.extension.space.SpacePackage;

import jat.coreNOSA.cm.Constants;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Point Mass Gravity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.impl.PointMassGravityImpl#getPositionList <em>Position</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.PointMassGravityImpl#getAccelerationList <em>Acceleration</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PointMassGravityImpl extends ModelImpl implements PointMassGravity {
	/**
	 * The cached value of the '{@link #getPositionList() <em>Position</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositionList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> position;

	/**
	 * The empty value for the '{@link #getPosition() <em>Position</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPosition()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] POSITION_EEMPTY_ARRAY = new Double [0];

	/**
	 * The cached value of the '{@link #getAccelerationList() <em>Acceleration</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccelerationList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> acceleration;

	/**
	 * The empty value for the '{@link #getAcceleration() <em>Acceleration</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAcceleration()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] ACCELERATION_EEMPTY_ARRAY = new Double [0];

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PointMassGravityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpacePackage.Literals.POINT_MASS_GRAVITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getPosition() {
		if (position == null || position.isEmpty()) return POSITION_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)position;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getPosition(int index) {
		return getPositionList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPositionLength() {
		return position == null ? 0 : position.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPosition(Double[] newPosition) {
		((BasicEList<Double>)getPositionList()).setData(newPosition.length, newPosition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPosition(int index, Double element) {
		getPositionList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getPositionList() {
		if (position == null) {
			position = new EDataTypeEList<Double>(Double.class, this, SpacePackage.POINT_MASS_GRAVITY__POSITION);
		}
		return position;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getAcceleration() {
		if (acceleration == null || acceleration.isEmpty()) return ACCELERATION_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)acceleration;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getAcceleration(int index) {
		return getAccelerationList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getAccelerationLength() {
		return acceleration == null ? 0 : acceleration.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcceleration(Double[] newAcceleration) {
		((BasicEList<Double>)getAccelerationList()).setData(newAcceleration.length, newAcceleration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcceleration(int index, Double element) {
		getAccelerationList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getAccelerationList() {
		if (acceleration == null) {
			acceleration = new EDataTypeEList<Double>(Double.class, this, SpacePackage.POINT_MASS_GRAVITY__ACCELERATION);
		}
		return acceleration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpacePackage.POINT_MASS_GRAVITY__POSITION:
				return getPositionList();
			case SpacePackage.POINT_MASS_GRAVITY__ACCELERATION:
				return getAccelerationList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpacePackage.POINT_MASS_GRAVITY__POSITION:
				getPositionList().clear();
				getPositionList().addAll((Collection<? extends Double>)newValue);
				return;
			case SpacePackage.POINT_MASS_GRAVITY__ACCELERATION:
				getAccelerationList().clear();
				getAccelerationList().addAll((Collection<? extends Double>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpacePackage.POINT_MASS_GRAVITY__POSITION:
				getPositionList().clear();
				return;
			case SpacePackage.POINT_MASS_GRAVITY__ACCELERATION:
				getAccelerationList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpacePackage.POINT_MASS_GRAVITY__POSITION:
				return position != null && !position.isEmpty();
			case SpacePackage.POINT_MASS_GRAVITY__ACCELERATION:
				return acceleration != null && !acceleration.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (position: ");
		result.append(position);
		result.append(", acceleration: ");
		result.append(acceleration);
		result.append(')');
		return result.toString();
	}

	@Override
	public void calc() {
		double mu = Constants.GM_Earth;
		Vector3D pos = new Vector3D( getPosition(0), getPosition(1), getPosition(2) );
		double r = pos.getNorm(); 

		pos = pos.normalize();
		double a = -(mu/(r * r));
		Vector3D accel = pos.scalarMultiply(a);
		//double g = accel.getNorm(); 
		
		setAcceleration(0,accel.getX());
		setAcceleration(1,accel.getY());
		setAcceleration(2,accel.getZ());

		//System.out.println("Acceleration: " + g + " Radius: " + r + " A: " + a );
	}

} //PointMassGravityImpl
