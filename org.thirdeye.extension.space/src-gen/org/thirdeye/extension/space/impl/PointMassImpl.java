/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.thirdeye.core.mesh.impl.ModelImpl;
import org.thirdeye.extension.space.PointMass;
import org.thirdeye.extension.space.SpacePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Point Mass</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.impl.PointMassImpl#getMass <em>Mass</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.PointMassImpl#getPositionList <em>Position</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.PointMassImpl#getSpeedList <em>Speed</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PointMassImpl extends ModelImpl implements PointMass {
	/**
	 * The default value of the '{@link #getMass() <em>Mass</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMass()
	 * @generated
	 * @ordered
	 */
	protected static final Double MASS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMass() <em>Mass</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMass()
	 * @generated
	 * @ordered
	 */
	protected Double mass = MASS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPositionList() <em>Position</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositionList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> position;

	/**
	 * The empty value for the '{@link #getPosition() <em>Position</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPosition()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] POSITION_EEMPTY_ARRAY = new Double [0];

	/**
	 * The cached value of the '{@link #getSpeedList() <em>Speed</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpeedList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> speed;

	/**
	 * The empty value for the '{@link #getSpeed() <em>Speed</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpeed()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] SPEED_EEMPTY_ARRAY = new Double [0];

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PointMassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpacePackage.Literals.POINT_MASS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getMass() {
		return mass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMass(Double newMass) {
		Double oldMass = mass;
		mass = newMass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.POINT_MASS__MASS, oldMass, mass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getPosition() {
		if (position == null || position.isEmpty()) return POSITION_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)position;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getPosition(int index) {
		return getPositionList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPositionLength() {
		return position == null ? 0 : position.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPosition(Double[] newPosition) {
		((BasicEList<Double>)getPositionList()).setData(newPosition.length, newPosition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPosition(int index, Double element) {
		getPositionList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getPositionList() {
		if (position == null) {
			position = new EDataTypeEList<Double>(Double.class, this, SpacePackage.POINT_MASS__POSITION);
		}
		return position;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getSpeed() {
		if (speed == null || speed.isEmpty()) return SPEED_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)speed;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getSpeed(int index) {
		return getSpeedList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSpeedLength() {
		return speed == null ? 0 : speed.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpeed(Double[] newSpeed) {
		((BasicEList<Double>)getSpeedList()).setData(newSpeed.length, newSpeed);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpeed(int index, Double element) {
		getSpeedList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getSpeedList() {
		if (speed == null) {
			speed = new EDataTypeEList<Double>(Double.class, this, SpacePackage.POINT_MASS__SPEED);
		}
		return speed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpacePackage.POINT_MASS__MASS:
				return getMass();
			case SpacePackage.POINT_MASS__POSITION:
				return getPositionList();
			case SpacePackage.POINT_MASS__SPEED:
				return getSpeedList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpacePackage.POINT_MASS__MASS:
				setMass((Double)newValue);
				return;
			case SpacePackage.POINT_MASS__POSITION:
				getPositionList().clear();
				getPositionList().addAll((Collection<? extends Double>)newValue);
				return;
			case SpacePackage.POINT_MASS__SPEED:
				getSpeedList().clear();
				getSpeedList().addAll((Collection<? extends Double>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpacePackage.POINT_MASS__MASS:
				setMass(MASS_EDEFAULT);
				return;
			case SpacePackage.POINT_MASS__POSITION:
				getPositionList().clear();
				return;
			case SpacePackage.POINT_MASS__SPEED:
				getSpeedList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpacePackage.POINT_MASS__MASS:
				return MASS_EDEFAULT == null ? mass != null : !MASS_EDEFAULT.equals(mass);
			case SpacePackage.POINT_MASS__POSITION:
				return position != null && !position.isEmpty();
			case SpacePackage.POINT_MASS__SPEED:
				return speed != null && !speed.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mass: ");
		result.append(mass);
		result.append(", position: ");
		result.append(position);
		result.append(", speed: ");
		result.append(speed);
		result.append(')');
		return result.toString();
	}

	@Override
	public void calc() {
		//System.out.println("Position: " + getPosition(0).toString() + " "+ getPosition(1).toString() + " "+ getPosition(2).toString() );
		//System.out.println("Speed: " + getSpeed(0).toString() + " "+ getSpeed(1).toString() + " "+ getSpeed(2).toString() );
		// Intentionally left blank
	}

} //PointMassImpl
