/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.impl;

import jat.coreNOSA.forces.GravityModel;
import jat.coreNOSA.math.MatrixVector.data.VectorN;
import jat.coreNOSA.spacetime.EarthFixedRef;

import java.io.File;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.thirdeye.core.mesh.impl.ModelImpl;
import org.thirdeye.extension.space.GravityField;
import org.thirdeye.extension.space.SpacePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Gravity Field</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.impl.GravityFieldImpl#getPositionList <em>Position</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.GravityFieldImpl#getCoefficientFilePath <em>Coefficient File Path</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.GravityFieldImpl#getAccelerationList <em>Acceleration</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.GravityFieldImpl#getGravityField <em>Gravity Field</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GravityFieldImpl extends ModelImpl implements GravityField {
	/**
	 * The cached value of the '{@link #getPositionList() <em>Position</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositionList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> position;

	/**
	 * The empty value for the '{@link #getPosition() <em>Position</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPosition()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] POSITION_EEMPTY_ARRAY = new Double [0];

	/**
	 * The default value of the '{@link #getCoefficientFilePath() <em>Coefficient File Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoefficientFilePath()
	 * @generated
	 * @ordered
	 */
	protected static final String COEFFICIENT_FILE_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCoefficientFilePath() <em>Coefficient File Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoefficientFilePath()
	 * @generated
	 * @ordered
	 */
	protected String coefficientFilePath = COEFFICIENT_FILE_PATH_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAccelerationList() <em>Acceleration</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccelerationList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> acceleration;

	/**
	 * The empty value for the '{@link #getAcceleration() <em>Acceleration</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAcceleration()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] ACCELERATION_EEMPTY_ARRAY = new Double [0];

	/**
	 * The default value of the '{@link #getGravityField() <em>Gravity Field</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGravityField()
	 * @generated
	 * @ordered
	 */
	protected static final GravityModel GRAVITY_FIELD_EDEFAULT = null;

	
	protected GravityModel gravityField = GRAVITY_FIELD_EDEFAULT;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GravityFieldImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpacePackage.Literals.GRAVITY_FIELD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getPosition() {
		if (position == null || position.isEmpty()) return POSITION_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)position;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getPosition(int index) {
		return getPositionList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPositionLength() {
		return position == null ? 0 : position.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPosition(Double[] newPosition) {
		((BasicEList<Double>)getPositionList()).setData(newPosition.length, newPosition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPosition(int index, Double element) {
		getPositionList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getPositionList() {
		if (position == null) {
			position = new EDataTypeEList<Double>(Double.class, this, SpacePackage.GRAVITY_FIELD__POSITION);
		}
		return position;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCoefficientFilePath() {
		return coefficientFilePath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCoefficientFilePath(String newCoefficientFilePath) {
		String oldCoefficientFilePath = coefficientFilePath;
		coefficientFilePath = newCoefficientFilePath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.GRAVITY_FIELD__COEFFICIENT_FILE_PATH, oldCoefficientFilePath, coefficientFilePath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getAcceleration() {
		if (acceleration == null || acceleration.isEmpty()) return ACCELERATION_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)acceleration;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getAcceleration(int index) {
		return getAccelerationList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getAccelerationLength() {
		return acceleration == null ? 0 : acceleration.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcceleration(Double[] newAcceleration) {
		((BasicEList<Double>)getAccelerationList()).setData(newAcceleration.length, newAcceleration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcceleration(int index, Double element) {
		getAccelerationList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getAccelerationList() {
		if (acceleration == null) {
			acceleration = new EDataTypeEList<Double>(Double.class, this, SpacePackage.GRAVITY_FIELD__ACCELERATION);
		}
		return acceleration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public GravityModel getGravityField() {
		int order = 3;
		int degree = 3;
		File file = new File( getCoefficientFilePath() );
		if( gravityField == null ) {
			gravityField = new GravityModel( order, degree, new EarthFixedRef(), file );
		}
		return gravityField;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void calc() {
		VectorN position = new VectorN( getPosition() );
		VectorN accel = getGravityField().bodyFixedGravity(position);
		setAcceleration(0,accel.get(0));
		setAcceleration(1,accel.get(1));
		setAcceleration(2,accel.get(2));
	}

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpacePackage.GRAVITY_FIELD__POSITION:
				return getPositionList();
			case SpacePackage.GRAVITY_FIELD__COEFFICIENT_FILE_PATH:
				return getCoefficientFilePath();
			case SpacePackage.GRAVITY_FIELD__ACCELERATION:
				return getAccelerationList();
			case SpacePackage.GRAVITY_FIELD__GRAVITY_FIELD:
				return getGravityField();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpacePackage.GRAVITY_FIELD__POSITION:
				getPositionList().clear();
				getPositionList().addAll((Collection<? extends Double>)newValue);
				return;
			case SpacePackage.GRAVITY_FIELD__COEFFICIENT_FILE_PATH:
				setCoefficientFilePath((String)newValue);
				return;
			case SpacePackage.GRAVITY_FIELD__ACCELERATION:
				getAccelerationList().clear();
				getAccelerationList().addAll((Collection<? extends Double>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpacePackage.GRAVITY_FIELD__POSITION:
				getPositionList().clear();
				return;
			case SpacePackage.GRAVITY_FIELD__COEFFICIENT_FILE_PATH:
				setCoefficientFilePath(COEFFICIENT_FILE_PATH_EDEFAULT);
				return;
			case SpacePackage.GRAVITY_FIELD__ACCELERATION:
				getAccelerationList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpacePackage.GRAVITY_FIELD__POSITION:
				return position != null && !position.isEmpty();
			case SpacePackage.GRAVITY_FIELD__COEFFICIENT_FILE_PATH:
				return COEFFICIENT_FILE_PATH_EDEFAULT == null ? coefficientFilePath != null : !COEFFICIENT_FILE_PATH_EDEFAULT.equals(coefficientFilePath);
			case SpacePackage.GRAVITY_FIELD__ACCELERATION:
				return acceleration != null && !acceleration.isEmpty();
			case SpacePackage.GRAVITY_FIELD__GRAVITY_FIELD:
				return GRAVITY_FIELD_EDEFAULT == null ? getGravityField() != null : !GRAVITY_FIELD_EDEFAULT.equals(getGravityField());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (position: ");
		result.append(position);
		result.append(", coefficientFilePath: ");
		result.append(coefficientFilePath);
		result.append(", acceleration: ");
		result.append(acceleration);
		result.append(')');
		return result.toString();
	}

} //GravityFieldImpl
