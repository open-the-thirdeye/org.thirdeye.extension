/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.impl;

import jat.coreNOSA.forces.GravityModel;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.thirdeye.core.dependencies.DependenciesPackage;
import org.thirdeye.core.mesh.MeshPackage;
import org.thirdeye.extension.space.AttitudeModel;
import org.thirdeye.extension.space.ECEFToECIProvider;
import org.thirdeye.extension.space.ECEFToLatLonAltProvider;
import org.thirdeye.extension.space.ECIToECEFProvider;
import org.thirdeye.extension.space.ECItoSCLocalProvider;
import org.thirdeye.extension.space.ElectricalLoad;
import org.thirdeye.extension.space.FixedAttitude;
import org.thirdeye.extension.space.GravityField;
import org.thirdeye.extension.space.MagneticField;
import org.thirdeye.extension.space.PointMass;
import org.thirdeye.extension.space.PointMassGravity;
import org.thirdeye.extension.space.RFLink;
import org.thirdeye.extension.space.Receiver;
import org.thirdeye.extension.space.SCLocalToECIProvider;
import org.thirdeye.extension.space.SolarCell;
import org.thirdeye.extension.space.SpaceFactory;
import org.thirdeye.extension.space.SpacePackage;
import org.thirdeye.extension.space.ThermalConnection;
import org.thirdeye.extension.space.ThermalNode;
import org.thirdeye.extension.space.Time;
import org.thirdeye.extension.space.Transmitter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SpacePackageImpl extends EPackageImpl implements SpacePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gravityFieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass magneticFieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pointMassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass thermalNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass thermalConnectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eciToECEFProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ecefToLatLonAltProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ecefToECIProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pointMassGravityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fixedAttitudeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scLocalToECIProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ecItoSCLocalProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attitudeModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rfLinkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass electricalLoadEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass solarCellEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass receiverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transmitterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType jatGravityModelEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.thirdeye.extension.space.SpacePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SpacePackageImpl() {
		super(eNS_URI, SpaceFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link SpacePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SpacePackage init() {
		if (isInited) return (SpacePackage)EPackage.Registry.INSTANCE.getEPackage(SpacePackage.eNS_URI);

		// Obtain or create and register package
		SpacePackageImpl theSpacePackage = (SpacePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof SpacePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new SpacePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		DependenciesPackage.eINSTANCE.eClass();
		MeshPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theSpacePackage.createPackageContents();

		// Initialize created meta-data
		theSpacePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSpacePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SpacePackage.eNS_URI, theSpacePackage);
		return theSpacePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTime() {
		return timeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTime_Time() {
		return (EAttribute)timeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTime_DTime() {
		return (EAttribute)timeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTime__Calc() {
		return timeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGravityField() {
		return gravityFieldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGravityField_Position() {
		return (EAttribute)gravityFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGravityField_CoefficientFilePath() {
		return (EAttribute)gravityFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGravityField_Acceleration() {
		return (EAttribute)gravityFieldEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGravityField_GravityField() {
		return (EAttribute)gravityFieldEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getGravityField__Calc() {
		return gravityFieldEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMagneticField() {
		return magneticFieldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMagneticField_LatLonAlt() {
		return (EAttribute)magneticFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMagneticField_CoefficientFilePath() {
		return (EAttribute)magneticFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMagneticField__Calc() {
		return magneticFieldEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPointMass() {
		return pointMassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPointMass_Mass() {
		return (EAttribute)pointMassEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPointMass_Position() {
		return (EAttribute)pointMassEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPointMass_Speed() {
		return (EAttribute)pointMassEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPointMass__Calc() {
		return pointMassEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getThermalNode() {
		return thermalNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getThermalNode_Temperature() {
		return (EAttribute)thermalNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getThermalNode_Q() {
		return (EAttribute)thermalNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getThermalNode_DTemperature() {
		return (EAttribute)thermalNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getThermalNode_HeatCapacity() {
		return (EAttribute)thermalNodeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getThermalNode__Calc() {
		return thermalNodeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getThermalConnection() {
		return thermalConnectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getThermalConnection_HeatConductivity() {
		return (EAttribute)thermalConnectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getThermalConnection_HeatFlow() {
		return (EAttribute)thermalConnectionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getThermalConnection_Temperature() {
		return (EAttribute)thermalConnectionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getThermalConnection__Calc() {
		return thermalConnectionEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getECIToECEFProvider() {
		return eciToECEFProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getECIToECEFProvider_Time() {
		return (EAttribute)eciToECEFProviderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getECIToECEFProvider__Calc() {
		return eciToECEFProviderEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getECEFToLatLonAltProvider() {
		return ecefToLatLonAltProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getECEFToLatLonAltProvider__Calc() {
		return ecefToLatLonAltProviderEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getECEFToECIProvider() {
		return ecefToECIProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getECEFToECIProvider_Time() {
		return (EAttribute)ecefToECIProviderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getECEFToECIProvider__Calc() {
		return ecefToECIProviderEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPointMassGravity() {
		return pointMassGravityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPointMassGravity_Position() {
		return (EAttribute)pointMassGravityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPointMassGravity_Acceleration() {
		return (EAttribute)pointMassGravityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getPointMassGravity__Calc() {
		return pointMassGravityEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFixedAttitude() {
		return fixedAttitudeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixedAttitude_Position() {
		return (EAttribute)fixedAttitudeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixedAttitude_Speed() {
		return (EAttribute)fixedAttitudeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixedAttitude_Quaternion() {
		return (EAttribute)fixedAttitudeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFixedAttitude__Calc() {
		return fixedAttitudeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSCLocalToECIProvider() {
		return scLocalToECIProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSCLocalToECIProvider_Quaternion() {
		return (EAttribute)scLocalToECIProviderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSCLocalToECIProvider__Calc() {
		return scLocalToECIProviderEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getECItoSCLocalProvider() {
		return ecItoSCLocalProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getECItoSCLocalProvider_Quaternion() {
		return (EAttribute)ecItoSCLocalProviderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getECItoSCLocalProvider__Calc() {
		return ecItoSCLocalProviderEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttitudeModel() {
		return attitudeModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAttitudeModel_Quaternion() {
		return (EAttribute)attitudeModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAttitudeModel_AngularVelocity() {
		return (EAttribute)attitudeModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAttitudeModel_InertiaMatrix() {
		return (EAttribute)attitudeModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAttitudeModel__Calc() {
		return attitudeModelEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRFLink() {
		return rfLinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRFLink_Azelra() {
		return (EAttribute)rfLinkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRFLink_Attenuation() {
		return (EAttribute)rfLinkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRFLink_Frequency() {
		return (EAttribute)rfLinkEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRFLink_Doppler() {
		return (EAttribute)rfLinkEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRFLink_RxLocation() {
		return (EAttribute)rfLinkEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRFLink_RxSpeed() {
		return (EAttribute)rfLinkEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRFLink_TxLocation() {
		return (EAttribute)rfLinkEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRFLink_TxSpeed() {
		return (EAttribute)rfLinkEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRFLink_TxPower() {
		return (EAttribute)rfLinkEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRFLink_RxPower() {
		return (EAttribute)rfLinkEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRFLink__Calc() {
		return rfLinkEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getElectricalLoad() {
		return electricalLoadEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getElectricalLoad_Resistance() {
		return (EAttribute)electricalLoadEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getElectricalLoad_Power() {
		return (EAttribute)electricalLoadEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getElectricalLoad__Calc() {
		return electricalLoadEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSolarCell() {
		return solarCellEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSolarCell_IShortCircuit() {
		return (EAttribute)solarCellEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSolarCell_Temperature() {
		return (EAttribute)solarCellEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSolarCell_K() {
		return (EAttribute)solarCellEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSolarCell_SolarFlux() {
		return (EAttribute)solarCellEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSolarCell_IPhotoCurrent() {
		return (EAttribute)solarCellEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSolarCell__Calc() {
		return solarCellEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReceiver() {
		return receiverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReceiver_Frequency() {
		return (EAttribute)receiverEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReceiver_Power() {
		return (EAttribute)receiverEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReceiver_Gain() {
		return (EAttribute)receiverEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReceiver_RxPower() {
		return (EAttribute)receiverEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getReceiver__Calc() {
		return receiverEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransmitter() {
		return transmitterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransmitter_Frequency() {
		return (EAttribute)transmitterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransmitter_Power() {
		return (EAttribute)transmitterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransmitter_Gain() {
		return (EAttribute)transmitterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransmitter_TxPower() {
		return (EAttribute)transmitterEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTransmitter__Calc() {
		return transmitterEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getJATGravityModel() {
		return jatGravityModelEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpaceFactory getSpaceFactory() {
		return (SpaceFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		timeEClass = createEClass(TIME);
		createEAttribute(timeEClass, TIME__TIME);
		createEAttribute(timeEClass, TIME__DTIME);
		createEOperation(timeEClass, TIME___CALC);

		gravityFieldEClass = createEClass(GRAVITY_FIELD);
		createEAttribute(gravityFieldEClass, GRAVITY_FIELD__POSITION);
		createEAttribute(gravityFieldEClass, GRAVITY_FIELD__COEFFICIENT_FILE_PATH);
		createEAttribute(gravityFieldEClass, GRAVITY_FIELD__ACCELERATION);
		createEAttribute(gravityFieldEClass, GRAVITY_FIELD__GRAVITY_FIELD);
		createEOperation(gravityFieldEClass, GRAVITY_FIELD___CALC);

		magneticFieldEClass = createEClass(MAGNETIC_FIELD);
		createEAttribute(magneticFieldEClass, MAGNETIC_FIELD__LAT_LON_ALT);
		createEAttribute(magneticFieldEClass, MAGNETIC_FIELD__COEFFICIENT_FILE_PATH);
		createEOperation(magneticFieldEClass, MAGNETIC_FIELD___CALC);

		pointMassEClass = createEClass(POINT_MASS);
		createEAttribute(pointMassEClass, POINT_MASS__MASS);
		createEAttribute(pointMassEClass, POINT_MASS__POSITION);
		createEAttribute(pointMassEClass, POINT_MASS__SPEED);
		createEOperation(pointMassEClass, POINT_MASS___CALC);

		thermalNodeEClass = createEClass(THERMAL_NODE);
		createEAttribute(thermalNodeEClass, THERMAL_NODE__TEMPERATURE);
		createEAttribute(thermalNodeEClass, THERMAL_NODE__Q);
		createEAttribute(thermalNodeEClass, THERMAL_NODE__DTEMPERATURE);
		createEAttribute(thermalNodeEClass, THERMAL_NODE__HEAT_CAPACITY);
		createEOperation(thermalNodeEClass, THERMAL_NODE___CALC);

		thermalConnectionEClass = createEClass(THERMAL_CONNECTION);
		createEAttribute(thermalConnectionEClass, THERMAL_CONNECTION__HEAT_CONDUCTIVITY);
		createEAttribute(thermalConnectionEClass, THERMAL_CONNECTION__HEAT_FLOW);
		createEAttribute(thermalConnectionEClass, THERMAL_CONNECTION__TEMPERATURE);
		createEOperation(thermalConnectionEClass, THERMAL_CONNECTION___CALC);

		eciToECEFProviderEClass = createEClass(ECI_TO_ECEF_PROVIDER);
		createEAttribute(eciToECEFProviderEClass, ECI_TO_ECEF_PROVIDER__TIME);
		createEOperation(eciToECEFProviderEClass, ECI_TO_ECEF_PROVIDER___CALC);

		ecefToLatLonAltProviderEClass = createEClass(ECEF_TO_LAT_LON_ALT_PROVIDER);
		createEOperation(ecefToLatLonAltProviderEClass, ECEF_TO_LAT_LON_ALT_PROVIDER___CALC);

		ecefToECIProviderEClass = createEClass(ECEF_TO_ECI_PROVIDER);
		createEAttribute(ecefToECIProviderEClass, ECEF_TO_ECI_PROVIDER__TIME);
		createEOperation(ecefToECIProviderEClass, ECEF_TO_ECI_PROVIDER___CALC);

		pointMassGravityEClass = createEClass(POINT_MASS_GRAVITY);
		createEAttribute(pointMassGravityEClass, POINT_MASS_GRAVITY__POSITION);
		createEAttribute(pointMassGravityEClass, POINT_MASS_GRAVITY__ACCELERATION);
		createEOperation(pointMassGravityEClass, POINT_MASS_GRAVITY___CALC);

		fixedAttitudeEClass = createEClass(FIXED_ATTITUDE);
		createEAttribute(fixedAttitudeEClass, FIXED_ATTITUDE__POSITION);
		createEAttribute(fixedAttitudeEClass, FIXED_ATTITUDE__SPEED);
		createEAttribute(fixedAttitudeEClass, FIXED_ATTITUDE__QUATERNION);
		createEOperation(fixedAttitudeEClass, FIXED_ATTITUDE___CALC);

		scLocalToECIProviderEClass = createEClass(SC_LOCAL_TO_ECI_PROVIDER);
		createEAttribute(scLocalToECIProviderEClass, SC_LOCAL_TO_ECI_PROVIDER__QUATERNION);
		createEOperation(scLocalToECIProviderEClass, SC_LOCAL_TO_ECI_PROVIDER___CALC);

		ecItoSCLocalProviderEClass = createEClass(EC_ITO_SC_LOCAL_PROVIDER);
		createEAttribute(ecItoSCLocalProviderEClass, EC_ITO_SC_LOCAL_PROVIDER__QUATERNION);
		createEOperation(ecItoSCLocalProviderEClass, EC_ITO_SC_LOCAL_PROVIDER___CALC);

		attitudeModelEClass = createEClass(ATTITUDE_MODEL);
		createEAttribute(attitudeModelEClass, ATTITUDE_MODEL__QUATERNION);
		createEAttribute(attitudeModelEClass, ATTITUDE_MODEL__ANGULAR_VELOCITY);
		createEAttribute(attitudeModelEClass, ATTITUDE_MODEL__INERTIA_MATRIX);
		createEOperation(attitudeModelEClass, ATTITUDE_MODEL___CALC);

		rfLinkEClass = createEClass(RF_LINK);
		createEAttribute(rfLinkEClass, RF_LINK__AZELRA);
		createEAttribute(rfLinkEClass, RF_LINK__ATTENUATION);
		createEAttribute(rfLinkEClass, RF_LINK__FREQUENCY);
		createEAttribute(rfLinkEClass, RF_LINK__DOPPLER);
		createEAttribute(rfLinkEClass, RF_LINK__RX_LOCATION);
		createEAttribute(rfLinkEClass, RF_LINK__RX_SPEED);
		createEAttribute(rfLinkEClass, RF_LINK__TX_LOCATION);
		createEAttribute(rfLinkEClass, RF_LINK__TX_SPEED);
		createEAttribute(rfLinkEClass, RF_LINK__TX_POWER);
		createEAttribute(rfLinkEClass, RF_LINK__RX_POWER);
		createEOperation(rfLinkEClass, RF_LINK___CALC);

		electricalLoadEClass = createEClass(ELECTRICAL_LOAD);
		createEAttribute(electricalLoadEClass, ELECTRICAL_LOAD__RESISTANCE);
		createEAttribute(electricalLoadEClass, ELECTRICAL_LOAD__POWER);
		createEOperation(electricalLoadEClass, ELECTRICAL_LOAD___CALC);

		solarCellEClass = createEClass(SOLAR_CELL);
		createEAttribute(solarCellEClass, SOLAR_CELL__ISHORT_CIRCUIT);
		createEAttribute(solarCellEClass, SOLAR_CELL__TEMPERATURE);
		createEAttribute(solarCellEClass, SOLAR_CELL__K);
		createEAttribute(solarCellEClass, SOLAR_CELL__SOLAR_FLUX);
		createEAttribute(solarCellEClass, SOLAR_CELL__IPHOTO_CURRENT);
		createEOperation(solarCellEClass, SOLAR_CELL___CALC);

		receiverEClass = createEClass(RECEIVER);
		createEAttribute(receiverEClass, RECEIVER__FREQUENCY);
		createEAttribute(receiverEClass, RECEIVER__POWER);
		createEAttribute(receiverEClass, RECEIVER__GAIN);
		createEAttribute(receiverEClass, RECEIVER__RX_POWER);
		createEOperation(receiverEClass, RECEIVER___CALC);

		transmitterEClass = createEClass(TRANSMITTER);
		createEAttribute(transmitterEClass, TRANSMITTER__FREQUENCY);
		createEAttribute(transmitterEClass, TRANSMITTER__POWER);
		createEAttribute(transmitterEClass, TRANSMITTER__GAIN);
		createEAttribute(transmitterEClass, TRANSMITTER__TX_POWER);
		createEOperation(transmitterEClass, TRANSMITTER___CALC);

		// Create data types
		jatGravityModelEDataType = createEDataType(JAT_GRAVITY_MODEL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		MeshPackage theMeshPackage = (MeshPackage)EPackage.Registry.INSTANCE.getEPackage(MeshPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		timeEClass.getESuperTypes().add(theMeshPackage.getModel());
		gravityFieldEClass.getESuperTypes().add(theMeshPackage.getModel());
		magneticFieldEClass.getESuperTypes().add(theMeshPackage.getModel());
		pointMassEClass.getESuperTypes().add(theMeshPackage.getModel());
		thermalNodeEClass.getESuperTypes().add(theMeshPackage.getModel());
		thermalConnectionEClass.getESuperTypes().add(theMeshPackage.getConnection());
		eciToECEFProviderEClass.getESuperTypes().add(theMeshPackage.getProvider());
		ecefToLatLonAltProviderEClass.getESuperTypes().add(theMeshPackage.getProvider());
		ecefToECIProviderEClass.getESuperTypes().add(theMeshPackage.getProvider());
		pointMassGravityEClass.getESuperTypes().add(theMeshPackage.getModel());
		fixedAttitudeEClass.getESuperTypes().add(theMeshPackage.getModel());
		scLocalToECIProviderEClass.getESuperTypes().add(theMeshPackage.getProvider());
		ecItoSCLocalProviderEClass.getESuperTypes().add(theMeshPackage.getProvider());
		attitudeModelEClass.getESuperTypes().add(theMeshPackage.getModel());
		rfLinkEClass.getESuperTypes().add(theMeshPackage.getModel());
		electricalLoadEClass.getESuperTypes().add(theMeshPackage.getModel());
		solarCellEClass.getESuperTypes().add(theMeshPackage.getModel());
		receiverEClass.getESuperTypes().add(theMeshPackage.getModel());
		transmitterEClass.getESuperTypes().add(theMeshPackage.getModel());

		// Initialize classes, features, and operations; add parameters
		initEClass(timeEClass, Time.class, "Time", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTime_Time(), ecorePackage.getEDoubleObject(), "time", null, 1, 1, Time.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTime_DTime(), ecorePackage.getEDoubleObject(), "dTime", null, 1, 1, Time.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getTime__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(gravityFieldEClass, GravityField.class, "GravityField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGravityField_Position(), ecorePackage.getEDoubleObject(), "position", null, 3, 3, GravityField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getGravityField_CoefficientFilePath(), ecorePackage.getEString(), "coefficientFilePath", null, 1, 1, GravityField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGravityField_Acceleration(), ecorePackage.getEDoubleObject(), "acceleration", null, 3, 3, GravityField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getGravityField_GravityField(), this.getJATGravityModel(), "gravityField", null, 1, 1, GravityField.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEOperation(getGravityField__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(magneticFieldEClass, MagneticField.class, "MagneticField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMagneticField_LatLonAlt(), ecorePackage.getEDoubleObject(), "latLonAlt", null, 3, 3, MagneticField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getMagneticField_CoefficientFilePath(), ecorePackage.getEString(), "coefficientFilePath", null, 1, 1, MagneticField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getMagneticField__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(pointMassEClass, PointMass.class, "PointMass", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPointMass_Mass(), ecorePackage.getEDoubleObject(), "mass", null, 1, 1, PointMass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPointMass_Position(), ecorePackage.getEDoubleObject(), "position", null, 3, 3, PointMass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getPointMass_Speed(), ecorePackage.getEDoubleObject(), "speed", null, 3, 3, PointMass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEOperation(getPointMass__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(thermalNodeEClass, ThermalNode.class, "ThermalNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getThermalNode_Temperature(), ecorePackage.getEDoubleObject(), "temperature", null, 1, 1, ThermalNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getThermalNode_Q(), ecorePackage.getEDoubleObject(), "q", null, 1, -1, ThermalNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getThermalNode_DTemperature(), ecorePackage.getEDoubleObject(), "dTemperature", null, 1, 1, ThermalNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getThermalNode_HeatCapacity(), ecorePackage.getEDoubleObject(), "heatCapacity", null, 1, 1, ThermalNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getThermalNode__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(thermalConnectionEClass, ThermalConnection.class, "ThermalConnection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getThermalConnection_HeatConductivity(), ecorePackage.getEDoubleObject(), "heatConductivity", null, 1, 1, ThermalConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getThermalConnection_HeatFlow(), ecorePackage.getEDoubleObject(), "heatFlow", null, 2, 2, ThermalConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getThermalConnection_Temperature(), ecorePackage.getEDoubleObject(), "temperature", null, 2, 2, ThermalConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEOperation(getThermalConnection__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(eciToECEFProviderEClass, ECIToECEFProvider.class, "ECIToECEFProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getECIToECEFProvider_Time(), ecorePackage.getEDoubleObject(), "time", null, 1, 1, ECIToECEFProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getECIToECEFProvider__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(ecefToLatLonAltProviderEClass, ECEFToLatLonAltProvider.class, "ECEFToLatLonAltProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getECEFToLatLonAltProvider__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(ecefToECIProviderEClass, ECEFToECIProvider.class, "ECEFToECIProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getECEFToECIProvider_Time(), ecorePackage.getEDoubleObject(), "time", null, 1, 1, ECEFToECIProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getECEFToECIProvider__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(pointMassGravityEClass, PointMassGravity.class, "PointMassGravity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPointMassGravity_Position(), ecorePackage.getEDoubleObject(), "position", null, 3, 3, PointMassGravity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getPointMassGravity_Acceleration(), ecorePackage.getEDoubleObject(), "acceleration", null, 3, 3, PointMassGravity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEOperation(getPointMassGravity__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(fixedAttitudeEClass, FixedAttitude.class, "FixedAttitude", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFixedAttitude_Position(), ecorePackage.getEDoubleObject(), "position", null, 3, 3, FixedAttitude.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getFixedAttitude_Speed(), ecorePackage.getEDoubleObject(), "speed", null, 3, 3, FixedAttitude.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getFixedAttitude_Quaternion(), ecorePackage.getEDoubleObject(), "quaternion", null, 4, 4, FixedAttitude.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEOperation(getFixedAttitude__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(scLocalToECIProviderEClass, SCLocalToECIProvider.class, "SCLocalToECIProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSCLocalToECIProvider_Quaternion(), ecorePackage.getEDoubleObject(), "quaternion", null, 4, 4, SCLocalToECIProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEOperation(getSCLocalToECIProvider__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(ecItoSCLocalProviderEClass, ECItoSCLocalProvider.class, "ECItoSCLocalProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getECItoSCLocalProvider_Quaternion(), ecorePackage.getEDoubleObject(), "quaternion", null, 4, 4, ECItoSCLocalProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEOperation(getECItoSCLocalProvider__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(attitudeModelEClass, AttitudeModel.class, "AttitudeModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAttitudeModel_Quaternion(), ecorePackage.getEDoubleObject(), "quaternion", null, 4, 4, AttitudeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getAttitudeModel_AngularVelocity(), ecorePackage.getEDoubleObject(), "angularVelocity", null, 3, 3, AttitudeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getAttitudeModel_InertiaMatrix(), ecorePackage.getEDoubleObject(), "inertiaMatrix", null, 9, 9, AttitudeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEOperation(getAttitudeModel__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(rfLinkEClass, RFLink.class, "RFLink", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRFLink_Azelra(), ecorePackage.getEDoubleObject(), "azelra", null, 3, 3, RFLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRFLink_Attenuation(), ecorePackage.getEDoubleObject(), "attenuation", null, 1, 1, RFLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRFLink_Frequency(), ecorePackage.getEDoubleObject(), "frequency", null, 1, 1, RFLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRFLink_Doppler(), ecorePackage.getEDoubleObject(), "doppler", null, 1, 1, RFLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRFLink_RxLocation(), ecorePackage.getEDoubleObject(), "rxLocation", null, 3, 3, RFLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRFLink_RxSpeed(), ecorePackage.getEDoubleObject(), "rxSpeed", null, 3, 3, RFLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRFLink_TxLocation(), ecorePackage.getEDoubleObject(), "txLocation", null, 3, 3, RFLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRFLink_TxSpeed(), ecorePackage.getEDoubleObject(), "txSpeed", null, 3, 3, RFLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRFLink_TxPower(), ecorePackage.getEDoubleObject(), "txPower", null, 1, 1, RFLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRFLink_RxPower(), ecorePackage.getEDoubleObject(), "rxPower", null, 1, 1, RFLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEOperation(getRFLink__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(electricalLoadEClass, ElectricalLoad.class, "ElectricalLoad", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getElectricalLoad_Resistance(), ecorePackage.getEDoubleObject(), "resistance", null, 1, 1, ElectricalLoad.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getElectricalLoad_Power(), ecorePackage.getEDoubleObject(), "power", null, 1, 1, ElectricalLoad.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEOperation(getElectricalLoad__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(solarCellEClass, SolarCell.class, "SolarCell", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSolarCell_IShortCircuit(), ecorePackage.getEDoubleObject(), "iShortCircuit", null, 1, 1, SolarCell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getSolarCell_Temperature(), ecorePackage.getEDoubleObject(), "temperature", null, 1, 1, SolarCell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getSolarCell_K(), ecorePackage.getEDoubleObject(), "k", null, 1, 1, SolarCell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getSolarCell_SolarFlux(), ecorePackage.getEDoubleObject(), "solarFlux", null, 1, 1, SolarCell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getSolarCell_IPhotoCurrent(), ecorePackage.getEDoubleObject(), "iPhotoCurrent", null, 1, 1, SolarCell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEOperation(getSolarCell__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(receiverEClass, Receiver.class, "Receiver", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getReceiver_Frequency(), ecorePackage.getEDoubleObject(), "frequency", null, 1, 1, Receiver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getReceiver_Power(), ecorePackage.getEDoubleObject(), "power", null, 1, 1, Receiver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getReceiver_Gain(), ecorePackage.getEDoubleObject(), "gain", null, 1, 1, Receiver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getReceiver_RxPower(), ecorePackage.getEDoubleObject(), "rxPower", null, 1, 1, Receiver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEOperation(getReceiver__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(transmitterEClass, Transmitter.class, "Transmitter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTransmitter_Frequency(), ecorePackage.getEDoubleObject(), "frequency", null, 1, 1, Transmitter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTransmitter_Power(), ecorePackage.getEDoubleObject(), "power", null, 1, 1, Transmitter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTransmitter_Gain(), ecorePackage.getEDoubleObject(), "gain", null, 1, 1, Transmitter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTransmitter_TxPower(), ecorePackage.getEDoubleObject(), "txPower", null, 1, 1, Transmitter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEOperation(getTransmitter__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		// Initialize data types
		initEDataType(jatGravityModelEDataType, GravityModel.class, "JATGravityModel", !IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// ThirdEye
		createThirdEyeAnnotations();
	}

	/**
	 * Initializes the annotations for <b>ThirdEye</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createThirdEyeAnnotations() {
		String source = "ThirdEye";	
		addAnnotation
		  (getTime_Time(), 
		   source, 
		   new String[] {
			 "unit", "s"
		   });	
		addAnnotation
		  (getTime_DTime(), 
		   source, 
		   new String[] {
			 "unit", "s/step"
		   });	
		addAnnotation
		  (getGravityField_Position(), 
		   source, 
		   new String[] {
			 "unit", "m,m,m"
		   });	
		addAnnotation
		  (getGravityField_Acceleration(), 
		   source, 
		   new String[] {
			 "unit", "m/s^2,m/s^2,m/s^2"
		   });	
		addAnnotation
		  (getMagneticField_LatLonAlt(), 
		   source, 
		   new String[] {
			 "unit", "rad,rad,m"
		   });	
		addAnnotation
		  (getPointMass_Mass(), 
		   source, 
		   new String[] {
			 "unit", "kg"
		   });	
		addAnnotation
		  (getPointMass_Position(), 
		   source, 
		   new String[] {
			 "unit", "m,m,m"
		   });	
		addAnnotation
		  (getPointMass_Speed(), 
		   source, 
		   new String[] {
			 "unit", "m/s,m/s,m/s"
		   });	
		addAnnotation
		  (getThermalNode_Temperature(), 
		   source, 
		   new String[] {
			 "unit", "K"
		   });	
		addAnnotation
		  (getThermalNode_Q(), 
		   source, 
		   new String[] {
			 "unit", "J/s"
		   });	
		addAnnotation
		  (getThermalNode_DTemperature(), 
		   source, 
		   new String[] {
			 "unit", "K/s"
		   });	
		addAnnotation
		  (getThermalNode_HeatCapacity(), 
		   source, 
		   new String[] {
			 "unit", "J/K"
		   });	
		addAnnotation
		  (getThermalConnection_HeatConductivity(), 
		   source, 
		   new String[] {
			 "unit", "J/(s*K)"
		   });	
		addAnnotation
		  (getThermalConnection_HeatFlow(), 
		   source, 
		   new String[] {
			 "unit", "J/s,J/s"
		   });	
		addAnnotation
		  (getThermalConnection_Temperature(), 
		   source, 
		   new String[] {
			 "unit", "K,K"
		   });	
		addAnnotation
		  (getECIToECEFProvider_Time(), 
		   source, 
		   new String[] {
			 "unit", "s"
		   });	
		addAnnotation
		  (getECEFToECIProvider_Time(), 
		   source, 
		   new String[] {
			 "unit", "s"
		   });	
		addAnnotation
		  (getPointMassGravity_Position(), 
		   source, 
		   new String[] {
			 "unit", "m,m,m"
		   });	
		addAnnotation
		  (getPointMassGravity_Acceleration(), 
		   source, 
		   new String[] {
			 "unit", "m/s^2,m/s^2,m/s^2"
		   });	
		addAnnotation
		  (getFixedAttitude_Position(), 
		   source, 
		   new String[] {
			 "unit", "m,m,m"
		   });	
		addAnnotation
		  (getFixedAttitude_Speed(), 
		   source, 
		   new String[] {
			 "unit", "m/s,m/s,m/s"
		   });	
		addAnnotation
		  (getFixedAttitude_Quaternion(), 
		   source, 
		   new String[] {
			 "unit", "none,none,none,none"
		   });	
		addAnnotation
		  (getSCLocalToECIProvider_Quaternion(), 
		   source, 
		   new String[] {
			 "unit", "none,none,none,none"
		   });	
		addAnnotation
		  (getECItoSCLocalProvider_Quaternion(), 
		   source, 
		   new String[] {
			 "unit", "none,none,none,none"
		   });	
		addAnnotation
		  (getAttitudeModel_Quaternion(), 
		   source, 
		   new String[] {
			 "unit", "none,none,none,none"
		   });	
		addAnnotation
		  (getAttitudeModel_AngularVelocity(), 
		   source, 
		   new String[] {
			 "unit", "rad/s,rad/s,rad/s"
		   });	
		addAnnotation
		  (getRFLink_Azelra(), 
		   source, 
		   new String[] {
			 "unit", "rad,rad,m"
		   });	
		addAnnotation
		  (getRFLink_Attenuation(), 
		   source, 
		   new String[] {
			 "unit", "dB"
		   });	
		addAnnotation
		  (getRFLink_Frequency(), 
		   source, 
		   new String[] {
			 "unit", "Hz"
		   });	
		addAnnotation
		  (getRFLink_Doppler(), 
		   source, 
		   new String[] {
			 "unit", "Hz"
		   });	
		addAnnotation
		  (getRFLink_RxLocation(), 
		   source, 
		   new String[] {
			 "unit", "m,m,m"
		   });	
		addAnnotation
		  (getRFLink_RxSpeed(), 
		   source, 
		   new String[] {
			 "unit", "m/s,m/s,m/s"
		   });	
		addAnnotation
		  (getRFLink_TxLocation(), 
		   source, 
		   new String[] {
			 "unit", "m,m,m"
		   });	
		addAnnotation
		  (getRFLink_TxSpeed(), 
		   source, 
		   new String[] {
			 "unit", "m/s,m/s,m/s"
		   });	
		addAnnotation
		  (getRFLink_TxPower(), 
		   source, 
		   new String[] {
			 "unit", "dB"
		   });	
		addAnnotation
		  (getRFLink_RxPower(), 
		   source, 
		   new String[] {
			 "unit", "dB"
		   });	
		addAnnotation
		  (getReceiver_Frequency(), 
		   source, 
		   new String[] {
			 "unit", "Hz"
		   });	
		addAnnotation
		  (getReceiver_Power(), 
		   source, 
		   new String[] {
			 "unit", "W"
		   });	
		addAnnotation
		  (getReceiver_Gain(), 
		   source, 
		   new String[] {
			 "unit", "dBi"
		   });	
		addAnnotation
		  (getReceiver_RxPower(), 
		   source, 
		   new String[] {
			 "unit", "dBm"
		   });	
		addAnnotation
		  (getTransmitter_Frequency(), 
		   source, 
		   new String[] {
			 "unit", "Hz"
		   });	
		addAnnotation
		  (getTransmitter_Power(), 
		   source, 
		   new String[] {
			 "unit", "W"
		   });	
		addAnnotation
		  (getTransmitter_Gain(), 
		   source, 
		   new String[] {
			 "unit", "dBi"
		   });	
		addAnnotation
		  (getTransmitter_TxPower(), 
		   source, 
		   new String[] {
			 "unit", "dBm"
		   });
	}

} //SpacePackageImpl
