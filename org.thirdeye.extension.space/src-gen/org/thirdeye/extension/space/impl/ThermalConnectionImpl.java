/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.thirdeye.core.mesh.impl.ConnectionImpl;
import org.thirdeye.extension.space.SpacePackage;
import org.thirdeye.extension.space.ThermalConnection;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Thermal Connection</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.impl.ThermalConnectionImpl#getHeatConductivity <em>Heat Conductivity</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.ThermalConnectionImpl#getHeatFlowList <em>Heat Flow</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.ThermalConnectionImpl#getTemperatureList <em>Temperature</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ThermalConnectionImpl extends ConnectionImpl implements ThermalConnection {
	/**
	 * The default value of the '{@link #getHeatConductivity() <em>Heat Conductivity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeatConductivity()
	 * @generated
	 * @ordered
	 */
	protected static final Double HEAT_CONDUCTIVITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHeatConductivity() <em>Heat Conductivity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeatConductivity()
	 * @generated
	 * @ordered
	 */
	protected Double heatConductivity = HEAT_CONDUCTIVITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHeatFlowList() <em>Heat Flow</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeatFlowList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> heatFlow;

	/**
	 * The empty value for the '{@link #getHeatFlow() <em>Heat Flow</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeatFlow()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] HEAT_FLOW_EEMPTY_ARRAY = new Double [0];

	/**
	 * The cached value of the '{@link #getTemperatureList() <em>Temperature</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemperatureList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> temperature;

	/**
	 * The empty value for the '{@link #getTemperature() <em>Temperature</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemperature()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] TEMPERATURE_EEMPTY_ARRAY = new Double [0];

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ThermalConnectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpacePackage.Literals.THERMAL_CONNECTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getHeatConductivity() {
		return heatConductivity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHeatConductivity(Double newHeatConductivity) {
		Double oldHeatConductivity = heatConductivity;
		heatConductivity = newHeatConductivity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.THERMAL_CONNECTION__HEAT_CONDUCTIVITY, oldHeatConductivity, heatConductivity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getHeatFlow() {
		if (heatFlow == null || heatFlow.isEmpty()) return HEAT_FLOW_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)heatFlow;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getHeatFlow(int index) {
		return getHeatFlowList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getHeatFlowLength() {
		return heatFlow == null ? 0 : heatFlow.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHeatFlow(Double[] newHeatFlow) {
		((BasicEList<Double>)getHeatFlowList()).setData(newHeatFlow.length, newHeatFlow);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHeatFlow(int index, Double element) {
		getHeatFlowList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getHeatFlowList() {
		if (heatFlow == null) {
			heatFlow = new EDataTypeEList<Double>(Double.class, this, SpacePackage.THERMAL_CONNECTION__HEAT_FLOW);
		}
		return heatFlow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getTemperature() {
		if (temperature == null || temperature.isEmpty()) return TEMPERATURE_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)temperature;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getTemperature(int index) {
		return getTemperatureList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getTemperatureLength() {
		return temperature == null ? 0 : temperature.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTemperature(Double[] newTemperature) {
		((BasicEList<Double>)getTemperatureList()).setData(newTemperature.length, newTemperature);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTemperature(int index, Double element) {
		getTemperatureList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getTemperatureList() {
		if (temperature == null) {
			temperature = new EDataTypeEList<Double>(Double.class, this, SpacePackage.THERMAL_CONNECTION__TEMPERATURE);
		}
		return temperature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void calc() {
		double deltaT = (double)getTemperature(0) - (double)getTemperature(1);
		double flow = deltaT * getHeatConductivity();
		setHeatFlow(0,-flow);
		setHeatFlow(1,flow);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpacePackage.THERMAL_CONNECTION__HEAT_CONDUCTIVITY:
				return getHeatConductivity();
			case SpacePackage.THERMAL_CONNECTION__HEAT_FLOW:
				return getHeatFlowList();
			case SpacePackage.THERMAL_CONNECTION__TEMPERATURE:
				return getTemperatureList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpacePackage.THERMAL_CONNECTION__HEAT_CONDUCTIVITY:
				setHeatConductivity((Double)newValue);
				return;
			case SpacePackage.THERMAL_CONNECTION__HEAT_FLOW:
				getHeatFlowList().clear();
				getHeatFlowList().addAll((Collection<? extends Double>)newValue);
				return;
			case SpacePackage.THERMAL_CONNECTION__TEMPERATURE:
				getTemperatureList().clear();
				getTemperatureList().addAll((Collection<? extends Double>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpacePackage.THERMAL_CONNECTION__HEAT_CONDUCTIVITY:
				setHeatConductivity(HEAT_CONDUCTIVITY_EDEFAULT);
				return;
			case SpacePackage.THERMAL_CONNECTION__HEAT_FLOW:
				getHeatFlowList().clear();
				return;
			case SpacePackage.THERMAL_CONNECTION__TEMPERATURE:
				getTemperatureList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpacePackage.THERMAL_CONNECTION__HEAT_CONDUCTIVITY:
				return HEAT_CONDUCTIVITY_EDEFAULT == null ? heatConductivity != null : !HEAT_CONDUCTIVITY_EDEFAULT.equals(heatConductivity);
			case SpacePackage.THERMAL_CONNECTION__HEAT_FLOW:
				return heatFlow != null && !heatFlow.isEmpty();
			case SpacePackage.THERMAL_CONNECTION__TEMPERATURE:
				return temperature != null && !temperature.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case SpacePackage.THERMAL_CONNECTION___CALC:
				calc();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (heatConductivity: ");
		result.append(heatConductivity);
		result.append(", heatFlow: ");
		result.append(heatFlow);
		result.append(", temperature: ");
		result.append(temperature);
		result.append(')');
		return result.toString();
	}

} //ThermalConnectionImpl
