/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.thirdeye.core.mesh.impl.ModelImpl;
import org.thirdeye.extension.space.MagneticField;
import org.thirdeye.extension.space.SpacePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Magnetic Field</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.impl.MagneticFieldImpl#getLatLonAltList <em>Lat Lon Alt</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.MagneticFieldImpl#getCoefficientFilePath <em>Coefficient File Path</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MagneticFieldImpl extends ModelImpl implements MagneticField {
	/**
	 * The cached value of the '{@link #getLatLonAltList() <em>Lat Lon Alt</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLatLonAltList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> latLonAlt;

	/**
	 * The empty value for the '{@link #getLatLonAlt() <em>Lat Lon Alt</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLatLonAlt()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] LAT_LON_ALT_EEMPTY_ARRAY = new Double [0];

	/**
	 * The default value of the '{@link #getCoefficientFilePath() <em>Coefficient File Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoefficientFilePath()
	 * @generated
	 * @ordered
	 */
	protected static final String COEFFICIENT_FILE_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCoefficientFilePath() <em>Coefficient File Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoefficientFilePath()
	 * @generated
	 * @ordered
	 */
	protected String coefficientFilePath = COEFFICIENT_FILE_PATH_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MagneticFieldImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpacePackage.Literals.MAGNETIC_FIELD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getLatLonAlt() {
		if (latLonAlt == null || latLonAlt.isEmpty()) return LAT_LON_ALT_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)latLonAlt;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getLatLonAlt(int index) {
		return getLatLonAltList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLatLonAltLength() {
		return latLonAlt == null ? 0 : latLonAlt.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLatLonAlt(Double[] newLatLonAlt) {
		((BasicEList<Double>)getLatLonAltList()).setData(newLatLonAlt.length, newLatLonAlt);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLatLonAlt(int index, Double element) {
		getLatLonAltList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getLatLonAltList() {
		if (latLonAlt == null) {
			latLonAlt = new EDataTypeEList<Double>(Double.class, this, SpacePackage.MAGNETIC_FIELD__LAT_LON_ALT);
		}
		return latLonAlt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCoefficientFilePath() {
		return coefficientFilePath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCoefficientFilePath(String newCoefficientFilePath) {
		String oldCoefficientFilePath = coefficientFilePath;
		coefficientFilePath = newCoefficientFilePath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.MAGNETIC_FIELD__COEFFICIENT_FILE_PATH, oldCoefficientFilePath, coefficientFilePath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpacePackage.MAGNETIC_FIELD__LAT_LON_ALT:
				return getLatLonAltList();
			case SpacePackage.MAGNETIC_FIELD__COEFFICIENT_FILE_PATH:
				return getCoefficientFilePath();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpacePackage.MAGNETIC_FIELD__LAT_LON_ALT:
				getLatLonAltList().clear();
				getLatLonAltList().addAll((Collection<? extends Double>)newValue);
				return;
			case SpacePackage.MAGNETIC_FIELD__COEFFICIENT_FILE_PATH:
				setCoefficientFilePath((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpacePackage.MAGNETIC_FIELD__LAT_LON_ALT:
				getLatLonAltList().clear();
				return;
			case SpacePackage.MAGNETIC_FIELD__COEFFICIENT_FILE_PATH:
				setCoefficientFilePath(COEFFICIENT_FILE_PATH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpacePackage.MAGNETIC_FIELD__LAT_LON_ALT:
				return latLonAlt != null && !latLonAlt.isEmpty();
			case SpacePackage.MAGNETIC_FIELD__COEFFICIENT_FILE_PATH:
				return COEFFICIENT_FILE_PATH_EDEFAULT == null ? coefficientFilePath != null : !COEFFICIENT_FILE_PATH_EDEFAULT.equals(coefficientFilePath);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (latLonAlt: ");
		result.append(latLonAlt);
		result.append(", coefficientFilePath: ");
		result.append(coefficientFilePath);
		result.append(')');
		return result.toString();
	}

	@Override
	public void calc() {
		// TODO Auto-generated method stub
		
	}

} //MagneticFieldImpl
