/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.thirdeye.core.mesh.impl.ModelImpl;
import org.thirdeye.extension.space.SpacePackage;
import org.thirdeye.extension.space.ThermalNode;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Thermal Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.impl.ThermalNodeImpl#getTemperature <em>Temperature</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.ThermalNodeImpl#getQList <em>Q</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.ThermalNodeImpl#getDTemperature <em>DTemperature</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.ThermalNodeImpl#getHeatCapacity <em>Heat Capacity</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ThermalNodeImpl extends ModelImpl implements ThermalNode {
	
	/**
	 * The default value of the '{@link #getTemperature() <em>Temperature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemperature()
	 * @generated
	 * @ordered
	 */
	protected static final Double TEMPERATURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTemperature() <em>Temperature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemperature()
	 * @generated
	 * @ordered
	 */
	protected Double temperature = TEMPERATURE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getQList() <em>Q</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> q;

	/**
	 * The empty value for the '{@link #getQ() <em>Q</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQ()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] Q_EEMPTY_ARRAY = new Double [0];

	/**
	 * The default value of the '{@link #getDTemperature() <em>DTemperature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDTemperature()
	 * @generated
	 * @ordered
	 */
	protected static final Double DTEMPERATURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDTemperature() <em>DTemperature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDTemperature()
	 * @generated
	 * @ordered
	 */
	protected Double dTemperature = DTEMPERATURE_EDEFAULT;

	/**
	 * The default value of the '{@link #getHeatCapacity() <em>Heat Capacity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeatCapacity()
	 * @generated
	 * @ordered
	 */
	protected static final Double HEAT_CAPACITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHeatCapacity() <em>Heat Capacity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeatCapacity()
	 * @generated
	 * @ordered
	 */
	protected Double heatCapacity = HEAT_CAPACITY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ThermalNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpacePackage.Literals.THERMAL_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getTemperature() {
		return temperature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTemperature(Double newTemperature) {
		Double oldTemperature = temperature;
		temperature = newTemperature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.THERMAL_NODE__TEMPERATURE, oldTemperature, temperature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getQ() {
		if (q == null || q.isEmpty()) return Q_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)q;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getQ(int index) {
		return getQList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getQLength() {
		return q == null ? 0 : q.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQ(Double[] newQ) {
		((BasicEList<Double>)getQList()).setData(newQ.length, newQ);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQ(int index, Double element) {
		getQList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getQList() {
		if (q == null) {
			q = new EDataTypeEList<Double>(Double.class, this, SpacePackage.THERMAL_NODE__Q);
		}
		return q;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getDTemperature() {
		return dTemperature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDTemperature(Double newDTemperature) {
		Double oldDTemperature = dTemperature;
		dTemperature = newDTemperature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.THERMAL_NODE__DTEMPERATURE, oldDTemperature, dTemperature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getHeatCapacity() {
		return heatCapacity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHeatCapacity(Double newHeatCapacity) {
		Double oldHeatCapacity = heatCapacity;
		heatCapacity = newHeatCapacity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.THERMAL_NODE__HEAT_CAPACITY, oldHeatCapacity, heatCapacity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void calc() {
		double qSum = 0.0;
		for( double temp: q ) {
			qSum+=temp;
		}
		dTemperature = qSum/heatCapacity;
		//System.out.println(toString());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpacePackage.THERMAL_NODE__TEMPERATURE:
				return getTemperature();
			case SpacePackage.THERMAL_NODE__Q:
				return getQList();
			case SpacePackage.THERMAL_NODE__DTEMPERATURE:
				return getDTemperature();
			case SpacePackage.THERMAL_NODE__HEAT_CAPACITY:
				return getHeatCapacity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpacePackage.THERMAL_NODE__TEMPERATURE:
				setTemperature((Double)newValue);
				return;
			case SpacePackage.THERMAL_NODE__Q:
				getQList().clear();
				getQList().addAll((Collection<? extends Double>)newValue);
				return;
			case SpacePackage.THERMAL_NODE__DTEMPERATURE:
				setDTemperature((Double)newValue);
				return;
			case SpacePackage.THERMAL_NODE__HEAT_CAPACITY:
				setHeatCapacity((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpacePackage.THERMAL_NODE__TEMPERATURE:
				setTemperature(TEMPERATURE_EDEFAULT);
				return;
			case SpacePackage.THERMAL_NODE__Q:
				getQList().clear();
				return;
			case SpacePackage.THERMAL_NODE__DTEMPERATURE:
				setDTemperature(DTEMPERATURE_EDEFAULT);
				return;
			case SpacePackage.THERMAL_NODE__HEAT_CAPACITY:
				setHeatCapacity(HEAT_CAPACITY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpacePackage.THERMAL_NODE__TEMPERATURE:
				return TEMPERATURE_EDEFAULT == null ? temperature != null : !TEMPERATURE_EDEFAULT.equals(temperature);
			case SpacePackage.THERMAL_NODE__Q:
				return q != null && !q.isEmpty();
			case SpacePackage.THERMAL_NODE__DTEMPERATURE:
				return DTEMPERATURE_EDEFAULT == null ? dTemperature != null : !DTEMPERATURE_EDEFAULT.equals(dTemperature);
			case SpacePackage.THERMAL_NODE__HEAT_CAPACITY:
				return HEAT_CAPACITY_EDEFAULT == null ? heatCapacity != null : !HEAT_CAPACITY_EDEFAULT.equals(heatCapacity);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case SpacePackage.THERMAL_NODE___CALC:
				calc();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (temperature: ");
		result.append(temperature);
		result.append(", q: ");
		result.append(q);
		result.append(", dTemperature: ");
		result.append(dTemperature);
		result.append(", heatCapacity: ");
		result.append(heatCapacity);
		result.append(')');
		return result.toString();
	}

} //ThermalNodeImpl
