/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.impl;

import jat.coreNOSA.math.MatrixVector.data.VectorN;
import jat.coreNOSA.spacetime.EarthFixedRef;
import jat.coreNOSA.spacetime.EarthRef;
import jat.coreNOSA.spacetime.ReferenceFrameTranslater;
import jat.coreNOSA.spacetime.Time;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.thirdeye.core.mesh.impl.ProviderImpl;
import org.thirdeye.extension.space.ECEFToECIProvider;
import org.thirdeye.extension.space.SpacePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ECEF To ECI Provider</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.impl.ECEFToECIProviderImpl#getTime <em>Time</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ECEFToECIProviderImpl extends ProviderImpl implements ECEFToECIProvider {
	/**
	 * The default value of the '{@link #getTime() <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTime()
	 * @generated
	 * @ordered
	 */
	protected static final Double TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTime() <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTime()
	 * @generated
	 * @ordered
	 */
	protected Double time = TIME_EDEFAULT;

	private ReferenceFrameTranslater trans;

	private EarthRef eciFrame = null;

	private EarthFixedRef ecefFrame;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ECEFToECIProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpacePackage.Literals.ECEF_TO_ECI_PROVIDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getTime() {
		return time;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTime(Double newTime) {
		Double oldTime = time;
		time = newTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.ECEF_TO_ECI_PROVIDER__TIME, oldTime, time));
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void calc() {
		
		if( eciFrame == null ) {
			Time t0 = new Time(56938.00278);
			eciFrame  = new EarthRef(t0);
		}
		if( ecefFrame == null ) {
			ecefFrame = new EarthFixedRef();
		}
		
		double mjd = getTime() / (24*60*60) + 56938.00278; 
		Time t1 = new Time(mjd);
		trans = new ReferenceFrameTranslater(ecefFrame, eciFrame, t1);

	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Object apply(Object obj) {
		Double[] temp;
		if( obj instanceof EList ) {
			@SuppressWarnings("unchecked") EList<Double> tempList = (EList<Double>)obj;
			temp = (Double[]) tempList.toArray();
		} else {
			temp = (Double[]) obj;
		}
		VectorN from = new VectorN( temp );
		VectorN to = trans.translatePoint(from);
		EList<Double> returnVal = new BasicEList<Double>();
		returnVal.add(0,to.get(0));
		returnVal.add(1,to.get(1));
		returnVal.add(2,to.get(2));
		return returnVal;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpacePackage.ECEF_TO_ECI_PROVIDER__TIME:
				return getTime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpacePackage.ECEF_TO_ECI_PROVIDER__TIME:
				setTime((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpacePackage.ECEF_TO_ECI_PROVIDER__TIME:
				setTime(TIME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpacePackage.ECEF_TO_ECI_PROVIDER__TIME:
				return TIME_EDEFAULT == null ? time != null : !TIME_EDEFAULT.equals(time);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (time: ");
		result.append(time);
		result.append(')');
		return result.toString();
	}

} //ECEFToECIProviderImpl
