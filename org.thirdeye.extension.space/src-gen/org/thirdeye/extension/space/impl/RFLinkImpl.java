/**
 */
package org.thirdeye.extension.space.impl;

import java.util.Collection;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.thirdeye.core.mesh.impl.ModelImpl;

import org.thirdeye.extension.space.RFLink;
import org.thirdeye.extension.space.SpacePackage;

import jat.coreNOSA.cm.Constants;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>RF Link</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.impl.RFLinkImpl#getAzelraList <em>Azelra</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.RFLinkImpl#getAttenuation <em>Attenuation</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.RFLinkImpl#getFrequency <em>Frequency</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.RFLinkImpl#getDoppler <em>Doppler</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.RFLinkImpl#getRxLocationList <em>Rx Location</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.RFLinkImpl#getRxSpeedList <em>Rx Speed</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.RFLinkImpl#getTxLocationList <em>Tx Location</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.RFLinkImpl#getTxSpeedList <em>Tx Speed</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.RFLinkImpl#getTxPower <em>Tx Power</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.RFLinkImpl#getRxPower <em>Rx Power</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RFLinkImpl extends ModelImpl implements RFLink {
	/**
	 * The cached value of the '{@link #getAzelraList() <em>Azelra</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAzelraList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> azelra;

	/**
	 * The empty value for the '{@link #getAzelra() <em>Azelra</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAzelra()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] AZELRA_EEMPTY_ARRAY = new Double [0];

	/**
	 * The default value of the '{@link #getAttenuation() <em>Attenuation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttenuation()
	 * @generated
	 * @ordered
	 */
	protected static final Double ATTENUATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAttenuation() <em>Attenuation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttenuation()
	 * @generated
	 * @ordered
	 */
	protected Double attenuation = ATTENUATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getFrequency() <em>Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrequency()
	 * @generated
	 * @ordered
	 */
	protected static final Double FREQUENCY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFrequency() <em>Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrequency()
	 * @generated
	 * @ordered
	 */
	protected Double frequency = FREQUENCY_EDEFAULT;

	/**
	 * The default value of the '{@link #getDoppler() <em>Doppler</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoppler()
	 * @generated
	 * @ordered
	 */
	protected static final Double DOPPLER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDoppler() <em>Doppler</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoppler()
	 * @generated
	 * @ordered
	 */
	protected Double doppler = DOPPLER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRxLocationList() <em>Rx Location</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRxLocationList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> rxLocation;

	/**
	 * The empty value for the '{@link #getRxLocation() <em>Rx Location</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRxLocation()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] RX_LOCATION_EEMPTY_ARRAY = new Double [0];

	/**
	 * The cached value of the '{@link #getRxSpeedList() <em>Rx Speed</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRxSpeedList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> rxSpeed;

	/**
	 * The empty value for the '{@link #getRxSpeed() <em>Rx Speed</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRxSpeed()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] RX_SPEED_EEMPTY_ARRAY = new Double [0];

	/**
	 * The cached value of the '{@link #getTxLocationList() <em>Tx Location</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTxLocationList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> txLocation;

	/**
	 * The empty value for the '{@link #getTxLocation() <em>Tx Location</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTxLocation()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] TX_LOCATION_EEMPTY_ARRAY = new Double [0];

	/**
	 * The cached value of the '{@link #getTxSpeedList() <em>Tx Speed</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTxSpeedList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> txSpeed;

	/**
	 * The empty value for the '{@link #getTxSpeed() <em>Tx Speed</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTxSpeed()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] TX_SPEED_EEMPTY_ARRAY = new Double [0];

	/**
	 * The default value of the '{@link #getTxPower() <em>Tx Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTxPower()
	 * @generated
	 * @ordered
	 */
	protected static final Double TX_POWER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTxPower() <em>Tx Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTxPower()
	 * @generated
	 * @ordered
	 */
	protected Double txPower = TX_POWER_EDEFAULT;

	/**
	 * The default value of the '{@link #getRxPower() <em>Rx Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRxPower()
	 * @generated
	 * @ordered
	 */
	protected static final Double RX_POWER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRxPower() <em>Rx Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRxPower()
	 * @generated
	 * @ordered
	 */
	protected Double rxPower = RX_POWER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RFLinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpacePackage.Literals.RF_LINK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getAzelra() {
		if (azelra == null || azelra.isEmpty()) return AZELRA_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)azelra;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getAzelra(int index) {
		return getAzelraList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getAzelraLength() {
		return azelra == null ? 0 : azelra.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAzelra(Double[] newAzelra) {
		((BasicEList<Double>)getAzelraList()).setData(newAzelra.length, newAzelra);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAzelra(int index, Double element) {
		getAzelraList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getAzelraList() {
		if (azelra == null) {
			azelra = new EDataTypeEList<Double>(Double.class, this, SpacePackage.RF_LINK__AZELRA);
		}
		return azelra;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getAttenuation() {
		return attenuation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttenuation(Double newAttenuation) {
		Double oldAttenuation = attenuation;
		attenuation = newAttenuation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.RF_LINK__ATTENUATION, oldAttenuation, attenuation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getFrequency() {
		return frequency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFrequency(Double newFrequency) {
		Double oldFrequency = frequency;
		frequency = newFrequency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.RF_LINK__FREQUENCY, oldFrequency, frequency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getDoppler() {
		return doppler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDoppler(Double newDoppler) {
		Double oldDoppler = doppler;
		doppler = newDoppler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.RF_LINK__DOPPLER, oldDoppler, doppler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getRxLocation() {
		if (rxLocation == null || rxLocation.isEmpty()) return RX_LOCATION_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)rxLocation;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getRxLocation(int index) {
		return getRxLocationList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getRxLocationLength() {
		return rxLocation == null ? 0 : rxLocation.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRxLocation(Double[] newRxLocation) {
		((BasicEList<Double>)getRxLocationList()).setData(newRxLocation.length, newRxLocation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRxLocation(int index, Double element) {
		getRxLocationList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getRxLocationList() {
		if (rxLocation == null) {
			rxLocation = new EDataTypeEList<Double>(Double.class, this, SpacePackage.RF_LINK__RX_LOCATION);
		}
		return rxLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getRxSpeed() {
		if (rxSpeed == null || rxSpeed.isEmpty()) return RX_SPEED_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)rxSpeed;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getRxSpeed(int index) {
		return getRxSpeedList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getRxSpeedLength() {
		return rxSpeed == null ? 0 : rxSpeed.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRxSpeed(Double[] newRxSpeed) {
		((BasicEList<Double>)getRxSpeedList()).setData(newRxSpeed.length, newRxSpeed);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRxSpeed(int index, Double element) {
		getRxSpeedList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getRxSpeedList() {
		if (rxSpeed == null) {
			rxSpeed = new EDataTypeEList<Double>(Double.class, this, SpacePackage.RF_LINK__RX_SPEED);
		}
		return rxSpeed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getTxLocation() {
		if (txLocation == null || txLocation.isEmpty()) return TX_LOCATION_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)txLocation;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getTxLocation(int index) {
		return getTxLocationList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getTxLocationLength() {
		return txLocation == null ? 0 : txLocation.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTxLocation(Double[] newTxLocation) {
		((BasicEList<Double>)getTxLocationList()).setData(newTxLocation.length, newTxLocation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTxLocation(int index, Double element) {
		getTxLocationList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getTxLocationList() {
		if (txLocation == null) {
			txLocation = new EDataTypeEList<Double>(Double.class, this, SpacePackage.RF_LINK__TX_LOCATION);
		}
		return txLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getTxSpeed() {
		if (txSpeed == null || txSpeed.isEmpty()) return TX_SPEED_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)txSpeed;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getTxSpeed(int index) {
		return getTxSpeedList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getTxSpeedLength() {
		return txSpeed == null ? 0 : txSpeed.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTxSpeed(Double[] newTxSpeed) {
		((BasicEList<Double>)getTxSpeedList()).setData(newTxSpeed.length, newTxSpeed);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTxSpeed(int index, Double element) {
		getTxSpeedList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getTxSpeedList() {
		if (txSpeed == null) {
			txSpeed = new EDataTypeEList<Double>(Double.class, this, SpacePackage.RF_LINK__TX_SPEED);
		}
		return txSpeed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getTxPower() {
		return txPower;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTxPower(Double newTxPower) {
		Double oldTxPower = txPower;
		txPower = newTxPower;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.RF_LINK__TX_POWER, oldTxPower, txPower));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getRxPower() {
		return rxPower;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRxPower(Double newRxPower) {
		Double oldRxPower = rxPower;
		rxPower = newRxPower;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.RF_LINK__RX_POWER, oldRxPower, rxPower));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpacePackage.RF_LINK__AZELRA:
				return getAzelraList();
			case SpacePackage.RF_LINK__ATTENUATION:
				return getAttenuation();
			case SpacePackage.RF_LINK__FREQUENCY:
				return getFrequency();
			case SpacePackage.RF_LINK__DOPPLER:
				return getDoppler();
			case SpacePackage.RF_LINK__RX_LOCATION:
				return getRxLocationList();
			case SpacePackage.RF_LINK__RX_SPEED:
				return getRxSpeedList();
			case SpacePackage.RF_LINK__TX_LOCATION:
				return getTxLocationList();
			case SpacePackage.RF_LINK__TX_SPEED:
				return getTxSpeedList();
			case SpacePackage.RF_LINK__TX_POWER:
				return getTxPower();
			case SpacePackage.RF_LINK__RX_POWER:
				return getRxPower();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpacePackage.RF_LINK__AZELRA:
				getAzelraList().clear();
				getAzelraList().addAll((Collection<? extends Double>)newValue);
				return;
			case SpacePackage.RF_LINK__ATTENUATION:
				setAttenuation((Double)newValue);
				return;
			case SpacePackage.RF_LINK__FREQUENCY:
				setFrequency((Double)newValue);
				return;
			case SpacePackage.RF_LINK__DOPPLER:
				setDoppler((Double)newValue);
				return;
			case SpacePackage.RF_LINK__RX_LOCATION:
				getRxLocationList().clear();
				getRxLocationList().addAll((Collection<? extends Double>)newValue);
				return;
			case SpacePackage.RF_LINK__RX_SPEED:
				getRxSpeedList().clear();
				getRxSpeedList().addAll((Collection<? extends Double>)newValue);
				return;
			case SpacePackage.RF_LINK__TX_LOCATION:
				getTxLocationList().clear();
				getTxLocationList().addAll((Collection<? extends Double>)newValue);
				return;
			case SpacePackage.RF_LINK__TX_SPEED:
				getTxSpeedList().clear();
				getTxSpeedList().addAll((Collection<? extends Double>)newValue);
				return;
			case SpacePackage.RF_LINK__TX_POWER:
				setTxPower((Double)newValue);
				return;
			case SpacePackage.RF_LINK__RX_POWER:
				setRxPower((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpacePackage.RF_LINK__AZELRA:
				getAzelraList().clear();
				return;
			case SpacePackage.RF_LINK__ATTENUATION:
				setAttenuation(ATTENUATION_EDEFAULT);
				return;
			case SpacePackage.RF_LINK__FREQUENCY:
				setFrequency(FREQUENCY_EDEFAULT);
				return;
			case SpacePackage.RF_LINK__DOPPLER:
				setDoppler(DOPPLER_EDEFAULT);
				return;
			case SpacePackage.RF_LINK__RX_LOCATION:
				getRxLocationList().clear();
				return;
			case SpacePackage.RF_LINK__RX_SPEED:
				getRxSpeedList().clear();
				return;
			case SpacePackage.RF_LINK__TX_LOCATION:
				getTxLocationList().clear();
				return;
			case SpacePackage.RF_LINK__TX_SPEED:
				getTxSpeedList().clear();
				return;
			case SpacePackage.RF_LINK__TX_POWER:
				setTxPower(TX_POWER_EDEFAULT);
				return;
			case SpacePackage.RF_LINK__RX_POWER:
				setRxPower(RX_POWER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpacePackage.RF_LINK__AZELRA:
				return azelra != null && !azelra.isEmpty();
			case SpacePackage.RF_LINK__ATTENUATION:
				return ATTENUATION_EDEFAULT == null ? attenuation != null : !ATTENUATION_EDEFAULT.equals(attenuation);
			case SpacePackage.RF_LINK__FREQUENCY:
				return FREQUENCY_EDEFAULT == null ? frequency != null : !FREQUENCY_EDEFAULT.equals(frequency);
			case SpacePackage.RF_LINK__DOPPLER:
				return DOPPLER_EDEFAULT == null ? doppler != null : !DOPPLER_EDEFAULT.equals(doppler);
			case SpacePackage.RF_LINK__RX_LOCATION:
				return rxLocation != null && !rxLocation.isEmpty();
			case SpacePackage.RF_LINK__RX_SPEED:
				return rxSpeed != null && !rxSpeed.isEmpty();
			case SpacePackage.RF_LINK__TX_LOCATION:
				return txLocation != null && !txLocation.isEmpty();
			case SpacePackage.RF_LINK__TX_SPEED:
				return txSpeed != null && !txSpeed.isEmpty();
			case SpacePackage.RF_LINK__TX_POWER:
				return TX_POWER_EDEFAULT == null ? txPower != null : !TX_POWER_EDEFAULT.equals(txPower);
			case SpacePackage.RF_LINK__RX_POWER:
				return RX_POWER_EDEFAULT == null ? rxPower != null : !RX_POWER_EDEFAULT.equals(rxPower);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (azelra: ");
		result.append(azelra);
		result.append(", attenuation: ");
		result.append(attenuation);
		result.append(", frequency: ");
		result.append(frequency);
		result.append(", doppler: ");
		result.append(doppler);
		result.append(", rxLocation: ");
		result.append(rxLocation);
		result.append(", rxSpeed: ");
		result.append(rxSpeed);
		result.append(", txLocation: ");
		result.append(txLocation);
		result.append(", txSpeed: ");
		result.append(txSpeed);
		result.append(", txPower: ");
		result.append(txPower);
		result.append(", rxPower: ");
		result.append(rxPower);
		result.append(')');
		return result.toString();
	}

	@Override
	public void calc() {
				
		Vector3D rxLocationVec = new Vector3D(rxLocation.get(0),rxLocation.get(1),rxLocation.get(2));
		Vector3D txLocationVec = new Vector3D(txLocation.get(0),txLocation.get(1),txLocation.get(2));
		Vector3D rxSpeedVec = new Vector3D(rxSpeed.get(0),rxSpeed.get(1),rxSpeed.get(2));
		Vector3D txSpeedVec = new Vector3D(txSpeed.get(0),txSpeed.get(1),txSpeed.get(2));
		
		Vector3D relRange = rxLocationVec.subtract(txLocationVec);
		Vector3D relSpeed = rxSpeedVec.add(txSpeedVec);
		
		double v = relSpeed.dotProduct(relRange.normalize());
		double attenuation = 20 * Math.log10( 4 * Math.PI * relRange.getNorm() * getFrequency() / Constants.c  );
		double doppler = ( v / Constants.c * getFrequency() ) + getFrequency();
		double rxP = getTxPower() - attenuation;
		//if(getAzelra(1) > 0.0) {
			setAttenuation( attenuation );
			setDoppler( doppler );
			setRxPower(rxP);
			//System.out.println( "Antenna Gain [dBi] = " + gainRx + " Received Power [dBm] = " + powerRx );
		//} else {
		//	setAttenuation( 0.0 );
		//	setDoppler( 0.0 );			
		//}	
	}

} //RFLinkImpl
