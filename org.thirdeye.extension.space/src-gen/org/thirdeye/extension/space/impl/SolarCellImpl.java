/**
 */
package org.thirdeye.extension.space.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.thirdeye.core.mesh.impl.ModelImpl;

import org.thirdeye.extension.space.SolarCell;
import org.thirdeye.extension.space.SpacePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Solar Cell</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.impl.SolarCellImpl#getIShortCircuit <em>IShort Circuit</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.SolarCellImpl#getTemperature <em>Temperature</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.SolarCellImpl#getK <em>K</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.SolarCellImpl#getSolarFlux <em>Solar Flux</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.SolarCellImpl#getIPhotoCurrent <em>IPhoto Current</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SolarCellImpl extends ModelImpl implements SolarCell {
	/**
	 * The default value of the '{@link #getIShortCircuit() <em>IShort Circuit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIShortCircuit()
	 * @generated
	 * @ordered
	 */
	protected static final Double ISHORT_CIRCUIT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIShortCircuit() <em>IShort Circuit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIShortCircuit()
	 * @generated
	 * @ordered
	 */
	protected Double iShortCircuit = ISHORT_CIRCUIT_EDEFAULT;

	/**
	 * The default value of the '{@link #getTemperature() <em>Temperature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemperature()
	 * @generated
	 * @ordered
	 */
	protected static final Double TEMPERATURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTemperature() <em>Temperature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemperature()
	 * @generated
	 * @ordered
	 */
	protected Double temperature = TEMPERATURE_EDEFAULT;

	/**
	 * The default value of the '{@link #getK() <em>K</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getK()
	 * @generated
	 * @ordered
	 */
	protected static final Double K_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getK() <em>K</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getK()
	 * @generated
	 * @ordered
	 */
	protected Double k = K_EDEFAULT;

	/**
	 * The default value of the '{@link #getSolarFlux() <em>Solar Flux</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSolarFlux()
	 * @generated
	 * @ordered
	 */
	protected static final Double SOLAR_FLUX_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSolarFlux() <em>Solar Flux</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSolarFlux()
	 * @generated
	 * @ordered
	 */
	protected Double solarFlux = SOLAR_FLUX_EDEFAULT;

	/**
	 * The default value of the '{@link #getIPhotoCurrent() <em>IPhoto Current</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIPhotoCurrent()
	 * @generated
	 * @ordered
	 */
	protected static final Double IPHOTO_CURRENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIPhotoCurrent() <em>IPhoto Current</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIPhotoCurrent()
	 * @generated
	 * @ordered
	 */
	protected Double iPhotoCurrent = IPHOTO_CURRENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SolarCellImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpacePackage.Literals.SOLAR_CELL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getIShortCircuit() {
		return iShortCircuit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIShortCircuit(Double newIShortCircuit) {
		Double oldIShortCircuit = iShortCircuit;
		iShortCircuit = newIShortCircuit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.SOLAR_CELL__ISHORT_CIRCUIT, oldIShortCircuit, iShortCircuit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getTemperature() {
		return temperature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTemperature(Double newTemperature) {
		Double oldTemperature = temperature;
		temperature = newTemperature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.SOLAR_CELL__TEMPERATURE, oldTemperature, temperature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getK() {
		return k;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setK(Double newK) {
		Double oldK = k;
		k = newK;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.SOLAR_CELL__K, oldK, k));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getSolarFlux() {
		return solarFlux;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSolarFlux(Double newSolarFlux) {
		Double oldSolarFlux = solarFlux;
		solarFlux = newSolarFlux;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.SOLAR_CELL__SOLAR_FLUX, oldSolarFlux, solarFlux));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getIPhotoCurrent() {
		return iPhotoCurrent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIPhotoCurrent(Double newIPhotoCurrent) {
		Double oldIPhotoCurrent = iPhotoCurrent;
		iPhotoCurrent = newIPhotoCurrent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpacePackage.SOLAR_CELL__IPHOTO_CURRENT, oldIPhotoCurrent, iPhotoCurrent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpacePackage.SOLAR_CELL__ISHORT_CIRCUIT:
				return getIShortCircuit();
			case SpacePackage.SOLAR_CELL__TEMPERATURE:
				return getTemperature();
			case SpacePackage.SOLAR_CELL__K:
				return getK();
			case SpacePackage.SOLAR_CELL__SOLAR_FLUX:
				return getSolarFlux();
			case SpacePackage.SOLAR_CELL__IPHOTO_CURRENT:
				return getIPhotoCurrent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpacePackage.SOLAR_CELL__ISHORT_CIRCUIT:
				setIShortCircuit((Double)newValue);
				return;
			case SpacePackage.SOLAR_CELL__TEMPERATURE:
				setTemperature((Double)newValue);
				return;
			case SpacePackage.SOLAR_CELL__K:
				setK((Double)newValue);
				return;
			case SpacePackage.SOLAR_CELL__SOLAR_FLUX:
				setSolarFlux((Double)newValue);
				return;
			case SpacePackage.SOLAR_CELL__IPHOTO_CURRENT:
				setIPhotoCurrent((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpacePackage.SOLAR_CELL__ISHORT_CIRCUIT:
				setIShortCircuit(ISHORT_CIRCUIT_EDEFAULT);
				return;
			case SpacePackage.SOLAR_CELL__TEMPERATURE:
				setTemperature(TEMPERATURE_EDEFAULT);
				return;
			case SpacePackage.SOLAR_CELL__K:
				setK(K_EDEFAULT);
				return;
			case SpacePackage.SOLAR_CELL__SOLAR_FLUX:
				setSolarFlux(SOLAR_FLUX_EDEFAULT);
				return;
			case SpacePackage.SOLAR_CELL__IPHOTO_CURRENT:
				setIPhotoCurrent(IPHOTO_CURRENT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpacePackage.SOLAR_CELL__ISHORT_CIRCUIT:
				return ISHORT_CIRCUIT_EDEFAULT == null ? iShortCircuit != null : !ISHORT_CIRCUIT_EDEFAULT.equals(iShortCircuit);
			case SpacePackage.SOLAR_CELL__TEMPERATURE:
				return TEMPERATURE_EDEFAULT == null ? temperature != null : !TEMPERATURE_EDEFAULT.equals(temperature);
			case SpacePackage.SOLAR_CELL__K:
				return K_EDEFAULT == null ? k != null : !K_EDEFAULT.equals(k);
			case SpacePackage.SOLAR_CELL__SOLAR_FLUX:
				return SOLAR_FLUX_EDEFAULT == null ? solarFlux != null : !SOLAR_FLUX_EDEFAULT.equals(solarFlux);
			case SpacePackage.SOLAR_CELL__IPHOTO_CURRENT:
				return IPHOTO_CURRENT_EDEFAULT == null ? iPhotoCurrent != null : !IPHOTO_CURRENT_EDEFAULT.equals(iPhotoCurrent);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (iShortCircuit: ");
		result.append(iShortCircuit);
		result.append(", temperature: ");
		result.append(temperature);
		result.append(", k: ");
		result.append(k);
		result.append(", solarFlux: ");
		result.append(solarFlux);
		result.append(", iPhotoCurrent: ");
		result.append(iPhotoCurrent);
		result.append(')');
		return result.toString();
	}

	@Override
	public void calc() {
		/*double loadResistance = 0;
		double seriesResistance = 0;
		double shuntResistance = 0;
		double electronCharge = 0;
		double diodeIdealityFactor = 0;
		double boltzmann = 0;
		double nominalTemperature;
		*/
		//double temp1 = 1.0d + ( ( loadResistance + seriesResistance ) / shuntResistance ); 
		//double temp2 = ( loadResistance + seriesResistance ) * electronCharge / diodeIdealityFactor / boltzmann / temperature;
		
		//reverseSaturationCurrent = Math.pow(temperature/nominalTemperature,3) * Math.exp( ( ( temperature / nominalTemperature ) - 1 ) * bandGapEnergy / diodeIdealityFactor / thermalVoltage );
		iPhotoCurrent = ( iShortCircuit + k * ( 301.15 - temperature ) ) * solarFlux;	
	}

} //SolarCellImpl
