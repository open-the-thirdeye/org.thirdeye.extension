/**
 */
package org.thirdeye.extension.space.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.thirdeye.core.mesh.impl.ModelImpl;

import org.thirdeye.extension.space.AttitudeModel;
import org.thirdeye.extension.space.SpacePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attitude Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.impl.AttitudeModelImpl#getQuaternionList <em>Quaternion</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.AttitudeModelImpl#getAngularVelocityList <em>Angular Velocity</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.impl.AttitudeModelImpl#getInertiaMatrixList <em>Inertia Matrix</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AttitudeModelImpl extends ModelImpl implements AttitudeModel {
	/**
	 * The cached value of the '{@link #getQuaternionList() <em>Quaternion</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuaternionList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> quaternion;

	/**
	 * The empty value for the '{@link #getQuaternion() <em>Quaternion</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuaternion()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] QUATERNION_EEMPTY_ARRAY = new Double [0];

	/**
	 * The cached value of the '{@link #getAngularVelocityList() <em>Angular Velocity</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAngularVelocityList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> angularVelocity;

	/**
	 * The empty value for the '{@link #getAngularVelocity() <em>Angular Velocity</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAngularVelocity()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] ANGULAR_VELOCITY_EEMPTY_ARRAY = new Double [0];

	/**
	 * The cached value of the '{@link #getInertiaMatrixList() <em>Inertia Matrix</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInertiaMatrixList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> inertiaMatrix;

	/**
	 * The empty value for the '{@link #getInertiaMatrix() <em>Inertia Matrix</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInertiaMatrix()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] INERTIA_MATRIX_EEMPTY_ARRAY = new Double [0];

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttitudeModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpacePackage.Literals.ATTITUDE_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getQuaternion() {
		if (quaternion == null || quaternion.isEmpty()) return QUATERNION_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)quaternion;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getQuaternion(int index) {
		return getQuaternionList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getQuaternionLength() {
		return quaternion == null ? 0 : quaternion.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuaternion(Double[] newQuaternion) {
		((BasicEList<Double>)getQuaternionList()).setData(newQuaternion.length, newQuaternion);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuaternion(int index, Double element) {
		getQuaternionList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getQuaternionList() {
		if (quaternion == null) {
			quaternion = new EDataTypeEList<Double>(Double.class, this, SpacePackage.ATTITUDE_MODEL__QUATERNION);
		}
		return quaternion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getAngularVelocity() {
		if (angularVelocity == null || angularVelocity.isEmpty()) return ANGULAR_VELOCITY_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)angularVelocity;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getAngularVelocity(int index) {
		return getAngularVelocityList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getAngularVelocityLength() {
		return angularVelocity == null ? 0 : angularVelocity.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAngularVelocity(Double[] newAngularVelocity) {
		((BasicEList<Double>)getAngularVelocityList()).setData(newAngularVelocity.length, newAngularVelocity);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAngularVelocity(int index, Double element) {
		getAngularVelocityList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getAngularVelocityList() {
		if (angularVelocity == null) {
			angularVelocity = new EDataTypeEList<Double>(Double.class, this, SpacePackage.ATTITUDE_MODEL__ANGULAR_VELOCITY);
		}
		return angularVelocity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getInertiaMatrix() {
		if (inertiaMatrix == null || inertiaMatrix.isEmpty()) return INERTIA_MATRIX_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)inertiaMatrix;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getInertiaMatrix(int index) {
		return getInertiaMatrixList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getInertiaMatrixLength() {
		return inertiaMatrix == null ? 0 : inertiaMatrix.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInertiaMatrix(Double[] newInertiaMatrix) {
		((BasicEList<Double>)getInertiaMatrixList()).setData(newInertiaMatrix.length, newInertiaMatrix);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInertiaMatrix(int index, Double element) {
		getInertiaMatrixList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getInertiaMatrixList() {
		if (inertiaMatrix == null) {
			inertiaMatrix = new EDataTypeEList<Double>(Double.class, this, SpacePackage.ATTITUDE_MODEL__INERTIA_MATRIX);
		}
		return inertiaMatrix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpacePackage.ATTITUDE_MODEL__QUATERNION:
				return getQuaternionList();
			case SpacePackage.ATTITUDE_MODEL__ANGULAR_VELOCITY:
				return getAngularVelocityList();
			case SpacePackage.ATTITUDE_MODEL__INERTIA_MATRIX:
				return getInertiaMatrixList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpacePackage.ATTITUDE_MODEL__QUATERNION:
				getQuaternionList().clear();
				getQuaternionList().addAll((Collection<? extends Double>)newValue);
				return;
			case SpacePackage.ATTITUDE_MODEL__ANGULAR_VELOCITY:
				getAngularVelocityList().clear();
				getAngularVelocityList().addAll((Collection<? extends Double>)newValue);
				return;
			case SpacePackage.ATTITUDE_MODEL__INERTIA_MATRIX:
				getInertiaMatrixList().clear();
				getInertiaMatrixList().addAll((Collection<? extends Double>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpacePackage.ATTITUDE_MODEL__QUATERNION:
				getQuaternionList().clear();
				return;
			case SpacePackage.ATTITUDE_MODEL__ANGULAR_VELOCITY:
				getAngularVelocityList().clear();
				return;
			case SpacePackage.ATTITUDE_MODEL__INERTIA_MATRIX:
				getInertiaMatrixList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpacePackage.ATTITUDE_MODEL__QUATERNION:
				return quaternion != null && !quaternion.isEmpty();
			case SpacePackage.ATTITUDE_MODEL__ANGULAR_VELOCITY:
				return angularVelocity != null && !angularVelocity.isEmpty();
			case SpacePackage.ATTITUDE_MODEL__INERTIA_MATRIX:
				return inertiaMatrix != null && !inertiaMatrix.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (quaternion: ");
		result.append(quaternion);
		result.append(", angularVelocity: ");
		result.append(angularVelocity);
		result.append(", inertiaMatrix: ");
		result.append(inertiaMatrix);
		result.append(')');
		return result.toString();
	}

	@Override
	public void calc() {
		// TODO Auto-generated method stub
		
	}

} //AttitudeModelImpl
