/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space;

import org.eclipse.emf.common.util.EList;

import org.thirdeye.core.mesh.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Point Mass Gravity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.PointMassGravity#getPositionList <em>Position</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.PointMassGravity#getAccelerationList <em>Acceleration</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extension.space.SpacePackage#getPointMassGravity()
 * @model
 * @generated
 */
public interface PointMassGravity extends Model {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getPosition();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getPosition(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getPositionLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setPosition(Double[] newPosition);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setPosition(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Position</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Position</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position</em>' attribute list.
	 * @see org.thirdeye.extension.space.SpacePackage#getPointMassGravity_Position()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 *        annotation="ThirdEye unit='m,m,m'"
	 * @generated
	 */
	EList<Double> getPositionList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getAcceleration();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getAcceleration(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getAccelerationLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setAcceleration(Double[] newAcceleration);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setAcceleration(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Acceleration</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Acceleration</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Acceleration</em>' attribute list.
	 * @see org.thirdeye.extension.space.SpacePackage#getPointMassGravity_Acceleration()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 *        annotation="ThirdEye unit='m/s^2,m/s^2,m/s^2'"
	 * @generated
	 */
	EList<Double> getAccelerationList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void calc();

} // PointMassGravity
