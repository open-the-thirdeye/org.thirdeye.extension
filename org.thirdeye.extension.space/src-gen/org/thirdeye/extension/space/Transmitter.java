/**
 */
package org.thirdeye.extension.space;

import org.thirdeye.core.mesh.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transmitter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.Transmitter#getFrequency <em>Frequency</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.Transmitter#getPower <em>Power</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.Transmitter#getGain <em>Gain</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.Transmitter#getTxPower <em>Tx Power</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extension.space.SpacePackage#getTransmitter()
 * @model
 * @generated
 */
public interface Transmitter extends Model {
	/**
	 * Returns the value of the '<em><b>Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Frequency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Frequency</em>' attribute.
	 * @see #setFrequency(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getTransmitter_Frequency()
	 * @model required="true" ordered="false"
	 *        annotation="ThirdEye unit='Hz'"
	 * @generated
	 */
	Double getFrequency();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.Transmitter#getFrequency <em>Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Frequency</em>' attribute.
	 * @see #getFrequency()
	 * @generated
	 */
	void setFrequency(Double value);

	/**
	 * Returns the value of the '<em><b>Power</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Power</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Power</em>' attribute.
	 * @see #setPower(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getTransmitter_Power()
	 * @model required="true" ordered="false"
	 *        annotation="ThirdEye unit='W'"
	 * @generated
	 */
	Double getPower();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.Transmitter#getPower <em>Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Power</em>' attribute.
	 * @see #getPower()
	 * @generated
	 */
	void setPower(Double value);

	/**
	 * Returns the value of the '<em><b>Gain</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Gain</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Gain</em>' attribute.
	 * @see #setGain(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getTransmitter_Gain()
	 * @model required="true" ordered="false"
	 *        annotation="ThirdEye unit='dBi'"
	 * @generated
	 */
	Double getGain();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.Transmitter#getGain <em>Gain</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Gain</em>' attribute.
	 * @see #getGain()
	 * @generated
	 */
	void setGain(Double value);

	/**
	 * Returns the value of the '<em><b>Tx Power</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tx Power</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tx Power</em>' attribute.
	 * @see #setTxPower(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getTransmitter_TxPower()
	 * @model required="true" ordered="false"
	 *        annotation="ThirdEye unit='dBm'"
	 * @generated
	 */
	Double getTxPower();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.Transmitter#getTxPower <em>Tx Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tx Power</em>' attribute.
	 * @see #getTxPower()
	 * @generated
	 */
	void setTxPower(Double value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void calc();

} // Transmitter
