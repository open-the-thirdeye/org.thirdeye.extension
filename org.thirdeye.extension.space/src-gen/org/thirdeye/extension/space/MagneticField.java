/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space;

import org.eclipse.emf.common.util.EList;

import org.thirdeye.core.mesh.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Magnetic Field</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.MagneticField#getLatLonAltList <em>Lat Lon Alt</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.MagneticField#getCoefficientFilePath <em>Coefficient File Path</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extension.space.SpacePackage#getMagneticField()
 * @model
 * @generated
 */
public interface MagneticField extends Model {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getLatLonAlt();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getLatLonAlt(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getLatLonAltLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setLatLonAlt(Double[] newLatLonAlt);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setLatLonAlt(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Lat Lon Alt</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lat Lon Alt</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lat Lon Alt</em>' attribute list.
	 * @see org.thirdeye.extension.space.SpacePackage#getMagneticField_LatLonAlt()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 *        annotation="ThirdEye unit='rad,rad,m'"
	 * @generated
	 */
	EList<Double> getLatLonAltList();

	/**
	 * Returns the value of the '<em><b>Coefficient File Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coefficient File Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coefficient File Path</em>' attribute.
	 * @see #setCoefficientFilePath(String)
	 * @see org.thirdeye.extension.space.SpacePackage#getMagneticField_CoefficientFilePath()
	 * @model required="true"
	 * @generated
	 */
	String getCoefficientFilePath();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.MagneticField#getCoefficientFilePath <em>Coefficient File Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Coefficient File Path</em>' attribute.
	 * @see #getCoefficientFilePath()
	 * @generated
	 */
	void setCoefficientFilePath(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void calc();

} // MagneticField
