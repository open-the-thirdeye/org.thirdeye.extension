/**
 */
package org.thirdeye.extension.space;

import org.eclipse.emf.common.util.EList;

import org.thirdeye.core.mesh.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attitude Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.AttitudeModel#getQuaternionList <em>Quaternion</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.AttitudeModel#getAngularVelocityList <em>Angular Velocity</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.AttitudeModel#getInertiaMatrixList <em>Inertia Matrix</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extension.space.SpacePackage#getAttitudeModel()
 * @model
 * @generated
 */
public interface AttitudeModel extends Model {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getQuaternion();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getQuaternion(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getQuaternionLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setQuaternion(Double[] newQuaternion);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setQuaternion(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Quaternion</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quaternion</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quaternion</em>' attribute list.
	 * @see org.thirdeye.extension.space.SpacePackage#getAttitudeModel_Quaternion()
	 * @model unique="false" lower="4" upper="4" ordered="false"
	 *        annotation="ThirdEye unit='none,none,none,none'"
	 * @generated
	 */
	EList<Double> getQuaternionList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getAngularVelocity();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getAngularVelocity(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getAngularVelocityLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setAngularVelocity(Double[] newAngularVelocity);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setAngularVelocity(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Angular Velocity</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Angular Velocity</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Angular Velocity</em>' attribute list.
	 * @see org.thirdeye.extension.space.SpacePackage#getAttitudeModel_AngularVelocity()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 *        annotation="ThirdEye unit='rad/s,rad/s,rad/s'"
	 * @generated
	 */
	EList<Double> getAngularVelocityList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getInertiaMatrix();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getInertiaMatrix(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getInertiaMatrixLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setInertiaMatrix(Double[] newInertiaMatrix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setInertiaMatrix(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Inertia Matrix</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inertia Matrix</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inertia Matrix</em>' attribute list.
	 * @see org.thirdeye.extension.space.SpacePackage#getAttitudeModel_InertiaMatrix()
	 * @model unique="false" lower="9" upper="9" ordered="false"
	 * @generated
	 */
	EList<Double> getInertiaMatrixList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void calc();

} // AttitudeModel
