/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space;

import org.eclipse.emf.common.util.EList;

import org.thirdeye.core.mesh.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Point Mass</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.PointMass#getMass <em>Mass</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.PointMass#getPositionList <em>Position</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.PointMass#getSpeedList <em>Speed</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extension.space.SpacePackage#getPointMass()
 * @model
 * @generated
 */
public interface PointMass extends Model {
	/**
	 * Returns the value of the '<em><b>Mass</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mass</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mass</em>' attribute.
	 * @see #setMass(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getPointMass_Mass()
	 * @model required="true"
	 *        annotation="ThirdEye unit='kg'"
	 * @generated
	 */
	Double getMass();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.PointMass#getMass <em>Mass</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mass</em>' attribute.
	 * @see #getMass()
	 * @generated
	 */
	void setMass(Double value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getPosition();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getPosition(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getPositionLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setPosition(Double[] newPosition);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setPosition(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Position</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Position</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position</em>' attribute list.
	 * @see org.thirdeye.extension.space.SpacePackage#getPointMass_Position()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 *        annotation="ThirdEye unit='m,m,m'"
	 * @generated
	 */
	EList<Double> getPositionList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getSpeed();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getSpeed(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getSpeedLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setSpeed(Double[] newSpeed);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setSpeed(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Speed</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Speed</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Speed</em>' attribute list.
	 * @see org.thirdeye.extension.space.SpacePackage#getPointMass_Speed()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 *        annotation="ThirdEye unit='m/s,m/s,m/s'"
	 * @generated
	 */
	EList<Double> getSpeedList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void calc();

} // PointMass
