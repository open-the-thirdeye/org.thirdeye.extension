/**
 */
package org.thirdeye.extension.space;

import org.thirdeye.core.mesh.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Receiver</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.Receiver#getFrequency <em>Frequency</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.Receiver#getPower <em>Power</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.Receiver#getGain <em>Gain</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.Receiver#getRxPower <em>Rx Power</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extension.space.SpacePackage#getReceiver()
 * @model
 * @generated
 */
public interface Receiver extends Model {
	/**
	 * Returns the value of the '<em><b>Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Frequency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Frequency</em>' attribute.
	 * @see #setFrequency(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getReceiver_Frequency()
	 * @model required="true" ordered="false"
	 *        annotation="ThirdEye unit='Hz'"
	 * @generated
	 */
	Double getFrequency();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.Receiver#getFrequency <em>Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Frequency</em>' attribute.
	 * @see #getFrequency()
	 * @generated
	 */
	void setFrequency(Double value);

	/**
	 * Returns the value of the '<em><b>Power</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Power</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Power</em>' attribute.
	 * @see #setPower(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getReceiver_Power()
	 * @model required="true" ordered="false"
	 *        annotation="ThirdEye unit='W'"
	 * @generated
	 */
	Double getPower();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.Receiver#getPower <em>Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Power</em>' attribute.
	 * @see #getPower()
	 * @generated
	 */
	void setPower(Double value);

	/**
	 * Returns the value of the '<em><b>Gain</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Gain</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Gain</em>' attribute.
	 * @see #setGain(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getReceiver_Gain()
	 * @model required="true" ordered="false"
	 *        annotation="ThirdEye unit='dBi'"
	 * @generated
	 */
	Double getGain();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.Receiver#getGain <em>Gain</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Gain</em>' attribute.
	 * @see #getGain()
	 * @generated
	 */
	void setGain(Double value);

	/**
	 * Returns the value of the '<em><b>Rx Power</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rx Power</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rx Power</em>' attribute.
	 * @see #setRxPower(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getReceiver_RxPower()
	 * @model required="true" ordered="false"
	 *        annotation="ThirdEye unit='dBm'"
	 * @generated
	 */
	Double getRxPower();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.Receiver#getRxPower <em>Rx Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rx Power</em>' attribute.
	 * @see #getRxPower()
	 * @generated
	 */
	void setRxPower(Double value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void calc();

} // Receiver
