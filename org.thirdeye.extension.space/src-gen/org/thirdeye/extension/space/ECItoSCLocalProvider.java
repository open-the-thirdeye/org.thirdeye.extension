/**
 */
package org.thirdeye.extension.space;

import org.eclipse.emf.common.util.EList;

import org.thirdeye.core.mesh.Provider;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EC Ito SC Local Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.ECItoSCLocalProvider#getQuaternionList <em>Quaternion</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extension.space.SpacePackage#getECItoSCLocalProvider()
 * @model
 * @generated
 */
public interface ECItoSCLocalProvider extends Provider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getQuaternion();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getQuaternion(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getQuaternionLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setQuaternion(Double[] newQuaternion);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setQuaternion(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Quaternion</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quaternion</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quaternion</em>' attribute list.
	 * @see org.thirdeye.extension.space.SpacePackage#getECItoSCLocalProvider_Quaternion()
	 * @model unique="false" lower="4" upper="4" ordered="false"
	 *        annotation="ThirdEye unit='none,none,none,none'"
	 * @generated
	 */
	EList<Double> getQuaternionList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void calc();

} // ECItoSCLocalProvider
