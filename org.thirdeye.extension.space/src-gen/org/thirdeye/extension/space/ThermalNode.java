/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space;

import org.eclipse.emf.common.util.EList;

import org.thirdeye.core.mesh.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Thermal Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.ThermalNode#getTemperature <em>Temperature</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.ThermalNode#getQList <em>Q</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.ThermalNode#getDTemperature <em>DTemperature</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.ThermalNode#getHeatCapacity <em>Heat Capacity</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extension.space.SpacePackage#getThermalNode()
 * @model
 * @generated
 */
public interface ThermalNode extends Model {
	/**
	 * Returns the value of the '<em><b>Temperature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Temperature</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Temperature</em>' attribute.
	 * @see #setTemperature(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getThermalNode_Temperature()
	 * @model required="true"
	 *        annotation="ThirdEye unit='K'"
	 * @generated
	 */
	Double getTemperature();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.ThermalNode#getTemperature <em>Temperature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Temperature</em>' attribute.
	 * @see #getTemperature()
	 * @generated
	 */
	void setTemperature(Double value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getQ();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getQ(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getQLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setQ(Double[] newQ);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setQ(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Q</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Q</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Q</em>' attribute list.
	 * @see org.thirdeye.extension.space.SpacePackage#getThermalNode_Q()
	 * @model unique="false" required="true" ordered="false"
	 *        annotation="ThirdEye unit='J/s'"
	 * @generated
	 */
	EList<Double> getQList();

	/**
	 * Returns the value of the '<em><b>DTemperature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>DTemperature</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DTemperature</em>' attribute.
	 * @see #setDTemperature(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getThermalNode_DTemperature()
	 * @model required="true"
	 *        annotation="ThirdEye unit='K/s'"
	 * @generated
	 */
	Double getDTemperature();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.ThermalNode#getDTemperature <em>DTemperature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DTemperature</em>' attribute.
	 * @see #getDTemperature()
	 * @generated
	 */
	void setDTemperature(Double value);

	/**
	 * Returns the value of the '<em><b>Heat Capacity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Heat Capacity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Heat Capacity</em>' attribute.
	 * @see #setHeatCapacity(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getThermalNode_HeatCapacity()
	 * @model required="true"
	 *        annotation="ThirdEye unit='J/K'"
	 * @generated
	 */
	Double getHeatCapacity();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.ThermalNode#getHeatCapacity <em>Heat Capacity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Heat Capacity</em>' attribute.
	 * @see #getHeatCapacity()
	 * @generated
	 */
	void setHeatCapacity(Double value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='double qSum = 0.0;\nfor( double temp: q ) {\n\tqSum+=temp;\n}\ndTemperature = qSum/heatCapacity;\n//System.out.println(toString());'"
	 * @generated
	 */
	void calc();

} // ThermalNode
