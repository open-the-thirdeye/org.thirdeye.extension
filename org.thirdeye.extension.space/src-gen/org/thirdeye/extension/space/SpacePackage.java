/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import org.thirdeye.core.mesh.MeshPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.thirdeye.extension.space.SpaceFactory
 * @model kind="package"
 * @generated
 */
public interface SpacePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "space";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://space/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "space";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SpacePackage eINSTANCE = org.thirdeye.extension.space.impl.SpacePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.impl.TimeImpl <em>Time</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.impl.TimeImpl
	 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getTime()
	 * @generated
	 */
	int TIME = 0;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME__VARIABLES = MeshPackage.MODEL__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME__NAME = MeshPackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME__TIME = MeshPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>DTime</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME__DTIME = MeshPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Time</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_FEATURE_COUNT = MeshPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME___RUN = MeshPackage.MODEL___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME___CALC = MeshPackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Time</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_OPERATION_COUNT = MeshPackage.MODEL_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.impl.GravityFieldImpl <em>Gravity Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.impl.GravityFieldImpl
	 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getGravityField()
	 * @generated
	 */
	int GRAVITY_FIELD = 1;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAVITY_FIELD__VARIABLES = MeshPackage.MODEL__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAVITY_FIELD__NAME = MeshPackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Position</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAVITY_FIELD__POSITION = MeshPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Coefficient File Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAVITY_FIELD__COEFFICIENT_FILE_PATH = MeshPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Acceleration</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAVITY_FIELD__ACCELERATION = MeshPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Gravity Field</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAVITY_FIELD__GRAVITY_FIELD = MeshPackage.MODEL_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Gravity Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAVITY_FIELD_FEATURE_COUNT = MeshPackage.MODEL_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAVITY_FIELD___RUN = MeshPackage.MODEL___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAVITY_FIELD___CALC = MeshPackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Gravity Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAVITY_FIELD_OPERATION_COUNT = MeshPackage.MODEL_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.impl.MagneticFieldImpl <em>Magnetic Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.impl.MagneticFieldImpl
	 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getMagneticField()
	 * @generated
	 */
	int MAGNETIC_FIELD = 2;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAGNETIC_FIELD__VARIABLES = MeshPackage.MODEL__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAGNETIC_FIELD__NAME = MeshPackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Lat Lon Alt</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAGNETIC_FIELD__LAT_LON_ALT = MeshPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Coefficient File Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAGNETIC_FIELD__COEFFICIENT_FILE_PATH = MeshPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Magnetic Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAGNETIC_FIELD_FEATURE_COUNT = MeshPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAGNETIC_FIELD___RUN = MeshPackage.MODEL___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAGNETIC_FIELD___CALC = MeshPackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Magnetic Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAGNETIC_FIELD_OPERATION_COUNT = MeshPackage.MODEL_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.impl.PointMassImpl <em>Point Mass</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.impl.PointMassImpl
	 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getPointMass()
	 * @generated
	 */
	int POINT_MASS = 3;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_MASS__VARIABLES = MeshPackage.MODEL__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_MASS__NAME = MeshPackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Mass</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_MASS__MASS = MeshPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Position</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_MASS__POSITION = MeshPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Speed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_MASS__SPEED = MeshPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Point Mass</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_MASS_FEATURE_COUNT = MeshPackage.MODEL_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_MASS___RUN = MeshPackage.MODEL___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_MASS___CALC = MeshPackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Point Mass</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_MASS_OPERATION_COUNT = MeshPackage.MODEL_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.impl.ThermalNodeImpl <em>Thermal Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.impl.ThermalNodeImpl
	 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getThermalNode()
	 * @generated
	 */
	int THERMAL_NODE = 4;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THERMAL_NODE__VARIABLES = MeshPackage.MODEL__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THERMAL_NODE__NAME = MeshPackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Temperature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THERMAL_NODE__TEMPERATURE = MeshPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Q</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THERMAL_NODE__Q = MeshPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>DTemperature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THERMAL_NODE__DTEMPERATURE = MeshPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Heat Capacity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THERMAL_NODE__HEAT_CAPACITY = MeshPackage.MODEL_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Thermal Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THERMAL_NODE_FEATURE_COUNT = MeshPackage.MODEL_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THERMAL_NODE___RUN = MeshPackage.MODEL___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THERMAL_NODE___CALC = MeshPackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Thermal Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THERMAL_NODE_OPERATION_COUNT = MeshPackage.MODEL_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.impl.ThermalConnectionImpl <em>Thermal Connection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.impl.ThermalConnectionImpl
	 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getThermalConnection()
	 * @generated
	 */
	int THERMAL_CONNECTION = 5;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THERMAL_CONNECTION__VARIABLES = MeshPackage.CONNECTION__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THERMAL_CONNECTION__NAME = MeshPackage.CONNECTION__NAME;

	/**
	 * The feature id for the '<em><b>Heat Conductivity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THERMAL_CONNECTION__HEAT_CONDUCTIVITY = MeshPackage.CONNECTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Heat Flow</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THERMAL_CONNECTION__HEAT_FLOW = MeshPackage.CONNECTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Temperature</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THERMAL_CONNECTION__TEMPERATURE = MeshPackage.CONNECTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Thermal Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THERMAL_CONNECTION_FEATURE_COUNT = MeshPackage.CONNECTION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THERMAL_CONNECTION___RUN = MeshPackage.CONNECTION___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THERMAL_CONNECTION___CALC = MeshPackage.CONNECTION_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Thermal Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THERMAL_CONNECTION_OPERATION_COUNT = MeshPackage.CONNECTION_OPERATION_COUNT + 1;


	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.impl.ECIToECEFProviderImpl <em>ECI To ECEF Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.impl.ECIToECEFProviderImpl
	 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getECIToECEFProvider()
	 * @generated
	 */
	int ECI_TO_ECEF_PROVIDER = 6;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECI_TO_ECEF_PROVIDER__VARIABLES = MeshPackage.PROVIDER__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECI_TO_ECEF_PROVIDER__NAME = MeshPackage.PROVIDER__NAME;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECI_TO_ECEF_PROVIDER__TIME = MeshPackage.PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>ECI To ECEF Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECI_TO_ECEF_PROVIDER_FEATURE_COUNT = MeshPackage.PROVIDER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECI_TO_ECEF_PROVIDER___RUN = MeshPackage.PROVIDER___RUN;

	/**
	 * The operation id for the '<em>Apply</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECI_TO_ECEF_PROVIDER___APPLY__OBJECT = MeshPackage.PROVIDER___APPLY__OBJECT;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECI_TO_ECEF_PROVIDER___CALC = MeshPackage.PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>ECI To ECEF Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECI_TO_ECEF_PROVIDER_OPERATION_COUNT = MeshPackage.PROVIDER_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.impl.ECEFToLatLonAltProviderImpl <em>ECEF To Lat Lon Alt Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.impl.ECEFToLatLonAltProviderImpl
	 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getECEFToLatLonAltProvider()
	 * @generated
	 */
	int ECEF_TO_LAT_LON_ALT_PROVIDER = 7;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECEF_TO_LAT_LON_ALT_PROVIDER__VARIABLES = MeshPackage.PROVIDER__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECEF_TO_LAT_LON_ALT_PROVIDER__NAME = MeshPackage.PROVIDER__NAME;

	/**
	 * The number of structural features of the '<em>ECEF To Lat Lon Alt Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECEF_TO_LAT_LON_ALT_PROVIDER_FEATURE_COUNT = MeshPackage.PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECEF_TO_LAT_LON_ALT_PROVIDER___RUN = MeshPackage.PROVIDER___RUN;

	/**
	 * The operation id for the '<em>Apply</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECEF_TO_LAT_LON_ALT_PROVIDER___APPLY__OBJECT = MeshPackage.PROVIDER___APPLY__OBJECT;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECEF_TO_LAT_LON_ALT_PROVIDER___CALC = MeshPackage.PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>ECEF To Lat Lon Alt Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECEF_TO_LAT_LON_ALT_PROVIDER_OPERATION_COUNT = MeshPackage.PROVIDER_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.impl.ECEFToECIProviderImpl <em>ECEF To ECI Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.impl.ECEFToECIProviderImpl
	 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getECEFToECIProvider()
	 * @generated
	 */
	int ECEF_TO_ECI_PROVIDER = 8;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECEF_TO_ECI_PROVIDER__VARIABLES = MeshPackage.PROVIDER__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECEF_TO_ECI_PROVIDER__NAME = MeshPackage.PROVIDER__NAME;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECEF_TO_ECI_PROVIDER__TIME = MeshPackage.PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>ECEF To ECI Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECEF_TO_ECI_PROVIDER_FEATURE_COUNT = MeshPackage.PROVIDER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECEF_TO_ECI_PROVIDER___RUN = MeshPackage.PROVIDER___RUN;

	/**
	 * The operation id for the '<em>Apply</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECEF_TO_ECI_PROVIDER___APPLY__OBJECT = MeshPackage.PROVIDER___APPLY__OBJECT;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECEF_TO_ECI_PROVIDER___CALC = MeshPackage.PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>ECEF To ECI Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECEF_TO_ECI_PROVIDER_OPERATION_COUNT = MeshPackage.PROVIDER_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.impl.PointMassGravityImpl <em>Point Mass Gravity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.impl.PointMassGravityImpl
	 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getPointMassGravity()
	 * @generated
	 */
	int POINT_MASS_GRAVITY = 9;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_MASS_GRAVITY__VARIABLES = MeshPackage.MODEL__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_MASS_GRAVITY__NAME = MeshPackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Position</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_MASS_GRAVITY__POSITION = MeshPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Acceleration</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_MASS_GRAVITY__ACCELERATION = MeshPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Point Mass Gravity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_MASS_GRAVITY_FEATURE_COUNT = MeshPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_MASS_GRAVITY___RUN = MeshPackage.MODEL___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_MASS_GRAVITY___CALC = MeshPackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Point Mass Gravity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_MASS_GRAVITY_OPERATION_COUNT = MeshPackage.MODEL_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.impl.FixedAttitudeImpl <em>Fixed Attitude</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.impl.FixedAttitudeImpl
	 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getFixedAttitude()
	 * @generated
	 */
	int FIXED_ATTITUDE = 10;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_ATTITUDE__VARIABLES = MeshPackage.MODEL__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_ATTITUDE__NAME = MeshPackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Position</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_ATTITUDE__POSITION = MeshPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Speed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_ATTITUDE__SPEED = MeshPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Quaternion</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_ATTITUDE__QUATERNION = MeshPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Fixed Attitude</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_ATTITUDE_FEATURE_COUNT = MeshPackage.MODEL_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_ATTITUDE___RUN = MeshPackage.MODEL___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_ATTITUDE___CALC = MeshPackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Fixed Attitude</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_ATTITUDE_OPERATION_COUNT = MeshPackage.MODEL_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.impl.SCLocalToECIProviderImpl <em>SC Local To ECI Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.impl.SCLocalToECIProviderImpl
	 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getSCLocalToECIProvider()
	 * @generated
	 */
	int SC_LOCAL_TO_ECI_PROVIDER = 11;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_LOCAL_TO_ECI_PROVIDER__VARIABLES = MeshPackage.PROVIDER__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_LOCAL_TO_ECI_PROVIDER__NAME = MeshPackage.PROVIDER__NAME;

	/**
	 * The feature id for the '<em><b>Quaternion</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_LOCAL_TO_ECI_PROVIDER__QUATERNION = MeshPackage.PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>SC Local To ECI Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_LOCAL_TO_ECI_PROVIDER_FEATURE_COUNT = MeshPackage.PROVIDER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_LOCAL_TO_ECI_PROVIDER___RUN = MeshPackage.PROVIDER___RUN;

	/**
	 * The operation id for the '<em>Apply</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_LOCAL_TO_ECI_PROVIDER___APPLY__OBJECT = MeshPackage.PROVIDER___APPLY__OBJECT;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_LOCAL_TO_ECI_PROVIDER___CALC = MeshPackage.PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>SC Local To ECI Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SC_LOCAL_TO_ECI_PROVIDER_OPERATION_COUNT = MeshPackage.PROVIDER_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.impl.ECItoSCLocalProviderImpl <em>EC Ito SC Local Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.impl.ECItoSCLocalProviderImpl
	 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getECItoSCLocalProvider()
	 * @generated
	 */
	int EC_ITO_SC_LOCAL_PROVIDER = 12;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EC_ITO_SC_LOCAL_PROVIDER__VARIABLES = MeshPackage.PROVIDER__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EC_ITO_SC_LOCAL_PROVIDER__NAME = MeshPackage.PROVIDER__NAME;

	/**
	 * The feature id for the '<em><b>Quaternion</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EC_ITO_SC_LOCAL_PROVIDER__QUATERNION = MeshPackage.PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>EC Ito SC Local Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EC_ITO_SC_LOCAL_PROVIDER_FEATURE_COUNT = MeshPackage.PROVIDER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EC_ITO_SC_LOCAL_PROVIDER___RUN = MeshPackage.PROVIDER___RUN;

	/**
	 * The operation id for the '<em>Apply</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EC_ITO_SC_LOCAL_PROVIDER___APPLY__OBJECT = MeshPackage.PROVIDER___APPLY__OBJECT;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EC_ITO_SC_LOCAL_PROVIDER___CALC = MeshPackage.PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>EC Ito SC Local Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EC_ITO_SC_LOCAL_PROVIDER_OPERATION_COUNT = MeshPackage.PROVIDER_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.impl.AttitudeModelImpl <em>Attitude Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.impl.AttitudeModelImpl
	 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getAttitudeModel()
	 * @generated
	 */
	int ATTITUDE_MODEL = 13;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTITUDE_MODEL__VARIABLES = MeshPackage.MODEL__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTITUDE_MODEL__NAME = MeshPackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Quaternion</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTITUDE_MODEL__QUATERNION = MeshPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Angular Velocity</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTITUDE_MODEL__ANGULAR_VELOCITY = MeshPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Inertia Matrix</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTITUDE_MODEL__INERTIA_MATRIX = MeshPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Attitude Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTITUDE_MODEL_FEATURE_COUNT = MeshPackage.MODEL_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTITUDE_MODEL___RUN = MeshPackage.MODEL___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTITUDE_MODEL___CALC = MeshPackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Attitude Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTITUDE_MODEL_OPERATION_COUNT = MeshPackage.MODEL_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.impl.RFLinkImpl <em>RF Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.impl.RFLinkImpl
	 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getRFLink()
	 * @generated
	 */
	int RF_LINK = 14;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RF_LINK__VARIABLES = MeshPackage.MODEL__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RF_LINK__NAME = MeshPackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Azelra</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RF_LINK__AZELRA = MeshPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Attenuation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RF_LINK__ATTENUATION = MeshPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RF_LINK__FREQUENCY = MeshPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Doppler</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RF_LINK__DOPPLER = MeshPackage.MODEL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Rx Location</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RF_LINK__RX_LOCATION = MeshPackage.MODEL_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Rx Speed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RF_LINK__RX_SPEED = MeshPackage.MODEL_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Tx Location</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RF_LINK__TX_LOCATION = MeshPackage.MODEL_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Tx Speed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RF_LINK__TX_SPEED = MeshPackage.MODEL_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Tx Power</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RF_LINK__TX_POWER = MeshPackage.MODEL_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Rx Power</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RF_LINK__RX_POWER = MeshPackage.MODEL_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>RF Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RF_LINK_FEATURE_COUNT = MeshPackage.MODEL_FEATURE_COUNT + 10;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RF_LINK___RUN = MeshPackage.MODEL___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RF_LINK___CALC = MeshPackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>RF Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RF_LINK_OPERATION_COUNT = MeshPackage.MODEL_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.impl.ElectricalLoadImpl <em>Electrical Load</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.impl.ElectricalLoadImpl
	 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getElectricalLoad()
	 * @generated
	 */
	int ELECTRICAL_LOAD = 15;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELECTRICAL_LOAD__VARIABLES = MeshPackage.MODEL__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELECTRICAL_LOAD__NAME = MeshPackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Resistance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELECTRICAL_LOAD__RESISTANCE = MeshPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Power</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELECTRICAL_LOAD__POWER = MeshPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Electrical Load</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELECTRICAL_LOAD_FEATURE_COUNT = MeshPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELECTRICAL_LOAD___RUN = MeshPackage.MODEL___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELECTRICAL_LOAD___CALC = MeshPackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Electrical Load</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELECTRICAL_LOAD_OPERATION_COUNT = MeshPackage.MODEL_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.impl.SolarCellImpl <em>Solar Cell</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.impl.SolarCellImpl
	 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getSolarCell()
	 * @generated
	 */
	int SOLAR_CELL = 16;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_CELL__VARIABLES = MeshPackage.MODEL__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_CELL__NAME = MeshPackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>IShort Circuit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_CELL__ISHORT_CIRCUIT = MeshPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Temperature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_CELL__TEMPERATURE = MeshPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>K</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_CELL__K = MeshPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Solar Flux</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_CELL__SOLAR_FLUX = MeshPackage.MODEL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>IPhoto Current</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_CELL__IPHOTO_CURRENT = MeshPackage.MODEL_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Solar Cell</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_CELL_FEATURE_COUNT = MeshPackage.MODEL_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_CELL___RUN = MeshPackage.MODEL___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_CELL___CALC = MeshPackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Solar Cell</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_CELL_OPERATION_COUNT = MeshPackage.MODEL_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.impl.ReceiverImpl <em>Receiver</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.impl.ReceiverImpl
	 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getReceiver()
	 * @generated
	 */
	int RECEIVER = 17;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVER__VARIABLES = MeshPackage.MODEL__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVER__NAME = MeshPackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVER__FREQUENCY = MeshPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Power</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVER__POWER = MeshPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Gain</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVER__GAIN = MeshPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Rx Power</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVER__RX_POWER = MeshPackage.MODEL_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Receiver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVER_FEATURE_COUNT = MeshPackage.MODEL_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVER___RUN = MeshPackage.MODEL___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVER___CALC = MeshPackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Receiver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEIVER_OPERATION_COUNT = MeshPackage.MODEL_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.space.impl.TransmitterImpl <em>Transmitter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.space.impl.TransmitterImpl
	 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getTransmitter()
	 * @generated
	 */
	int TRANSMITTER = 18;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSMITTER__VARIABLES = MeshPackage.MODEL__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSMITTER__NAME = MeshPackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSMITTER__FREQUENCY = MeshPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Power</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSMITTER__POWER = MeshPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Gain</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSMITTER__GAIN = MeshPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Tx Power</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSMITTER__TX_POWER = MeshPackage.MODEL_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Transmitter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSMITTER_FEATURE_COUNT = MeshPackage.MODEL_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSMITTER___RUN = MeshPackage.MODEL___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSMITTER___CALC = MeshPackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Transmitter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSMITTER_OPERATION_COUNT = MeshPackage.MODEL_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '<em>JAT Gravity Model</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see jat.coreNOSA.forces.GravityModel
	 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getJATGravityModel()
	 * @generated
	 */
	int JAT_GRAVITY_MODEL = 19;


	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.Time <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time</em>'.
	 * @see org.thirdeye.extension.space.Time
	 * @generated
	 */
	EClass getTime();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.Time#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see org.thirdeye.extension.space.Time#getTime()
	 * @see #getTime()
	 * @generated
	 */
	EAttribute getTime_Time();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.Time#getDTime <em>DTime</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>DTime</em>'.
	 * @see org.thirdeye.extension.space.Time#getDTime()
	 * @see #getTime()
	 * @generated
	 */
	EAttribute getTime_DTime();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.Time#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.space.Time#calc()
	 * @generated
	 */
	EOperation getTime__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.GravityField <em>Gravity Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Gravity Field</em>'.
	 * @see org.thirdeye.extension.space.GravityField
	 * @generated
	 */
	EClass getGravityField();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.GravityField#getPositionList <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Position</em>'.
	 * @see org.thirdeye.extension.space.GravityField#getPositionList()
	 * @see #getGravityField()
	 * @generated
	 */
	EAttribute getGravityField_Position();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.GravityField#getCoefficientFilePath <em>Coefficient File Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Coefficient File Path</em>'.
	 * @see org.thirdeye.extension.space.GravityField#getCoefficientFilePath()
	 * @see #getGravityField()
	 * @generated
	 */
	EAttribute getGravityField_CoefficientFilePath();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.GravityField#getAccelerationList <em>Acceleration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Acceleration</em>'.
	 * @see org.thirdeye.extension.space.GravityField#getAccelerationList()
	 * @see #getGravityField()
	 * @generated
	 */
	EAttribute getGravityField_Acceleration();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.GravityField#getGravityField <em>Gravity Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Gravity Field</em>'.
	 * @see org.thirdeye.extension.space.GravityField#getGravityField()
	 * @see #getGravityField()
	 * @generated
	 */
	EAttribute getGravityField_GravityField();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.GravityField#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.space.GravityField#calc()
	 * @generated
	 */
	EOperation getGravityField__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.MagneticField <em>Magnetic Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Magnetic Field</em>'.
	 * @see org.thirdeye.extension.space.MagneticField
	 * @generated
	 */
	EClass getMagneticField();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.MagneticField#getLatLonAltList <em>Lat Lon Alt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Lat Lon Alt</em>'.
	 * @see org.thirdeye.extension.space.MagneticField#getLatLonAltList()
	 * @see #getMagneticField()
	 * @generated
	 */
	EAttribute getMagneticField_LatLonAlt();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.MagneticField#getCoefficientFilePath <em>Coefficient File Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Coefficient File Path</em>'.
	 * @see org.thirdeye.extension.space.MagneticField#getCoefficientFilePath()
	 * @see #getMagneticField()
	 * @generated
	 */
	EAttribute getMagneticField_CoefficientFilePath();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.MagneticField#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.space.MagneticField#calc()
	 * @generated
	 */
	EOperation getMagneticField__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.PointMass <em>Point Mass</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Point Mass</em>'.
	 * @see org.thirdeye.extension.space.PointMass
	 * @generated
	 */
	EClass getPointMass();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.PointMass#getMass <em>Mass</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mass</em>'.
	 * @see org.thirdeye.extension.space.PointMass#getMass()
	 * @see #getPointMass()
	 * @generated
	 */
	EAttribute getPointMass_Mass();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.PointMass#getPositionList <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Position</em>'.
	 * @see org.thirdeye.extension.space.PointMass#getPositionList()
	 * @see #getPointMass()
	 * @generated
	 */
	EAttribute getPointMass_Position();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.PointMass#getSpeedList <em>Speed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Speed</em>'.
	 * @see org.thirdeye.extension.space.PointMass#getSpeedList()
	 * @see #getPointMass()
	 * @generated
	 */
	EAttribute getPointMass_Speed();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.PointMass#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.space.PointMass#calc()
	 * @generated
	 */
	EOperation getPointMass__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.ThermalNode <em>Thermal Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Thermal Node</em>'.
	 * @see org.thirdeye.extension.space.ThermalNode
	 * @generated
	 */
	EClass getThermalNode();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.ThermalNode#getTemperature <em>Temperature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Temperature</em>'.
	 * @see org.thirdeye.extension.space.ThermalNode#getTemperature()
	 * @see #getThermalNode()
	 * @generated
	 */
	EAttribute getThermalNode_Temperature();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.ThermalNode#getQList <em>Q</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Q</em>'.
	 * @see org.thirdeye.extension.space.ThermalNode#getQList()
	 * @see #getThermalNode()
	 * @generated
	 */
	EAttribute getThermalNode_Q();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.ThermalNode#getDTemperature <em>DTemperature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>DTemperature</em>'.
	 * @see org.thirdeye.extension.space.ThermalNode#getDTemperature()
	 * @see #getThermalNode()
	 * @generated
	 */
	EAttribute getThermalNode_DTemperature();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.ThermalNode#getHeatCapacity <em>Heat Capacity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Heat Capacity</em>'.
	 * @see org.thirdeye.extension.space.ThermalNode#getHeatCapacity()
	 * @see #getThermalNode()
	 * @generated
	 */
	EAttribute getThermalNode_HeatCapacity();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.ThermalNode#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.space.ThermalNode#calc()
	 * @generated
	 */
	EOperation getThermalNode__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.ThermalConnection <em>Thermal Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Thermal Connection</em>'.
	 * @see org.thirdeye.extension.space.ThermalConnection
	 * @generated
	 */
	EClass getThermalConnection();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.ThermalConnection#getHeatConductivity <em>Heat Conductivity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Heat Conductivity</em>'.
	 * @see org.thirdeye.extension.space.ThermalConnection#getHeatConductivity()
	 * @see #getThermalConnection()
	 * @generated
	 */
	EAttribute getThermalConnection_HeatConductivity();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.ThermalConnection#getHeatFlowList <em>Heat Flow</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Heat Flow</em>'.
	 * @see org.thirdeye.extension.space.ThermalConnection#getHeatFlowList()
	 * @see #getThermalConnection()
	 * @generated
	 */
	EAttribute getThermalConnection_HeatFlow();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.ThermalConnection#getTemperatureList <em>Temperature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Temperature</em>'.
	 * @see org.thirdeye.extension.space.ThermalConnection#getTemperatureList()
	 * @see #getThermalConnection()
	 * @generated
	 */
	EAttribute getThermalConnection_Temperature();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.ThermalConnection#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.space.ThermalConnection#calc()
	 * @generated
	 */
	EOperation getThermalConnection__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.ECIToECEFProvider <em>ECI To ECEF Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ECI To ECEF Provider</em>'.
	 * @see org.thirdeye.extension.space.ECIToECEFProvider
	 * @generated
	 */
	EClass getECIToECEFProvider();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.ECIToECEFProvider#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see org.thirdeye.extension.space.ECIToECEFProvider#getTime()
	 * @see #getECIToECEFProvider()
	 * @generated
	 */
	EAttribute getECIToECEFProvider_Time();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.ECIToECEFProvider#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.space.ECIToECEFProvider#calc()
	 * @generated
	 */
	EOperation getECIToECEFProvider__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.ECEFToLatLonAltProvider <em>ECEF To Lat Lon Alt Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ECEF To Lat Lon Alt Provider</em>'.
	 * @see org.thirdeye.extension.space.ECEFToLatLonAltProvider
	 * @generated
	 */
	EClass getECEFToLatLonAltProvider();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.ECEFToLatLonAltProvider#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.space.ECEFToLatLonAltProvider#calc()
	 * @generated
	 */
	EOperation getECEFToLatLonAltProvider__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.ECEFToECIProvider <em>ECEF To ECI Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ECEF To ECI Provider</em>'.
	 * @see org.thirdeye.extension.space.ECEFToECIProvider
	 * @generated
	 */
	EClass getECEFToECIProvider();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.ECEFToECIProvider#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see org.thirdeye.extension.space.ECEFToECIProvider#getTime()
	 * @see #getECEFToECIProvider()
	 * @generated
	 */
	EAttribute getECEFToECIProvider_Time();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.ECEFToECIProvider#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.space.ECEFToECIProvider#calc()
	 * @generated
	 */
	EOperation getECEFToECIProvider__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.PointMassGravity <em>Point Mass Gravity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Point Mass Gravity</em>'.
	 * @see org.thirdeye.extension.space.PointMassGravity
	 * @generated
	 */
	EClass getPointMassGravity();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.PointMassGravity#getPositionList <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Position</em>'.
	 * @see org.thirdeye.extension.space.PointMassGravity#getPositionList()
	 * @see #getPointMassGravity()
	 * @generated
	 */
	EAttribute getPointMassGravity_Position();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.PointMassGravity#getAccelerationList <em>Acceleration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Acceleration</em>'.
	 * @see org.thirdeye.extension.space.PointMassGravity#getAccelerationList()
	 * @see #getPointMassGravity()
	 * @generated
	 */
	EAttribute getPointMassGravity_Acceleration();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.PointMassGravity#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.space.PointMassGravity#calc()
	 * @generated
	 */
	EOperation getPointMassGravity__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.FixedAttitude <em>Fixed Attitude</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fixed Attitude</em>'.
	 * @see org.thirdeye.extension.space.FixedAttitude
	 * @generated
	 */
	EClass getFixedAttitude();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.FixedAttitude#getPositionList <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Position</em>'.
	 * @see org.thirdeye.extension.space.FixedAttitude#getPositionList()
	 * @see #getFixedAttitude()
	 * @generated
	 */
	EAttribute getFixedAttitude_Position();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.FixedAttitude#getSpeedList <em>Speed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Speed</em>'.
	 * @see org.thirdeye.extension.space.FixedAttitude#getSpeedList()
	 * @see #getFixedAttitude()
	 * @generated
	 */
	EAttribute getFixedAttitude_Speed();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.FixedAttitude#getQuaternionList <em>Quaternion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Quaternion</em>'.
	 * @see org.thirdeye.extension.space.FixedAttitude#getQuaternionList()
	 * @see #getFixedAttitude()
	 * @generated
	 */
	EAttribute getFixedAttitude_Quaternion();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.FixedAttitude#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.space.FixedAttitude#calc()
	 * @generated
	 */
	EOperation getFixedAttitude__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.SCLocalToECIProvider <em>SC Local To ECI Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SC Local To ECI Provider</em>'.
	 * @see org.thirdeye.extension.space.SCLocalToECIProvider
	 * @generated
	 */
	EClass getSCLocalToECIProvider();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.SCLocalToECIProvider#getQuaternionList <em>Quaternion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Quaternion</em>'.
	 * @see org.thirdeye.extension.space.SCLocalToECIProvider#getQuaternionList()
	 * @see #getSCLocalToECIProvider()
	 * @generated
	 */
	EAttribute getSCLocalToECIProvider_Quaternion();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.SCLocalToECIProvider#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.space.SCLocalToECIProvider#calc()
	 * @generated
	 */
	EOperation getSCLocalToECIProvider__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.ECItoSCLocalProvider <em>EC Ito SC Local Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EC Ito SC Local Provider</em>'.
	 * @see org.thirdeye.extension.space.ECItoSCLocalProvider
	 * @generated
	 */
	EClass getECItoSCLocalProvider();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.ECItoSCLocalProvider#getQuaternionList <em>Quaternion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Quaternion</em>'.
	 * @see org.thirdeye.extension.space.ECItoSCLocalProvider#getQuaternionList()
	 * @see #getECItoSCLocalProvider()
	 * @generated
	 */
	EAttribute getECItoSCLocalProvider_Quaternion();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.ECItoSCLocalProvider#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.space.ECItoSCLocalProvider#calc()
	 * @generated
	 */
	EOperation getECItoSCLocalProvider__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.AttitudeModel <em>Attitude Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attitude Model</em>'.
	 * @see org.thirdeye.extension.space.AttitudeModel
	 * @generated
	 */
	EClass getAttitudeModel();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.AttitudeModel#getQuaternionList <em>Quaternion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Quaternion</em>'.
	 * @see org.thirdeye.extension.space.AttitudeModel#getQuaternionList()
	 * @see #getAttitudeModel()
	 * @generated
	 */
	EAttribute getAttitudeModel_Quaternion();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.AttitudeModel#getAngularVelocityList <em>Angular Velocity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Angular Velocity</em>'.
	 * @see org.thirdeye.extension.space.AttitudeModel#getAngularVelocityList()
	 * @see #getAttitudeModel()
	 * @generated
	 */
	EAttribute getAttitudeModel_AngularVelocity();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.AttitudeModel#getInertiaMatrixList <em>Inertia Matrix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Inertia Matrix</em>'.
	 * @see org.thirdeye.extension.space.AttitudeModel#getInertiaMatrixList()
	 * @see #getAttitudeModel()
	 * @generated
	 */
	EAttribute getAttitudeModel_InertiaMatrix();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.AttitudeModel#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.space.AttitudeModel#calc()
	 * @generated
	 */
	EOperation getAttitudeModel__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.RFLink <em>RF Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>RF Link</em>'.
	 * @see org.thirdeye.extension.space.RFLink
	 * @generated
	 */
	EClass getRFLink();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.RFLink#getAzelraList <em>Azelra</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Azelra</em>'.
	 * @see org.thirdeye.extension.space.RFLink#getAzelraList()
	 * @see #getRFLink()
	 * @generated
	 */
	EAttribute getRFLink_Azelra();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.RFLink#getAttenuation <em>Attenuation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attenuation</em>'.
	 * @see org.thirdeye.extension.space.RFLink#getAttenuation()
	 * @see #getRFLink()
	 * @generated
	 */
	EAttribute getRFLink_Attenuation();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.RFLink#getFrequency <em>Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Frequency</em>'.
	 * @see org.thirdeye.extension.space.RFLink#getFrequency()
	 * @see #getRFLink()
	 * @generated
	 */
	EAttribute getRFLink_Frequency();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.RFLink#getDoppler <em>Doppler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Doppler</em>'.
	 * @see org.thirdeye.extension.space.RFLink#getDoppler()
	 * @see #getRFLink()
	 * @generated
	 */
	EAttribute getRFLink_Doppler();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.RFLink#getRxLocationList <em>Rx Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Rx Location</em>'.
	 * @see org.thirdeye.extension.space.RFLink#getRxLocationList()
	 * @see #getRFLink()
	 * @generated
	 */
	EAttribute getRFLink_RxLocation();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.RFLink#getRxSpeedList <em>Rx Speed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Rx Speed</em>'.
	 * @see org.thirdeye.extension.space.RFLink#getRxSpeedList()
	 * @see #getRFLink()
	 * @generated
	 */
	EAttribute getRFLink_RxSpeed();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.RFLink#getTxLocationList <em>Tx Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Tx Location</em>'.
	 * @see org.thirdeye.extension.space.RFLink#getTxLocationList()
	 * @see #getRFLink()
	 * @generated
	 */
	EAttribute getRFLink_TxLocation();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extension.space.RFLink#getTxSpeedList <em>Tx Speed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Tx Speed</em>'.
	 * @see org.thirdeye.extension.space.RFLink#getTxSpeedList()
	 * @see #getRFLink()
	 * @generated
	 */
	EAttribute getRFLink_TxSpeed();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.RFLink#getTxPower <em>Tx Power</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tx Power</em>'.
	 * @see org.thirdeye.extension.space.RFLink#getTxPower()
	 * @see #getRFLink()
	 * @generated
	 */
	EAttribute getRFLink_TxPower();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.RFLink#getRxPower <em>Rx Power</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rx Power</em>'.
	 * @see org.thirdeye.extension.space.RFLink#getRxPower()
	 * @see #getRFLink()
	 * @generated
	 */
	EAttribute getRFLink_RxPower();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.RFLink#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.space.RFLink#calc()
	 * @generated
	 */
	EOperation getRFLink__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.ElectricalLoad <em>Electrical Load</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Electrical Load</em>'.
	 * @see org.thirdeye.extension.space.ElectricalLoad
	 * @generated
	 */
	EClass getElectricalLoad();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.ElectricalLoad#getResistance <em>Resistance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Resistance</em>'.
	 * @see org.thirdeye.extension.space.ElectricalLoad#getResistance()
	 * @see #getElectricalLoad()
	 * @generated
	 */
	EAttribute getElectricalLoad_Resistance();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.ElectricalLoad#getPower <em>Power</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Power</em>'.
	 * @see org.thirdeye.extension.space.ElectricalLoad#getPower()
	 * @see #getElectricalLoad()
	 * @generated
	 */
	EAttribute getElectricalLoad_Power();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.ElectricalLoad#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.space.ElectricalLoad#calc()
	 * @generated
	 */
	EOperation getElectricalLoad__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.SolarCell <em>Solar Cell</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Solar Cell</em>'.
	 * @see org.thirdeye.extension.space.SolarCell
	 * @generated
	 */
	EClass getSolarCell();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.SolarCell#getIShortCircuit <em>IShort Circuit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>IShort Circuit</em>'.
	 * @see org.thirdeye.extension.space.SolarCell#getIShortCircuit()
	 * @see #getSolarCell()
	 * @generated
	 */
	EAttribute getSolarCell_IShortCircuit();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.SolarCell#getTemperature <em>Temperature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Temperature</em>'.
	 * @see org.thirdeye.extension.space.SolarCell#getTemperature()
	 * @see #getSolarCell()
	 * @generated
	 */
	EAttribute getSolarCell_Temperature();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.SolarCell#getK <em>K</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>K</em>'.
	 * @see org.thirdeye.extension.space.SolarCell#getK()
	 * @see #getSolarCell()
	 * @generated
	 */
	EAttribute getSolarCell_K();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.SolarCell#getSolarFlux <em>Solar Flux</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Solar Flux</em>'.
	 * @see org.thirdeye.extension.space.SolarCell#getSolarFlux()
	 * @see #getSolarCell()
	 * @generated
	 */
	EAttribute getSolarCell_SolarFlux();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.SolarCell#getIPhotoCurrent <em>IPhoto Current</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>IPhoto Current</em>'.
	 * @see org.thirdeye.extension.space.SolarCell#getIPhotoCurrent()
	 * @see #getSolarCell()
	 * @generated
	 */
	EAttribute getSolarCell_IPhotoCurrent();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.SolarCell#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.space.SolarCell#calc()
	 * @generated
	 */
	EOperation getSolarCell__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.Receiver <em>Receiver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Receiver</em>'.
	 * @see org.thirdeye.extension.space.Receiver
	 * @generated
	 */
	EClass getReceiver();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.Receiver#getFrequency <em>Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Frequency</em>'.
	 * @see org.thirdeye.extension.space.Receiver#getFrequency()
	 * @see #getReceiver()
	 * @generated
	 */
	EAttribute getReceiver_Frequency();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.Receiver#getPower <em>Power</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Power</em>'.
	 * @see org.thirdeye.extension.space.Receiver#getPower()
	 * @see #getReceiver()
	 * @generated
	 */
	EAttribute getReceiver_Power();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.Receiver#getGain <em>Gain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Gain</em>'.
	 * @see org.thirdeye.extension.space.Receiver#getGain()
	 * @see #getReceiver()
	 * @generated
	 */
	EAttribute getReceiver_Gain();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.Receiver#getRxPower <em>Rx Power</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rx Power</em>'.
	 * @see org.thirdeye.extension.space.Receiver#getRxPower()
	 * @see #getReceiver()
	 * @generated
	 */
	EAttribute getReceiver_RxPower();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.Receiver#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.space.Receiver#calc()
	 * @generated
	 */
	EOperation getReceiver__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.space.Transmitter <em>Transmitter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transmitter</em>'.
	 * @see org.thirdeye.extension.space.Transmitter
	 * @generated
	 */
	EClass getTransmitter();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.Transmitter#getFrequency <em>Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Frequency</em>'.
	 * @see org.thirdeye.extension.space.Transmitter#getFrequency()
	 * @see #getTransmitter()
	 * @generated
	 */
	EAttribute getTransmitter_Frequency();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.Transmitter#getPower <em>Power</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Power</em>'.
	 * @see org.thirdeye.extension.space.Transmitter#getPower()
	 * @see #getTransmitter()
	 * @generated
	 */
	EAttribute getTransmitter_Power();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.Transmitter#getGain <em>Gain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Gain</em>'.
	 * @see org.thirdeye.extension.space.Transmitter#getGain()
	 * @see #getTransmitter()
	 * @generated
	 */
	EAttribute getTransmitter_Gain();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.space.Transmitter#getTxPower <em>Tx Power</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tx Power</em>'.
	 * @see org.thirdeye.extension.space.Transmitter#getTxPower()
	 * @see #getTransmitter()
	 * @generated
	 */
	EAttribute getTransmitter_TxPower();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.space.Transmitter#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.space.Transmitter#calc()
	 * @generated
	 */
	EOperation getTransmitter__Calc();

	/**
	 * Returns the meta object for data type '{@link jat.coreNOSA.forces.GravityModel <em>JAT Gravity Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>JAT Gravity Model</em>'.
	 * @see jat.coreNOSA.forces.GravityModel
	 * @model instanceClass="jat.coreNOSA.forces.GravityModel" serializeable="false"
	 * @generated
	 */
	EDataType getJATGravityModel();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SpaceFactory getSpaceFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.impl.TimeImpl <em>Time</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.impl.TimeImpl
		 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getTime()
		 * @generated
		 */
		EClass TIME = eINSTANCE.getTime();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME__TIME = eINSTANCE.getTime_Time();

		/**
		 * The meta object literal for the '<em><b>DTime</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME__DTIME = eINSTANCE.getTime_DTime();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TIME___CALC = eINSTANCE.getTime__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.impl.GravityFieldImpl <em>Gravity Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.impl.GravityFieldImpl
		 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getGravityField()
		 * @generated
		 */
		EClass GRAVITY_FIELD = eINSTANCE.getGravityField();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRAVITY_FIELD__POSITION = eINSTANCE.getGravityField_Position();

		/**
		 * The meta object literal for the '<em><b>Coefficient File Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRAVITY_FIELD__COEFFICIENT_FILE_PATH = eINSTANCE.getGravityField_CoefficientFilePath();

		/**
		 * The meta object literal for the '<em><b>Acceleration</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRAVITY_FIELD__ACCELERATION = eINSTANCE.getGravityField_Acceleration();

		/**
		 * The meta object literal for the '<em><b>Gravity Field</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRAVITY_FIELD__GRAVITY_FIELD = eINSTANCE.getGravityField_GravityField();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation GRAVITY_FIELD___CALC = eINSTANCE.getGravityField__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.impl.MagneticFieldImpl <em>Magnetic Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.impl.MagneticFieldImpl
		 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getMagneticField()
		 * @generated
		 */
		EClass MAGNETIC_FIELD = eINSTANCE.getMagneticField();

		/**
		 * The meta object literal for the '<em><b>Lat Lon Alt</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAGNETIC_FIELD__LAT_LON_ALT = eINSTANCE.getMagneticField_LatLonAlt();

		/**
		 * The meta object literal for the '<em><b>Coefficient File Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAGNETIC_FIELD__COEFFICIENT_FILE_PATH = eINSTANCE.getMagneticField_CoefficientFilePath();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MAGNETIC_FIELD___CALC = eINSTANCE.getMagneticField__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.impl.PointMassImpl <em>Point Mass</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.impl.PointMassImpl
		 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getPointMass()
		 * @generated
		 */
		EClass POINT_MASS = eINSTANCE.getPointMass();

		/**
		 * The meta object literal for the '<em><b>Mass</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POINT_MASS__MASS = eINSTANCE.getPointMass_Mass();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POINT_MASS__POSITION = eINSTANCE.getPointMass_Position();

		/**
		 * The meta object literal for the '<em><b>Speed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POINT_MASS__SPEED = eINSTANCE.getPointMass_Speed();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation POINT_MASS___CALC = eINSTANCE.getPointMass__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.impl.ThermalNodeImpl <em>Thermal Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.impl.ThermalNodeImpl
		 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getThermalNode()
		 * @generated
		 */
		EClass THERMAL_NODE = eINSTANCE.getThermalNode();

		/**
		 * The meta object literal for the '<em><b>Temperature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute THERMAL_NODE__TEMPERATURE = eINSTANCE.getThermalNode_Temperature();

		/**
		 * The meta object literal for the '<em><b>Q</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute THERMAL_NODE__Q = eINSTANCE.getThermalNode_Q();

		/**
		 * The meta object literal for the '<em><b>DTemperature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute THERMAL_NODE__DTEMPERATURE = eINSTANCE.getThermalNode_DTemperature();

		/**
		 * The meta object literal for the '<em><b>Heat Capacity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute THERMAL_NODE__HEAT_CAPACITY = eINSTANCE.getThermalNode_HeatCapacity();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation THERMAL_NODE___CALC = eINSTANCE.getThermalNode__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.impl.ThermalConnectionImpl <em>Thermal Connection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.impl.ThermalConnectionImpl
		 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getThermalConnection()
		 * @generated
		 */
		EClass THERMAL_CONNECTION = eINSTANCE.getThermalConnection();

		/**
		 * The meta object literal for the '<em><b>Heat Conductivity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute THERMAL_CONNECTION__HEAT_CONDUCTIVITY = eINSTANCE.getThermalConnection_HeatConductivity();

		/**
		 * The meta object literal for the '<em><b>Heat Flow</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute THERMAL_CONNECTION__HEAT_FLOW = eINSTANCE.getThermalConnection_HeatFlow();

		/**
		 * The meta object literal for the '<em><b>Temperature</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute THERMAL_CONNECTION__TEMPERATURE = eINSTANCE.getThermalConnection_Temperature();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation THERMAL_CONNECTION___CALC = eINSTANCE.getThermalConnection__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.impl.ECIToECEFProviderImpl <em>ECI To ECEF Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.impl.ECIToECEFProviderImpl
		 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getECIToECEFProvider()
		 * @generated
		 */
		EClass ECI_TO_ECEF_PROVIDER = eINSTANCE.getECIToECEFProvider();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ECI_TO_ECEF_PROVIDER__TIME = eINSTANCE.getECIToECEFProvider_Time();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ECI_TO_ECEF_PROVIDER___CALC = eINSTANCE.getECIToECEFProvider__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.impl.ECEFToLatLonAltProviderImpl <em>ECEF To Lat Lon Alt Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.impl.ECEFToLatLonAltProviderImpl
		 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getECEFToLatLonAltProvider()
		 * @generated
		 */
		EClass ECEF_TO_LAT_LON_ALT_PROVIDER = eINSTANCE.getECEFToLatLonAltProvider();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ECEF_TO_LAT_LON_ALT_PROVIDER___CALC = eINSTANCE.getECEFToLatLonAltProvider__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.impl.ECEFToECIProviderImpl <em>ECEF To ECI Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.impl.ECEFToECIProviderImpl
		 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getECEFToECIProvider()
		 * @generated
		 */
		EClass ECEF_TO_ECI_PROVIDER = eINSTANCE.getECEFToECIProvider();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ECEF_TO_ECI_PROVIDER__TIME = eINSTANCE.getECEFToECIProvider_Time();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ECEF_TO_ECI_PROVIDER___CALC = eINSTANCE.getECEFToECIProvider__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.impl.PointMassGravityImpl <em>Point Mass Gravity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.impl.PointMassGravityImpl
		 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getPointMassGravity()
		 * @generated
		 */
		EClass POINT_MASS_GRAVITY = eINSTANCE.getPointMassGravity();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POINT_MASS_GRAVITY__POSITION = eINSTANCE.getPointMassGravity_Position();

		/**
		 * The meta object literal for the '<em><b>Acceleration</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POINT_MASS_GRAVITY__ACCELERATION = eINSTANCE.getPointMassGravity_Acceleration();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation POINT_MASS_GRAVITY___CALC = eINSTANCE.getPointMassGravity__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.impl.FixedAttitudeImpl <em>Fixed Attitude</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.impl.FixedAttitudeImpl
		 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getFixedAttitude()
		 * @generated
		 */
		EClass FIXED_ATTITUDE = eINSTANCE.getFixedAttitude();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_ATTITUDE__POSITION = eINSTANCE.getFixedAttitude_Position();

		/**
		 * The meta object literal for the '<em><b>Speed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_ATTITUDE__SPEED = eINSTANCE.getFixedAttitude_Speed();

		/**
		 * The meta object literal for the '<em><b>Quaternion</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_ATTITUDE__QUATERNION = eINSTANCE.getFixedAttitude_Quaternion();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FIXED_ATTITUDE___CALC = eINSTANCE.getFixedAttitude__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.impl.SCLocalToECIProviderImpl <em>SC Local To ECI Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.impl.SCLocalToECIProviderImpl
		 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getSCLocalToECIProvider()
		 * @generated
		 */
		EClass SC_LOCAL_TO_ECI_PROVIDER = eINSTANCE.getSCLocalToECIProvider();

		/**
		 * The meta object literal for the '<em><b>Quaternion</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SC_LOCAL_TO_ECI_PROVIDER__QUATERNION = eINSTANCE.getSCLocalToECIProvider_Quaternion();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SC_LOCAL_TO_ECI_PROVIDER___CALC = eINSTANCE.getSCLocalToECIProvider__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.impl.ECItoSCLocalProviderImpl <em>EC Ito SC Local Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.impl.ECItoSCLocalProviderImpl
		 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getECItoSCLocalProvider()
		 * @generated
		 */
		EClass EC_ITO_SC_LOCAL_PROVIDER = eINSTANCE.getECItoSCLocalProvider();

		/**
		 * The meta object literal for the '<em><b>Quaternion</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EC_ITO_SC_LOCAL_PROVIDER__QUATERNION = eINSTANCE.getECItoSCLocalProvider_Quaternion();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation EC_ITO_SC_LOCAL_PROVIDER___CALC = eINSTANCE.getECItoSCLocalProvider__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.impl.AttitudeModelImpl <em>Attitude Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.impl.AttitudeModelImpl
		 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getAttitudeModel()
		 * @generated
		 */
		EClass ATTITUDE_MODEL = eINSTANCE.getAttitudeModel();

		/**
		 * The meta object literal for the '<em><b>Quaternion</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTITUDE_MODEL__QUATERNION = eINSTANCE.getAttitudeModel_Quaternion();

		/**
		 * The meta object literal for the '<em><b>Angular Velocity</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTITUDE_MODEL__ANGULAR_VELOCITY = eINSTANCE.getAttitudeModel_AngularVelocity();

		/**
		 * The meta object literal for the '<em><b>Inertia Matrix</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTITUDE_MODEL__INERTIA_MATRIX = eINSTANCE.getAttitudeModel_InertiaMatrix();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATTITUDE_MODEL___CALC = eINSTANCE.getAttitudeModel__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.impl.RFLinkImpl <em>RF Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.impl.RFLinkImpl
		 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getRFLink()
		 * @generated
		 */
		EClass RF_LINK = eINSTANCE.getRFLink();

		/**
		 * The meta object literal for the '<em><b>Azelra</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RF_LINK__AZELRA = eINSTANCE.getRFLink_Azelra();

		/**
		 * The meta object literal for the '<em><b>Attenuation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RF_LINK__ATTENUATION = eINSTANCE.getRFLink_Attenuation();

		/**
		 * The meta object literal for the '<em><b>Frequency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RF_LINK__FREQUENCY = eINSTANCE.getRFLink_Frequency();

		/**
		 * The meta object literal for the '<em><b>Doppler</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RF_LINK__DOPPLER = eINSTANCE.getRFLink_Doppler();

		/**
		 * The meta object literal for the '<em><b>Rx Location</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RF_LINK__RX_LOCATION = eINSTANCE.getRFLink_RxLocation();

		/**
		 * The meta object literal for the '<em><b>Rx Speed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RF_LINK__RX_SPEED = eINSTANCE.getRFLink_RxSpeed();

		/**
		 * The meta object literal for the '<em><b>Tx Location</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RF_LINK__TX_LOCATION = eINSTANCE.getRFLink_TxLocation();

		/**
		 * The meta object literal for the '<em><b>Tx Speed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RF_LINK__TX_SPEED = eINSTANCE.getRFLink_TxSpeed();

		/**
		 * The meta object literal for the '<em><b>Tx Power</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RF_LINK__TX_POWER = eINSTANCE.getRFLink_TxPower();

		/**
		 * The meta object literal for the '<em><b>Rx Power</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RF_LINK__RX_POWER = eINSTANCE.getRFLink_RxPower();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RF_LINK___CALC = eINSTANCE.getRFLink__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.impl.ElectricalLoadImpl <em>Electrical Load</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.impl.ElectricalLoadImpl
		 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getElectricalLoad()
		 * @generated
		 */
		EClass ELECTRICAL_LOAD = eINSTANCE.getElectricalLoad();

		/**
		 * The meta object literal for the '<em><b>Resistance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELECTRICAL_LOAD__RESISTANCE = eINSTANCE.getElectricalLoad_Resistance();

		/**
		 * The meta object literal for the '<em><b>Power</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELECTRICAL_LOAD__POWER = eINSTANCE.getElectricalLoad_Power();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ELECTRICAL_LOAD___CALC = eINSTANCE.getElectricalLoad__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.impl.SolarCellImpl <em>Solar Cell</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.impl.SolarCellImpl
		 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getSolarCell()
		 * @generated
		 */
		EClass SOLAR_CELL = eINSTANCE.getSolarCell();

		/**
		 * The meta object literal for the '<em><b>IShort Circuit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLAR_CELL__ISHORT_CIRCUIT = eINSTANCE.getSolarCell_IShortCircuit();

		/**
		 * The meta object literal for the '<em><b>Temperature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLAR_CELL__TEMPERATURE = eINSTANCE.getSolarCell_Temperature();

		/**
		 * The meta object literal for the '<em><b>K</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLAR_CELL__K = eINSTANCE.getSolarCell_K();

		/**
		 * The meta object literal for the '<em><b>Solar Flux</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLAR_CELL__SOLAR_FLUX = eINSTANCE.getSolarCell_SolarFlux();

		/**
		 * The meta object literal for the '<em><b>IPhoto Current</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLAR_CELL__IPHOTO_CURRENT = eINSTANCE.getSolarCell_IPhotoCurrent();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SOLAR_CELL___CALC = eINSTANCE.getSolarCell__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.impl.ReceiverImpl <em>Receiver</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.impl.ReceiverImpl
		 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getReceiver()
		 * @generated
		 */
		EClass RECEIVER = eINSTANCE.getReceiver();

		/**
		 * The meta object literal for the '<em><b>Frequency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RECEIVER__FREQUENCY = eINSTANCE.getReceiver_Frequency();

		/**
		 * The meta object literal for the '<em><b>Power</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RECEIVER__POWER = eINSTANCE.getReceiver_Power();

		/**
		 * The meta object literal for the '<em><b>Gain</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RECEIVER__GAIN = eINSTANCE.getReceiver_Gain();

		/**
		 * The meta object literal for the '<em><b>Rx Power</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RECEIVER__RX_POWER = eINSTANCE.getReceiver_RxPower();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RECEIVER___CALC = eINSTANCE.getReceiver__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.space.impl.TransmitterImpl <em>Transmitter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.space.impl.TransmitterImpl
		 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getTransmitter()
		 * @generated
		 */
		EClass TRANSMITTER = eINSTANCE.getTransmitter();

		/**
		 * The meta object literal for the '<em><b>Frequency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSMITTER__FREQUENCY = eINSTANCE.getTransmitter_Frequency();

		/**
		 * The meta object literal for the '<em><b>Power</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSMITTER__POWER = eINSTANCE.getTransmitter_Power();

		/**
		 * The meta object literal for the '<em><b>Gain</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSMITTER__GAIN = eINSTANCE.getTransmitter_Gain();

		/**
		 * The meta object literal for the '<em><b>Tx Power</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSMITTER__TX_POWER = eINSTANCE.getTransmitter_TxPower();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRANSMITTER___CALC = eINSTANCE.getTransmitter__Calc();

		/**
		 * The meta object literal for the '<em>JAT Gravity Model</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see jat.coreNOSA.forces.GravityModel
		 * @see org.thirdeye.extension.space.impl.SpacePackageImpl#getJATGravityModel()
		 * @generated
		 */
		EDataType JAT_GRAVITY_MODEL = eINSTANCE.getJATGravityModel();

	}

} //SpacePackage
