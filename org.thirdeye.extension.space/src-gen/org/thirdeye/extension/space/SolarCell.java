/**
 */
package org.thirdeye.extension.space;

import org.thirdeye.core.mesh.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Solar Cell</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.SolarCell#getIShortCircuit <em>IShort Circuit</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.SolarCell#getTemperature <em>Temperature</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.SolarCell#getK <em>K</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.SolarCell#getSolarFlux <em>Solar Flux</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.SolarCell#getIPhotoCurrent <em>IPhoto Current</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extension.space.SpacePackage#getSolarCell()
 * @model
 * @generated
 */
public interface SolarCell extends Model {
	/**
	 * Returns the value of the '<em><b>IShort Circuit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>IShort Circuit</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>IShort Circuit</em>' attribute.
	 * @see #setIShortCircuit(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getSolarCell_IShortCircuit()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Double getIShortCircuit();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.SolarCell#getIShortCircuit <em>IShort Circuit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>IShort Circuit</em>' attribute.
	 * @see #getIShortCircuit()
	 * @generated
	 */
	void setIShortCircuit(Double value);

	/**
	 * Returns the value of the '<em><b>Temperature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Temperature</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Temperature</em>' attribute.
	 * @see #setTemperature(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getSolarCell_Temperature()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Double getTemperature();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.SolarCell#getTemperature <em>Temperature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Temperature</em>' attribute.
	 * @see #getTemperature()
	 * @generated
	 */
	void setTemperature(Double value);

	/**
	 * Returns the value of the '<em><b>K</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>K</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>K</em>' attribute.
	 * @see #setK(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getSolarCell_K()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Double getK();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.SolarCell#getK <em>K</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>K</em>' attribute.
	 * @see #getK()
	 * @generated
	 */
	void setK(Double value);

	/**
	 * Returns the value of the '<em><b>Solar Flux</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Solar Flux</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Solar Flux</em>' attribute.
	 * @see #setSolarFlux(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getSolarCell_SolarFlux()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Double getSolarFlux();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.SolarCell#getSolarFlux <em>Solar Flux</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Solar Flux</em>' attribute.
	 * @see #getSolarFlux()
	 * @generated
	 */
	void setSolarFlux(Double value);

	/**
	 * Returns the value of the '<em><b>IPhoto Current</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>IPhoto Current</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>IPhoto Current</em>' attribute.
	 * @see #setIPhotoCurrent(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getSolarCell_IPhotoCurrent()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Double getIPhotoCurrent();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.SolarCell#getIPhotoCurrent <em>IPhoto Current</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>IPhoto Current</em>' attribute.
	 * @see #getIPhotoCurrent()
	 * @generated
	 */
	void setIPhotoCurrent(Double value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void calc();

} // SolarCell
