/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.thirdeye.core.mesh.Connection;
import org.thirdeye.core.mesh.Model;
import org.thirdeye.core.mesh.Node;

import org.thirdeye.core.mesh.Provider;
import org.thirdeye.extension.space.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.thirdeye.extension.space.SpacePackage
 * @generated
 */
public class SpaceAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SpacePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpaceAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = SpacePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SpaceSwitch<Adapter> modelSwitch =
		new SpaceSwitch<Adapter>() {
			@Override
			public Adapter caseTime(Time object) {
				return createTimeAdapter();
			}
			@Override
			public Adapter caseGravityField(GravityField object) {
				return createGravityFieldAdapter();
			}
			@Override
			public Adapter caseMagneticField(MagneticField object) {
				return createMagneticFieldAdapter();
			}
			@Override
			public Adapter casePointMass(PointMass object) {
				return createPointMassAdapter();
			}
			@Override
			public Adapter caseThermalNode(ThermalNode object) {
				return createThermalNodeAdapter();
			}
			@Override
			public Adapter caseThermalConnection(ThermalConnection object) {
				return createThermalConnectionAdapter();
			}
			@Override
			public Adapter caseECIToECEFProvider(ECIToECEFProvider object) {
				return createECIToECEFProviderAdapter();
			}
			@Override
			public Adapter caseECEFToLatLonAltProvider(ECEFToLatLonAltProvider object) {
				return createECEFToLatLonAltProviderAdapter();
			}
			@Override
			public Adapter caseECEFToECIProvider(ECEFToECIProvider object) {
				return createECEFToECIProviderAdapter();
			}
			@Override
			public Adapter casePointMassGravity(PointMassGravity object) {
				return createPointMassGravityAdapter();
			}
			@Override
			public Adapter caseFixedAttitude(FixedAttitude object) {
				return createFixedAttitudeAdapter();
			}
			@Override
			public Adapter caseSCLocalToECIProvider(SCLocalToECIProvider object) {
				return createSCLocalToECIProviderAdapter();
			}
			@Override
			public Adapter caseECItoSCLocalProvider(ECItoSCLocalProvider object) {
				return createECItoSCLocalProviderAdapter();
			}
			@Override
			public Adapter caseAttitudeModel(AttitudeModel object) {
				return createAttitudeModelAdapter();
			}
			@Override
			public Adapter caseRFLink(RFLink object) {
				return createRFLinkAdapter();
			}
			@Override
			public Adapter caseElectricalLoad(ElectricalLoad object) {
				return createElectricalLoadAdapter();
			}
			@Override
			public Adapter caseSolarCell(SolarCell object) {
				return createSolarCellAdapter();
			}
			@Override
			public Adapter caseReceiver(Receiver object) {
				return createReceiverAdapter();
			}
			@Override
			public Adapter caseTransmitter(Transmitter object) {
				return createTransmitterAdapter();
			}
			@Override
			public Adapter caseERunnable(Runnable object) {
				return createERunnableAdapter();
			}
			@Override
			public Adapter caseNode(Node object) {
				return createNodeAdapter();
			}
			@Override
			public Adapter caseModel(Model object) {
				return createModelAdapter();
			}
			@Override
			public Adapter caseConnection(Connection object) {
				return createConnectionAdapter();
			}
			@Override
			public Adapter caseProvider(Provider object) {
				return createProviderAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.extension.space.Time <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.extension.space.Time
	 * @generated
	 */
	public Adapter createTimeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.extension.space.GravityField <em>Gravity Field</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.extension.space.GravityField
	 * @generated
	 */
	public Adapter createGravityFieldAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.extension.space.MagneticField <em>Magnetic Field</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.extension.space.MagneticField
	 * @generated
	 */
	public Adapter createMagneticFieldAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.extension.space.PointMass <em>Point Mass</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.extension.space.PointMass
	 * @generated
	 */
	public Adapter createPointMassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.extension.space.ThermalNode <em>Thermal Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.extension.space.ThermalNode
	 * @generated
	 */
	public Adapter createThermalNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.extension.space.ThermalConnection <em>Thermal Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.extension.space.ThermalConnection
	 * @generated
	 */
	public Adapter createThermalConnectionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.extension.space.ECIToECEFProvider <em>ECI To ECEF Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.extension.space.ECIToECEFProvider
	 * @generated
	 */
	public Adapter createECIToECEFProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.extension.space.ECEFToLatLonAltProvider <em>ECEF To Lat Lon Alt Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.extension.space.ECEFToLatLonAltProvider
	 * @generated
	 */
	public Adapter createECEFToLatLonAltProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.extension.space.ECEFToECIProvider <em>ECEF To ECI Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.extension.space.ECEFToECIProvider
	 * @generated
	 */
	public Adapter createECEFToECIProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.extension.space.PointMassGravity <em>Point Mass Gravity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.extension.space.PointMassGravity
	 * @generated
	 */
	public Adapter createPointMassGravityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.extension.space.FixedAttitude <em>Fixed Attitude</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.extension.space.FixedAttitude
	 * @generated
	 */
	public Adapter createFixedAttitudeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.extension.space.SCLocalToECIProvider <em>SC Local To ECI Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.extension.space.SCLocalToECIProvider
	 * @generated
	 */
	public Adapter createSCLocalToECIProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.extension.space.ECItoSCLocalProvider <em>EC Ito SC Local Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.extension.space.ECItoSCLocalProvider
	 * @generated
	 */
	public Adapter createECItoSCLocalProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.extension.space.AttitudeModel <em>Attitude Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.extension.space.AttitudeModel
	 * @generated
	 */
	public Adapter createAttitudeModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.extension.space.RFLink <em>RF Link</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.extension.space.RFLink
	 * @generated
	 */
	public Adapter createRFLinkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.extension.space.ElectricalLoad <em>Electrical Load</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.extension.space.ElectricalLoad
	 * @generated
	 */
	public Adapter createElectricalLoadAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.extension.space.SolarCell <em>Solar Cell</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.extension.space.SolarCell
	 * @generated
	 */
	public Adapter createSolarCellAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.extension.space.Receiver <em>Receiver</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.extension.space.Receiver
	 * @generated
	 */
	public Adapter createReceiverAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.extension.space.Transmitter <em>Transmitter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.extension.space.Transmitter
	 * @generated
	 */
	public Adapter createTransmitterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.lang.Runnable <em>ERunnable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.lang.Runnable
	 * @generated
	 */
	public Adapter createERunnableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.core.mesh.Node <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.core.mesh.Node
	 * @generated
	 */
	public Adapter createNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.core.mesh.Model <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.core.mesh.Model
	 * @generated
	 */
	public Adapter createModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.core.mesh.Connection <em>Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.core.mesh.Connection
	 * @generated
	 */
	public Adapter createConnectionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.thirdeye.core.mesh.Provider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.thirdeye.core.mesh.Provider
	 * @generated
	 */
	public Adapter createProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //SpaceAdapterFactory
