/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.thirdeye.core.mesh.Connection;
import org.thirdeye.core.mesh.Model;
import org.thirdeye.core.mesh.Node;

import org.thirdeye.core.mesh.Provider;
import org.thirdeye.extension.space.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.thirdeye.extension.space.SpacePackage
 * @generated
 */
public class SpaceSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SpacePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpaceSwitch() {
		if (modelPackage == null) {
			modelPackage = SpacePackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case SpacePackage.TIME: {
				Time time = (Time)theEObject;
				T result = caseTime(time);
				if (result == null) result = caseModel(time);
				if (result == null) result = caseNode(time);
				if (result == null) result = caseERunnable(time);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SpacePackage.GRAVITY_FIELD: {
				GravityField gravityField = (GravityField)theEObject;
				T result = caseGravityField(gravityField);
				if (result == null) result = caseModel(gravityField);
				if (result == null) result = caseNode(gravityField);
				if (result == null) result = caseERunnable(gravityField);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SpacePackage.MAGNETIC_FIELD: {
				MagneticField magneticField = (MagneticField)theEObject;
				T result = caseMagneticField(magneticField);
				if (result == null) result = caseModel(magneticField);
				if (result == null) result = caseNode(magneticField);
				if (result == null) result = caseERunnable(magneticField);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SpacePackage.POINT_MASS: {
				PointMass pointMass = (PointMass)theEObject;
				T result = casePointMass(pointMass);
				if (result == null) result = caseModel(pointMass);
				if (result == null) result = caseNode(pointMass);
				if (result == null) result = caseERunnable(pointMass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SpacePackage.THERMAL_NODE: {
				ThermalNode thermalNode = (ThermalNode)theEObject;
				T result = caseThermalNode(thermalNode);
				if (result == null) result = caseModel(thermalNode);
				if (result == null) result = caseNode(thermalNode);
				if (result == null) result = caseERunnable(thermalNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SpacePackage.THERMAL_CONNECTION: {
				ThermalConnection thermalConnection = (ThermalConnection)theEObject;
				T result = caseThermalConnection(thermalConnection);
				if (result == null) result = caseConnection(thermalConnection);
				if (result == null) result = caseNode(thermalConnection);
				if (result == null) result = caseERunnable(thermalConnection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SpacePackage.ECI_TO_ECEF_PROVIDER: {
				ECIToECEFProvider eciToECEFProvider = (ECIToECEFProvider)theEObject;
				T result = caseECIToECEFProvider(eciToECEFProvider);
				if (result == null) result = caseProvider(eciToECEFProvider);
				if (result == null) result = caseNode(eciToECEFProvider);
				if (result == null) result = caseERunnable(eciToECEFProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SpacePackage.ECEF_TO_LAT_LON_ALT_PROVIDER: {
				ECEFToLatLonAltProvider ecefToLatLonAltProvider = (ECEFToLatLonAltProvider)theEObject;
				T result = caseECEFToLatLonAltProvider(ecefToLatLonAltProvider);
				if (result == null) result = caseProvider(ecefToLatLonAltProvider);
				if (result == null) result = caseNode(ecefToLatLonAltProvider);
				if (result == null) result = caseERunnable(ecefToLatLonAltProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SpacePackage.ECEF_TO_ECI_PROVIDER: {
				ECEFToECIProvider ecefToECIProvider = (ECEFToECIProvider)theEObject;
				T result = caseECEFToECIProvider(ecefToECIProvider);
				if (result == null) result = caseProvider(ecefToECIProvider);
				if (result == null) result = caseNode(ecefToECIProvider);
				if (result == null) result = caseERunnable(ecefToECIProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SpacePackage.POINT_MASS_GRAVITY: {
				PointMassGravity pointMassGravity = (PointMassGravity)theEObject;
				T result = casePointMassGravity(pointMassGravity);
				if (result == null) result = caseModel(pointMassGravity);
				if (result == null) result = caseNode(pointMassGravity);
				if (result == null) result = caseERunnable(pointMassGravity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SpacePackage.FIXED_ATTITUDE: {
				FixedAttitude fixedAttitude = (FixedAttitude)theEObject;
				T result = caseFixedAttitude(fixedAttitude);
				if (result == null) result = caseModel(fixedAttitude);
				if (result == null) result = caseNode(fixedAttitude);
				if (result == null) result = caseERunnable(fixedAttitude);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SpacePackage.SC_LOCAL_TO_ECI_PROVIDER: {
				SCLocalToECIProvider scLocalToECIProvider = (SCLocalToECIProvider)theEObject;
				T result = caseSCLocalToECIProvider(scLocalToECIProvider);
				if (result == null) result = caseProvider(scLocalToECIProvider);
				if (result == null) result = caseNode(scLocalToECIProvider);
				if (result == null) result = caseERunnable(scLocalToECIProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SpacePackage.EC_ITO_SC_LOCAL_PROVIDER: {
				ECItoSCLocalProvider ecItoSCLocalProvider = (ECItoSCLocalProvider)theEObject;
				T result = caseECItoSCLocalProvider(ecItoSCLocalProvider);
				if (result == null) result = caseProvider(ecItoSCLocalProvider);
				if (result == null) result = caseNode(ecItoSCLocalProvider);
				if (result == null) result = caseERunnable(ecItoSCLocalProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SpacePackage.ATTITUDE_MODEL: {
				AttitudeModel attitudeModel = (AttitudeModel)theEObject;
				T result = caseAttitudeModel(attitudeModel);
				if (result == null) result = caseModel(attitudeModel);
				if (result == null) result = caseNode(attitudeModel);
				if (result == null) result = caseERunnable(attitudeModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SpacePackage.RF_LINK: {
				RFLink rfLink = (RFLink)theEObject;
				T result = caseRFLink(rfLink);
				if (result == null) result = caseModel(rfLink);
				if (result == null) result = caseNode(rfLink);
				if (result == null) result = caseERunnable(rfLink);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SpacePackage.ELECTRICAL_LOAD: {
				ElectricalLoad electricalLoad = (ElectricalLoad)theEObject;
				T result = caseElectricalLoad(electricalLoad);
				if (result == null) result = caseModel(electricalLoad);
				if (result == null) result = caseNode(electricalLoad);
				if (result == null) result = caseERunnable(electricalLoad);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SpacePackage.SOLAR_CELL: {
				SolarCell solarCell = (SolarCell)theEObject;
				T result = caseSolarCell(solarCell);
				if (result == null) result = caseModel(solarCell);
				if (result == null) result = caseNode(solarCell);
				if (result == null) result = caseERunnable(solarCell);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SpacePackage.RECEIVER: {
				Receiver receiver = (Receiver)theEObject;
				T result = caseReceiver(receiver);
				if (result == null) result = caseModel(receiver);
				if (result == null) result = caseNode(receiver);
				if (result == null) result = caseERunnable(receiver);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SpacePackage.TRANSMITTER: {
				Transmitter transmitter = (Transmitter)theEObject;
				T result = caseTransmitter(transmitter);
				if (result == null) result = caseModel(transmitter);
				if (result == null) result = caseNode(transmitter);
				if (result == null) result = caseERunnable(transmitter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Time</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Time</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTime(Time object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Gravity Field</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gravity Field</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGravityField(GravityField object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Magnetic Field</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Magnetic Field</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMagneticField(MagneticField object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Point Mass</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Point Mass</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePointMass(PointMass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Thermal Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Thermal Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseThermalNode(ThermalNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Thermal Connection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Thermal Connection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseThermalConnection(ThermalConnection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ECI To ECEF Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ECI To ECEF Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseECIToECEFProvider(ECIToECEFProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ECEF To Lat Lon Alt Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ECEF To Lat Lon Alt Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseECEFToLatLonAltProvider(ECEFToLatLonAltProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ECEF To ECI Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ECEF To ECI Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseECEFToECIProvider(ECEFToECIProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Point Mass Gravity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Point Mass Gravity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePointMassGravity(PointMassGravity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fixed Attitude</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fixed Attitude</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFixedAttitude(FixedAttitude object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SC Local To ECI Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SC Local To ECI Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSCLocalToECIProvider(SCLocalToECIProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EC Ito SC Local Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EC Ito SC Local Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseECItoSCLocalProvider(ECItoSCLocalProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attitude Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attitude Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttitudeModel(AttitudeModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>RF Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>RF Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRFLink(RFLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Electrical Load</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Electrical Load</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElectricalLoad(ElectricalLoad object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Solar Cell</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Solar Cell</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSolarCell(SolarCell object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Receiver</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Receiver</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReceiver(Receiver object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Transmitter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Transmitter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransmitter(Transmitter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ERunnable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ERunnable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseERunnable(Runnable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNode(Node object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModel(Model object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnection(Connection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProvider(Provider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //SpaceSwitch
