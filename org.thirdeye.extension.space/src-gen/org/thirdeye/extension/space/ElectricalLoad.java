/**
 */
package org.thirdeye.extension.space;

import org.thirdeye.core.mesh.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Electrical Load</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.ElectricalLoad#getResistance <em>Resistance</em>}</li>
 *   <li>{@link org.thirdeye.extension.space.ElectricalLoad#getPower <em>Power</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extension.space.SpacePackage#getElectricalLoad()
 * @model
 * @generated
 */
public interface ElectricalLoad extends Model {
	/**
	 * Returns the value of the '<em><b>Resistance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resistance</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resistance</em>' attribute.
	 * @see #setResistance(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getElectricalLoad_Resistance()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Double getResistance();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.ElectricalLoad#getResistance <em>Resistance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resistance</em>' attribute.
	 * @see #getResistance()
	 * @generated
	 */
	void setResistance(Double value);

	/**
	 * Returns the value of the '<em><b>Power</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Power</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Power</em>' attribute.
	 * @see #setPower(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getElectricalLoad_Power()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Double getPower();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.ElectricalLoad#getPower <em>Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Power</em>' attribute.
	 * @see #getPower()
	 * @generated
	 */
	void setPower(Double value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void calc();

} // ElectricalLoad
