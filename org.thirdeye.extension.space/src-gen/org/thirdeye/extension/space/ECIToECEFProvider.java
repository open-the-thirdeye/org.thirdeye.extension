/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space;

import org.thirdeye.core.mesh.Provider;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ECI To ECEF Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extension.space.ECIToECEFProvider#getTime <em>Time</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extension.space.SpacePackage#getECIToECEFProvider()
 * @model
 * @generated
 */
public interface ECIToECEFProvider extends Provider {
	/**
	 * Returns the value of the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time</em>' attribute.
	 * @see #setTime(Double)
	 * @see org.thirdeye.extension.space.SpacePackage#getECIToECEFProvider_Time()
	 * @model required="true"
	 *        annotation="ThirdEye unit='s'"
	 * @generated
	 */
	Double getTime();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.space.ECIToECEFProvider#getTime <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time</em>' attribute.
	 * @see #getTime()
	 * @generated
	 */
	void setTime(Double value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void calc();

} // ECIToECEFProvider
