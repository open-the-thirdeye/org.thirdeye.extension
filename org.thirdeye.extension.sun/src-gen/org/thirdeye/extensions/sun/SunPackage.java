/**
 */
package org.thirdeye.extensions.sun;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import org.thirdeye.core.mesh.MeshPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.thirdeye.extensions.sun.SunFactory
 * @model kind="package"
 * @generated
 */
public interface SunPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "sun";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://sun/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "sun";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SunPackage eINSTANCE = org.thirdeye.extensions.sun.impl.SunPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.thirdeye.extensions.sun.impl.SunModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extensions.sun.impl.SunModelImpl
	 * @see org.thirdeye.extensions.sun.impl.SunPackageImpl#getSunModel()
	 * @generated
	 */
	int SUN_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUN_MODEL__VARIABLES = MeshPackage.MODEL__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUN_MODEL__NAME = MeshPackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Solar Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUN_MODEL__SOLAR_CONSTANT = MeshPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>ECI Coordinates</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUN_MODEL__ECI_COORDINATES = MeshPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUN_MODEL_FEATURE_COUNT = MeshPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUN_MODEL___RUN = MeshPackage.MODEL___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUN_MODEL___CALC = MeshPackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUN_MODEL_OPERATION_COUNT = MeshPackage.MODEL_OPERATION_COUNT + 1;


	/**
	 * The meta object id for the '{@link org.thirdeye.extensions.sun.impl.SolarFluxConnectionImpl <em>Solar Flux Connection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extensions.sun.impl.SolarFluxConnectionImpl
	 * @see org.thirdeye.extensions.sun.impl.SunPackageImpl#getSolarFluxConnection()
	 * @generated
	 */
	int SOLAR_FLUX_CONNECTION = 1;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_FLUX_CONNECTION__VARIABLES = MeshPackage.CONNECTION__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_FLUX_CONNECTION__NAME = MeshPackage.CONNECTION__NAME;

	/**
	 * The feature id for the '<em><b>Sun Location ECI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_FLUX_CONNECTION__SUN_LOCATION_ECI = MeshPackage.CONNECTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sat Location ECI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_FLUX_CONNECTION__SAT_LOCATION_ECI = MeshPackage.CONNECTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Solar Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_FLUX_CONNECTION__SOLAR_CONSTANT = MeshPackage.CONNECTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Solar Flux ECI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_FLUX_CONNECTION__SOLAR_FLUX_ECI = MeshPackage.CONNECTION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Solar Flux Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_FLUX_CONNECTION_FEATURE_COUNT = MeshPackage.CONNECTION_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_FLUX_CONNECTION___RUN = MeshPackage.CONNECTION___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_FLUX_CONNECTION___CALC = MeshPackage.CONNECTION___CALC;

	/**
	 * The number of operations of the '<em>Solar Flux Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_FLUX_CONNECTION_OPERATION_COUNT = MeshPackage.CONNECTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.thirdeye.extensions.sun.impl.SolarArrayImpl <em>Solar Array</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extensions.sun.impl.SolarArrayImpl
	 * @see org.thirdeye.extensions.sun.impl.SunPackageImpl#getSolarArray()
	 * @generated
	 */
	int SOLAR_ARRAY = 2;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_ARRAY__VARIABLES = MeshPackage.MODEL__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_ARRAY__NAME = MeshPackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>SA Normal Local</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_ARRAY__SA_NORMAL_LOCAL = MeshPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>SC Quaternion</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_ARRAY__SC_QUATERNION = MeshPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Solar Flux ECI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_ARRAY__SOLAR_FLUX_ECI = MeshPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Efficiency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_ARRAY__EFFICIENCY = MeshPackage.MODEL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Area</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_ARRAY__AREA = MeshPackage.MODEL_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Power</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_ARRAY__POWER = MeshPackage.MODEL_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Solar Array</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_ARRAY_FEATURE_COUNT = MeshPackage.MODEL_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_ARRAY___RUN = MeshPackage.MODEL___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_ARRAY___CALC = MeshPackage.MODEL___CALC;

	/**
	 * The number of operations of the '<em>Solar Array</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLAR_ARRAY_OPERATION_COUNT = MeshPackage.MODEL_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.thirdeye.extensions.sun.SunModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see org.thirdeye.extensions.sun.SunModel
	 * @generated
	 */
	EClass getSunModel();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extensions.sun.SunModel#getSolarConstant <em>Solar Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Solar Constant</em>'.
	 * @see org.thirdeye.extensions.sun.SunModel#getSolarConstant()
	 * @see #getSunModel()
	 * @generated
	 */
	EAttribute getSunModel_SolarConstant();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extensions.sun.SunModel#getECICoordinatesList <em>ECI Coordinates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>ECI Coordinates</em>'.
	 * @see org.thirdeye.extensions.sun.SunModel#getECICoordinatesList()
	 * @see #getSunModel()
	 * @generated
	 */
	EAttribute getSunModel_ECICoordinates();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extensions.sun.SunModel#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extensions.sun.SunModel#calc()
	 * @generated
	 */
	EOperation getSunModel__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extensions.sun.SolarFluxConnection <em>Solar Flux Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Solar Flux Connection</em>'.
	 * @see org.thirdeye.extensions.sun.SolarFluxConnection
	 * @generated
	 */
	EClass getSolarFluxConnection();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extensions.sun.SolarFluxConnection#getSunLocationECIList <em>Sun Location ECI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Sun Location ECI</em>'.
	 * @see org.thirdeye.extensions.sun.SolarFluxConnection#getSunLocationECIList()
	 * @see #getSolarFluxConnection()
	 * @generated
	 */
	EAttribute getSolarFluxConnection_SunLocationECI();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extensions.sun.SolarFluxConnection#getSatLocationECIList <em>Sat Location ECI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Sat Location ECI</em>'.
	 * @see org.thirdeye.extensions.sun.SolarFluxConnection#getSatLocationECIList()
	 * @see #getSolarFluxConnection()
	 * @generated
	 */
	EAttribute getSolarFluxConnection_SatLocationECI();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extensions.sun.SolarFluxConnection#getSolarConstant <em>Solar Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Solar Constant</em>'.
	 * @see org.thirdeye.extensions.sun.SolarFluxConnection#getSolarConstant()
	 * @see #getSolarFluxConnection()
	 * @generated
	 */
	EAttribute getSolarFluxConnection_SolarConstant();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extensions.sun.SolarFluxConnection#getSolarFluxECIList <em>Solar Flux ECI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Solar Flux ECI</em>'.
	 * @see org.thirdeye.extensions.sun.SolarFluxConnection#getSolarFluxECIList()
	 * @see #getSolarFluxConnection()
	 * @generated
	 */
	EAttribute getSolarFluxConnection_SolarFluxECI();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extensions.sun.SolarArray <em>Solar Array</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Solar Array</em>'.
	 * @see org.thirdeye.extensions.sun.SolarArray
	 * @generated
	 */
	EClass getSolarArray();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extensions.sun.SolarArray#getSANormalLocalList <em>SA Normal Local</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>SA Normal Local</em>'.
	 * @see org.thirdeye.extensions.sun.SolarArray#getSANormalLocalList()
	 * @see #getSolarArray()
	 * @generated
	 */
	EAttribute getSolarArray_SANormalLocal();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extensions.sun.SolarArray#getSCQuaternionList <em>SC Quaternion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>SC Quaternion</em>'.
	 * @see org.thirdeye.extensions.sun.SolarArray#getSCQuaternionList()
	 * @see #getSolarArray()
	 * @generated
	 */
	EAttribute getSolarArray_SCQuaternion();

	/**
	 * Returns the meta object for the attribute list '{@link org.thirdeye.extensions.sun.SolarArray#getSolarFluxECIList <em>Solar Flux ECI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Solar Flux ECI</em>'.
	 * @see org.thirdeye.extensions.sun.SolarArray#getSolarFluxECIList()
	 * @see #getSolarArray()
	 * @generated
	 */
	EAttribute getSolarArray_SolarFluxECI();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extensions.sun.SolarArray#getEfficiency <em>Efficiency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Efficiency</em>'.
	 * @see org.thirdeye.extensions.sun.SolarArray#getEfficiency()
	 * @see #getSolarArray()
	 * @generated
	 */
	EAttribute getSolarArray_Efficiency();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extensions.sun.SolarArray#getArea <em>Area</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Area</em>'.
	 * @see org.thirdeye.extensions.sun.SolarArray#getArea()
	 * @see #getSolarArray()
	 * @generated
	 */
	EAttribute getSolarArray_Area();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extensions.sun.SolarArray#getPower <em>Power</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Power</em>'.
	 * @see org.thirdeye.extensions.sun.SolarArray#getPower()
	 * @see #getSolarArray()
	 * @generated
	 */
	EAttribute getSolarArray_Power();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SunFactory getSunFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.thirdeye.extensions.sun.impl.SunModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extensions.sun.impl.SunModelImpl
		 * @see org.thirdeye.extensions.sun.impl.SunPackageImpl#getSunModel()
		 * @generated
		 */
		EClass SUN_MODEL = eINSTANCE.getSunModel();

		/**
		 * The meta object literal for the '<em><b>Solar Constant</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUN_MODEL__SOLAR_CONSTANT = eINSTANCE.getSunModel_SolarConstant();

		/**
		 * The meta object literal for the '<em><b>ECI Coordinates</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUN_MODEL__ECI_COORDINATES = eINSTANCE.getSunModel_ECICoordinates();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SUN_MODEL___CALC = eINSTANCE.getSunModel__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extensions.sun.impl.SolarFluxConnectionImpl <em>Solar Flux Connection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extensions.sun.impl.SolarFluxConnectionImpl
		 * @see org.thirdeye.extensions.sun.impl.SunPackageImpl#getSolarFluxConnection()
		 * @generated
		 */
		EClass SOLAR_FLUX_CONNECTION = eINSTANCE.getSolarFluxConnection();

		/**
		 * The meta object literal for the '<em><b>Sun Location ECI</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLAR_FLUX_CONNECTION__SUN_LOCATION_ECI = eINSTANCE.getSolarFluxConnection_SunLocationECI();

		/**
		 * The meta object literal for the '<em><b>Sat Location ECI</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLAR_FLUX_CONNECTION__SAT_LOCATION_ECI = eINSTANCE.getSolarFluxConnection_SatLocationECI();

		/**
		 * The meta object literal for the '<em><b>Solar Constant</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLAR_FLUX_CONNECTION__SOLAR_CONSTANT = eINSTANCE.getSolarFluxConnection_SolarConstant();

		/**
		 * The meta object literal for the '<em><b>Solar Flux ECI</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLAR_FLUX_CONNECTION__SOLAR_FLUX_ECI = eINSTANCE.getSolarFluxConnection_SolarFluxECI();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extensions.sun.impl.SolarArrayImpl <em>Solar Array</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extensions.sun.impl.SolarArrayImpl
		 * @see org.thirdeye.extensions.sun.impl.SunPackageImpl#getSolarArray()
		 * @generated
		 */
		EClass SOLAR_ARRAY = eINSTANCE.getSolarArray();

		/**
		 * The meta object literal for the '<em><b>SA Normal Local</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLAR_ARRAY__SA_NORMAL_LOCAL = eINSTANCE.getSolarArray_SANormalLocal();

		/**
		 * The meta object literal for the '<em><b>SC Quaternion</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLAR_ARRAY__SC_QUATERNION = eINSTANCE.getSolarArray_SCQuaternion();

		/**
		 * The meta object literal for the '<em><b>Solar Flux ECI</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLAR_ARRAY__SOLAR_FLUX_ECI = eINSTANCE.getSolarArray_SolarFluxECI();

		/**
		 * The meta object literal for the '<em><b>Efficiency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLAR_ARRAY__EFFICIENCY = eINSTANCE.getSolarArray_Efficiency();

		/**
		 * The meta object literal for the '<em><b>Area</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLAR_ARRAY__AREA = eINSTANCE.getSolarArray_Area();

		/**
		 * The meta object literal for the '<em><b>Power</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLAR_ARRAY__POWER = eINSTANCE.getSolarArray_Power();

	}

} //SunPackage
