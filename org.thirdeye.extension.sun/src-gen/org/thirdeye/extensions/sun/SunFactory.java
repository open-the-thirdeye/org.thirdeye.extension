/**
 */
package org.thirdeye.extensions.sun;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.thirdeye.extensions.sun.SunPackage
 * @generated
 */
public interface SunFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SunFactory eINSTANCE = org.thirdeye.extensions.sun.impl.SunFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model</em>'.
	 * @generated
	 */
	SunModel createSunModel();

	/**
	 * Returns a new object of class '<em>Solar Flux Connection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Solar Flux Connection</em>'.
	 * @generated
	 */
	SolarFluxConnection createSolarFluxConnection();

	/**
	 * Returns a new object of class '<em>Solar Array</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Solar Array</em>'.
	 * @generated
	 */
	SolarArray createSolarArray();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	SunPackage getSunPackage();

} //SunFactory
