/**
 */
package org.thirdeye.extensions.sun;

import org.thirdeye.core.mesh.Connection;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Example Connection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extensions.sun.ExampleConnection#getInput <em>Input</em>}</li>
 *   <li>{@link org.thirdeye.extensions.sun.ExampleConnection#getOutput <em>Output</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extensions.sun.SunPackage#getExampleConnection()
 * @model
 * @generated
 */
public interface ExampleConnection extends Connection {
	/**
	 * Returns the value of the '<em><b>Input</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input</em>' attribute.
	 * @see #setInput(int)
	 * @see org.thirdeye.extensions.sun.SunPackage#getExampleConnection_Input()
	 * @model required="true"
	 *        annotation="ThirdEye unit='step'"
	 * @generated
	 */
	int getInput();

	/**
	 * Sets the value of the '{@link org.thirdeye.extensions.sun.ExampleConnection#getInput <em>Input</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input</em>' attribute.
	 * @see #getInput()
	 * @generated
	 */
	void setInput(int value);

	/**
	 * Returns the value of the '<em><b>Output</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output</em>' attribute.
	 * @see #setOutput(int)
	 * @see org.thirdeye.extensions.sun.SunPackage#getExampleConnection_Output()
	 * @model required="true"
	 *        annotation="ThirdEye unit='step'"
	 * @generated
	 */
	int getOutput();

	/**
	 * Sets the value of the '{@link org.thirdeye.extensions.sun.ExampleConnection#getOutput <em>Output</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output</em>' attribute.
	 * @see #getOutput()
	 * @generated
	 */
	void setOutput(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void calc();

} // ExampleConnection
