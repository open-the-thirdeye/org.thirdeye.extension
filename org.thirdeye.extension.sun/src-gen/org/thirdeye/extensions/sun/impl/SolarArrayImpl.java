/**
 */
package org.thirdeye.extensions.sun.impl;

import java.util.Collection;

import org.apache.commons.math3.complex.Quaternion;
import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import org.thirdeye.core.mesh.impl.ModelImpl;
import org.thirdeye.extensions.sun.SolarArray;
import org.thirdeye.extensions.sun.SunPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Solar Array</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extensions.sun.impl.SolarArrayImpl#getSANormalLocalList <em>SA Normal Local</em>}</li>
 *   <li>{@link org.thirdeye.extensions.sun.impl.SolarArrayImpl#getSCQuaternionList <em>SC Quaternion</em>}</li>
 *   <li>{@link org.thirdeye.extensions.sun.impl.SolarArrayImpl#getSolarFluxECIList <em>Solar Flux ECI</em>}</li>
 *   <li>{@link org.thirdeye.extensions.sun.impl.SolarArrayImpl#getEfficiency <em>Efficiency</em>}</li>
 *   <li>{@link org.thirdeye.extensions.sun.impl.SolarArrayImpl#getArea <em>Area</em>}</li>
 *   <li>{@link org.thirdeye.extensions.sun.impl.SolarArrayImpl#getPower <em>Power</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SolarArrayImpl extends ModelImpl implements SolarArray {
	/**
	 * The cached value of the '{@link #getSANormalLocalList() <em>SA Normal Local</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSANormalLocalList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> saNormalLocal;

	/**
	 * The empty value for the '{@link #getSANormalLocal() <em>SA Normal Local</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSANormalLocal()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] SA_NORMAL_LOCAL_EEMPTY_ARRAY = new Double [0];

	/**
	 * The cached value of the '{@link #getSCQuaternionList() <em>SC Quaternion</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSCQuaternionList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> scQuaternion;

	/**
	 * The empty value for the '{@link #getSCQuaternion() <em>SC Quaternion</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSCQuaternion()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] SC_QUATERNION_EEMPTY_ARRAY = new Double [0];

	/**
	 * The cached value of the '{@link #getSolarFluxECIList() <em>Solar Flux ECI</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSolarFluxECIList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> solarFluxECI;

	/**
	 * The empty value for the '{@link #getSolarFluxECI() <em>Solar Flux ECI</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSolarFluxECI()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] SOLAR_FLUX_ECI_EEMPTY_ARRAY = new Double [0];

	/**
	 * The default value of the '{@link #getEfficiency() <em>Efficiency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEfficiency()
	 * @generated
	 * @ordered
	 */
	protected static final Double EFFICIENCY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEfficiency() <em>Efficiency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEfficiency()
	 * @generated
	 * @ordered
	 */
	protected Double efficiency = EFFICIENCY_EDEFAULT;

	/**
	 * The default value of the '{@link #getArea() <em>Area</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArea()
	 * @generated
	 * @ordered
	 */
	protected static final Double AREA_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getArea() <em>Area</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArea()
	 * @generated
	 * @ordered
	 */
	protected Double area = AREA_EDEFAULT;

	/**
	 * The default value of the '{@link #getPower() <em>Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPower()
	 * @generated
	 * @ordered
	 */
	protected static final Double POWER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPower() <em>Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPower()
	 * @generated
	 * @ordered
	 */
	protected Double power = POWER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SolarArrayImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SunPackage.Literals.SOLAR_ARRAY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getSANormalLocal() {
		if (saNormalLocal == null || saNormalLocal.isEmpty()) return SA_NORMAL_LOCAL_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)saNormalLocal;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getSANormalLocal(int index) {
		return getSANormalLocalList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSANormalLocalLength() {
		return saNormalLocal == null ? 0 : saNormalLocal.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSANormalLocal(Double[] newSANormalLocal) {
		((BasicEList<Double>)getSANormalLocalList()).setData(newSANormalLocal.length, newSANormalLocal);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSANormalLocal(int index, Double element) {
		getSANormalLocalList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getSANormalLocalList() {
		if (saNormalLocal == null) {
			saNormalLocal = new EDataTypeEList<Double>(Double.class, this, SunPackage.SOLAR_ARRAY__SA_NORMAL_LOCAL);
		}
		return saNormalLocal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getSCQuaternion() {
		if (scQuaternion == null || scQuaternion.isEmpty()) return SC_QUATERNION_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)scQuaternion;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getSCQuaternion(int index) {
		return getSCQuaternionList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSCQuaternionLength() {
		return scQuaternion == null ? 0 : scQuaternion.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSCQuaternion(Double[] newSCQuaternion) {
		((BasicEList<Double>)getSCQuaternionList()).setData(newSCQuaternion.length, newSCQuaternion);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSCQuaternion(int index, Double element) {
		getSCQuaternionList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getSCQuaternionList() {
		if (scQuaternion == null) {
			scQuaternion = new EDataTypeEList<Double>(Double.class, this, SunPackage.SOLAR_ARRAY__SC_QUATERNION);
		}
		return scQuaternion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getSolarFluxECI() {
		if (solarFluxECI == null || solarFluxECI.isEmpty()) return SOLAR_FLUX_ECI_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)solarFluxECI;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getSolarFluxECI(int index) {
		return getSolarFluxECIList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSolarFluxECILength() {
		return solarFluxECI == null ? 0 : solarFluxECI.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSolarFluxECI(Double[] newSolarFluxECI) {
		((BasicEList<Double>)getSolarFluxECIList()).setData(newSolarFluxECI.length, newSolarFluxECI);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSolarFluxECI(int index, Double element) {
		getSolarFluxECIList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getSolarFluxECIList() {
		if (solarFluxECI == null) {
			solarFluxECI = new EDataTypeEList<Double>(Double.class, this, SunPackage.SOLAR_ARRAY__SOLAR_FLUX_ECI);
		}
		return solarFluxECI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getEfficiency() {
		return efficiency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEfficiency(Double newEfficiency) {
		Double oldEfficiency = efficiency;
		efficiency = newEfficiency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SunPackage.SOLAR_ARRAY__EFFICIENCY, oldEfficiency, efficiency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getArea() {
		return area;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setArea(Double newArea) {
		Double oldArea = area;
		area = newArea;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SunPackage.SOLAR_ARRAY__AREA, oldArea, area));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getPower() {
		return power;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPower(Double newPower) {
		Double oldPower = power;
		power = newPower;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SunPackage.SOLAR_ARRAY__POWER, oldPower, power));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SunPackage.SOLAR_ARRAY__SA_NORMAL_LOCAL:
				return getSANormalLocalList();
			case SunPackage.SOLAR_ARRAY__SC_QUATERNION:
				return getSCQuaternionList();
			case SunPackage.SOLAR_ARRAY__SOLAR_FLUX_ECI:
				return getSolarFluxECIList();
			case SunPackage.SOLAR_ARRAY__EFFICIENCY:
				return getEfficiency();
			case SunPackage.SOLAR_ARRAY__AREA:
				return getArea();
			case SunPackage.SOLAR_ARRAY__POWER:
				return getPower();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SunPackage.SOLAR_ARRAY__SA_NORMAL_LOCAL:
				getSANormalLocalList().clear();
				getSANormalLocalList().addAll((Collection<? extends Double>)newValue);
				return;
			case SunPackage.SOLAR_ARRAY__SC_QUATERNION:
				getSCQuaternionList().clear();
				getSCQuaternionList().addAll((Collection<? extends Double>)newValue);
				return;
			case SunPackage.SOLAR_ARRAY__SOLAR_FLUX_ECI:
				getSolarFluxECIList().clear();
				getSolarFluxECIList().addAll((Collection<? extends Double>)newValue);
				return;
			case SunPackage.SOLAR_ARRAY__EFFICIENCY:
				setEfficiency((Double)newValue);
				return;
			case SunPackage.SOLAR_ARRAY__AREA:
				setArea((Double)newValue);
				return;
			case SunPackage.SOLAR_ARRAY__POWER:
				setPower((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SunPackage.SOLAR_ARRAY__SA_NORMAL_LOCAL:
				getSANormalLocalList().clear();
				return;
			case SunPackage.SOLAR_ARRAY__SC_QUATERNION:
				getSCQuaternionList().clear();
				return;
			case SunPackage.SOLAR_ARRAY__SOLAR_FLUX_ECI:
				getSolarFluxECIList().clear();
				return;
			case SunPackage.SOLAR_ARRAY__EFFICIENCY:
				setEfficiency(EFFICIENCY_EDEFAULT);
				return;
			case SunPackage.SOLAR_ARRAY__AREA:
				setArea(AREA_EDEFAULT);
				return;
			case SunPackage.SOLAR_ARRAY__POWER:
				setPower(POWER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SunPackage.SOLAR_ARRAY__SA_NORMAL_LOCAL:
				return saNormalLocal != null && !saNormalLocal.isEmpty();
			case SunPackage.SOLAR_ARRAY__SC_QUATERNION:
				return scQuaternion != null && !scQuaternion.isEmpty();
			case SunPackage.SOLAR_ARRAY__SOLAR_FLUX_ECI:
				return solarFluxECI != null && !solarFluxECI.isEmpty();
			case SunPackage.SOLAR_ARRAY__EFFICIENCY:
				return EFFICIENCY_EDEFAULT == null ? efficiency != null : !EFFICIENCY_EDEFAULT.equals(efficiency);
			case SunPackage.SOLAR_ARRAY__AREA:
				return AREA_EDEFAULT == null ? area != null : !AREA_EDEFAULT.equals(area);
			case SunPackage.SOLAR_ARRAY__POWER:
				return POWER_EDEFAULT == null ? power != null : !POWER_EDEFAULT.equals(power);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (SANormalLocal: ");
		result.append(saNormalLocal);
		result.append(", SCQuaternion: ");
		result.append(scQuaternion);
		result.append(", SolarFluxECI: ");
		result.append(solarFluxECI);
		result.append(", Efficiency: ");
		result.append(efficiency);
		result.append(", Area: ");
		result.append(area);
		result.append(", Power: ");
		result.append(power);
		result.append(')');
		return result.toString();
	}

	@Override
	public void calc() {
		Vector3D solarFlux = new Vector3D( getSolarFluxECI(0),getSolarFluxECI(1),getSolarFluxECI(2) );
		Vector3D saNormalLocal = new Vector3D( getSANormalLocal(0),getSANormalLocal(1),getSANormalLocal(2) );
		Quaternion quaternion = new Quaternion( getSCQuaternion(0),getSCQuaternion(1),getSCQuaternion(2),getSCQuaternion(3) );
		quaternion = quaternion.normalize();
		
		Rotation rot = new Rotation(quaternion.getQ0(),quaternion.getQ1(),quaternion.getQ2(),quaternion.getQ3(),false);
		Vector3D saNormalECI = rot.applyTo(saNormalLocal);
		//System.out.println("SA = " + saNormalECI.getX() + " " + saNormalECI.getY() + " " + saNormalECI.getZ());
		double flux = solarFlux.dotProduct(saNormalECI.normalize());
		double angle = flux/solarFlux.getNorm()*180/Math.PI;
		//System.out.println("Flux = " + flux);
		//System.out.println("Angle = " + angle);
		double power = flux * getArea() * getEfficiency();
		//System.out.println("Power = " + power);
		if( angle > 0.0 ) {
			setPower(power);		
		} else {
			setPower(0.0);		
		} 
	}

} //SolarArrayImpl
