/**
 */
package org.thirdeye.extensions.sun.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.thirdeye.core.dependencies.DependenciesPackage;

import org.thirdeye.core.mesh.MeshPackage;
import org.thirdeye.extensions.sun.SolarArray;
import org.thirdeye.extensions.sun.SolarFluxConnection;
import org.thirdeye.extensions.sun.SunFactory;
import org.thirdeye.extensions.sun.SunModel;
import org.thirdeye.extensions.sun.SunPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SunPackageImpl extends EPackageImpl implements SunPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sunModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass solarFluxConnectionEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass solarArrayEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.thirdeye.extensions.sun.SunPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SunPackageImpl() {
		super(eNS_URI, SunFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link SunPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SunPackage init() {
		if (isInited) return (SunPackage)EPackage.Registry.INSTANCE.getEPackage(SunPackage.eNS_URI);

		// Obtain or create and register package
		SunPackageImpl theSunPackage = (SunPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof SunPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new SunPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		DependenciesPackage.eINSTANCE.eClass();
		MeshPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theSunPackage.createPackageContents();

		// Initialize created meta-data
		theSunPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSunPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SunPackage.eNS_URI, theSunPackage);
		return theSunPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSunModel() {
		return sunModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSunModel_SolarConstant() {
		return (EAttribute)sunModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSunModel_ECICoordinates() {
		return (EAttribute)sunModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSunModel__Calc() {
		return sunModelEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSolarFluxConnection() {
		return solarFluxConnectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSolarFluxConnection_SunLocationECI() {
		return (EAttribute)solarFluxConnectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSolarFluxConnection_SatLocationECI() {
		return (EAttribute)solarFluxConnectionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSolarFluxConnection_SolarConstant() {
		return (EAttribute)solarFluxConnectionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSolarFluxConnection_SolarFluxECI() {
		return (EAttribute)solarFluxConnectionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSolarArray() {
		return solarArrayEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSolarArray_SANormalLocal() {
		return (EAttribute)solarArrayEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSolarArray_SCQuaternion() {
		return (EAttribute)solarArrayEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSolarArray_SolarFluxECI() {
		return (EAttribute)solarArrayEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSolarArray_Efficiency() {
		return (EAttribute)solarArrayEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSolarArray_Area() {
		return (EAttribute)solarArrayEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSolarArray_Power() {
		return (EAttribute)solarArrayEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SunFactory getSunFactory() {
		return (SunFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		sunModelEClass = createEClass(SUN_MODEL);
		createEAttribute(sunModelEClass, SUN_MODEL__SOLAR_CONSTANT);
		createEAttribute(sunModelEClass, SUN_MODEL__ECI_COORDINATES);
		createEOperation(sunModelEClass, SUN_MODEL___CALC);

		solarFluxConnectionEClass = createEClass(SOLAR_FLUX_CONNECTION);
		createEAttribute(solarFluxConnectionEClass, SOLAR_FLUX_CONNECTION__SUN_LOCATION_ECI);
		createEAttribute(solarFluxConnectionEClass, SOLAR_FLUX_CONNECTION__SAT_LOCATION_ECI);
		createEAttribute(solarFluxConnectionEClass, SOLAR_FLUX_CONNECTION__SOLAR_CONSTANT);
		createEAttribute(solarFluxConnectionEClass, SOLAR_FLUX_CONNECTION__SOLAR_FLUX_ECI);

		solarArrayEClass = createEClass(SOLAR_ARRAY);
		createEAttribute(solarArrayEClass, SOLAR_ARRAY__SA_NORMAL_LOCAL);
		createEAttribute(solarArrayEClass, SOLAR_ARRAY__SC_QUATERNION);
		createEAttribute(solarArrayEClass, SOLAR_ARRAY__SOLAR_FLUX_ECI);
		createEAttribute(solarArrayEClass, SOLAR_ARRAY__EFFICIENCY);
		createEAttribute(solarArrayEClass, SOLAR_ARRAY__AREA);
		createEAttribute(solarArrayEClass, SOLAR_ARRAY__POWER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		MeshPackage theMeshPackage = (MeshPackage)EPackage.Registry.INSTANCE.getEPackage(MeshPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		sunModelEClass.getESuperTypes().add(theMeshPackage.getModel());
		solarFluxConnectionEClass.getESuperTypes().add(theMeshPackage.getConnection());
		solarArrayEClass.getESuperTypes().add(theMeshPackage.getModel());

		// Initialize classes, features, and operations; add parameters
		initEClass(sunModelEClass, SunModel.class, "SunModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSunModel_SolarConstant(), ecorePackage.getEDoubleObject(), "SolarConstant", null, 1, 1, SunModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getSunModel_ECICoordinates(), ecorePackage.getEDoubleObject(), "ECICoordinates", null, 3, 3, SunModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEOperation(getSunModel__Calc(), null, "calc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(solarFluxConnectionEClass, SolarFluxConnection.class, "SolarFluxConnection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSolarFluxConnection_SunLocationECI(), ecorePackage.getEDoubleObject(), "SunLocationECI", null, 3, 3, SolarFluxConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getSolarFluxConnection_SatLocationECI(), ecorePackage.getEDoubleObject(), "SatLocationECI", null, 3, 3, SolarFluxConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getSolarFluxConnection_SolarConstant(), ecorePackage.getEDoubleObject(), "SolarConstant", null, 1, 1, SolarFluxConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getSolarFluxConnection_SolarFluxECI(), ecorePackage.getEDoubleObject(), "SolarFluxECI", null, 3, 3, SolarFluxConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(solarArrayEClass, SolarArray.class, "SolarArray", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSolarArray_SANormalLocal(), ecorePackage.getEDoubleObject(), "SANormalLocal", null, 3, 3, SolarArray.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getSolarArray_SCQuaternion(), ecorePackage.getEDoubleObject(), "SCQuaternion", null, 4, 4, SolarArray.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getSolarArray_SolarFluxECI(), ecorePackage.getEDoubleObject(), "SolarFluxECI", null, 3, 3, SolarArray.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getSolarArray_Efficiency(), ecorePackage.getEDoubleObject(), "Efficiency", null, 1, 1, SolarArray.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getSolarArray_Area(), ecorePackage.getEDoubleObject(), "Area", null, 1, 1, SolarArray.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getSolarArray_Power(), ecorePackage.getEDoubleObject(), "Power", null, 1, 1, SolarArray.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// ThirdEye
		createThirdEyeAnnotations();
	}

	/**
	 * Initializes the annotations for <b>ThirdEye</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createThirdEyeAnnotations() {
		String source = "ThirdEye";	
		addAnnotation
		  (getSunModel_SolarConstant(), 
		   source, 
		   new String[] {
			 "unit", "W/m^2"
		   });	
		addAnnotation
		  (getSunModel_ECICoordinates(), 
		   source, 
		   new String[] {
			 "unit", "m,m,m"
		   });	
		addAnnotation
		  (getSolarFluxConnection_SunLocationECI(), 
		   source, 
		   new String[] {
			 "unit", "m,m,m"
		   });	
		addAnnotation
		  (getSolarFluxConnection_SatLocationECI(), 
		   source, 
		   new String[] {
			 "unit", "m,m,m"
		   });	
		addAnnotation
		  (getSolarFluxConnection_SolarConstant(), 
		   source, 
		   new String[] {
			 "unit", "W/m^2"
		   });	
		addAnnotation
		  (getSolarFluxConnection_SolarFluxECI(), 
		   source, 
		   new String[] {
			 "unit", "W/m^2,W/m^2,W/m^2"
		   });	
		addAnnotation
		  (getSolarArray_SANormalLocal(), 
		   source, 
		   new String[] {
			 "unit", "none,none,none"
		   });	
		addAnnotation
		  (getSolarArray_SCQuaternion(), 
		   source, 
		   new String[] {
			 "unit", "none,none,none,none"
		   });	
		addAnnotation
		  (getSolarArray_SolarFluxECI(), 
		   source, 
		   new String[] {
			 "unit", "W/m^2,W/m^2,W/m^2"
		   });	
		addAnnotation
		  (getSolarArray_Efficiency(), 
		   source, 
		   new String[] {
			 "unit", "none"
		   });	
		addAnnotation
		  (getSolarArray_Area(), 
		   source, 
		   new String[] {
			 "unit", "m^2"
		   });	
		addAnnotation
		  (getSolarArray_Power(), 
		   source, 
		   new String[] {
			 "unit", "W"
		   });
	}

} //SunPackageImpl
