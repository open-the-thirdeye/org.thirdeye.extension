/**
 */
package org.thirdeye.extensions.sun.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.thirdeye.extensions.sun.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SunFactoryImpl extends EFactoryImpl implements SunFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SunFactory init() {
		try {
			SunFactory theSunFactory = (SunFactory)EPackage.Registry.INSTANCE.getEFactory(SunPackage.eNS_URI);
			if (theSunFactory != null) {
				return theSunFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SunFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SunFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SunPackage.SUN_MODEL: return createSunModel();
			case SunPackage.SOLAR_FLUX_CONNECTION: return createSolarFluxConnection();
			case SunPackage.SOLAR_ARRAY: return createSolarArray();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SunModel createSunModel() {
		SunModelImpl sunModel = new SunModelImpl();
		return sunModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SolarFluxConnection createSolarFluxConnection() {
		SolarFluxConnectionImpl solarFluxConnection = new SolarFluxConnectionImpl();
		return solarFluxConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SolarArray createSolarArray() {
		SolarArrayImpl solarArray = new SolarArrayImpl();
		return solarArray;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SunPackage getSunPackage() {
		return (SunPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SunPackage getPackage() {
		return SunPackage.eINSTANCE;
	}

} //SunFactoryImpl
