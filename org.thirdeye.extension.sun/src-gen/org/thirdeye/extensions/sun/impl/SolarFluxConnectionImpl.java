/**
 */
package org.thirdeye.extensions.sun.impl;

import java.util.Collection;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.thirdeye.core.mesh.impl.ConnectionImpl;
import org.thirdeye.extensions.sun.SolarFluxConnection;
import org.thirdeye.extensions.sun.SunPackage;

import jat.coreNOSA.cm.Constants;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Solar Flux Connection</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extensions.sun.impl.SolarFluxConnectionImpl#getSunLocationECIList <em>Sun Location ECI</em>}</li>
 *   <li>{@link org.thirdeye.extensions.sun.impl.SolarFluxConnectionImpl#getSatLocationECIList <em>Sat Location ECI</em>}</li>
 *   <li>{@link org.thirdeye.extensions.sun.impl.SolarFluxConnectionImpl#getSolarConstant <em>Solar Constant</em>}</li>
 *   <li>{@link org.thirdeye.extensions.sun.impl.SolarFluxConnectionImpl#getSolarFluxECIList <em>Solar Flux ECI</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SolarFluxConnectionImpl extends ConnectionImpl implements SolarFluxConnection {
	/**
	 * The cached value of the '{@link #getSunLocationECIList() <em>Sun Location ECI</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSunLocationECIList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> sunLocationECI;

	/**
	 * The empty value for the '{@link #getSunLocationECI() <em>Sun Location ECI</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSunLocationECI()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] SUN_LOCATION_ECI_EEMPTY_ARRAY = new Double [0];

	/**
	 * The cached value of the '{@link #getSatLocationECIList() <em>Sat Location ECI</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSatLocationECIList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> satLocationECI;

	/**
	 * The empty value for the '{@link #getSatLocationECI() <em>Sat Location ECI</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSatLocationECI()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] SAT_LOCATION_ECI_EEMPTY_ARRAY = new Double [0];

	/**
	 * The default value of the '{@link #getSolarConstant() <em>Solar Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSolarConstant()
	 * @generated
	 * @ordered
	 */
	protected static final Double SOLAR_CONSTANT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSolarConstant() <em>Solar Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSolarConstant()
	 * @generated
	 * @ordered
	 */
	protected Double solarConstant = SOLAR_CONSTANT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSolarFluxECIList() <em>Solar Flux ECI</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSolarFluxECIList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> solarFluxECI;

	/**
	 * The empty value for the '{@link #getSolarFluxECI() <em>Solar Flux ECI</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSolarFluxECI()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] SOLAR_FLUX_ECI_EEMPTY_ARRAY = new Double [0];

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SolarFluxConnectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SunPackage.Literals.SOLAR_FLUX_CONNECTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getSunLocationECI() {
		if (sunLocationECI == null || sunLocationECI.isEmpty()) return SUN_LOCATION_ECI_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)sunLocationECI;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getSunLocationECI(int index) {
		return getSunLocationECIList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSunLocationECILength() {
		return sunLocationECI == null ? 0 : sunLocationECI.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSunLocationECI(Double[] newSunLocationECI) {
		((BasicEList<Double>)getSunLocationECIList()).setData(newSunLocationECI.length, newSunLocationECI);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSunLocationECI(int index, Double element) {
		getSunLocationECIList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getSunLocationECIList() {
		if (sunLocationECI == null) {
			sunLocationECI = new EDataTypeEList<Double>(Double.class, this, SunPackage.SOLAR_FLUX_CONNECTION__SUN_LOCATION_ECI);
		}
		return sunLocationECI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getSatLocationECI() {
		if (satLocationECI == null || satLocationECI.isEmpty()) return SAT_LOCATION_ECI_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)satLocationECI;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getSatLocationECI(int index) {
		return getSatLocationECIList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSatLocationECILength() {
		return satLocationECI == null ? 0 : satLocationECI.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSatLocationECI(Double[] newSatLocationECI) {
		((BasicEList<Double>)getSatLocationECIList()).setData(newSatLocationECI.length, newSatLocationECI);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSatLocationECI(int index, Double element) {
		getSatLocationECIList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getSatLocationECIList() {
		if (satLocationECI == null) {
			satLocationECI = new EDataTypeEList<Double>(Double.class, this, SunPackage.SOLAR_FLUX_CONNECTION__SAT_LOCATION_ECI);
		}
		return satLocationECI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getSolarConstant() {
		return solarConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSolarConstant(Double newSolarConstant) {
		Double oldSolarConstant = solarConstant;
		solarConstant = newSolarConstant;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SunPackage.SOLAR_FLUX_CONNECTION__SOLAR_CONSTANT, oldSolarConstant, solarConstant));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getSolarFluxECI() {
		if (solarFluxECI == null || solarFluxECI.isEmpty()) return SOLAR_FLUX_ECI_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)solarFluxECI;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getSolarFluxECI(int index) {
		return getSolarFluxECIList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSolarFluxECILength() {
		return solarFluxECI == null ? 0 : solarFluxECI.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSolarFluxECI(Double[] newSolarFluxECI) {
		((BasicEList<Double>)getSolarFluxECIList()).setData(newSolarFluxECI.length, newSolarFluxECI);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSolarFluxECI(int index, Double element) {
		getSolarFluxECIList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getSolarFluxECIList() {
		if (solarFluxECI == null) {
			solarFluxECI = new EDataTypeEList<Double>(Double.class, this, SunPackage.SOLAR_FLUX_CONNECTION__SOLAR_FLUX_ECI);
		}
		return solarFluxECI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SunPackage.SOLAR_FLUX_CONNECTION__SUN_LOCATION_ECI:
				return getSunLocationECIList();
			case SunPackage.SOLAR_FLUX_CONNECTION__SAT_LOCATION_ECI:
				return getSatLocationECIList();
			case SunPackage.SOLAR_FLUX_CONNECTION__SOLAR_CONSTANT:
				return getSolarConstant();
			case SunPackage.SOLAR_FLUX_CONNECTION__SOLAR_FLUX_ECI:
				return getSolarFluxECIList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SunPackage.SOLAR_FLUX_CONNECTION__SUN_LOCATION_ECI:
				getSunLocationECIList().clear();
				getSunLocationECIList().addAll((Collection<? extends Double>)newValue);
				return;
			case SunPackage.SOLAR_FLUX_CONNECTION__SAT_LOCATION_ECI:
				getSatLocationECIList().clear();
				getSatLocationECIList().addAll((Collection<? extends Double>)newValue);
				return;
			case SunPackage.SOLAR_FLUX_CONNECTION__SOLAR_CONSTANT:
				setSolarConstant((Double)newValue);
				return;
			case SunPackage.SOLAR_FLUX_CONNECTION__SOLAR_FLUX_ECI:
				getSolarFluxECIList().clear();
				getSolarFluxECIList().addAll((Collection<? extends Double>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SunPackage.SOLAR_FLUX_CONNECTION__SUN_LOCATION_ECI:
				getSunLocationECIList().clear();
				return;
			case SunPackage.SOLAR_FLUX_CONNECTION__SAT_LOCATION_ECI:
				getSatLocationECIList().clear();
				return;
			case SunPackage.SOLAR_FLUX_CONNECTION__SOLAR_CONSTANT:
				setSolarConstant(SOLAR_CONSTANT_EDEFAULT);
				return;
			case SunPackage.SOLAR_FLUX_CONNECTION__SOLAR_FLUX_ECI:
				getSolarFluxECIList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SunPackage.SOLAR_FLUX_CONNECTION__SUN_LOCATION_ECI:
				return sunLocationECI != null && !sunLocationECI.isEmpty();
			case SunPackage.SOLAR_FLUX_CONNECTION__SAT_LOCATION_ECI:
				return satLocationECI != null && !satLocationECI.isEmpty();
			case SunPackage.SOLAR_FLUX_CONNECTION__SOLAR_CONSTANT:
				return SOLAR_CONSTANT_EDEFAULT == null ? solarConstant != null : !SOLAR_CONSTANT_EDEFAULT.equals(solarConstant);
			case SunPackage.SOLAR_FLUX_CONNECTION__SOLAR_FLUX_ECI:
				return solarFluxECI != null && !solarFluxECI.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (SunLocationECI: ");
		result.append(sunLocationECI);
		result.append(", SatLocationECI: ");
		result.append(satLocationECI);
		result.append(", SolarConstant: ");
		result.append(solarConstant);
		result.append(", SolarFluxECI: ");
		result.append(solarFluxECI);
		result.append(')');
		return result.toString();
	}

	@Override
	public void calc() {
		double au = Constants.AU;
		double solarConstant = getSolarConstant();
		Vector3D sunPos = new Vector3D( getSunLocationECI(0), getSunLocationECI(1), getSunLocationECI(2) );
		Vector3D satPos = new Vector3D( getSatLocationECI(0), getSatLocationECI(1), getSatLocationECI(2) );
		Vector3D result = satPos.subtract(sunPos);

		double r = result.getNorm();
		result = result.normalize();

		double distanceInAU = r/au;
		Vector3D newSolarFluxECI = result.scalarMultiply((solarConstant/(distanceInAU * distanceInAU)));
		
		setSolarFluxECI(0,newSolarFluxECI.getX());
		setSolarFluxECI(1,newSolarFluxECI.getY());
		setSolarFluxECI(2,newSolarFluxECI.getZ());
	}

} //SolarFluxConnectionImpl
