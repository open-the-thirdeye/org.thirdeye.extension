/**
 */
package org.thirdeye.extensions.sun.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.thirdeye.core.mesh.impl.ModelImpl;
import org.thirdeye.extensions.sun.SunModel;
import org.thirdeye.extensions.sun.SunPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extensions.sun.impl.SunModelImpl#getSolarConstant <em>Solar Constant</em>}</li>
 *   <li>{@link org.thirdeye.extensions.sun.impl.SunModelImpl#getECICoordinatesList <em>ECI Coordinates</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SunModelImpl extends ModelImpl implements SunModel {
	/**
	 * The default value of the '{@link #getSolarConstant() <em>Solar Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSolarConstant()
	 * @generated
	 * @ordered
	 */
	protected static final Double SOLAR_CONSTANT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSolarConstant() <em>Solar Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSolarConstant()
	 * @generated
	 * @ordered
	 */
	protected Double solarConstant = SOLAR_CONSTANT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getECICoordinatesList() <em>ECI Coordinates</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getECICoordinatesList()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> eciCoordinates;

	/**
	 * The empty value for the '{@link #getECICoordinates() <em>ECI Coordinates</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getECICoordinates()
	 * @generated
	 * @ordered
	 */
	protected static final Double[] ECI_COORDINATES_EEMPTY_ARRAY = new Double [0];

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SunModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SunPackage.Literals.SUN_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getSolarConstant() {
		return solarConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSolarConstant(Double newSolarConstant) {
		Double oldSolarConstant = solarConstant;
		solarConstant = newSolarConstant;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SunPackage.SUN_MODEL__SOLAR_CONSTANT, oldSolarConstant, solarConstant));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double[] getECICoordinates() {
		if (eciCoordinates == null || eciCoordinates.isEmpty()) return ECI_COORDINATES_EEMPTY_ARRAY;
		BasicEList<Double> list = (BasicEList<Double>)eciCoordinates;
		list.shrink();
		return (Double[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getECICoordinates(int index) {
		return getECICoordinatesList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getECICoordinatesLength() {
		return eciCoordinates == null ? 0 : eciCoordinates.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setECICoordinates(Double[] newECICoordinates) {
		((BasicEList<Double>)getECICoordinatesList()).setData(newECICoordinates.length, newECICoordinates);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setECICoordinates(int index, Double element) {
		getECICoordinatesList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getECICoordinatesList() {
		if (eciCoordinates == null) {
			eciCoordinates = new EDataTypeEList<Double>(Double.class, this, SunPackage.SUN_MODEL__ECI_COORDINATES);
		}
		return eciCoordinates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SunPackage.SUN_MODEL__SOLAR_CONSTANT:
				return getSolarConstant();
			case SunPackage.SUN_MODEL__ECI_COORDINATES:
				return getECICoordinatesList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SunPackage.SUN_MODEL__SOLAR_CONSTANT:
				setSolarConstant((Double)newValue);
				return;
			case SunPackage.SUN_MODEL__ECI_COORDINATES:
				getECICoordinatesList().clear();
				getECICoordinatesList().addAll((Collection<? extends Double>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SunPackage.SUN_MODEL__SOLAR_CONSTANT:
				setSolarConstant(SOLAR_CONSTANT_EDEFAULT);
				return;
			case SunPackage.SUN_MODEL__ECI_COORDINATES:
				getECICoordinatesList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SunPackage.SUN_MODEL__SOLAR_CONSTANT:
				return SOLAR_CONSTANT_EDEFAULT == null ? solarConstant != null : !SOLAR_CONSTANT_EDEFAULT.equals(solarConstant);
			case SunPackage.SUN_MODEL__ECI_COORDINATES:
				return eciCoordinates != null && !eciCoordinates.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toStringGen() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (SolarConstant: ");
		result.append(solarConstant);
		result.append(", ECICoordinates: ");
		result.append(eciCoordinates);
		result.append(')');
		return result.toString();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 */
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer(toStringGen());
		result.append(" HALLO");
		return result.toString();
	}

	@Override
	public void calc() {
		// TODO Auto-generated method stub
		
	}

} //SunModelImpl
