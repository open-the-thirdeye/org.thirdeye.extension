/**
 */
package org.thirdeye.extensions.sun;

import org.eclipse.emf.common.util.EList;

import org.thirdeye.core.mesh.Connection;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Solar Flux Connection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extensions.sun.SolarFluxConnection#getSunLocationECIList <em>Sun Location ECI</em>}</li>
 *   <li>{@link org.thirdeye.extensions.sun.SolarFluxConnection#getSatLocationECIList <em>Sat Location ECI</em>}</li>
 *   <li>{@link org.thirdeye.extensions.sun.SolarFluxConnection#getSolarConstant <em>Solar Constant</em>}</li>
 *   <li>{@link org.thirdeye.extensions.sun.SolarFluxConnection#getSolarFluxECIList <em>Solar Flux ECI</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extensions.sun.SunPackage#getSolarFluxConnection()
 * @model
 * @generated
 */
public interface SolarFluxConnection extends Connection {
	/**
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sun Location ECI</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getSunLocationECI();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getSunLocationECI(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getSunLocationECILength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setSunLocationECI(Double[] newSunLocationECI);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setSunLocationECI(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Sun Location ECI</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sun Location ECI</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sun Location ECI</em>' attribute list.
	 * @see org.thirdeye.extensions.sun.SunPackage#getSolarFluxConnection_SunLocationECI()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 *        annotation="ThirdEye unit='m,m,m'"
	 * @generated
	 */
	EList<Double> getSunLocationECIList();

	/**
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sat Location ECI</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getSatLocationECI();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getSatLocationECI(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getSatLocationECILength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setSatLocationECI(Double[] newSatLocationECI);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setSatLocationECI(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Sat Location ECI</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sat Location ECI</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sat Location ECI</em>' attribute list.
	 * @see org.thirdeye.extensions.sun.SunPackage#getSolarFluxConnection_SatLocationECI()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 *        annotation="ThirdEye unit='m,m,m'"
	 * @generated
	 */
	EList<Double> getSatLocationECIList();

	/**
	 * Returns the value of the '<em><b>Solar Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Solar Constant</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Solar Constant</em>' attribute.
	 * @see #setSolarConstant(Double)
	 * @see org.thirdeye.extensions.sun.SunPackage#getSolarFluxConnection_SolarConstant()
	 * @model required="true" ordered="false"
	 *        annotation="ThirdEye unit='W/m^2'"
	 * @generated
	 */
	Double getSolarConstant();

	/**
	 * Sets the value of the '{@link org.thirdeye.extensions.sun.SolarFluxConnection#getSolarConstant <em>Solar Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Solar Constant</em>' attribute.
	 * @see #getSolarConstant()
	 * @generated
	 */
	void setSolarConstant(Double value);

	/**
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Solar Flux ECI</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getSolarFluxECI();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getSolarFluxECI(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getSolarFluxECILength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setSolarFluxECI(Double[] newSolarFluxECI);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setSolarFluxECI(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Solar Flux ECI</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Solar Flux ECI</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Solar Flux ECI</em>' attribute list.
	 * @see org.thirdeye.extensions.sun.SunPackage#getSolarFluxConnection_SolarFluxECI()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 *        annotation="ThirdEye unit='W/m^2,W/m^2,W/m^2'"
	 * @generated
	 */
	EList<Double> getSolarFluxECIList();

} // SolarFluxConnection
