/**
 */
package org.thirdeye.extensions.sun;

import org.thirdeye.core.mesh.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Example Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extensions.sun.ExampleModel#getCounter <em>Counter</em>}</li>
 *   <li>{@link org.thirdeye.extensions.sun.ExampleModel#getOther <em>Other</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extensions.sun.SunPackage#getExampleModel()
 * @model
 * @generated
 */
public interface ExampleModel extends Model {
	/**
	 * Returns the value of the '<em><b>Counter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Counter</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Counter</em>' attribute.
	 * @see #setCounter(int)
	 * @see org.thirdeye.extensions.sun.SunPackage#getExampleModel_Counter()
	 * @model required="true"
	 *        annotation="ThirdEye unit='step'"
	 * @generated
	 */
	int getCounter();

	/**
	 * Sets the value of the '{@link org.thirdeye.extensions.sun.ExampleModel#getCounter <em>Counter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Counter</em>' attribute.
	 * @see #getCounter()
	 * @generated
	 */
	void setCounter(int value);

	/**
	 * Returns the value of the '<em><b>Other</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Other</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Other</em>' attribute.
	 * @see #setOther(int)
	 * @see org.thirdeye.extensions.sun.SunPackage#getExampleModel_Other()
	 * @model required="true"
	 *        annotation="ThirdEye unit='step'"
	 * @generated
	 */
	int getOther();

	/**
	 * Sets the value of the '{@link org.thirdeye.extensions.sun.ExampleModel#getOther <em>Other</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Other</em>' attribute.
	 * @see #getOther()
	 * @generated
	 */
	void setOther(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void calc();

} // ExampleModel
