/**
 */
package org.thirdeye.extensions.sun;

import org.eclipse.emf.common.util.EList;
import org.thirdeye.core.mesh.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extensions.sun.SunModel#getSolarConstant <em>Solar Constant</em>}</li>
 *   <li>{@link org.thirdeye.extensions.sun.SunModel#getECICoordinatesList <em>ECI Coordinates</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extensions.sun.SunPackage#getSunModel()
 * @model
 * @generated
 */
public interface SunModel extends Model {
	/**
	 * Returns the value of the '<em><b>Solar Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Solar Constant</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Solar Constant</em>' attribute.
	 * @see #setSolarConstant(Double)
	 * @see org.thirdeye.extensions.sun.SunPackage#getSunModel_SolarConstant()
	 * @model required="true" ordered="false"
	 *        annotation="ThirdEye unit='W/m^2'"
	 * @generated
	 */
	Double getSolarConstant();

	/**
	 * Sets the value of the '{@link org.thirdeye.extensions.sun.SunModel#getSolarConstant <em>Solar Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Solar Constant</em>' attribute.
	 * @see #getSolarConstant()
	 * @generated
	 */
	void setSolarConstant(Double value);

	/**
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ECI Coordinates</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getECICoordinates();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getECICoordinates(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getECICoordinatesLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setECICoordinates(Double[] newECICoordinates);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setECICoordinates(int index, Double element);

	/**
	 * Returns the value of the '<em><b>ECI Coordinates</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ECI Coordinates</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ECI Coordinates</em>' attribute list.
	 * @see org.thirdeye.extensions.sun.SunPackage#getSunModel_ECICoordinates()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 *        annotation="ThirdEye unit='m,m,m'"
	 * @generated
	 */
	EList<Double> getECICoordinatesList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void calc();

} // SunModel
