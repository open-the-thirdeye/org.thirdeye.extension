/**
 */
package org.thirdeye.extensions.sun;

import org.eclipse.emf.common.util.EList;

import org.thirdeye.core.mesh.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Solar Array</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.thirdeye.extensions.sun.SolarArray#getSANormalLocalList <em>SA Normal Local</em>}</li>
 *   <li>{@link org.thirdeye.extensions.sun.SolarArray#getSCQuaternionList <em>SC Quaternion</em>}</li>
 *   <li>{@link org.thirdeye.extensions.sun.SolarArray#getSolarFluxECIList <em>Solar Flux ECI</em>}</li>
 *   <li>{@link org.thirdeye.extensions.sun.SolarArray#getEfficiency <em>Efficiency</em>}</li>
 *   <li>{@link org.thirdeye.extensions.sun.SolarArray#getArea <em>Area</em>}</li>
 *   <li>{@link org.thirdeye.extensions.sun.SolarArray#getPower <em>Power</em>}</li>
 * </ul>
 *
 * @see org.thirdeye.extensions.sun.SunPackage#getSolarArray()
 * @model
 * @generated
 */
public interface SolarArray extends Model {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getSANormalLocal();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getSANormalLocal(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getSANormalLocalLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setSANormalLocal(Double[] newSANormalLocal);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setSANormalLocal(int index, Double element);

	/**
	 * Returns the value of the '<em><b>SA Normal Local</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SA Normal Local</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SA Normal Local</em>' attribute list.
	 * @see org.thirdeye.extensions.sun.SunPackage#getSolarArray_SANormalLocal()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 *        annotation="ThirdEye unit='none,none,none'"
	 * @generated
	 */
	EList<Double> getSANormalLocalList();

	/**
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SC Quaternion</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getSCQuaternion();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getSCQuaternion(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getSCQuaternionLength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setSCQuaternion(Double[] newSCQuaternion);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setSCQuaternion(int index, Double element);

	/**
	 * Returns the value of the '<em><b>SC Quaternion</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SC Quaternion</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SC Quaternion</em>' attribute list.
	 * @see org.thirdeye.extensions.sun.SunPackage#getSolarArray_SCQuaternion()
	 * @model unique="false" lower="4" upper="4" ordered="false"
	 *        annotation="ThirdEye unit='none,none,none,none'"
	 * @generated
	 */
	EList<Double> getSCQuaternionList();

	/**
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Solar Flux ECI</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double[] getSolarFluxECI();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Double getSolarFluxECI(int index);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	int getSolarFluxECILength();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setSolarFluxECI(Double[] newSolarFluxECI);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void setSolarFluxECI(int index, Double element);

	/**
	 * Returns the value of the '<em><b>Solar Flux ECI</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Solar Flux ECI</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Solar Flux ECI</em>' attribute list.
	 * @see org.thirdeye.extensions.sun.SunPackage#getSolarArray_SolarFluxECI()
	 * @model unique="false" lower="3" upper="3" ordered="false"
	 *        annotation="ThirdEye unit='W/m^2,W/m^2,W/m^2'"
	 * @generated
	 */
	EList<Double> getSolarFluxECIList();

	/**
	 * Returns the value of the '<em><b>Efficiency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Efficiency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Efficiency</em>' attribute.
	 * @see #setEfficiency(Double)
	 * @see org.thirdeye.extensions.sun.SunPackage#getSolarArray_Efficiency()
	 * @model required="true" ordered="false"
	 *        annotation="ThirdEye unit='none'"
	 * @generated
	 */
	Double getEfficiency();

	/**
	 * Sets the value of the '{@link org.thirdeye.extensions.sun.SolarArray#getEfficiency <em>Efficiency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Efficiency</em>' attribute.
	 * @see #getEfficiency()
	 * @generated
	 */
	void setEfficiency(Double value);

	/**
	 * Returns the value of the '<em><b>Area</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Area</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Area</em>' attribute.
	 * @see #setArea(Double)
	 * @see org.thirdeye.extensions.sun.SunPackage#getSolarArray_Area()
	 * @model required="true" ordered="false"
	 *        annotation="ThirdEye unit='m^2'"
	 * @generated
	 */
	Double getArea();

	/**
	 * Sets the value of the '{@link org.thirdeye.extensions.sun.SolarArray#getArea <em>Area</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Area</em>' attribute.
	 * @see #getArea()
	 * @generated
	 */
	void setArea(Double value);

	/**
	 * Returns the value of the '<em><b>Power</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Power</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Power</em>' attribute.
	 * @see #setPower(Double)
	 * @see org.thirdeye.extensions.sun.SunPackage#getSolarArray_Power()
	 * @model required="true" ordered="false"
	 *        annotation="ThirdEye unit='W'"
	 * @generated
	 */
	Double getPower();

	/**
	 * Sets the value of the '{@link org.thirdeye.extensions.sun.SolarArray#getPower <em>Power</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Power</em>' attribute.
	 * @see #getPower()
	 * @generated
	 */
	void setPower(Double value);

} // SolarArray
