/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.example;

import org.thirdeye.core.mesh.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.thirdeye.extension.example.ExampleModel#getCounter <em>Counter</em>}</li>
 *   <li>{@link org.thirdeye.extension.example.ExampleModel#getOther <em>Other</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.thirdeye.extension.example.ExamplePackage#getExampleModel()
 * @model
 * @generated
 */
public interface ExampleModel extends Model {
	/**
	 * Returns the value of the '<em><b>Counter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Counter</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Counter</em>' attribute.
	 * @see #setCounter(int)
	 * @see org.thirdeye.extension.example.ExamplePackage#getExampleModel_Counter()
	 * @model required="true"
	 *        annotation="ThirdEye unit='step'"
	 * @generated
	 */
	int getCounter();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.example.ExampleModel#getCounter <em>Counter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Counter</em>' attribute.
	 * @see #getCounter()
	 * @generated
	 */
	void setCounter(int value);

	/**
	 * Returns the value of the '<em><b>Other</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Other</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Other</em>' attribute.
	 * @see #setOther(int)
	 * @see org.thirdeye.extension.example.ExamplePackage#getExampleModel_Other()
	 * @model required="true"
	 *        annotation="ThirdEye unit='step'"
	 * @generated
	 */
	int getOther();

	/**
	 * Sets the value of the '{@link org.thirdeye.extension.example.ExampleModel#getOther <em>Other</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Other</em>' attribute.
	 * @see #getOther()
	 * @generated
	 */
	void setOther(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void calc();

} // ExampleModel
