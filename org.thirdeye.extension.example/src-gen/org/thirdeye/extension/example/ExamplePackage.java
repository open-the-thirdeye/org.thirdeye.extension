/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.example;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import org.thirdeye.core.mesh.MeshPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.thirdeye.extension.example.ExampleFactory
 * @model kind="package"
 * @generated
 */
public interface ExamplePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "example";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://example/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "example";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ExamplePackage eINSTANCE = org.thirdeye.extension.example.impl.ExamplePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.thirdeye.extension.example.impl.ExampleModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.example.impl.ExampleModelImpl
	 * @see org.thirdeye.extension.example.impl.ExamplePackageImpl#getExampleModel()
	 * @generated
	 */
	int EXAMPLE_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMPLE_MODEL__VARIABLES = MeshPackage.MODEL__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMPLE_MODEL__NAME = MeshPackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Counter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMPLE_MODEL__COUNTER = MeshPackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Other</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMPLE_MODEL__OTHER = MeshPackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMPLE_MODEL_FEATURE_COUNT = MeshPackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMPLE_MODEL___RUN = MeshPackage.MODEL___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMPLE_MODEL___CALC = MeshPackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMPLE_MODEL_OPERATION_COUNT = MeshPackage.MODEL_OPERATION_COUNT + 1;


	/**
	 * The meta object id for the '{@link org.thirdeye.extension.example.impl.ExampleConnectionImpl <em>Connection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.thirdeye.extension.example.impl.ExampleConnectionImpl
	 * @see org.thirdeye.extension.example.impl.ExamplePackageImpl#getExampleConnection()
	 * @generated
	 */
	int EXAMPLE_CONNECTION = 1;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMPLE_CONNECTION__VARIABLES = MeshPackage.CONNECTION__VARIABLES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMPLE_CONNECTION__NAME = MeshPackage.CONNECTION__NAME;

	/**
	 * The feature id for the '<em><b>Input</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMPLE_CONNECTION__INPUT = MeshPackage.CONNECTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Output</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMPLE_CONNECTION__OUTPUT = MeshPackage.CONNECTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMPLE_CONNECTION_FEATURE_COUNT = MeshPackage.CONNECTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Run</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMPLE_CONNECTION___RUN = MeshPackage.CONNECTION___RUN;

	/**
	 * The operation id for the '<em>Calc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMPLE_CONNECTION___CALC = MeshPackage.CONNECTION_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXAMPLE_CONNECTION_OPERATION_COUNT = MeshPackage.CONNECTION_OPERATION_COUNT + 1;


	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.example.ExampleModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see org.thirdeye.extension.example.ExampleModel
	 * @generated
	 */
	EClass getExampleModel();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.example.ExampleModel#getCounter <em>Counter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Counter</em>'.
	 * @see org.thirdeye.extension.example.ExampleModel#getCounter()
	 * @see #getExampleModel()
	 * @generated
	 */
	EAttribute getExampleModel_Counter();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.example.ExampleModel#getOther <em>Other</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Other</em>'.
	 * @see org.thirdeye.extension.example.ExampleModel#getOther()
	 * @see #getExampleModel()
	 * @generated
	 */
	EAttribute getExampleModel_Other();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.example.ExampleModel#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.example.ExampleModel#calc()
	 * @generated
	 */
	EOperation getExampleModel__Calc();

	/**
	 * Returns the meta object for class '{@link org.thirdeye.extension.example.ExampleConnection <em>Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection</em>'.
	 * @see org.thirdeye.extension.example.ExampleConnection
	 * @generated
	 */
	EClass getExampleConnection();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.example.ExampleConnection#getInput <em>Input</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Input</em>'.
	 * @see org.thirdeye.extension.example.ExampleConnection#getInput()
	 * @see #getExampleConnection()
	 * @generated
	 */
	EAttribute getExampleConnection_Input();

	/**
	 * Returns the meta object for the attribute '{@link org.thirdeye.extension.example.ExampleConnection#getOutput <em>Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output</em>'.
	 * @see org.thirdeye.extension.example.ExampleConnection#getOutput()
	 * @see #getExampleConnection()
	 * @generated
	 */
	EAttribute getExampleConnection_Output();

	/**
	 * Returns the meta object for the '{@link org.thirdeye.extension.example.ExampleConnection#calc() <em>Calc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calc</em>' operation.
	 * @see org.thirdeye.extension.example.ExampleConnection#calc()
	 * @generated
	 */
	EOperation getExampleConnection__Calc();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ExampleFactory getExampleFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.example.impl.ExampleModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.example.impl.ExampleModelImpl
		 * @see org.thirdeye.extension.example.impl.ExamplePackageImpl#getExampleModel()
		 * @generated
		 */
		EClass EXAMPLE_MODEL = eINSTANCE.getExampleModel();

		/**
		 * The meta object literal for the '<em><b>Counter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXAMPLE_MODEL__COUNTER = eINSTANCE.getExampleModel_Counter();

		/**
		 * The meta object literal for the '<em><b>Other</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXAMPLE_MODEL__OTHER = eINSTANCE.getExampleModel_Other();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation EXAMPLE_MODEL___CALC = eINSTANCE.getExampleModel__Calc();

		/**
		 * The meta object literal for the '{@link org.thirdeye.extension.example.impl.ExampleConnectionImpl <em>Connection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.thirdeye.extension.example.impl.ExampleConnectionImpl
		 * @see org.thirdeye.extension.example.impl.ExamplePackageImpl#getExampleConnection()
		 * @generated
		 */
		EClass EXAMPLE_CONNECTION = eINSTANCE.getExampleConnection();

		/**
		 * The meta object literal for the '<em><b>Input</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXAMPLE_CONNECTION__INPUT = eINSTANCE.getExampleConnection_Input();

		/**
		 * The meta object literal for the '<em><b>Output</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXAMPLE_CONNECTION__OUTPUT = eINSTANCE.getExampleConnection_Output();

		/**
		 * The meta object literal for the '<em><b>Calc</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation EXAMPLE_CONNECTION___CALC = eINSTANCE.getExampleConnection__Calc();

	}

} //ExamplePackage
